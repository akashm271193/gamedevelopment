#include "cAIGameObject.h"
#include "cPlayerGameObject.h"

cAIGameObject::cAIGameObject()
{
	this->movementMinPos = glm::vec3(0.0f);
	this->movementMaxPos = glm::vec3(0.0f);
	this->objectMovmentDir = eAIDirection::MOVING_TO_MAX;
	this->isRotating = false;
	this->isTranslating = false;
	this->targetAngleRotationY = 0.0f;
	this->originalOrientationY = 0.0f;
	this->attentionRadius= 2.0f;
}

void cAIGameObject::animateObject(float deltaTime, cPlayerGameObject* playerObj){
	cGameObject* tmpGameObject = NULL;
	if (this->hasBoundingBoxOrSphere && this->boundingSphere != NULL) {
		tmpGameObject= this->boundingSphere;
	}

	this->interactWithPlayer(playerObj, tmpGameObject);

	float angleInRadians = 0.0f;
	glm::vec3 targetRotation = this->ownAxisOrientation;
		std::cout << "This rotation angle : " << this->ownAxisOrientation.x << " " << this->ownAxisOrientation.y << " " << this->ownAxisOrientation.z << std::endl;
		switch (this->objectMovmentDir) {
		case eAIDirection::MOVING_TO_MAX:
			if (!this->isRotating && !this->isTranslating) {
				this->calculateAngleOfOrientation(this->movementMaxPos, angleInRadians);
				this->targetAngleRotationY = fmodf(this->ownAxisOrientation.y + angleInRadians,glm::radians(360.0f));
				this->isRotating = true;
				this->isTranslating = false;
			}

			//Check if facing right direction
			if (fabs(this->targetAngleRotationY - this->ownAxisOrientation.y) < 0.01f) {
				this->isRotating = false;
				this->isTranslating = true;
			}
			else {
				this->rotateObject(glm::vec3(0.0f, this->targetAngleRotationY, 0.0f), deltaTime, tmpGameObject);
			}

			//Check if reached destination
			if (fabs(this->position.x - this->movementMaxPos.x) <= 0.05f && fabs(this->position.z - this->movementMaxPos.z) <= 0.05f) {
				if (!this->isRotating && !this->isTranslating) {
					this->calculateAngleOfOrientation(this->movementMinPos, angleInRadians);
					this->targetAngleRotationY = fmodf(this->ownAxisOrientation.y + angleInRadians, glm::radians(360.0f));
					this->isRotating = true;
					this->isTranslating = false;
				}

				this->isTranslating = false;
				if (fabs(this->targetAngleRotationY - this->ownAxisOrientation.y) < 0.01f) {
					this->isRotating = false;
					this->isTranslating = true;
				}
				else {
					this->rotateObject(glm::vec3(0.0f, this->targetAngleRotationY, 0.0f), deltaTime, tmpGameObject);
				}

				if (!this->isRotating && this->isTranslating) {
					this->isTranslating = false;
					this->objectMovmentDir = eAIDirection::MOVING_TO_MIN;
					break;
				}
			}
			else {
				if (!this->isRotating && this->isTranslating)
					translateObject(this->movementMaxPos, deltaTime, tmpGameObject);
			}

			break;

		case eAIDirection::MOVING_TO_MIN:
			if (!this->isRotating && !this->isTranslating) {
				this->calculateAngleOfOrientation(this->movementMinPos, angleInRadians);
				this->targetAngleRotationY = fmodf(this->ownAxisOrientation.y + angleInRadians, glm::radians(360.0f));
				this->isRotating = true;
				this->isTranslating = false;
			}
			
			if (fabs(this->targetAngleRotationY - this->ownAxisOrientation.y) < 0.01f) {
				this->isRotating = false;
				this->isTranslating = true;
			}
			else {
				this->rotateObject(glm::vec3(0.0f, this->targetAngleRotationY, 0.0f), deltaTime, tmpGameObject);
			}

			if (fabs(this->position.x - this->movementMinPos.x) <= 0.05f && fabs(this->position.z - this->movementMinPos.z) <= 0.05f) {
				if (!this->isRotating && !this->isTranslating) {
					this->calculateAngleOfOrientation(this->movementMaxPos, angleInRadians);
					this->targetAngleRotationY = fmodf(this->ownAxisOrientation.y + angleInRadians, glm::radians(360.0f));
					this->isRotating = true;
					this->isTranslating = false;
				}

				if (fabs(this->targetAngleRotationY - this->ownAxisOrientation.y) < 0.01f) {
					this->isRotating = false;
					this->isTranslating = true;
				}
				else {
					this->rotateObject(glm::vec3(0.0f, this->targetAngleRotationY, 0.0f), deltaTime, tmpGameObject);
				}

				if (!isRotating && this->isTranslating) {
					this->objectMovmentDir = eAIDirection::MOVING_TO_MAX;
					this->isTranslating = false;
					break;
				}
		}
			else {
				if(!isRotating && this->isTranslating)
					translateObject(this->movementMinPos, deltaTime, tmpGameObject);
			}
			break;

		case eAIDirection::APPROACH_PLAYER:
			if (!this->isRotating && !this->isTranslating) {
				this->calculateAngleOfOrientation(playerObj->position, angleInRadians);
				this->targetAngleRotationY = fmodf(this->ownAxisOrientation.y + angleInRadians, glm::radians(360.0f));
				this->targetPosDir =  this->position - playerObj->position;
				this->targetPosDir = glm::normalize(this->targetPosDir);
				this->targetPos = (playerObj->position - (1.5f * targetPosDir));
				this->isRotating = true;
				this->isTranslating = false;
			}

			if (fabs(this->targetAngleRotationY - this->ownAxisOrientation.y) < 0.01f) {
				this->isRotating = false;
				this->isTranslating = true;
			}
			else {
				this->rotateObject(glm::vec3(0.0f, this->targetAngleRotationY, 0.0f), deltaTime, tmpGameObject);
			}

			if (fabs(this->position.x - this->targetPos.x) <= 0.05f && fabs(this->position.z - this->targetPos.z) <= 0.05f)  {
				this->isTranslating = false;
			}
			else {
				if (!isRotating && this->isTranslating) {
					translateObject(this->targetPos, deltaTime, tmpGameObject);
				}
			}
			break;

		case eAIDirection::EVADE_PLAYER:
			if (!this->isRotating && !this->isTranslating) {
				this->calculateAngleOfOrientation(playerObj->position, angleInRadians);
				this->targetAngleRotationY = fmodf(this->ownAxisOrientation.y + angleInRadians, glm::radians(360.0f));
				this->targetPosDir = this->position - playerObj->position;
				this->targetPosDir = glm::normalize(this->targetPosDir);
				this->targetPos = (this->position + (this->attentionRadius * targetPosDir));
				this->isRotating = true;
				this->isTranslating = false;
			}

			if (fabs(this->targetAngleRotationY - this->ownAxisOrientation.y) < 0.01f) {
				this->isRotating = false;
				this->isTranslating = true;
			}
			else {
				this->rotateObject(glm::vec3(0.0f, this->targetAngleRotationY, 0.0f), deltaTime, tmpGameObject);
			}

			if (fabs(this->position.x - this->targetPos.x) <= 0.05f && fabs(this->position.z - this->targetPos.z) <= 0.05f) {
				this->isTranslating = false;
			}
			else {
				if (!isRotating && this->isTranslating) {
					translateObject(this->targetPos, deltaTime, tmpGameObject);
				}
			}
			break;

		case eAIDirection::ATTACK_PLAYER:
			if (!this->isRotating && !this->isTranslating) {
				this->calculateAngleOfOrientation(playerObj->position, angleInRadians);
				this->targetAngleRotationY = fmodf(this->ownAxisOrientation.y + angleInRadians, glm::radians(360.0f));
				this->targetPosDir = playerObj->position - this->position;
				this->targetPosDir = glm::normalize(this->targetPosDir);
				this->targetPos = playerObj->position;
				this->isRotating = true;
				this->isTranslating = false;
			}

			if (fabs(this->targetAngleRotationY - this->ownAxisOrientation.y) < 0.01f) {
				this->isRotating = false;
				this->isTranslating = true;
			}
			else {
				this->rotateObject(glm::vec3(0.0f, this->targetAngleRotationY, 0.0f), deltaTime, tmpGameObject);
			}

			if (fabs(this->position.x - this->targetPos.x) <= 0.05f && fabs(this->position.z - this->targetPos.z) <= 0.05f) {
				this->isTranslating = false;
			}
			else {
				if (!isRotating && this->isTranslating) {
					translateObject(this->targetPos, deltaTime, tmpGameObject);
				}
			}
			break;

		case eAIDirection::FOLLOW_PLAYER:
			this->targetPosDir = this->position - playerObj->position;
			this->targetPosDir = glm::normalize(this->targetPosDir);
			this->targetPos = (playerObj->position - (1.5f * targetPosDir));
			this->prevKnownPlayerPos = playerObj->position;
			
			if (fabs(this->position.x - this->targetPos.x) <= 0.05f && fabs(this->position.z - this->targetPos.z) <= 0.05f) {
				this->isTranslating = false;
			}
			else {
				if (this->isTranslating) {
					translateObject(this->targetPos, deltaTime, tmpGameObject);
				}
			}
			break;
		}
}

void cAIGameObject::calculateAngleOfOrientation(glm::vec3 destinationPoint, float &angleInRadians) {
	//glm::vec3 playerPos = this->objectFront;
	//float dotProduct = glm::dot(glm::normalize(playerPos), glm::normalize(destinationPoint));
	//float magnitudeVec1 = sqrt((playerPos.x * playerPos.x) + (playerPos.y * playerPos.y) + (playerPos.z * playerPos.z));
	//float magnitudeVec2 = sqrt((destinationPoint.x * destinationPoint.x) + (destinationPoint.y * destinationPoint.y) + (destinationPoint.z * destinationPoint.z));
	//float cosAngle = dotProduct / (magnitudeVec1 * magnitudeVec2);
	//float tmpAngle = glm::degrees(acosf(cosAngle));
	//tmpAngle = 360.0f - tmpAngle - this->originalOrientationY;
	//angleInRadians = glm::radians(tmpAngle);
	glm::vec3 dist = destinationPoint - this->position;
	glm::vec3 distDir = glm::normalize(dist);
	float dotProduct = glm::dot(this->objectFront,distDir);

	if (dotProduct > -0.1f && dotProduct < 0.1f) {
		angleInRadians = 0.0f;
		return;
	}

	//Hack to remove NaN scenarios
	if (dotProduct < -1.0f)
		dotProduct = -1.0f;
	if (dotProduct > 1.0f)
		dotProduct = 1.0f;


	float tmpAngle = glm::degrees(acosf(dotProduct));
	tmpAngle = 360.0f - tmpAngle;

	angleInRadians = glm::radians(tmpAngle);

	float tmpNewAngle = fmodf(this->ownAxisOrientation.y + angleInRadians, glm::radians(360.0f));
	this->objectFront = glm::vec3(cos(tmpNewAngle),0.0f,sin(tmpNewAngle));

};

//All points are in radians
void cAIGameObject::rotateObject(glm::vec3 destinationAngle, float stepTime, cGameObject* tmpGameObject) {
	//this->ownAxisOrientation.x = ((destinationAngle.x - this->ownAxisOrientation.x) * stepTime) + this->ownAxisOrientation.x;
	this->ownAxisOrientation.y = ((destinationAngle.y - this->ownAxisOrientation.y) * stepTime) + this->ownAxisOrientation.y;
	//this->ownAxisOrientation.z = ((destinationAngle.z - this->ownAxisOrientation.z) * stepTime) + this->ownAxisOrientation.z;
};

void cAIGameObject::translateObject(glm::vec3 destinationPoint, float stepTime, cGameObject* tmpGameObject) {
	glm::vec3 newPos = glm::vec3(0.0f);

	if (this->objectMovmentDir == eAIDirection::MOVING_TO_MAX) {
		newPos.x = ((destinationPoint.x - this->position.x) * stepTime) + this->position.x;
		newPos.y = ((destinationPoint.y - this->position.y) * stepTime) + this->position.y;
		newPos.z = ((destinationPoint.z - this->position.z) * stepTime) + this->position.z;
		this->position = newPos;
	}
	else if(this->objectMovmentDir == eAIDirection::MOVING_TO_MIN) {
		newPos.x = this->position.x - ((this->position.x - destinationPoint.x) * stepTime);
		newPos.y = this->position.y - ((this->position.y - destinationPoint.y) * stepTime);
		newPos.z = this->position.z - ((this->position.z - destinationPoint.z) * stepTime) ;
		this->position = newPos;
	}
	else if(this->objectMovmentDir == eAIDirection::APPROACH_PLAYER){
		newPos.x = this->position.x + (-(this->targetPosDir.x) * stepTime);
		newPos.y = this->position.y + (-(this->targetPosDir.y) * stepTime);
		newPos.z = this->position.z + (-(this->targetPosDir.z) * stepTime);
		this->position = newPos;
	}
	else if (this->objectMovmentDir == eAIDirection::EVADE_PLAYER) {
		newPos.x = this->position.x + (this->targetPosDir.x * stepTime);
		newPos.y = this->position.y + (this->targetPosDir.y * stepTime);
		newPos.z = this->position.z + (this->targetPosDir.z * stepTime);
		this->position = newPos;
	}
	else if (this->objectMovmentDir == eAIDirection::ATTACK_PLAYER) {
		newPos.x = this->position.x + (this->targetPosDir.x * stepTime);
		newPos.y = this->position.y + (this->targetPosDir.y * stepTime);
		newPos.z = this->position.z + (this->targetPosDir.z * stepTime);
		this->position = newPos;
	}
	else if (this->objectMovmentDir == eAIDirection::FOLLOW_PLAYER) {
		newPos.x = this->position.x + (-(this->targetPosDir.x) * stepTime);
		newPos.y = this->position.y + (-(this->targetPosDir.y) * stepTime);
		newPos.z = this->position.z + (-(this->targetPosDir.z) * stepTime);
		this->position = newPos;
	}

	if (tmpGameObject != NULL) {
		tmpGameObject->position = this->position;
	} 
};

bool cAIGameObject::isFacingPlayer(cPlayerGameObject* playerObj) {
	float isAIFacing = glm::dot(this->objectFront, playerObj->objectFront);

	if ((-0.95 > isAIFacing) && (isAIFacing > -1.0f)) {
		return true;
	}
	return false;
}


bool cAIGameObject::isInAttentionRadius(cPlayerGameObject* playerObj) {
	float distPlayAI = glm::distance(this->position, playerObj->position);
	if (distPlayAI <= this->attentionRadius) {
		return true;
	}
	return false;
}

bool cAIGameObject::hasPlayerMoved(cPlayerGameObject* playerObj) {
	if (this->prevKnownPlayerPos.x != playerObj->position.x || this->prevKnownPlayerPos.y != playerObj->position.y || this->prevKnownPlayerPos.z != playerObj->position.z)
	{
		return true;
	}
	
	return false;
}

void cAIGameObject::interactWithPlayer(cPlayerGameObject* playerObj, cGameObject* tmpGameObject) {
	if (this->typeOfObject == eTypeOfObject::FOLLOWER_AI) {
		this->action = eAiAction::FOLLOW;
		this->objectMovmentDir = eAIDirection::FOLLOW_PLAYER;
		if (hasPlayerMoved(playerObj))
			isTranslating = true;
		return;
	}
	if (!this->isInAttentionRadius(playerObj) && !playerObj->isPlayerHealthLow()) {
		if (this->action != eAiAction::ANIMATE_AI) {
			this->action = eAiAction::ANIMATE_AI;
			this->objectMovmentDir = eAIDirection::MOVING_TO_MAX;
		}
		return;
	}
	else if (this->typeOfObject == eTypeOfObject::ANGRY_AI && playerObj->isPlayerHealthLow() && !this->isInAttentionRadius(playerObj)) {
		this->action = eAiAction::ATTACK;
		this->objectMovmentDir = eAIDirection::ATTACK_PLAYER;
		tmpGameObject->colour = glm::vec3(1.0f, 0.0f, 0.0f);
		this->isRotating = false;
		this->isTranslating = false;
		return;
	}
	else if (!this->isInAttentionRadius(playerObj) && playerObj->isPlayerHealthLow())
		return;
	

	switch (this->typeOfObject) {
		case eTypeOfObject::ANGRY_AI:
			if (this->isFacingPlayer(playerObj) && this->action != eAiAction::EVADE) {
				this->action = eAiAction::EVADE;
				this->objectMovmentDir = eAIDirection::EVADE_PLAYER;
				tmpGameObject->colour = glm::vec3(1.0f, 0.6f, 0.0f);
			}
			else if(!this->isFacingPlayer(playerObj) && this->action != eAiAction::ATTACK){
				this->action = eAiAction::ATTACK;
				this->objectMovmentDir = eAIDirection::ATTACK_PLAYER;
				this->isRotating = false;
				this->isTranslating = false;
				tmpGameObject->colour = glm::vec3(1.0f, 0.0f, 0.0f);
			}
			break;

		case eTypeOfObject::CURIOUS_AI:
			if (this->isFacingPlayer(playerObj) && this->action != eAiAction::EVADE) {
				this->action = eAiAction::EVADE;
				this->objectMovmentDir = eAIDirection::EVADE_PLAYER;
				tmpGameObject->colour = glm::vec3(1.0f, 1.0f, 0.0f);
			}
			else if (!this->isFacingPlayer(playerObj) && this->action != eAiAction::APPROACH) {
					this->action = eAiAction::APPROACH;
					this->objectMovmentDir = eAIDirection::APPROACH_PLAYER;
					this->isRotating = false;
					this->isTranslating = false;
					tmpGameObject->colour = glm::vec3(0.8f, 0.8f, 0.8f);
				}
			break;
	}
}