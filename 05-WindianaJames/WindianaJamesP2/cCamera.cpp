#include "cCamera.h"

cCamera::cCamera( glm::vec3 position, glm::vec3 up, GLfloat yaw, GLfloat pitch)
{
	this->front = glm::vec3( 0.0f, 0.0f, -1.0f );
	this->movementSpeed = SPEED;
	this->mouseSensitivity = SENSITIVITY;
	this->zoom = ZOOM;

	this->position = position;
	this->worldUp = up;
	this->yaw = yaw;
	this->pitch = pitch;
	this->updateCameraVectors( );
}

cCamera::cCamera( GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch )
{
	this->front = glm::vec3(-0.15738f, 0.8326f, 0.4281f);
	this->movementSpeed = SPEED;
	this->mouseSensitivity = SENSITIVITY;
	this->zoom = ZOOM;

	this->position = glm::vec3( posX, posY, posZ );
	this->worldUp = glm::vec3( upX, upY, upZ );
	this->yaw = yaw;
	this->pitch = pitch;
	this->updateCameraVectors( );
}

glm::mat4 cCamera::GetViewMatrix( )
{
	//return glm::lookAt(this->position, glm::vec3(-0.15738f, 0.8326f, 0.4281f), this->up);
	return glm::lookAt( this->position, this->front, this->up );
}

void cCamera::ProcessKeyboard( cameraMovement direction, GLfloat deltaTime )
{
	GLfloat velocity = this->movementSpeed * deltaTime;

	if( FORWARD == direction )
	{
		this->position += this->front * velocity;
	}
	if( BACKWARD == direction )
	{
		this->position -= this->front * velocity;
	}
	if( LEFT == direction )
	{
		this->position -= this->right * velocity;
	}
	if( RIGHT == direction )
	{
		this->position += this->right * velocity;
	}
}

void cCamera::ProcessMouseMovement( GLfloat xOffset, GLfloat yOffset, GLboolean constrainPitch )
{
	xOffset *= this->mouseSensitivity;
	yOffset *= this->mouseSensitivity;

	this->yaw += xOffset;
	this->pitch += yOffset;

	if( constrainPitch )
	{
		if( this->pitch > 89.0f )
		{
			this->pitch = 89.0f;
		}
		if( this->pitch < -89.0f )
		{
			this->pitch = -89.0f;
		}
	}
	this->updateCameraVectors( );
}

void cCamera::ProcessMouseScroll( GLfloat yOffset )
{
	if( this->zoom >= 1.0f && this->zoom <= 45.0f )
	{
		this->zoom -= yOffset;
	}

	if( this->zoom <= 1.0f )
	{
		this->zoom = 1.0f;
	}
	if( this->zoom >= 45.0f )
	{
		this->zoom = 45.0f;
	}
}

void cCamera::updateCameraVectors( )
{
	glm::vec3 front;
	front.x = cos( this->yaw ) * cos( this->pitch );
	front.y = sin( this->pitch );
	front.z = sin( this->yaw ) * cos( this->pitch );

	this->front = glm::normalize( front );
	this->right = glm::normalize( glm::cross( this->front, this->worldUp ) );
	this->up = glm::normalize( glm::cross( this->right, this->front ) );
}
