#include "cShader.h"

#include <iostream>

cShader::cShader()
{
}

cShader& cShader::Use()
{
	glUseProgram(this->ID);
	return *this;
}

void cShader::Compile(const GLchar* vertexSource, const GLchar* fragmentSource)
{
	GLuint sVertex, sFragment;

	sVertex = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(sVertex, 1, &vertexSource, NULL);
	glCompileShader(sVertex);
	checkCompileErrors(sVertex, "VERTEX");

	sFragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(sFragment, 1, &fragmentSource, NULL);
	glCompileShader(sFragment);
	checkCompileErrors(sFragment, "FRAGMENT");

	this->ID = glCreateProgram();
	glAttachShader(this->ID, sVertex);
	glAttachShader(this->ID, sFragment);
	glLinkProgram(this->ID);
	checkCompileErrors(this->ID, "PROGRAM");

	glDeleteShader(sVertex);
	glDeleteShader(sFragment);
}

void cShader::SetFloat(const GLchar* name, GLfloat value, GLboolean useShader /*= false */)
{
	if (useShader)
		this->Use();
	glUniform1f(glGetUniformLocation(this->ID, name), value);
}

void cShader::SetInteger(const GLchar *name, GLint value, GLboolean useShader /*= false */)
{
	GLuint intLocation;
	if (useShader)
		this->Use();
	intLocation = glGetUniformLocation(this->ID, name);
	glEnableVertexAttribArray(intLocation);
	glUniform1i(intLocation, value);
}

void cShader::SetVector2f(const GLchar * name, GLfloat x, GLfloat y, GLboolean useShader /*= false */)
{
	if (useShader)
		this->Use();
	glUniform2f(glGetUniformLocation(this->ID, name), x, y);
}

void cShader::SetVector2f(const GLchar * name, const glm::vec2 & value, GLboolean useShader /*= false */)
{
	if (useShader)
		this->Use();
	glUniform2f(glGetUniformLocation(this->ID, name), value.x, value.y);
}

void cShader::SetVector3f(const GLchar * name, GLfloat x, GLfloat y, GLfloat z, GLboolean useShader /*= false */)
{
	if (useShader)
		this->Use();
	glUniform3f(glGetUniformLocation(this->ID, name), x, y, z);
}

void cShader::SetVector3f(const GLchar * name, const glm::vec3 & value, GLboolean useShader /*= false */)
{
	if (useShader)
		this->Use();
	glUniform3f(glGetUniformLocation(this->ID, name), value.x, value.y, value.z);
}

void cShader::SetVector4f(const GLchar * name, GLfloat x, GLfloat y, GLfloat z, GLfloat w, GLboolean useShader /*= false */)
{
	if (useShader)
		this->Use();
	glUniform4f(glGetUniformLocation(this->ID, name), x, y, z, w);
}

void cShader::SetVector4f(const GLchar * name, const glm::vec4 & value, GLboolean useShader /*= false */)
{
	if (useShader)
		this->Use();
	glUniform4f(glGetUniformLocation(this->ID, name), value.x, value.y, value.z, value.w);
}

void cShader::SetMatrix4f(const GLchar * name, const glm::mat4 & matrix, GLboolean useShader /*= false */)
{
	GLuint matrixLocation;
	if (useShader)
		this->Use();
	matrixLocation = glGetUniformLocation(this->ID, name);
	glEnableVertexAttribArray(matrixLocation);
	glUniformMatrix4fv(matrixLocation, 1, GL_FALSE, glm::value_ptr(matrix));
}

void cShader::checkCompileErrors(GLuint object, std::string type)
{
	GLint success;
	GLchar infoLog[1024];
	if (type != "PROGRAM")
	{
		glGetShaderiv(object, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(object, 1024, NULL, infoLog);
			std::cout << "| ERROR::SHADER: Compile-time error: Type: " << type << "\n"
				<< infoLog << "\n -- --------------------------------------------------- -- "
				<< std::endl;
		}
	}
	else
	{
		glGetProgramiv(object, GL_LINK_STATUS, &success);
		if (!success)
		{
			glGetProgramInfoLog(object, 1024, NULL, infoLog);
			std::cout << "| ERROR::Shader: Link-time error: Type: " << type << "\n"
				<< infoLog << "\n -- --------------------------------------------------- -- "
				<< std::endl;
		}
	}
}

