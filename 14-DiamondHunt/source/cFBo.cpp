#include "cFBO.h"
#include <iostream>

cFBO::cFBO(int numBuffers, int width, int height)
{
	this->screenWidth = width;
	this->screenHeight = height;
	this->numBuffers = numBuffers;
	bufferArray = new unsigned int[3];
	init();
	fboInit();
}

void cFBO::Bind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
}

void cFBO::Unbind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void cFBO::Draw()
{
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}


void cFBO::Reset(int width, int height)
{
	for (int i = 0; i != numBuffers; i++)
		glDeleteTextures(1, &bufferArray[i]);

	glDeleteFramebuffers(1, &(this->fbo));

	this->screenWidth = width;
	this->screenHeight = height;
	this->fboInit();
}


bool cFBO::fboInit()
{
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	for (int i = 0; i != numBuffers; i++)
	{
		glGenTextures(1, &bufferArray[i]);
		glBindTexture(GL_TEXTURE_2D, bufferArray[i]);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, screenWidth, screenHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, bufferArray[i], 0);
	}

	glGenTextures(1, &depthBuffer);
	glBindTexture(GL_TEXTURE_2D, depthBuffer);

	glTexImage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, screenWidth, screenHeight, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	//change this for blur
	static const GLenum draw[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
	glDrawBuffers(numBuffers, draw);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE)
	{
		std::cout << "Framebuffer created with " << numBuffers << " buffers." << std::endl;
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		return true;
	}
	return false;
}



void cFBO::init()
{
	float vertices[] =
	{
		-1.0f,  1.0f,  0.0f, 1.0f,
		-1.0f, -1.0f,  0.0f, 0.0f,
		1.0f, -1.0f,  1.0f, 0.0f,

		-1.0f,  1.0f,  0.0f, 1.0f,
		1.0f, -1.0f,  1.0f, 0.0f,
		1.0f,  1.0f,  1.0f, 1.0f
	};

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), &vertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));

	glBindVertexArray(0);
}
