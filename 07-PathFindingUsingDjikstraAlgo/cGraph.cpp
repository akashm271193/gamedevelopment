#include "cGraph.h"


cGraph::cGraph()
{
}


cGraph::~cGraph()
{
}

void cGraph::addNode() {
	cNode* newNode = new cNode();
	if (this->allNodes.size() == 0)
		newNode->id = 1;
	else
	{
		cNode* prevNode = this->allNodes.back();
		newNode->id = prevNode->id + 1;
	}
	allNodes.push_back(newNode);
}

void cGraph::joinNodes(int node1Id, int node2Id) {
	if (this->allNodes.size() < 2)
	{
		std::cout << "Minimum number of nodes required for a graph is 2";
	}
	
	bool isAdded = this->addIdToConnectedNodes(node1Id, node2Id);
	if (!isAdded) {
		std::cout << "Node " << node1Id << "already connected to node " << node2Id << " !";
	}

	isAdded = this->addIdToConnectedNodes(node2Id, node1Id);
	if (!isAdded) {
		std::cout << "Node " << node2Id << "already connected to node " << node1Id << " !";
	}

}

bool cGraph::addIdToConnectedNodes(int mainNodeId, int nodeIdToConnect) {
	for (int i = 0; i < this->allNodes.size(); i++) {
		if (allNodes[i]->id == mainNodeId) {
			return allNodes[i]->addConnection(nodeIdToConnect);
		}
	}
}

void cGraph::printAllNodes() {
	std::cout << "All nodes present in this graph are :: \n";
		for(int i = 0; i < this->allNodes.size(); i++) {
			std::cout << this->allNodes[i]->id << "\t";
		}
}


void cGraph::printGraph() {
	std::cout << "Entire graph represntation :: \n";
	std::cout << "Node ID              || \t  Connections \n";
	for (int i = 0; i < this->allNodes.size(); i++) {
		std::cout << this->allNodes[i]->id << "                    || \t ";
		this->allNodes[i]->printConnections();
		std::cout << "\n";
	}
	std::cout << "\n\n";
}

bool cGraph::findIfNodePresentInGraph(int nodeId) {
	for (int i = 0; i < this->allNodes.size(); i++) {
		if (allNodes[i]->id == nodeId)
			return true;
	}
	
	return false;
}

void cGraph::findShortestPath(int node1Id, int node2Id) {
	bool isPresent = this->findIfNodePresentInGraph(node1Id);
	if (!isPresent) {
		std::cout << "Node " << node1Id << " not present in the graph ! \n";
	}

	int shortestPathLength = 9999;

	isPresent = this->findIfNodePresentInGraph(node2Id);
	if (!isPresent) {
		std::cout << "Node " << node2Id << " not present in the graph ! \n";
	}
	std::vector<cPath*> allPaths;
	this->populateNodeMap(this->mapNodeToConnectedNodes);
	this->parseGraph(node1Id,node2Id,allPaths,this->mapNodeToConnectedNodes);

	std::cout << "All Paths parsed in the graph : \n";
	for (int i = 0; i < allPaths.size(); i++) {
		
		std::cout << "Path Size : " << allPaths[i]->pathLength << " \t Path :  ";
		for (int j = 0; j < allPaths[i]->pathNodes.size(); j++) {
				std::cout << allPaths[i]->pathNodes[j];
				if (j < allPaths[i]->pathNodes.size() - 1)
					std::cout << "->";
		}
		std::cout << "\n";
	}

	std::cout << "\nPaths from source to destination : \n";
	for (int i = 0; i < allPaths.size(); i++) {
		if ((std::find(allPaths[i]->pathNodes.begin(), allPaths[i]->pathNodes.end(), node1Id) != allPaths[i]->pathNodes.end()) && (std::find(allPaths[i]->pathNodes.begin(), allPaths[i]->pathNodes.end(), node2Id) != allPaths[i]->pathNodes.end())) {
			if (allPaths[i]->pathLength < shortestPathLength)
				shortestPathLength = allPaths[i]->pathLength;
			std::cout << "Path Size : " << allPaths[i]->pathLength << " \t Path :  ";
			for (int j = 0; j < allPaths[i]->pathNodes.size(); j++) {
				std::cout << allPaths[i]->pathNodes[j];
				if (j < allPaths[i]->pathNodes.size() - 1)
					std::cout << "->";
			}
			std::cout << "\n";
		}
	}

	std::cout << "\nThe shortest path is/are : ";
	for (int i = 0; i < allPaths.size(); i++) {
		if ((std::find(allPaths[i]->pathNodes.begin(), allPaths[i]->pathNodes.end(), node1Id) != allPaths[i]->pathNodes.end()) && (std::find(allPaths[i]->pathNodes.begin(), allPaths[i]->pathNodes.end(), node2Id) != allPaths[i]->pathNodes.end())) {	
			if (allPaths[i]->pathLength == shortestPathLength) {
				for (int j = 0; j < allPaths[i]->pathNodes.size(); j++) {
					std::cout << allPaths[i]->pathNodes[j];
					if (j < allPaths[i]->pathNodes.size() - 1)
						std::cout << "->";
				}
				std::cout << ", ";
			}
		}
	}
	std::cout << "having the least number of hops : "<< shortestPathLength << "\n\n";
}

void cGraph::parseGraph(int startNodeId , int endNodeId,std::vector<cPath*> &allPaths, std::map<int, std::vector<int>> &nodeMap) {
	cNode* startNode = getCNode(startNodeId);
	for (int j = 0; j < startNode->connectedNodes.size(); j++) {
		cPath* newPath = new cPath(startNodeId);
		if (std::find(nodeMap[startNode->id].begin(), nodeMap[startNode->id].end(), startNode->connectedNodes[j]) != nodeMap[startNode->id].end()) {
			if (startNode->connectedNodes[j] == endNodeId) {
				bool newPathToCreate = false;
				for (int k = 0; k < allPaths.size(); k++) {
					if (allPaths[k]->pathNodes.back() == startNode->id) {
						if (j < startNode->connectedNodes.size() - 1) {
							cPath* tmpPath = new cPath();
							tmpPath->pathLength = allPaths[k]->pathLength;
							tmpPath->pathNodes = allPaths[k]->pathNodes;
							allPaths.push_back(tmpPath);
						}
						allPaths[k]->pathLength += 1;
						allPaths[k]->pathNodes.push_back(startNode->connectedNodes[j]);
						//nodeMap[startNode->id].erase(std::remove(nodeMap[startNode->id].begin(), nodeMap[startNode->id].end(), startNode->connectedNodes[j]), nodeMap[startNode->id].end());
						break;
					}
					else
						newPathToCreate = true;
				}
				if(newPathToCreate){
					newPath->pathLength = newPath->pathLength + 1;
					newPath->pathNodes.push_back(endNodeId);
					allPaths.push_back(newPath);
					break;
				}
			}	
			else
			{
				bool hasFoundExistingPath = false;
				for (int k = 0; k < allPaths.size(); k++) {
					if (allPaths[k]->pathNodes.back() == startNode->id && (std::find(nodeMap[startNode->id].begin(), nodeMap[startNode->id].end(), startNode->connectedNodes[j]) != nodeMap[startNode->id].end())) {
						hasFoundExistingPath = true;
						if (std::find(allPaths[k]->pathNodes.begin(), allPaths[k]->pathNodes.end(), startNode->connectedNodes[j]) != allPaths[k]->pathNodes.end()) {}
						else {
							if (j < startNode->connectedNodes.size() - 1) {
								cPath* tmpPath = new cPath();
								tmpPath->pathLength = allPaths[k]->pathLength;
								tmpPath->pathNodes = allPaths[k]->pathNodes;
								allPaths.push_back(tmpPath);
							}
							allPaths[k]->pathLength += 1;
							allPaths[k]->pathNodes.push_back(startNode->connectedNodes[j]);
							nodeMap[startNode->id].erase(std::remove(nodeMap[startNode->id].begin(), nodeMap[startNode->id].end(), startNode->connectedNodes[j]), nodeMap[startNode->id].end());
							parseGraph(startNode->connectedNodes[j], endNodeId, allPaths, nodeMap);
						}
					}
				}
				if (hasFoundExistingPath == false) {
					newPath->pathLength = newPath->pathLength + 1;
					newPath->pathNodes.push_back(startNode->connectedNodes[j]);
					allPaths.push_back(newPath);
					parseGraph(startNode->connectedNodes[j], endNodeId, allPaths, nodeMap);
				}
			}
		}
	}
}

cNode* cGraph::getCNode(int nodeId) {
	for (int i = 0; i < this->allNodes.size(); i++) {
		if (allNodes[i]->id == nodeId)
			return allNodes[i];
	}

	return false;
}

void cGraph::populateNodeMap(std::map<int, std::vector<int>> &nodeMap) {
	nodeMap.clear();
	for (int i = 0; i < allNodes.size(); i++) {
		nodeMap.insert(std::pair<int,std::vector<int>>(i+1, allNodes[i]->connectedNodes));
	}
}