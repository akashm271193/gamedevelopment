#pragma once

#include "cCollisionBody.h"
#include "btBulletDynamicsCommon.h"
namespace libPhysics {
	class cCollision
	{
		public:
			cCollision();
			~cCollision();
			cCollisionBody* A;
			cCollisionBody* B;
			btVector3 contactPtA;
			btVector3 contactPtB;
			btVector3 normalOnB;
			cCollisionBody* getKinematicBody();
			cCollisionBody* getStaticBody();
			btVector3 getNewPosition();
			cCollisionBody* cCollision::getOtherKinematicBody(cCollisionBody* otherBody);
	};
}