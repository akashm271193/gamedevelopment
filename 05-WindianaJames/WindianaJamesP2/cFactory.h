#ifndef _FACTORY_HG_
#define _FACTORY_HG_

#include <string>
#include <vector>
#include <map>
#include <glm\vec3.hpp>
using namespace std;

class cGameObject;
class cGameLevel;

class Entity
{
public:
	Entity() {}
	virtual ~Entity() {}
};

class Factory
{
public:
	virtual Entity* CreateObjectOfType(string type, glm::vec3 position) = 0;
};

class MonsterFactory : public Factory
{
public:
	virtual Entity* CreateObjectOfType(string type, glm::vec3 position);
	void makeWindyMove(int facing, Entity* Windy,cGameLevel &gameLevel);
	std::map<std::string, cGameObject> FactoryObjects;
private:
	std::vector<Entity*> vecEntities;
	
};


#endif