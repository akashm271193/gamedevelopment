#pragma once
#ifndef _cPHYSICS_ENGINE_HG_
#define _cPHYSICS_ENGINE_HG_

#define GLM_ENABLE_EXPERIMENTAL
#include <glm\game_math.h>
#include <string>
#include <vector>
#include <map>
#include "extern.h"

class cBall;
class cPlane;
class cModel;

class cPhysicsEngine
{
public:
	cPhysicsEngine();
	~cPhysicsEngine();
	bool LoadModelsIntoScene(std::string fileName);

	void RenderScene(int shaderId);

	std::vector<cBall*> Balls;
	std::vector<cPlane*> Planes;
	int numBalls;
private:
	std::map<std::string, cModel*> mapLoadedModels;
	int numPlanes;
	
};
#endif