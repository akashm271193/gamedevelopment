#include "globalGameStuff.h"
#include "cMenu.h"
#include "cMenuObj.h"
cMenuLoader::cMenuLoader(){
	loadMenuObjectShape();
	loadMenus();
	loadTextFontShape();
	initfreetype();
	this->activeMenuIndex = 0;
	this->action = eMenuObjType::UNKNOWN_OBJ;
}


cMenuLoader::~cMenuLoader()
{
}

void cMenuLoader::loadMenus() {
	cMenu* tmpMenu = new cMenu(eMenuType::MAIN,4);
	addMenu(tmpMenu);
	tmpMenu = new cMenu(eMenuType::GAME_STATE_MENU,4);
	addMenu(tmpMenu);
	tmpMenu = new cMenu(eMenuType::GAME_END_MENU, 1);
	addMenu(tmpMenu);
	tmpMenu = new cMenu(eMenuType::GAME_START_MENU, 1);
	addMenu(tmpMenu);
	tmpMenu = new cMenu(eMenuType::FIRE_END_MENU, 1);
	addMenu(tmpMenu);
}

void cMenuLoader::addMenu(cMenu* newMenu) {
	this->vecMenus.push_back(newMenu);
}

void cMenuLoader::loadMenuObjectShape() {

	///Code to load menu Obj//
	//Code to load shaders ends
	float vertices[] = {
		0.3f,  0.1f, 0.0f,  // top right
		0.3f, -0.1f, 0.0f,  // bottom right
		-0.3f, -0.1f, 0.0f,  // bottom left
		-0.3f,  0.1f, 0.0f   // top left 
	};

	maxExtentXYZ = glm::vec3(0.6f,0.2f,0.0f);
	unsigned int indices[] = {  // note that we start from 0!
		0, 1, 3,   // first triangle
		1, 2, 3    // second triangle
	};

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	// bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);


	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	// note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
	// VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
	glBindVertexArray(0);
}


void cMenuLoader::loadTextFontShape() {
	mwidth = SCR_WIDTH;
	mheight = SCR_HEIGHT;

	sx = 2.0f / mwidth;
	sy = 2.0f / mheight;

	yoffset = 8 * sx;
	xoffset = 8 * sx;

	//generate and bind vbo 
	glGenBuffers(1, &textVBO);

	//generate and bind vao
	glGenVertexArrays(1, &textVAO);
	// bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
	glBindVertexArray(textVAO);

}

void cMenuLoader::renderMenu(int menuShaderId, int textShaderId) {
	glDisable(GL_DEPTH_TEST);

	cMenu* menuToRender = this->vecMenus[activeMenuIndex];

	gPShaderManager->useShaderProgram(menuShaderId);
	glBindVertexArray(VAO); // seeing as we only have a single VAO there's no need to bind it every time, but we'll do so to keep things a bit more organized
	unsigned int transformPos = glGetUniformLocation(menuShaderId,"transform");
	
	for (int i = 0; i < menuToRender->menuObjects; i++) {
		glm::mat4 transform = glm::mat4(1.0f);
		transform = glm::translate(transform, menuToRender->vecMenuObjects[i]->position);
		transform = glm::scale(transform, menuToRender->vecMenuObjects[i]->scale);
		glUniformMatrix4fv(transformPos, 1, GL_FALSE, glm::value_ptr(transform));
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	}
	glBindVertexArray(0);


	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	/////////////////////////////// Text Rendering ///////////////////////////////////////////
	gPShaderManager->useShaderProgram(textShaderId);
	attribute_coord = glGetAttribLocation(textShaderId, "coord");
	uniform_igcolour = glGetUniformLocation(textShaderId, "igcolour");
	uniform_tex = glGetUniformLocation(textShaderId, "tex");
	uniform_color = glGetUniformLocation(textShaderId, "color");


	glUniform4fv(uniform_color, 1, glm::value_ptr(glm::vec4(1.0f, 0.0f, 0.0f, 1.0f)));

	glBindVertexArray(textVAO);

	if (activeMenuIndex == 2 || activeMenuIndex == 3 || activeMenuIndex == 4) {
		for (int i = 0; i < menuToRender->menuObjects; i++) {
			xoffset = (menuToRender->vecMenuObjects[i]->position.x) - ((maxExtentXYZ.x/2.0f) * menuToRender->vecMenuObjects[i]->scale.x) + 0.16;
			yoffset = (menuToRender->vecMenuObjects[i]->position.y) + ((maxExtentXYZ.y / 2.0f) * menuToRender->vecMenuObjects[i]->scale.y) - 0.1;
			render_text(menuToRender->vecMenuObjects[i]->objText.c_str(), xoffset - 0.15, yoffset, sx, sy);
		}
	}
	else {
		for (int i = 0; i < menuToRender->menuObjects; i++) {
			xoffset = menuToRender->vecMenuObjects[i]->position.x;
			yoffset = menuToRender->vecMenuObjects[i]->position.y;
			render_text(menuToRender->vecMenuObjects[i]->objText.c_str(), xoffset - 0.15, yoffset, sx, sy);
		}
	}

	glBindVertexArray(0);
	//Example how to render text
	/////////////////////////////// Text Rendering Ends ///////////////////////////////////////////
	glEnable(GL_DEPTH_TEST);
}


bool cMenuLoader::initfreetype() {

	if (FT_Init_FreeType(&mft))
	{
		fprintf(stderr, "unable to init free type\n");
		return GL_FALSE;
	}

	if (FT_New_Face(mft, "../extern/fonts/FreeSans.ttf", 0, &mface))
	{
		fprintf(stderr, "unable to open font\n");
		return GL_FALSE;
	}

	//set font size
	FT_Set_Pixel_Sizes(mface, 0, 36);


	if (FT_Load_Char(mface, 'X', FT_LOAD_RENDER))
	{
		fprintf(stderr, "unable to load character\n");
		return GL_FALSE;
	}


	return GL_TRUE;
}


void cMenuLoader::render_text(const char *text, float x, float y, float sx, float sy) {

	glUniform1i(uniform_igcolour, false);

	const char *p;
	FT_GlyphSlot g = mface->glyph;

	GLuint tex;
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glUniform1i(uniform_tex, 0);

	/* We require 1 byte alignment when uploading texture data */
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	/* Clamping to edges is important to prevent artifacts when scaling */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	/* Linear filtering usually looks best for text */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	/* Set up the VBO for our vertex data */
	glEnableVertexAttribArray(attribute_coord);
	glBindBuffer(GL_ARRAY_BUFFER, textVBO);
	glVertexAttribPointer(attribute_coord, 4, GL_FLOAT, GL_FALSE, 0, 0);

	int nextLines = 0;
	/* Loop through all characters */
	for (p = text; *p; p++) {

		if (*p == '|') {
			nextLines++;
			cMenu* menuToRender = this->vecMenus[activeMenuIndex];
			x = (menuToRender->vecMenuObjects[0]->position.x) - ((maxExtentXYZ.x / 2.0f) * menuToRender->vecMenuObjects[0]->scale.x) + 0.01;
			y = (y) - (0.2f);
			continue;
		}

		/* Try to load and render the character */
		if (FT_Load_Char(mface, *p, FT_LOAD_RENDER))
			continue;

		/* Upload the "bitmap", which contains an 8-bit grayscale image, as an alpha texture */
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, g->bitmap.width, g->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE, g->bitmap.buffer);


		/* Calculate the vertex and texture coordinates */
		float x2 = x + g->bitmap_left * sx;
		float y2 = -y - g->bitmap_top * sy;
		float w = g->bitmap.width * sx;
		float h = g->bitmap.rows * sy;

		point box[4] = {
			{ x2, -y2, 0, 0 },
			{ x2 + w, -y2, 1, 0 },
			{ x2, -y2 - h, 0, 1 },
			{ x2 + w, -y2 - h, 1, 1 }
		};

		/* Draw the character on the screen */
		glBufferData(GL_ARRAY_BUFFER, sizeof(box), box, GL_DYNAMIC_DRAW);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		/* Advance the cursor to the start of the next character */
		x += (g->advance.x >> 6) * sx;
		y += (g->advance.y >> 6) * sy;
	}

	glDisableVertexAttribArray(attribute_coord);
	glDeleteTextures(1, &tex);
}


void cMenuLoader::selectMenuOption(float posX, float posY) {
	float screenPosX = convertToScreenPos(posX,'X');
	float screenPosY = convertToScreenPos(posY,'Y');
	//std::cout << "screenPosX : " << screenPosX << std::endl;
	//std::cout << "screenPosY : " << screenPosY << std::endl;
	cMenu* currentMenu = this->vecMenus[activeMenuIndex];
	glm::vec3 curPos = glm::vec3(0.0f, 0.7f, 0.0f);
	for (int i = 0; i < currentMenu->menuObjects; i++) {
		if (isButtonClicked(screenPosX, screenPosY, currentMenu->vecMenuObjects[i])) {
			std::cout << "Menu item selected " << currentMenu->vecMenuObjects[i]->objText << std::endl;
			currentMenu->vecMenuObjects[i]->doAction(this->activeMenuIndex,this->action);
		}
	}
	///if()
};

float cMenuLoader::convertToScreenPos(float pos, char axis) {
	float newPos = 0.0f;
	switch (axis) {
		case 'X':
			newPos = pos - (SCR_WIDTH / 2) ;
			newPos = newPos / (SCR_WIDTH / 2);
			break;

		case 'Y':
			newPos = (SCR_HEIGHT / 2) - pos;
			newPos = newPos / (SCR_HEIGHT / 2);
			break;
	}

	return newPos;
};

bool cMenuLoader::isButtonClicked(float posX, float posY, cMenuObj* obj) {
	float buttonMinX = obj->position.x - (maxExtentXYZ.x / 2);
	float buttonMaxX = obj->position.x + (maxExtentXYZ.x / 2);

	float buttonMinY = obj->position.y - (maxExtentXYZ.y / 2);
	float buttonMaxY = obj->position.y + (maxExtentXYZ.y / 2);

	if ((posX >= buttonMinX) && (posX <= buttonMaxX) && (posY >= buttonMinY) && (posY <= buttonMaxY))
		return true;
	return false;

}


void cMenuLoader::setActiveMenu(int index) {
	this->activeMenuIndex = index;
};

int cMenuLoader::getActiveMenu() {
	return this->activeMenuIndex;

};