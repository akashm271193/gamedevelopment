#include "cAnimationState.h"

bool cAnimationState::sAnimationState::IncrementTime(bool bResetToZero)
{
	bool isAnimationReset = false;

	this->currentTime += this->frameStepTime;

	if (this->currentTime >= this->totalTime)
	{
		this->currentTime = 0.0f;
		isAnimationReset = true;
	}
	return isAnimationReset;
}
