#include "HealthObserver.h"

#include <iostream>
using namespace std;

void HealthObserver::Notify(string message)
{
	if (message == "Damage")
	{
		cout << "[HealthObserver] notified:" << message << endl;
		cout << "[HealthObserver] Health updated" << endl;

		// Add function: what happens if health decrease/increase


	}

}