#pragma once
#include <vector>
#include <string> 
#include <iostream>
#include <algorithm>
class cNode
{
public:
	cNode();
	~cNode();
	int id;
	bool addConnection(int);
	void printConnections();
	std::vector<int> connectedNodes;

};

