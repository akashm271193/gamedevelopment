#include "Factory.h"
#include <string>
using namespace std;

Entity* entityArray[];
int entityIndex = 1; // Windy's character is going to be entity no.1, with index 0

//call in scene loader as a new object
class Windiana : public Entity
{
public:
	Windiana() { }
	unsigned int health;
	unsigned int experience;
	float speed;
};


// type of monster class
// gotta make one for each monster type
class Goblin : public Entity
{
public:
	Goblin() { }
	unsigned int health;
	unsigned int experience;
	float speed;
};

class Skeleton : public Entity
{
public:
	Skeleton(){}
	unsigned int health;
	unsigned int experience;
	float speed;
};


Entity* MonsterFactory::CreateObjectOfType(string type)
{
	// Insert new type of monsters
	if (type == "Goblin") 
	{	
		//add new class to the entityArray
		Entity* monster = new Goblin();
		entityArray[entityIndex] = monster;
		entityIndex++;
		return entityArray[entityIndex];
	} 
	else if (type == "Skeleton")
	{
		Entity* monster = new Skeleton();
		entityArray[entityIndex] = monster;
		entityIndex++;
		return entityArray[entityIndex];

		//return new Skeleton();
	}
	//go on adding more


	return 0;
}