#ifndef Model_Utilities_HG_
#define Model_Utilities_HG_


#include <iostream>
#include <fstream>
#include <string>

#include "cMesh.h"

void ReadFileToToken(std::ifstream &file , std::string token);

bool LoadPlyFileIntoMesh(std::string filename , cMesh &testMesh);

#endif
