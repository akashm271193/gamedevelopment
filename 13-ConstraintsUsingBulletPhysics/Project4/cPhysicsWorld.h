#ifndef _WORLD_HG_
#define _WORLD_HG_

#include "cGameObject.h"
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"	
class cPhysicsWorld
{
public:
	cPhysicsWorld();
	void stepSimulation();
	void UpdateBodies();
	btDiscreteDynamicsWorld * world;
	std::vector<btRigidBody*> vecBodies;


	void sliderConstraint(btRigidBody *body, btTransform trans);
	void sliderConstraint(btRigidBody *body1, btRigidBody *body2, btTransform trans1, btTransform trans2);
	void p2pConstraint(btRigidBody *body, btVector3 trans);
	void p2pConstraint(btRigidBody *body1, btRigidBody *body2, btVector3 trans1, btVector3 trans2);
	void hingeConstraint(btRigidBody* body, btVector3 pivot, btVector3 axis);
	void hingeConstraint(btRigidBody* body1, btVector3 pivot1, btVector3 axis1, btRigidBody* body2, btVector3 pivot2, btVector3 axis2);
	void dofConstraint(btRigidBody* body, btTransform trans);
	void dofConstraint(btRigidBody* body1, btRigidBody* body2, btTransform trans1, btTransform trans2);

private:
	btDefaultCollisionConfiguration * config;
	btCollisionDispatcher * dispatcher;
	btBroadphaseInterface * broadphase;
	btSequentialImpulseConstraintSolver * constraintSolver;

};
#endif // !_WORLD_HG_
