#include "cAIGameObject.h"
#include "cPlayerGameObject.h"

cAIGameObject::cAIGameObject()
{
	this->movementMinPos = glm::vec3(0.0f);
	this->movementMaxPos = glm::vec3(0.0f);
	this->objectMovmentDir = eAIDirection::MOVING_TO_MIN;
	this->isRotating = false;
	this->isTranslating = false;
	this->targetAngleRotationY = 0.0f;
	this->originalOrientationY = 0.0f;
	this->attentionRadius= 2.0f;
}

void cAIGameObject::animateObject(float deltaTime, cPlayerGameObject* playerObj){
	cGameObject* tmpGameObject = NULL;
	/*if (this->hasBoundingBoxOrSphere && this->boundingSphereOrBox != NULL) {
		tmpGameObject= this->boundingSphereOrBox;
	}*/

	this->interactWithPlayer(playerObj, tmpGameObject);

		switch (this->objectMovmentDir) {
		case eAIDirection::MOVING_TO_MAX:
			//Check if reached destination
			if (fabs(this->position.x - this->movementMaxPos.x) <= 0.05f && fabs(this->position.z - this->movementMaxPos.z) <= 0.05f) {
					this->objectMovmentDir = eAIDirection::MOVING_TO_MIN;
					this->ownAxisOrientation.y += glm::radians(180.0f);
					fmod(glm::degrees(this->ownAxisOrientation.y), 360.0f);
					break;
			}
			else {
				translateObject(this->movementMaxPos, deltaTime, tmpGameObject);
			}
			break;

		case eAIDirection::MOVING_TO_MIN:
			if (fabs(this->position.x - this->movementMinPos.x) <= 0.05f && fabs(this->position.z - this->movementMinPos.z) <= 0.05f) {
					this->objectMovmentDir = eAIDirection::MOVING_TO_MAX;
					this->ownAxisOrientation.y += glm::radians(180.0f);
					fmod(glm::degrees(this->ownAxisOrientation.y), 360.0f);
					break;
			}
			else {
					translateObject(this->movementMinPos, deltaTime, tmpGameObject);
			}
			break;

		case eAIDirection::APPROACH_PLAYER:
			this->objectFront = playerObj->objectFront;
			this->updateFromPhysicsWorld();
			this->targetPos = playerObj->position - (0.5f * this->objectFront);
			if (fabs(this->position.x - this->targetPos.x) <= 0.05f && fabs(this->position.z - this->targetPos.z) <= 0.05f)  {
				this->isTranslating = false;
			}
			else {
					translateObject(this->targetPos, deltaTime, tmpGameObject);
			}
			break;

		case eAIDirection::ATTACK_PLAYER:
			translateObject(playerObj->position, deltaTime, tmpGameObject);
			break;

		case eAIDirection::FOLLOW_PLAYER:
			this->targetPos = playerObj->position - (playerObj->objectFront * 0.5f);
			this->ownAxisOrientation = playerObj->ownAxisOrientation;
			this->position = targetPos;
			this->updateObjectVectors();
			break;
		}

		this->collisionBodyDesc.position = this->position;
		this->updatePhysicsWorldObject();
}

void cAIGameObject::calculateAngleOfOrientation(glm::vec3 destinationPoint, float &angleInRadians) {
	glm::vec3 dist = destinationPoint - this->position;
	glm::vec3 distDir = glm::normalize(dist);
	float dotProduct = glm::dot(this->objectFront,distDir);

	if (dotProduct > -0.1f && dotProduct < 0.1f) {
		angleInRadians = 0.0f;
		return;
	}

	//Hack to remove NaN scenarios
	if (dotProduct < -1.0f)
		dotProduct = -1.0f;
	if (dotProduct > 1.0f)
		dotProduct = 1.0f;


	float tmpAngle = glm::degrees(acosf(dotProduct));
	tmpAngle = 360.0f - tmpAngle;

	angleInRadians = glm::radians(tmpAngle);

	float tmpNewAngle = fmodf(this->ownAxisOrientation.y + angleInRadians, glm::radians(360.0f));
	this->objectFront = glm::vec3(cos(tmpNewAngle),0.0f,sin(tmpNewAngle));

};

//All points are in radians
void cAIGameObject::rotateObject(glm::vec3 destinationAngle, float stepTime, cGameObject* tmpGameObject) {
	this->ownAxisOrientation.y = ((destinationAngle.y - this->ownAxisOrientation.y) * stepTime) + this->ownAxisOrientation.y;
};

void cAIGameObject::translateObject(glm::vec3 destinationPoint, float stepTime, cGameObject* tmpGameObject) {
	glm::vec3 newPos = glm::vec3(0.0f);

	if (this->objectMovmentDir == eAIDirection::MOVING_TO_MAX) {
		newPos.x = ((destinationPoint.x - this->position.x) * stepTime) + this->position.x;
		newPos.y = ((destinationPoint.y - this->position.y) * stepTime) + this->position.y;
		newPos.z = ((destinationPoint.z - this->position.z) * stepTime) + this->position.z;
		this->position = newPos;
	}
	else if(this->objectMovmentDir == eAIDirection::MOVING_TO_MIN) {
		newPos.x = this->position.x - ((this->position.x - destinationPoint.x) * stepTime);
		newPos.y = this->position.y - ((this->position.y - destinationPoint.y) * stepTime);
		newPos.z = this->position.z - ((this->position.z - destinationPoint.z) * stepTime) ;
		this->position = newPos;
	}
	else if(this->objectMovmentDir == eAIDirection::APPROACH_PLAYER){
		newPos.x = this->position.x + (-(this->targetPosDir.x) * stepTime);
		newPos.y = this->position.y + (-(this->targetPosDir.y) * stepTime);
		newPos.z = this->position.z + (-(this->targetPosDir.z) * stepTime);
		this->position = newPos;
	}
	else if (this->objectMovmentDir == eAIDirection::EVADE_PLAYER) {
		newPos.x = this->position.x + (this->targetPosDir.x * stepTime);
		newPos.y = this->position.y + (this->targetPosDir.y * stepTime);
		newPos.z = this->position.z + (this->targetPosDir.z * stepTime);
		this->position = newPos;
	}
	else if (this->objectMovmentDir == eAIDirection::ATTACK_PLAYER) {
		newPos.x = this->position.x + (this->targetPosDir.x * stepTime);
		newPos.y = this->position.y + (this->targetPosDir.y * stepTime);
		newPos.z = this->position.z + (this->targetPosDir.z * stepTime);
		this->position = newPos;
	}
	else if (this->objectMovmentDir == eAIDirection::FOLLOW_PLAYER) {
		newPos.x = ((destinationPoint.x - this->position.x) * stepTime) + this->position.x;
		newPos.y = ((destinationPoint.y - this->position.y) * stepTime) + this->position.y;
		newPos.z = ((destinationPoint.z - this->position.z) * stepTime) + this->position.z;
		if(glm::distance(destinationPoint,this->position) <3.0f)
			this->position = newPos;
	}

	if (tmpGameObject != NULL) {
		tmpGameObject->position = this->position;
	} 
};

bool cAIGameObject::isFacingPlayer(cPlayerGameObject* playerObj) {
	float isAIFacing = glm::dot(this->objectFront, playerObj->objectFront);

	if ((-0.95 > isAIFacing) && (isAIFacing > -1.0f)) {
		return true;
	}
	return false;
}


bool cAIGameObject::isInAttentionRadius(cPlayerGameObject* playerObj) {
	float distPlayAI = glm::distance(this->position, playerObj->position);
	if (distPlayAI <= this->attentionRadius) {
		return true;
	}
	return false;
}

bool cAIGameObject::hasPlayerMoved(cPlayerGameObject* playerObj) {
	if (this->prevKnownPlayerPos.x != playerObj->position.x || this->prevKnownPlayerPos.y != playerObj->position.y || this->prevKnownPlayerPos.z != playerObj->position.z)
	{
		return true;
	}
	
	return false;
}

void cAIGameObject::interactWithPlayer(cPlayerGameObject* playerObj, cGameObject* tmpGameObject) {
	switch (this->typeOfObject) {
		case libPhysics::OBJ_FOLLOWER_AI: 
			this->objectMovmentDir = eAIDirection::FOLLOW_PLAYER;
			this->action = eAiAction::FOLLOW;
			break;
		
		case libPhysics::OBJ_ANGRY_AI:
			if (isInAttentionRadius(playerObj)) {
				this->objectMovmentDir = eAIDirection::ATTACK_PLAYER;
				this->action = eAiAction::ATTACK;
			}
			else {
				if (this->action != eAiAction::ANIMATE_AI) {
					this->objectMovmentDir = eAIDirection::MOVING_TO_MIN;
					this->action = eAiAction::ANIMATE_AI;
				}
			}
			break;

		case libPhysics::OBJ_CURIOUS_AI:
			if (this->isInAttentionRadius(playerObj)) {
				this->objectMovmentDir = eAIDirection::APPROACH_PLAYER;
				this->action = eAiAction::APPROACH;
			}
			else {
				if (this->action != eAiAction::ANIMATE_AI) {
					this->objectMovmentDir = eAIDirection::MOVING_TO_MIN;
					this->action = eAiAction::ANIMATE_AI;
				}
			}
			break;
	}
}