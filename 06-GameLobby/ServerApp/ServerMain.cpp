#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <thread>
#include <future>
#include "buffer.h"
#include <errno.h> 
#include <sys/types.h> 
#include <map> 
#include "ClientModel.h"
#include <ctime>
#include <cstdlib>
#include <random>
#include <algorithm>
#include <iterator>


// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")

#define TRUE   1 
#define FALSE  0 
#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

std::map<std::string, std::string> gMapLobbyToSessions;
std::map<int, int> gMapClientToRequests;
std::map<std::string, ClientModel*> gMapClientToSessions;
std::vector<ClientModel*> gVecClientSockets;
buffer sendBuffer(512);
buffer recvBuffer(512);

std::vector<std::string> explodeString(std::string inputString, char delim)
{
	std::vector<std::string> result;
	std::istringstream iss(inputString);

	if (inputString != "") {
		for (std::string token; std::getline(iss, token, delim); )
		{
			if (!token.empty() && token != "")
				result.push_back(std::move(token));
		}
	}

	return result;
}

void resizeBufferIfRequired(buffer &bufferType, int packetLength)
{
	if (packetLength > DEFAULT_BUFLEN) {
		bufferType.resizeBuffer((unsigned int)packetLength);
	}
}

//Example code: A simple server side code, which echos back the received message.
//Handle multiple socket connections with select and fd_set on Linux 


bool sendMessageToClients(SOCKET clientSocket, std::string sessionToken,std::string message, std::string room,int msgType)
{
	//If the message is untampered or coming from other clients
	/*if (msgType == 0) {
		int eraseHead = room.length() + 7; // "/send RoomName " to be removed
		message.erase(0, eraseHead);
	}*/
	std::string sendString = "";
	if (msgType != 9) {
		int roomLength = room.length();
		int sessionTokenLength = sessionToken.length();
		int messageLength = message.length();
		int packetLength = 0;

		if (msgType == 0) {
			packetLength = roomLength + sessionTokenLength + 16;
		}
		else
			packetLength = roomLength + sessionTokenLength + messageLength + 20;

		resizeBufferIfRequired(sendBuffer, packetLength);

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(msgType);
		sendBuffer.writeInt32BE(roomLength);
		sendBuffer.writeString(room);
		sendBuffer.writeInt32BE(sessionTokenLength);
		sendBuffer.writeString(sessionToken);
		if (msgType > 0) {
			sendBuffer.writeInt32BE(messageLength);
			sendBuffer.writeString(message);
		}

		sendString = sendBuffer.readString(packetLength);
		sendBuffer.resetIndicesManually();
	}
	else
		sendString = message;

	int sendLength = sendString.length();
	int iResult = send(clientSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(clientSocket);
		WSACleanup();
		return 0;
	}
	
	sendBuffer.resizeBuffer(DEFAULT_BUFLEN);

	return 1;
}

bool sendMessageToAuthServer(buffer recvBuffer,SOCKET clientSocket, std::string message, int msgType)
{
	int packetLength = 0;
	if (msgType == 0) {
		int messageID = msgType;
		int messageLength = message.length();	
		packetLength = messageLength + 12;

		resizeBufferIfRequired(sendBuffer, packetLength);

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageID);
		sendBuffer.writeInt32BE(messageLength);
		sendBuffer.writeString(message);
	}
	else if (msgType == 1) {
		int messageID = msgType;
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string sessionToken = recvBuffer.readString(sessionTokenLength);
		int emailLength = recvBuffer.readInt32BE();
		std::string email = recvBuffer.readString(emailLength);
		int passwordLength = recvBuffer.readInt32BE();
		std::string password = recvBuffer.readString(passwordLength);
		packetLength = sessionTokenLength + emailLength + passwordLength + 20;

		resizeBufferIfRequired(sendBuffer, packetLength);
		
		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageID);
		sendBuffer.writeInt32BE(sessionTokenLength);
		sendBuffer.writeString(sessionToken);
		sendBuffer.writeInt32BE(emailLength);
		sendBuffer.writeString(email);
		sendBuffer.writeInt32BE(passwordLength);
		sendBuffer.writeString(password);
	}
	else if (msgType == 2) {
		int messageID = msgType;
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string sessionToken = recvBuffer.readString(sessionTokenLength);
		int emailLength = recvBuffer.readInt32BE();
		std::string email = recvBuffer.readString(emailLength);
		int passwordLength = recvBuffer.readInt32BE();
		std::string password = recvBuffer.readString(passwordLength);
		packetLength = sessionTokenLength + emailLength + passwordLength + 20;

		resizeBufferIfRequired(sendBuffer, packetLength);
		
		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageID);
		sendBuffer.writeInt32BE(sessionTokenLength);
		sendBuffer.writeString(sessionToken);
		sendBuffer.writeInt32BE(emailLength);
		sendBuffer.writeString(email);
		sendBuffer.writeInt32BE(passwordLength);
		sendBuffer.writeString(password);
	}
	else if (msgType == 3) {
		int messageID = msgType;
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string sessionToken = recvBuffer.readString(sessionTokenLength);
		packetLength = sessionTokenLength + 12;

		resizeBufferIfRequired(sendBuffer, packetLength);

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageID);
		sendBuffer.writeInt32BE(sessionTokenLength);
		sendBuffer.writeString(sessionToken);
	}
	else if (msgType == 4) {
		int messageID = msgType;
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string sessionToken = recvBuffer.readString(sessionTokenLength);
		packetLength = sessionTokenLength + 12;

		resizeBufferIfRequired(sendBuffer, packetLength);

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageID);
		sendBuffer.writeInt32BE(sessionTokenLength);
		sendBuffer.writeString(sessionToken);
	}
	else if (msgType == 5) {
		int messageID = msgType;
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string sessionToken = recvBuffer.readString(sessionTokenLength);
		int mapLength          = recvBuffer.readInt32BE();
		std::string map        = recvBuffer.readString(mapLength);
		int lobbyNameLength    = recvBuffer.readInt32BE();
		std::string lobbyName  = recvBuffer.readString(lobbyNameLength);
		int gameModeLength     = recvBuffer.readInt32BE();
		std::string gameMode   = recvBuffer.readString(gameModeLength);
		int maxPlayersLength   = recvBuffer.readInt32BE();
		std::string maxPlayers = recvBuffer.readString(maxPlayersLength);
		packetLength = sessionTokenLength + mapLength + lobbyNameLength + gameModeLength + maxPlayersLength + 28;

		resizeBufferIfRequired(sendBuffer, packetLength);

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageID);
		sendBuffer.writeInt32BE(sessionTokenLength);
		sendBuffer.writeString(sessionToken);
		sendBuffer.writeInt32BE(mapLength);
		sendBuffer.writeString(map);
		sendBuffer.writeInt32BE(lobbyNameLength);
		sendBuffer.writeString(lobbyName);
		sendBuffer.writeInt32BE(gameModeLength);
		sendBuffer.writeString(gameMode);
		sendBuffer.writeInt32BE(maxPlayersLength);
		sendBuffer.writeString(maxPlayers);
	}
	else if (msgType == 6) {
		int messageID = msgType;
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string sessionToken = recvBuffer.readString(sessionTokenLength);
		int lobbyLength = recvBuffer.readInt32BE();
		std::string lobby = recvBuffer.readString(lobbyLength);
		packetLength = sessionTokenLength + lobbyLength + 16;

		resizeBufferIfRequired(sendBuffer, packetLength);

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageID);
		sendBuffer.writeInt32BE(sessionTokenLength);
		sendBuffer.writeString(sessionToken);
		sendBuffer.writeInt32BE(lobbyLength);
		sendBuffer.writeString(lobby);
		
	}
	else if (msgType == 7) {
		int messageID = msgType;
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string sessionToken = recvBuffer.readString(sessionTokenLength);
		int lobbyLength = recvBuffer.readInt32BE();
		std::string lobby = recvBuffer.readString(lobbyLength);
		packetLength = sessionTokenLength + lobbyLength + 16;

		resizeBufferIfRequired(sendBuffer, packetLength);

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageID);
		sendBuffer.writeInt32BE(sessionTokenLength);
		sendBuffer.writeString(sessionToken);
		sendBuffer.writeInt32BE(lobbyLength);
		sendBuffer.writeString(lobby);
	}

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();
	sendBuffer.resetIndicesManually();

	int iResult = send(clientSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(clientSocket);
		WSACleanup();
		return 0;
	}
	sendBuffer.resizeBuffer(DEFAULT_BUFLEN);
	return 1;
}

void processMessage(buffer recvBuffer,ClientModel* client,std::string inputStringData) {
	int packetLength = recvBuffer.readInt32BE();
	int messageID = recvBuffer.readInt32BE();
	if (messageID == 0) {
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string sessionToken = recvBuffer.readString(sessionTokenLength);
		ClientModel* client = gMapClientToSessions[sessionToken];
		gVecClientSockets.push_back(client);
		sendMessageToClients(client->socketDescriptor,sessionToken,"","Welcome",0);
	}
	else if (messageID == 1)
	{
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string sessionToken = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		if (::gMapClientToSessions.find(sessionToken) != ::gMapClientToSessions.end()) {
			ClientModel* client = gMapClientToSessions[sessionToken];
			sendMessageToClients(client->socketDescriptor, sessionToken, message, "Welcome", 1);
		}
	}
	else if (messageID == 2)
	{
		sendMessageToAuthServer(recvBuffer,::gVecClientSockets[0]->socketDescriptor, "", 1);
	}
	else if (messageID == 3)
	{
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string sessionToken = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		ClientModel* client = gMapClientToSessions[sessionToken];
		sendMessageToClients(client->socketDescriptor, sessionToken, message, "Login", 2);
	}
	else if (messageID == 4)
	{
		sendMessageToAuthServer(recvBuffer,::gVecClientSockets[0]->socketDescriptor, "", 2);
	}
	else if (messageID == 5)
	{
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string sessionToken = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		ClientModel* client = gMapClientToSessions[sessionToken];
		sendMessageToClients(client->socketDescriptor, sessionToken, message, "Register", 3);
	}
	else if (messageID == 6)
	{
		sendMessageToAuthServer(recvBuffer, ::gVecClientSockets[0]->socketDescriptor, "", 3);
	}
	else if (messageID == 7)
	{
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string sessionToken = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		if (::gMapClientToSessions.find(sessionToken) != ::gMapClientToSessions.end()) {
			ClientModel* client = gMapClientToSessions[sessionToken];
			sendMessageToClients(client->socketDescriptor, sessionToken, message, "Logout", 4);
		}
	}
	else if (messageID == 8)
	{
		sendMessageToAuthServer(recvBuffer, ::gVecClientSockets[0]->socketDescriptor, "", 4);
	}
	else if (messageID == 9)
	{
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string sessionToken = recvBuffer.readString(sessionTokenLength);
		ClientModel* client = gMapClientToSessions[sessionToken];
		sendMessageToClients(client->socketDescriptor, sessionToken, inputStringData, "Logout", 9);
	}
	else if (messageID == 10)
	{
		sendMessageToAuthServer(recvBuffer, ::gVecClientSockets[0]->socketDescriptor, inputStringData, 5);
	}
	else if (messageID == 11)
	{
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string sessionToken = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		gMapLobbyToSessions[sessionToken] = message;
		ClientModel* client = gMapClientToSessions[sessionToken];
		sendMessageToClients(client->socketDescriptor, sessionToken, message, "LobbyCreated", 5);
	}
	else if (messageID == 12)
	{
		sendMessageToAuthServer(recvBuffer, ::gVecClientSockets[0]->socketDescriptor, "", 6);
	}
	else if (messageID == 13)
	{
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string sessionToken = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		std::vector<std::string> msgData = explodeString(message, '||');
		gMapLobbyToSessions[sessionToken] = msgData[0];
		int sessionTokensToBrodcast = recvBuffer.readInt32BE();
		if (sessionTokensToBrodcast > 0) {
			for (int i = 0; i < sessionTokensToBrodcast; i++) {
				sessionTokenLength = recvBuffer.readInt32BE();
				std::string brodcastSessionToken = recvBuffer.readString(sessionTokenLength);
				ClientModel* client = gMapClientToSessions[brodcastSessionToken];
				if(brodcastSessionToken == sessionToken)
					sendMessageToClients(client->socketDescriptor, brodcastSessionToken, message, "LobbyJoined", 6);
				else {
					sendMessageToClients(client->socketDescriptor, brodcastSessionToken, msgData[1], "LobbyJoined", 1);
				}
			}
		}	
	}
	else if (messageID == 14)
	{
		sendMessageToAuthServer(recvBuffer, ::gVecClientSockets[0]->socketDescriptor, "", 7);
	}
	else if (messageID == 15)
	{
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string sessionToken = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		::gMapLobbyToSessions.erase(sessionToken);
		if (::gMapClientToSessions.find(sessionToken) != ::gMapClientToSessions.end()) {
			ClientModel* client = gMapClientToSessions[sessionToken];
			sendMessageToClients(client->socketDescriptor, sessionToken, message, "LobbyLeft", 7);
		}
		int sessionTokensToBrodcast = recvBuffer.readInt32BE();
		if (sessionTokensToBrodcast > 0) {
			for (int i = 0; i < sessionTokensToBrodcast; i++) {
				sessionTokenLength = recvBuffer.readInt32BE();
				std::string brodcastSessionToken = recvBuffer.readString(sessionTokenLength);
				ClientModel* client = gMapClientToSessions[brodcastSessionToken];
				std::vector<std::string> msgData = explodeString(message,'||');
				sendMessageToClients(client->socketDescriptor, brodcastSessionToken, msgData[1], "LobbyLeft", 1);
			}
		}

	}
	else if (messageID == 16)
	{
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string sessionToken = recvBuffer.readString(sessionTokenLength);
		int sessionTokensKicked = recvBuffer.readInt32BE();
		std::string message = "Padding for Lobby Name ||Left lobby successfully";
		::gMapLobbyToSessions.erase(sessionToken);
		if (::gMapClientToSessions.find(sessionToken) != ::gMapClientToSessions.end()) {
			ClientModel* client = gMapClientToSessions[sessionToken];
			sendMessageToClients(client->socketDescriptor, sessionToken, message, "LobbyLeft", 7);
		}
		if(sessionTokensKicked > 0 ){
			for (int i = 0; i < sessionTokensKicked; i++) {
				sessionTokenLength = recvBuffer.readInt32BE();
				sessionToken = recvBuffer.readString(sessionTokenLength);
				::gMapLobbyToSessions.erase(sessionToken);
				message = "You have been booted from the lobby as the host as left the lobby !";
				ClientModel* client = gMapClientToSessions[sessionToken];
				sendMessageToClients(client->socketDescriptor, sessionToken, message, "BootedFromLobby", 8);
			}
		}
	}
	return;

}

void closeClientConnection(SOCKET clientSocket, fd_set &readfds) {
		int iResult = closesocket(clientSocket);
		if (iResult == SOCKET_ERROR)
			printf("Error in closing socket...\n");
		else
			printf("Socket connection closed...\n");
		FD_CLR(clientSocket, &readfds);
		std::string clientSessionToken = "";
		for (std::map<std::string, ClientModel*>::iterator it = gMapClientToSessions.begin(); it != gMapClientToSessions.end(); ++it) {
			if (it->second->socketDescriptor == clientSocket) {
				clientSessionToken = it->first;
				break;
			}
		}
		::gMapClientToSessions.erase(clientSessionToken);
		if (::gMapLobbyToSessions.find(clientSessionToken) != ::gMapLobbyToSessions.end()) {
			recvBuffer.resetIndicesManually();
			std::string lobby = ::gMapLobbyToSessions[clientSessionToken];
			if(lobby != ""){
				recvBuffer.writeInt32BE(clientSessionToken.length());
				recvBuffer.writeString(clientSessionToken);		
				recvBuffer.writeInt32BE(lobby.length());
				recvBuffer.writeString(lobby);
				sendMessageToAuthServer(recvBuffer, ::gVecClientSockets[0]->socketDescriptor, "", 7);
			}
		}

		recvBuffer.resetIndicesManually();
		recvBuffer.writeInt32BE(clientSessionToken.length());
		recvBuffer.writeString(clientSessionToken);
		sendMessageToAuthServer(recvBuffer, ::gVecClientSockets[0]->socketDescriptor, "", 3);

		printf("Removed Socket from FD SET...\n");
		for (int index = 0; index < ::gVecClientSockets.size(); index++) {
			if (::gVecClientSockets[index]->socketDescriptor == clientSocket) {
				::gVecClientSockets.erase(::gVecClientSockets.begin() + index);
				break;
			}
		}
		
}

std::string generateSessionToken(const int tokenLength) {
	std::string sessionToken = "";
	std::string const alphanum =
		"0123456789"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";

	std::mt19937_64 gen{ std::random_device()() };

	std::uniform_int_distribution<size_t> dist{ 0, alphanum.length() - 1 };

	std::generate_n(std::back_inserter(sessionToken), tokenLength, [&] { return alphanum[dist(gen)]; });

	return sessionToken;
}

int main(int argc, char *argv[])
{
	int opt = TRUE;
	int activity,sd;
	int max_sd;

	WSADATA wsaData;
	int iResult;

	SOCKET masterSocket = INVALID_SOCKET;
	SOCKET clientSocket = INVALID_SOCKET;


	struct addrinfo *result = NULL;
	struct addrinfo hints;

	int iSendResult;
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;

	fd_set readfds;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Create a SOCKET for connecting to server
	masterSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (masterSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	//set master socket to allow multiple connections , 
	//this is just a good habit, it will work without this 
	if (setsockopt(masterSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt,
		sizeof(opt)) < 0)
	{
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	// Setup the TCP listening socket
	iResult = bind(masterSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(masterSocket);
		WSACleanup();
		return 1;
	}

	//freeaddrinfo(result);

	//try to specify maximum of 3 pending connections for the master socket 
	/*if (listen(masterSocket, 3) < 0)
	{
		perror("listen");
		exit(EXIT_FAILURE);
	}*/

	iResult = listen(masterSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(masterSocket);
		WSACleanup();
		return 1;
	}

	//accept the incoming connection 
	//addrlen = sizeof(address);
	puts("Waiting for connections ...");

	while (TRUE)
	{
		//clear the socket set 
		FD_ZERO(&readfds);

		//add master socket to set 
		FD_SET(masterSocket, &readfds);
		max_sd = masterSocket;


		//add child sockets to set 
		for (int index = 0; index < gVecClientSockets.size(); index++)
		{
			//socket descriptor 
			sd = gVecClientSockets[index]->socketDescriptor;

			//if valid socket descriptor then add to read list 
			if (sd > 0)
				FD_SET(sd, &readfds);

			//highest file descriptor number, need it for the select function 
			if (sd > max_sd)
				max_sd = sd;
		}

		//wait for an activity on one of the sockets , timeout is NULL , 
		//so wait indefinitely 
		activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);

		if ((activity < 0) && (errno != EINTR))
		{
			printf("select error");
		}

		//If something happened on the master socket , 
		//then its an incoming connection 
		if (FD_ISSET(masterSocket, &readfds))
		{
			clientSocket = accept(masterSocket, NULL, NULL);
			if (clientSocket == INVALID_SOCKET) {
				printf("accept failed with error: %d\n", WSAGetLastError());
			}

			//inform user of socket number - used in send and receive commands 
			//printf("New connection , socket fd is %d , ip is : %s , port : %d \n" , new_socket , inet_ntoa(address.sin_addr) , ntohs
			//	(address.sin_port));f

			//add new socket to array of sockets 
			ClientModel* tmpClientModel = new ClientModel();
			tmpClientModel->socketDescriptor = clientSocket;
			std::time_t epochTime = std::time(0);
			std::stringstream ss;
			ss << epochTime;
			std::string strEpochTime = ss.str();
			tmpClientModel->ClientName = tmpClientModel->ClientName + strEpochTime;

			std::string message = "";
			if (gVecClientSockets.size() == 0) {
				gVecClientSockets.push_back(tmpClientModel);
			}
			else {
				message = generateSessionToken(10);
				gMapClientToSessions[message] = tmpClientModel;
				sendMessageToAuthServer(recvBuffer,gVecClientSockets[0]->socketDescriptor, message,0);
			}			
			clientSocket = INVALID_SOCKET;

			puts("Welcome message sent successfully");
		}


		int authServerSocket = gVecClientSockets[0]->socketDescriptor;

		for (int index = 0; index < gVecClientSockets.size(); index++)
		{
			sd = gVecClientSockets[index]->socketDescriptor;

			if (FD_ISSET(sd, &readfds))
			{
					iResult = recv(sd, recvbuf, recvbuflen, 0);
					if (iResult > 0) {
						printf("Bytes received: %d\n", iResult);
						std::string getData = "";
						recvBuffer.displayIndices();
						recvBuffer.resetIndicesManually();
						//Hack as write Index gets restored .... need to understand
						if (!gVecClientSockets[index]->isMessagePending) {
							for (int i = 0; i < 4; i++)
								getData += recvbuf[i];
							recvBuffer.writeString(getData);
							recvBuffer.displayIndices();
							gVecClientSockets[index]->recvSize = recvBuffer.readInt32BE();
							gVecClientSockets[index]->pendingMessageSize = gVecClientSockets[index]->recvSize;
						}
						recvBuffer.displayIndices();
						int i = 0;
						if (gVecClientSockets[index]->pendingMessageSize > iResult) {
							recvBuffer.resizeBuffer(gVecClientSockets[index]->recvSize);
							gVecClientSockets[index]->isMessagePending = true;
							gVecClientSockets[index]->pendingMessageSize -= iResult;
							getData = "";
						}
						else {
							gVecClientSockets[index]->isMessagePending = false;
							if(getData != "")
								i = 4;
						}

						for (i; i < iResult; i++)
							getData += recvbuf[i];
						
						gVecClientSockets[index]->partialMessage += getData;
						if(gVecClientSockets[index]->isMessagePending == false){
							recvBuffer.writeString(gVecClientSockets[index]->partialMessage);
							recvBuffer.displayIndices();
							gVecClientSockets[index]->isMessagePending = false;
							gVecClientSockets[index]->recvSize = 0;
							gVecClientSockets[index]->pendingMessageSize = 0;
							processMessage(recvBuffer,gVecClientSockets[index], gVecClientSockets[index]->partialMessage);
							gVecClientSockets[index]->partialMessage = "";
						}
					}
					else if (iResult == 0) {
						closeClientConnection(sd, readfds);
					}
					else {
						printf("recv failed with error: %d\n", WSAGetLastError());
						closeClientConnection(sd, readfds);
						//return 1;
					}
			}
		}
	}

	WSACleanup();
	return 0;
}