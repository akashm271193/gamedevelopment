#include "cMapLoader.h"
#include "FileUtilities.h"
#include <array>
/*	
	Constructor whic takes string as an input and then calls loadMap from file.
*/
cMapLoader::cMapLoader(std::string filePath)
{
	this->map = new cMap();
	loadMapFromFile(filePath);
}


/*
	Parameterless Constructor
*/
cMapLoader::cMapLoader()
{

}

/*
	Parameterless Destructor
*/
cMapLoader::~cMapLoader()
{
}

/*
	Function to load config file from map.txt
*/
void cMapLoader::loadMapFromFile(std::string filePath){
	rapidjson::Document doc = readConfigFromJsonFile(filePath);
	const rapidjson::Value& mapLayout = doc["MapLayout"];

	this->map->setMapStartPoint(mapLayout["playerStartCell"].GetString());
	this->map->setMapEndPoint(mapLayout["playerEndCell"].GetString());

	this->map->setMapRows(mapLayout["mapRows"].GetInt());
	this->map->setMapColumns(mapLayout["mapColumns"].GetInt());
	this->map->mapScale = mapLayout["mapScale"].GetFloat();
	std::string mapLayoutFile = mapLayout["mapLayout"].GetString();
	const rapidjson::Value &specialChars = mapLayout["mapSpecialChars"];

	std::fstream infoFile(mapLayoutFile.c_str());
	if (!infoFile.is_open()) {
		std::cout << "Error in reading the config file";
		return;
	}
	else {
		std::cout << "File read. Storing config now !" << std::endl;
		std::string strConfigValue;
		infoFile >> strConfigValue;
				if (strConfigValue == "Map:") {
				std::vector<cMapCell*> mapCells;
				this->map->getMapCells(mapCells);
				for (int i = 0; i < this->map->getMapRows(); i++) {
					for (int j = 0; j < this->map->getMapColumns(); j++) {
						cMapCell* tmpCell = new cMapCell();
						tmpCell->rowId = i;
						tmpCell->columnId = j;
						infoFile >> strConfigValue;
						if (this->checkIfSpecialCell(strConfigValue, specialChars)) {
							std::cout << " Special Charater " << strConfigValue << std::endl;
							tmpCell->isWalkable = true;
							loadSpecialCharIntoCell(tmpCell, strConfigValue);
						}
						else 
							tmpCell->isWalkable = std::stoi(strConfigValue);
						mapCells.push_back(tmpCell);
					}
				}
				this->map->setMapCells(mapCells);
				this->map->setupStartAndEndPoints();
			}
			infoFile >> strConfigValue;
			std::cout << "Config Value :" + strConfigValue << std::endl;
	}
}

/*
	Function to return the map object created when 
	the config was loaded.
*/
cMap* cMapLoader::getMap() {
	return this->map;
}

bool cMapLoader::checkIfSpecialCell(std::string cell, const rapidjson::Value &arrayChars) {
	for (int index = 0; index < arrayChars.Size(); index++) {
		std::string tmpElement = arrayChars[index].GetString();
		if ((tmpElement.find(cell[0]) != std::string::npos)) {
			return true;
		}
	}
	return false;
}

void  cMapLoader::loadSpecialCharIntoCell(cMapCell* tmpCell, std::string cellValue) {
	if (cellValue == "B") {
		tmpCell->hasObstacle = true;
		tmpCell->obstacleType = libPhysics::OBJ_WRECKING_BALL;
	}
	else if (cellValue == "F") {
		tmpCell->hasObstacle = true;
		tmpCell->obstacleType = libPhysics::OBJ_FIRE;
	}
	else if (cellValue == "W") {
		tmpCell->isWalkable = false;
		tmpCell->hasObstacle = true;
		tmpCell->obstacleType = libPhysics::OBJ_SLIDING_WALL;
	}
	else if (cellValue == "S") {
		tmpCell->hasObstacle = true;
		tmpCell->obstacleType = libPhysics::OBJ_SPIKES;
	}
	else if (cellValue == "T") {
		tmpCell->hasObstacle = true;
		tmpCell->obstacleType = libPhysics::OBJ_TREASURE;
	}
	else if (cellValue == "R") {
		tmpCell->hasObstacle = true;
		tmpCell->obstacleType = libPhysics::RESPAWN_PT;
	}
	else if (cellValue == "Z") {
		tmpCell->hasObstacle = true;
		tmpCell->obstacleType = libPhysics::OBJ_ANGRY_AI;
	}
	else if (cellValue == "M") {
		tmpCell->hasObstacle = true;
		tmpCell->obstacleType = libPhysics::OBJ_CURIOUS_AI;
	}
	else if (cellValue == "N") {
		tmpCell->hasObstacle = true;
		tmpCell->obstacleType = libPhysics::OBJ_FOLLOWER_AI;
	}


};
