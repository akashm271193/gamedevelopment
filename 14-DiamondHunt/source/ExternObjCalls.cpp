#include "ExternObjCalls.h"
#include "cPhysicsFactory.h"

libPhysics::iPhysicsFactory* gPhysicsFactory = 0;
libPhysics::iPhysicsWorld* gPhysicsWorld = 0;

bool InitPhysics()
{
	gPhysicsFactory = new libPhysics::cPhysicsFactory();
	gPhysicsWorld = new libPhysics::cPhysicsWorld();
	return true;
}