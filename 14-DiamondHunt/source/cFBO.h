#pragma once

#include <glad\glad.h>
#include <GLFW\glfw3.h>


class cFBO
{
public:
	cFBO(int numBuffers, int width, int height);
	void Bind();
	void Unbind();
	void Draw();
	void Reset(int width, int height);


	unsigned int* bufferArray;
	unsigned int depthBuffer;
	unsigned int screenWidth;
	unsigned int screenHeight;
private:
	int numBuffers;
	unsigned int fbo;
	unsigned int vao;
	unsigned int vbo;
	bool fboInit();
	void init();
};