#include "cAABBBroadPhase.h"


#include <algorithm>			// for the std::copy

cAABBBroadPhase::cAABBBroadPhase()
{
	this->pDebugRenderer = NULL;
	return;
}

cAABBBroadPhase::~cAABBBroadPhase()
{
	return;
}

bool cAABBBroadPhase::genAABBGridFromMesh(cMesh &theMesh)
{
	// Assume that the debug renderer is valid
	//	this->pDebugRenderer->addTriangle( drTri(glm::vec3(0.0f), glm::vec3(1.0f), glm::vec3(2.0f), glm::vec3(1.0f)) );
	//float tmpMaxExtent = (theMesh.maxExtentXYZ.x < theMesh.maxExtentXYZ.z) ? theMesh.maxExtentXYZ.z : theMesh.maxExtentXYZ.x;
	cAABBv2* tempAABB = new cAABBv2(theMesh.minXYZ, theMesh.maxExtent/2);
	unsigned long long tempId = tempAABB->getID();
	this->mapIDToAABB.insert(std::pair<unsigned long long, cAABBv2*>(tempId, tempAABB));
	return true;
}

bool cAABBBroadPhase::areThereTrianglesInAABB(glm::vec3 testPoint, cAABBv2* &ptheAABB)
{
	// Generate an AABB ID for this test location
	unsigned long long testPointAABBID = cAABBv2::calculateID(testPoint);

	// Find an AABB IF it exists
	std::map< unsigned long long, cAABBv2* >::iterator itAABB = 
				this->mapIDToAABB.find( testPointAABBID );

	// Does it exist? 
	if (itAABB == this->mapIDToAABB.end())
	{
		// NOPE. There are NO AABBs at that location
		return false;
	}
	// Otherwise there IS an AABB
	ptheAABB = itAABB->second;
	return true;
}