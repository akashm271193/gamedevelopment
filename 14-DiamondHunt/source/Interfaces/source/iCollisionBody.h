#pragma once
#include <glm\game_math.h>
#include "sCollisionBodyDesc.h"

namespace libPhysics
{
	class iCollisionBody
	{
	public:
		virtual ~iCollisionBody() {}

		virtual void updateCollisionBody(sCollisionBodyDesc &bodyDesc) = 0;
		virtual void getCollisionBodyStateFromPhysics(sCollisionBodyDesc &bodyDesc) = 0;

		//virtual void GetTransform(glm::mat4& transformOut) = 0;
		//virtual void GetPosition(glm::vec3& positionOut) = 0;
		//virtual void GetRotation(glm::vec3& rotationOut) = 0;
		//virtual void ApplyForce(glm::vec3& forceOut) = 0;

		//virtual iShape* GetShape() = 0;
	};
}