#ifndef _cGameObject_HP_
#define _cGameObject_HP_

#include <glm/vec3.hpp> 
#include <glm/vec4.hpp> 
#include <string>
#include <glad/glad.h>

class cGameObject
{
public:
	cGameObject();
	~cGameObject();
	std::string meshName;
	glm::vec3 position;
	glm::vec3 orientation;
	glm::vec3 orientation2;
	glm::vec4 diffuseColour;
	GLenum objectLayoutType;
	float scale;
};

#endif