#pragma once
#include "cMesh.h"
#include "cSkinnedMesh.h"
#include "sVertexAllWithBones.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "stb_image.h"
unsigned int TextureFromFile(const char *path, const std::string &directory, bool gamma = false);

class cModel
{
public:
	/*  Functions   */
	cModel(char *path,bool loadColour, bool isSkinnedMesh = false);
	cModel();
	~cModel();
	void translateModel(glm::vec3 newPos);
	void DrawModel(GLint shaderId,glm::vec3 colour);
	void getModelExtents();
	glm::vec3 minXYZ;
	glm::vec3 maxXYZ;
	glm::vec3 maxExtentXYZ;
	std::vector<cMesh*> getAllMeshes();
	float maxExtent;
	float scaleForUnitBBox;
	void addSeparateTexturesToModel(std::string fileName, std::string directory, std::string typeName, cMesh* mesh);

	//Skinned Mesh
	void loadSkinnedModel(std::string path);
	void LoadBones(const aiMesh* Mesh, std::vector<sVertexBoneData> &Bones);
	void loadAnimation(std::string path);
	void BoneTransform(float TimeInSeconds, std::string animationName,
		std::vector<glm::mat4> &FinalTransformation,
		std::vector<glm::mat4> &Globals,
		std::vector<glm::mat4> &Offsets);
	//looks into animation map and returns total time
	float FindAnimationTotalTime(std::string animationName);
	float GetDuration(void);
	bool isSkinnedMesh;
	void blurIndices();


private:
	/*  Model Data  */
	Assimp::Importer import;
	const aiScene *scene;
	
	//Mesh
	std::vector<cMesh*> meshes;
	std::string directory;
	std::vector<Texture> textures_loaded;
	//Skinned Mesh
	std::vector<cSkinnedMesh*> skinnedMeshes;
	std::string skinnedDirectory;
	std::vector<Texture> skinnedTextures_loaded;
	std::map<std::string, unsigned int> m_mapBoneNameToBoneIndex;
	std::vector<sBoneInfo> mBoneInfo;
	unsigned int mNumBones;
	//animations map
	std::map< std::string, const aiScene*> mapAnimationNameToScene;

	unsigned int GetNums(void) const { return this->mNumBones; }

	/*  Functions   */
	//Mesh
	void loadModel(std::string path);
	cMesh* processMesh(aiMesh *mesh, const aiScene *scene);

	////Skinned Mesh
	//void loadSkinnedModel(std::string path); 
	//void LoadBones(const aiMesh* Mesh, std::vector<sVertexBoneData> &Bones);
	//void loadAnimation(std::string path);
	cSkinnedMesh* processSkinnedMesh(aiMesh *mesh, const aiScene *scene);
	

	std::vector<sVertexBoneData> vecVertexBoneData;
	glm::mat4 mGlobalInverseTransformation;

	bool Initialize(unsigned int meshIndex);

	void CalcInterpolatedRotation(float AnimationTime, const aiNodeAnim* pNodeAnim, aiQuaternion& out);
	void CalcInterpolatedPosition(float AnimationTime, const aiNodeAnim* pNodeAnim, aiVector3D& out);
	void CalcInterpolatedScaling(float AnimationTime, const aiNodeAnim* pNodeAnim, aiVector3D& out);

	void CalcGLMInterpolatedRotation(float AnimationTime, const aiNodeAnim* pNodeAnim, glm::quat& out);
	void CalcGLMInterpolatedPosition(float AnimationTime, const aiNodeAnim* pNodeAnim, glm::vec3& out);
	void CalcGLMInterpolatedScaling(float AnimationTime, const aiNodeAnim* pNodeAnim, glm::vec3& out);

	unsigned int FindRotation(float AnimationTime, const aiNodeAnim* pNodeAnim);
	unsigned int FindPosition(float AnimationTime, const aiNodeAnim* pNodeAnim);
	unsigned int FindScaling(float AnimationTime, const aiNodeAnim* pNodeAnim);

	const aiNodeAnim* FindNodeAnimationChannel(const aiAnimation* pAnimation, aiString nodeOrBoneName);
	
	void ReadNodeHeirarchy(float AnimationTime, std::string animationName,
						const aiNode* pNode, const glm::mat4 &parentTransformMatrix);

	


	void processNode(aiNode *node, const aiScene *scene);
	std::vector<Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type,std::string typeName);
	bool toLoadColour;



};