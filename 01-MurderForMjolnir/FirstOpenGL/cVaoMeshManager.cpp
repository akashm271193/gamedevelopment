#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "cVaoMeshManager.h"
#include "cMesh.h"
class cVertex
{
public:
	float x, y, z;		// Position (vec2)	float x, y;	
	float r, g, b;		// Colour (vec3)
};

cVAOMeshManager::cVAOMeshManager() {

	return;
}

cVAOMeshManager::~cVAOMeshManager() {

	return;
}

bool cVAOMeshManager::LoadMeshIntoVAO(cMesh &theMesh, int shaderId) {

	sVAOInfo theVAOInfo;

	glGenVertexArrays(1,&(theVAOInfo.vaoId));
	glBindVertexArray(theVAOInfo.vaoId);

	glGenBuffers(1, &(theVAOInfo.vertexBufferId));
	glBindBuffer(GL_ARRAY_BUFFER, theVAOInfo.vertexBufferId);
	
	cVertex* pVertices = new cVertex[theMesh.numberOfVertices];

	for (int index = 0; index < theMesh.numberOfVertices; index++)
	{
		pVertices[index].x = theMesh.pVertices[index].x;
		pVertices[index].y = theMesh.pVertices[index].y;
		pVertices[index].z = theMesh.pVertices[index].z;	// ADDED

		pVertices[index].r = theMesh.pVertices[index].r;
		pVertices[index].g = theMesh.pVertices[index].g;
		pVertices[index].b = theMesh.pVertices[index].b;
	}

	// NOTE: OpenGL error checks have been omitted for brevity
	int sizeOfVertexArray = sizeof(cVertex_xyz_rgb) * theMesh.numberOfVertices;

	glBufferData(GL_ARRAY_BUFFER, sizeOfVertexArray, pVertices, GL_STATIC_DRAW);

	delete[] pVertices;

	glGenBuffers(1, &(theVAOInfo.indexBufferId));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, theVAOInfo.indexBufferId);

	int numberOfIndices = theMesh.numberOfTriangles * 3;
	unsigned int* indexArray = new unsigned int[numberOfIndices];

	int triIndex = 0;
	int indexIndex = 0;
	for (; triIndex < theMesh.numberOfTriangles; triIndex++, indexIndex += 3) {
		indexArray[indexIndex + 0] = theMesh.pTriangles[triIndex].vertex_ID_0;
		indexArray[indexIndex + 1] = theMesh.pTriangles[triIndex].vertex_ID_1;
		indexArray[indexIndex + 2] = theMesh.pTriangles[triIndex].vertex_ID_2;
	}

	int sizeOfIndexArray = sizeof(unsigned int) * numberOfIndices;

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeOfIndexArray, indexArray, GL_STATIC_DRAW);

	delete[] indexArray;

	GLuint vpos_location = glGetAttribLocation(shaderId, "vPos");	// 6
	GLuint vcol_location = glGetAttribLocation(shaderId, "vCol");	// 13

	glEnableVertexAttribArray(vpos_location);
	const int offsetOf_x_into_cVertex = 0;
	glVertexAttribPointer(vpos_location,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(float) * 6,
		(void*)(sizeof(float) * offsetOf_x_into_cVertex));

	glEnableVertexAttribArray(vcol_location);
	const int offsetOf_r_into_cVertex = 3;
	glVertexAttribPointer(vcol_location,
		3, GL_FLOAT,
		GL_FALSE,
		sizeof(float) * 6,
		(void*)(sizeof(float) * offsetOf_r_into_cVertex));

	theVAOInfo.numberOfVertices = theMesh.numberOfVertices;
	theVAOInfo.numberOfTriangles = theMesh.numberOfTriangles;
	theVAOInfo.numberOfIndices = theMesh.numberOfTriangles*3;
	theVAOInfo.friendlyName = theMesh.name;
	theVAOInfo.shaderId = shaderId;

	theVAOInfo.scaleForUnitBBox = theMesh.scaleForUnitBBox;

	this->m_mapNameToVAO[theMesh.name] = theVAOInfo;
	
	//Critical ..... Always unbind before returning
	glBindVertexArray(0);

	glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glad_glDisableVertexAttribArray(vcol_location);
	glad_glDisableVertexAttribArray(vpos_location);
	return true;
}

bool cVAOMeshManager::LookUpVAOByName(std::string meshName, sVAOInfo &theVAOInfo) {
	
	std::map<std::string, sVAOInfo> ::iterator itVao = m_mapNameToVAO.find(meshName);

	if (itVao == m_mapNameToVAO.end()) {
		return false;
	}

	theVAOInfo = itVao->second;
}