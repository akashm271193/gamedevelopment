#ifndef _WORLD_HG_
#define _WORLD_HG_

#include <vector>
#include <glm\game_math.h>
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"	
#include "cCollisionBody.h"
#include "iPhysicsWorld.h"
#include "cBulletDebugDraw.h"
#include "cCollision.h"
namespace libPhysics {
class cPhysicsWorld : public iPhysicsWorld
{
	public:
		cPhysicsWorld();
		virtual void stepSimulation(float deltaTime);
		void UpdateBoundingBox(iCollisionBody* bBox);
		btDiscreteDynamicsWorld * world;
		//std::vector<btRigidBody*> vecBodies;
		btAlignedObjectArray<btRigidBody*> arrBoundingBoxes;
		btAlignedObjectArray<btCollisionShape*> mPhysicsShapes;
		virtual void AddBoundingBox(iCollisionBody* bBox);
		virtual  void RemoveBoundingBox(iCollisionBody* bBodyDesc);
		void RemoveAllBoundingBoxes();
		btAlignedObjectArray<cCollision*> collisions;
		virtual void addDebugRender(iDebugRenderer* renderer);
		virtual void drawDebugWorld();
		virtual bool checkIfObjectAlreadyPresent(iCollisionBody* object);

		/*void sliderConstraint(btRigidBody *body, btTransform trans);
		void sliderConstraint(btRigidBody *body1, btRigidBody *body2, btTransform trans1, btTransform trans2);
		void p2pConstraint(btRigidBody *body, btVector3 trans);
		void p2pConstraint(btRigidBody *body1, btRigidBody *body2, btVector3 trans1, btVector3 trans2);
		void hingeConstraint(btRigidBody* body, btVector3 pivot, btVector3 axis);
		void hingeConstraint(btRigidBody* body1, btVector3 pivot1, btVector3 axis1, btRigidBody* body2, btVector3 pivot2, btVector3 axis2);
		void dofConstraint(btRigidBody* body, btTransform trans);
		void dofConstraint(btRigidBody* body1, btRigidBody* body2, btTransform trans1, btTransform trans2);*/
		virtual void setViewAndProjectionMatrix(glm::mat4 viewMat, glm::mat4 projectionMat, int shadId);
	private:
		btDefaultCollisionConfiguration * config;
		btCollisionDispatcher * dispatcher;
		btBroadphaseInterface * broadphase;
		btSequentialImpulseConstraintSolver * constraintSolver;
		glm::mat4 viewMatrix;
		glm::mat4 projectionMatrix;
		int shaderId;
		cBulletDebugDraw* mDebugDraw;
	};
	void myTickCallback(btDynamicsWorld* dynamicsWorld, btScalar timeStep);
}
#endif // !_WORLD_HG_
