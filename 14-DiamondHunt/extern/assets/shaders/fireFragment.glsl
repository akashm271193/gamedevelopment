#version 420
struct Material {
    sampler2D texture_diffuse0;
	sampler2D texture_diffuse1;
	sampler2D texture_diffuse2;
	sampler2D texture_diffuse3;
	sampler2D texture_diffuse4;
	sampler2D texture_specular0;
	sampler2D texture_specular1;
	float texture_diffuseBlendRatio[5];
	float texture_specularBlendRatio[2];
    float shininess;
}; 

uniform Material material;
uniform bool isObjColour;
uniform vec3 objColour;

out vec4 FragColor;
out vec4 FragNormal;
out vec4 FBOout_colour;			// GL_COLOR_ATTACHMENT0
out vec4 FBOout_normal;			// GL_COLOR_ATTACHMENT1
out vec4 FBOout_vertexWorldPos;	// GL_COLOR_ATTACHMENT2

//in vec3 objWorldNormal;
in vec3 objWorldPos;
in vec2 objTexture;
uniform float border_width;
uniform float aspect;  // ratio of width to height
uniform int diffuseTextures;
uniform int specularTextures;

//OBJECT MATERIAL VALUE
uniform vec4 materialDiffuse;	
uniform bool bUseTextureAsDiffuse;
uniform float ambientToDiffuseRatio;
uniform vec4 materialSpecular;  // rgb = colour of HIGHLIGHT only


//uniform int renderPassNumber;
uniform sampler2D texFBOColour2D;
uniform sampler2D texFBONormal2D;
uniform sampler2D texFBOVertexWorldPos2D;
uniform sampler2D fullRenderedImage2D;
uniform float screenWidth;
uniform float screenHeight;




void main()
{ 

	float filterLimit = 0.0f;	
	float filterStep = 0.001f;
	int count = 0;
	
	vec4 texResult = vec4(0.0,0.0,0.0,1.0);
	vec3 outColour = vec3(0.0f, 0.0f, 0.0f);
	
	float xMin = -( filterLimit * filterStep );
	float xMax =  (filterLimit+1) * filterStep;
	float yMin = -( filterLimit * filterStep );
	float yMax = (filterLimit+1) * filterStep;
	vec2 textCoords = vec2( gl_FragCoord.x / screenWidth, gl_FragCoord.y / screenHeight );

	for ( float xOff = xMin; xOff < xMax; xOff += filterStep )
	{
		for ( float yOff = yMin; yOff < yMax; yOff += filterStep )
		{
			vec2 sampOffset = vec2(xOff, yOff);	
			texResult += (texture(material.texture_diffuse0, textCoords + sampOffset) * material.texture_diffuseBlendRatio[0]);						
			count++;
		}//for ( float yOff
	}
	
	FragColor = texResult;
	
	
}