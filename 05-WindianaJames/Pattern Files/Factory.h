#ifndef _FACTORY_HG_
#define _FACTORY_HG_

#include <string>
using namespace std;

class Entity
{
public:
	Entity() {}
	virtual ~Entity() {}
};

class Factory
{
public:
	virtual Entity* CreateObjectOfType(string type) = 0;
};

class MonsterFactory : public Factory
{
public:
	MonsterFactory();
	virtual Entity* CreateObjectOfType(string type);

};


#endif