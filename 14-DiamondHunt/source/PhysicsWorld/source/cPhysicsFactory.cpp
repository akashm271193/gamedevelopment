#include "cPhysicsFactory.h"
#include "cCollisionBody.h"

namespace libPhysics
{
	cPhysicsFactory::~cPhysicsFactory() 
	{
	}

	iPhysicsWorld* cPhysicsFactory::CreateWorld()
	{
		return new cPhysicsWorld();
	}

	iCollisionBody* cPhysicsFactory::CreateCollisionBody(const sCollisionBodyDesc &bodyDesc)
	{
		return new cCollisionBody(bodyDesc);
	}
}