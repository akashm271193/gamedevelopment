#include "cBall.h"

cBall::cBall(std::string modelDir)
{
	this->Model = new cModel(modelDir.c_str(),false);

	this->Position = glm::vec3(0.0f);
	this->Scale = glm::vec3(1.0f);
	this->OrientationQuat = glm::quat(0.0f, 0.0f, 0.0f, 1.0f);
	this->OrientationEuler = glm::vec3(0.0f);
	this->Velocity = glm::vec3(0.0f);
	this->Mass = 1.0f;
	RBDesc.Position = this->Position;
	RBDesc.Rotation = this->OrientationEuler;
	RBDesc.Velocity = this->Velocity;
	RBDesc.Mass = this->Mass;


	this->Shape = gPhysicsFactory->CreateSphere(1.0f);
	this->RigidBody = gPhysicsFactory->CreateRigidBody(this->RBDesc, this->Shape);
	gPhysicsWorld->AddRigidBody(this->RigidBody);
	return;
}
cBall::cBall(std::string modelDir, glm::vec3 position, glm::vec3 scale, glm::vec3 orientationEuler)
{
	this->Model = new cModel(modelDir.c_str(),false);

	this->Position = position;
	this->Scale = scale;
	this->OrientationQuat = glm::quat(orientationEuler);
	this->OrientationEuler = orientationEuler;
	this->Velocity = glm::vec3(0.0f);
	this->Mass = 1.0f;

	RBDesc.Position = this->Position;
	RBDesc.Rotation = this->OrientationEuler;
	RBDesc.Velocity = this->Velocity;
	RBDesc.Mass = this->Mass;

	this->Shape = gPhysicsFactory->CreateSphere(1.0f);
	this->RigidBody = gPhysicsFactory->CreateRigidBody(this->RBDesc, this->Shape);
	gPhysicsWorld->AddRigidBody(this->RigidBody);
	return;
}
cBall::cBall(std::string modelDir, glm::vec3 position, glm::vec3 scale, glm::quat orientationQuat)
{
	this->Model = new cModel(modelDir.c_str(),false);

	this->Position = position;
	this->Scale = scale;
	this->OrientationQuat = orientationQuat;
	this->OrientationEuler = glm::eulerAngles(orientationQuat);
	this->Velocity = glm::vec3(0.0f);
	this->Mass = 1.0f;

	//RBDesc.Position = this->Position;
	//RBDesc.Rotation = this->OrientationEuler;
	//RBDesc.Velocity = this->Velocity;
	//RBDesc.Mass = this->Mass;

	//switch (this->ObjectType)
	//{
	//case eObjectType::OBJECT_SPHERE:
	//	this->Shape = gPhysicsFactory->CreateSphere(1.0f);
	//	this->RigidBody = gPhysicsFactory->CreateRigidBody(this->RBDesc, this->Shape);
	//	break;
	//default:
	//	break;
	//}
}

cBall::cBall(std::string modelDir, glm::vec3 position, glm::vec3 scale, glm::vec3 orientation, glm::vec3 velocity, float mass,std::map<std::string,cModel*> &mapLoadedModels)
{
	this->Model = new cModel(modelDir.c_str(),false);
	mapLoadedModels.insert(std::pair<std::string, cModel*>("ball", this->Model));
	this->Position = position;
	this->Scale = scale;
	this->OrientationQuat = glm::quat(orientation);
	this->OrientationEuler = orientation;
	this->Velocity = velocity;
	this->Acceleration = glm::vec3(0.0f);
	this->Mass = scale.x;

	this->isCurrent = false;

	RBDesc.Position = this->Position;
	RBDesc.Rotation = this->OrientationEuler;
	RBDesc.Velocity = this->Velocity;
	RBDesc.Mass = this->Mass;

	this->Shape = gPhysicsFactory->CreateSphere(scale.x/2.0f);
	this->RigidBody = gPhysicsFactory->CreateRigidBody(this->RBDesc, this->Shape);
	gPhysicsWorld->AddRigidBody(this->RigidBody);
	return;
}

cBall::cBall(cModel* model, glm::vec3 position, glm::vec3 scale, glm::vec3 orientation, glm::vec3 velocity, float mass)
{
	this->Model = model;

	this->Position = position;
	this->Scale = scale;
	this->OrientationQuat = glm::quat(orientation);
	this->OrientationEuler = orientation;
	this->Velocity = velocity;
	this->Acceleration = glm::vec3(0.0f);
	this->Mass = scale.x;

	this->isCurrent = false;

	RBDesc.Position = this->Position;
	RBDesc.Rotation = this->OrientationEuler;
	RBDesc.Velocity = this->Velocity;
	RBDesc.Mass = this->Mass;

	this->Shape = gPhysicsFactory->CreateSphere(scale.x / 2.0f);
	this->RigidBody = gPhysicsFactory->CreateRigidBody(this->RBDesc, this->Shape);
	gPhysicsWorld->AddRigidBody(this->RigidBody);
	return;
}

void cBall::Draw(int shaderId)
{
	this->Model->DrawModel(shaderId);
}
void cBall::SetMass(float mass)
{
	this->Mass = mass;
	this->RBDesc.Mass = mass;
}
void cBall::SetVelocity(glm::vec3 vel)
{
	this->Velocity = vel;
	this->RBDesc.Velocity = vel;
}