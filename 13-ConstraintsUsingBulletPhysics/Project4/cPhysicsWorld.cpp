#include "cPhysicsWorld.h"

cPhysicsWorld::cPhysicsWorld()
{
	this->config = new btDefaultCollisionConfiguration();
	this->dispatcher = new btCollisionDispatcher(config);
	this->broadphase = new btDbvtBroadphase();
	this->constraintSolver = new btSequentialImpulseConstraintSolver();
	this->world = new btDiscreteDynamicsWorld(dispatcher, broadphase, constraintSolver, config);
	world->setGravity(btVector3(0.0f, -9.8f, 0.0f));
}
void cPhysicsWorld::UpdateBodies()
{
	for (int i = 0; i != vecBodies.size(); i++)
	{
		world->addRigidBody(vecBodies[i]);
	}
}

void cPhysicsWorld::sliderConstraint(btRigidBody *body, btTransform trans)
{
	btSliderConstraint* slider = new btSliderConstraint(*body, trans, false);
	world->addConstraint(slider);
}

void cPhysicsWorld::sliderConstraint(btRigidBody *body1, btRigidBody *body2, btTransform trans1, btTransform trans2)
{
	btSliderConstraint* slider = new btSliderConstraint(*body1,*body2, trans1,trans2, false);
	world->addConstraint(slider);
}

void cPhysicsWorld::p2pConstraint(btRigidBody *body, btVector3 trans)
{
	btPoint2PointConstraint* p2p = new btPoint2PointConstraint(*body, trans);
	world->addConstraint(p2p);
}

void cPhysicsWorld::p2pConstraint(btRigidBody *body1 , btRigidBody *body2, btVector3 trans1, btVector3 trans2)
{
	btPoint2PointConstraint* p2p = new btPoint2PointConstraint(*body1, *body2,trans1, trans2);
	world->addConstraint(p2p);
}

void cPhysicsWorld::hingeConstraint(btRigidBody* body, btVector3 pivot, btVector3 axis)
{
	btHingeConstraint* hinge = new btHingeConstraint(*body, pivot, axis, true);
	world->addConstraint(hinge);
}

void cPhysicsWorld::hingeConstraint(btRigidBody* body1, btVector3 pivot1, btVector3 axis1, btRigidBody* body2, btVector3 pivot2, btVector3 axis2)
{
	btHingeConstraint* hinge = new btHingeConstraint(*body1, *body2, pivot1, pivot2,axis1, axis2, true);
	world->addConstraint(hinge);
}
void cPhysicsWorld::dofConstraint(btRigidBody* body, btTransform trans)
{
	btGeneric6DofConstraint* dof = new btGeneric6DofConstraint(*body, trans, false);

	world->addConstraint(dof);
}
void cPhysicsWorld::dofConstraint(btRigidBody* body1, btRigidBody* body2, btTransform trans1, btTransform trans2)
{
	btGeneric6DofConstraint* dof = new btGeneric6DofConstraint(*body1, *body2, trans1, trans2, false);
	world->addConstraint(dof);
}
void cPhysicsWorld::stepSimulation()
{
	this->world->stepSimulation(1.0f / 60.0f, 10);
}