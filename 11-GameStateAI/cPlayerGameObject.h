#pragma once
#include "cGameObject.h"
class cPlayerGameObject : public cGameObject
{
public:
	cPlayerGameObject();
	float playerHealth;
	bool isPlayerHealthLow();
};

