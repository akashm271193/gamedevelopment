#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <fstream>
#include <iostream>			// C++ cin, cout, etc.
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
#include <string>
#include <glm/vec3.hpp> 
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <map>
#include "ModelUtilities.h"
#include "cMesh.h"
#include "cShaderManager.h"
#include "cGameObject.h"
#include "Utilities.h"
#include "cVaoMeshManager.h"


int maxGameObjects = 0;                                          //Max number of Game objects the scene can have
cShaderManager* gpShaderManager;								 //Global pointer for the Shader Manager
glm::vec3 gCameraXYZ = glm::vec3(0.0f, 0.0f, 0.0f);				//Vector position of the camera
glm::vec3 gCameraTarget = glm::vec3(0.0f, 0.0f, 0.0f);			 //Vector position of the target of the camera
std::vector<cGameObject*> gVecGameObjects;						//Global Vector object for storing Game Objects
cVAOMeshManager* gpVAOMeshManager = 0;							//Global pointer for the VOA MEsh Manager
std::string meshBasePath = "";
std::string shaderBasePath = "";
float sizeOfWorld = 0.0f;
std::string layoutFormat = "";
std::string gameObjectsFile = "";
std::string gCameraCoordinates = "";
GLenum gObjectLayoutType = GL_LINE;
float gCameraSpeed = 0.1f;


static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);

	if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
	{
		::gVecGameObjects[1]->position.x += 0.1f;
	}
	if (key == GLFW_KEY_BACKSPACE && action == GLFW_PRESS)
	{
		::gVecGameObjects[1]->position.x -= 0.1f;
	}


	switch (key)
	{
		case GLFW_KEY_A:		// Left
			gCameraXYZ.x -= gCameraSpeed;
			// If you want to move the target as well , then you can do as below
			//g_cameraTarget.x -= CAMERASPEED;
			break;
		case GLFW_KEY_D:		// Right
			gCameraXYZ.x += gCameraSpeed;
			// If you want to move  the target as well , then you can do as below
			//g_cameraTarget.x += CAMERASPEED;
			break;
		case GLFW_KEY_W:		// Forward (along z)
			gCameraXYZ.z -= gCameraSpeed;
			break;
		case GLFW_KEY_S:		// Backwards (along z)
			gCameraXYZ.z += gCameraSpeed;
			break;
		case GLFW_KEY_Q:		// "Down" (along y axis)
			gCameraXYZ.y -= gCameraSpeed;
			break;
		case GLFW_KEY_E:		// "Up" (along y axis)
			gCameraXYZ.y += gCameraSpeed;
			break;
	}
}

float* parseStringToArray(std::string stringToParse) {
	float *vecValues = new float[3];
	std::string delimiter = ",";

	size_t last = 0; 
	size_t next = 0; 
	int index = 0;
	std::string token;
	while (index<3)
	{
		next = stringToParse.find(delimiter, last);
		token = stringToParse.substr(last, next - last);
		vecValues[index] = strtof(token.c_str(), 0);
		last = next + 1;
		index++;
	}
	return vecValues;

}

void loadCameraCoordinates(glm::vec3 &cameraXYZ,std::string cameraCoordinates) {
	float* vecValues = parseStringToArray(cameraCoordinates);
	cameraXYZ.x = vecValues[0];
	cameraXYZ.y = vecValues[1];
	cameraXYZ.z = vecValues[2];
}

bool loadObjectIntoVaoAndGOVec(std::map<std::string,std::string> mapGameObject, int customShaderId) {
	cMesh testMesh;
	testMesh.name = mapGameObject["gameObject"];
	int numOfObjects = atoi(mapGameObject["numOfObjects"].c_str());
	if (!LoadPlyFileIntoMesh(meshBasePath + mapGameObject["gameObject"] + ".ply", testMesh))
	{
		std::cout << "Didn't load model" << std::endl;
		return false;
	}
	if (!::gpVAOMeshManager->LoadMeshIntoVAO(testMesh, customShaderId))
	{
		std::cout << "Could not load mesh into VAO" << std::endl;
		return false;
	}

	

	for (int i = 0; i < numOfObjects; i++) {
		cGameObject* p_tmpGO = new cGameObject();
		p_tmpGO->meshName = mapGameObject["gameObject"];

		float* vecValues = parseStringToArray(mapGameObject["position"]);
		p_tmpGO->position.x = vecValues[0];
		p_tmpGO->position.y = vecValues[1];
		p_tmpGO->position.z = vecValues[2];

		p_tmpGO->scale = strtof(mapGameObject["scale"].c_str(),0);

		vecValues = parseStringToArray(mapGameObject["colour"]);
		p_tmpGO->diffuseColour.r = vecValues[0];
		p_tmpGO->diffuseColour.g = vecValues[1];
		p_tmpGO->diffuseColour.b = vecValues[2];
		
		vecValues = parseStringToArray(mapGameObject["preRotation"]);
		p_tmpGO->orientation.x = vecValues[0];
		p_tmpGO->orientation.y = vecValues[1];
		p_tmpGO->orientation.z = vecValues[2];

		vecValues = parseStringToArray(mapGameObject["postRotation"]);
		p_tmpGO->orientation2.x = vecValues[0];
		p_tmpGO->orientation2.y = vecValues[1];
		p_tmpGO->orientation2.z = vecValues[2];
		

		if (mapGameObject["objectLayoutType"] == "POINT") {
			p_tmpGO->objectLayoutType = GL_POINT;
		}
		else if(mapGameObject["objectLayoutType"] == "LINE") {
			p_tmpGO->objectLayoutType = GL_LINE;
		}
		else if (mapGameObject["objectLayoutType"] == "FILL") {
			p_tmpGO->objectLayoutType = GL_FILL;
		}
		else {
				std::cout << " Layout Format not specified or is incorrect !" << std::endl;
		}

		::gVecGameObjects.push_back(p_tmpGO);


	}
	return true;
}


bool loadGameObjectsFromFile(std::string file,int customShaderId) {
	std::fstream infoFile(file);
	std::map<std::string, std::string> mapGameObject ;
	if (!infoFile.is_open()) {
		std::cout << "Error in reading the config file";
		return false;
	}
	else {
		std::cout << "File read. Inputting Game Objects Now !" << std::endl;
		std::string strConfigKey, strConfigValue;
		infoFile >> strConfigKey;
		std::cout << "Config Value :" + strConfigKey << std::endl;
		while (strConfigKey != "#####") {
			if (strConfigKey == "==================================================================================") {
				if (!loadObjectIntoVaoAndGOVec(mapGameObject, customShaderId)) {
					std::cout << "Error in loading Object " << mapGameObject["gameObject"] <<" into VAO and GOVec" << std::endl;
					}
				else
				{
					std::cout << "Successfully loaded " << mapGameObject["numOfObjects"] << " "<< mapGameObject["gameObject"] << " into VAO and GOVec" << std::endl;
					
				}
				std::cout << "==================================================================================" << std::endl;
				mapGameObject.clear();
			}
			else {
				infoFile >> strConfigValue;
				mapGameObject.insert(std::pair<std::string, std::string>(strConfigKey, strConfigValue));
				std::cout << strConfigKey << " : " << mapGameObject[strConfigKey] << std::endl;
			}
			infoFile >> strConfigKey;
		}

	}
}

int main(void)
{
	GLFWwindow* window;
	GLuint vertex_buffer, vertex_shader, fragment_shader, program;
	GLint mvp_location, vpos_location, vcol_location;
	glfwSetErrorCallback(error_callback);

	if (!glfwInit())
		exit(EXIT_FAILURE);

	int height = 480;						/* default */
	int width = 640;						/* default */
	std::string title = "Project 1";		/* default */

	std::fstream infoFile("config.txt");
	if (!infoFile.is_open()) {
		std::cout << "Error in reading the config file";
		return -1;
	}
	else {
		std::cout << "File read. Storing config now !" << std::endl;
		std::string strConfigValue;
		infoFile >> strConfigValue;
		std::cout << "Config Value :" + strConfigValue << std::endl;
		while (strConfigValue != "#####") {
			if (strConfigValue == "width") {
				infoFile >> width;
				std::cout << "New width :" << width << std::endl;
			}
			else if (strConfigValue == "height") {
				infoFile >> height;
				std::cout << "New height :" << height << std::endl;
			}
			else if (strConfigValue == "maxGameObjects") {
				infoFile >> maxGameObjects;
				std::cout << "Max Game Objects :" << maxGameObjects << std::endl;
			}
			else if (strConfigValue == "meshBasePath") {
				infoFile >> meshBasePath;
				std::cout << "Mesh Base Path :" << meshBasePath << std::endl;
			}
			else if (strConfigValue == "shaderBasePath") {
				infoFile >> shaderBasePath;
				std::cout << "Shader Base Path :" << shaderBasePath << std::endl;
			}
			else if (strConfigValue == "sizeOfWorld") {
				infoFile >> sizeOfWorld;
				std::cout << "Size Of World :" << sizeOfWorld << std::endl;
			}
			else if (strConfigValue == "layoutFormat") {
				infoFile >> layoutFormat;
				std::cout << "Layout Format :" << layoutFormat << std::endl;
			}
			else if (strConfigValue == "gameObjectsFile") {
				infoFile >> gameObjectsFile;
				std::cout << "Game Object File :" << gameObjectsFile << std::endl;
			}
			else if (strConfigValue == "cameraCoordinates") {
				infoFile >> gCameraCoordinates;
				std::cout << "Game Coordinates :" << gCameraCoordinates << std::endl;
				loadCameraCoordinates(gCameraXYZ,gCameraCoordinates);
			}
			else if (strConfigValue == "cameraTarget") {
				infoFile >> gCameraCoordinates;
				std::cout << "Game Coordinates :" << gCameraCoordinates << std::endl;
				loadCameraCoordinates(gCameraTarget, gCameraCoordinates);
			}
			else if (strConfigValue == "cameraSpeed") {
				std::string cSpeed = "";
				infoFile >> cSpeed;
				gCameraSpeed = strtof(cSpeed.c_str(), 0);
				std::cout << "Game Speed :" << gCameraSpeed << std::endl;
			}
			else if (strConfigValue == "title") {
				bool bKeepReading = true;
				std::stringstream ssTitle;
				do {
					infoFile >> strConfigValue;
					if (strConfigValue == "[e]") {
						bKeepReading = false;
						title = ssTitle.str();
					}
					else if (strConfigValue == "[s]") {}
					else {
						ssTitle << strConfigValue << " ";
					}
				} while (bKeepReading);
				std::cout << "New title :" + ssTitle.str() << std::endl;
			}
			infoFile >> strConfigValue;
			std::cout << "Config Value :" + strConfigValue << std::endl;
		}

	}

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    window = glfwCreateWindow( width, height, title.c_str(),NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwSetKeyCallback(window, key_callback);
    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);

	std::cout << glGetString(GL_VENDOR) << " " 
		<< glGetString(GL_RENDERER) << ", " 
		<< glGetString(GL_VERSION) << std::endl;
	std::cout << "Shader language version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
		
	::gpShaderManager = new cShaderManager();
	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	vertShader.fileName = "simpleVertex.glsl";
	fragShader.fileName = "simpleFragment.glsl";

	::gpShaderManager->setBasePath(shaderBasePath);

	if (! ::gpShaderManager->createProgramFromFile("myCustomShader", vertShader, fragShader)) {
		std::cout << " Error in loading shader "<<std::endl;
		return -1;
	}

	::gpVAOMeshManager = new cVAOMeshManager();

	GLint customShaderId = ::gpShaderManager->getIDFromFriendlyName("myCustomShader");
	
	
	loadGameObjectsFromFile(gameObjectsFile,customShaderId);
	
	GLint currProgramId = ::gpShaderManager->getIDFromFriendlyName("myCustomShader");
	mvp_location = glGetUniformLocation(currProgramId, "MVP");

	while ( ! glfwWindowShouldClose(window) )
    {
        float ratio;
        int width, height;
        glm::mat4x4 m, p, mvp;
        glfwGetFramebufferSize(window, &width, &height);
        ratio = width / (float) height;
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT);

		for (int index = 0; index < maxGameObjects; index++) {
			if (::gVecGameObjects[index] == 0) {
				continue;
			}

			std::string meshToDraw = ::gVecGameObjects[index]->meshName;

			sVAOInfo VAODrawInfo;

			if (!::gpVAOMeshManager->LookUpVAOByName(meshToDraw, VAODrawInfo)) {
				std::cout << "Mesh not found" << meshToDraw << std::endl;
				return -1;
			}

			m = glm::mat4x4(1.0f);

			glm::mat4 matPreRotZ = glm::mat4x4(1.0f);
			matPreRotZ = glm::rotate(matPreRotZ, ::gVecGameObjects[index]->orientation.z, glm::vec3(0.0f, 0.0f, 1.0f));
			m = m * matPreRotZ;

			glm::mat4 matPreRotY = glm::mat4x4(1.0f);
			matPreRotY = glm::rotate(matPreRotY, ::gVecGameObjects[index]->orientation.y, glm::vec3(0.0f, 1.0f, 0.0f));
			m = m * matPreRotY;

			glm::mat4 matPreRotX = glm::mat4x4(1.0f);
			matPreRotX = glm::rotate(matPreRotX, ::gVecGameObjects[index]->orientation.x, glm::vec3(1.0f, 0.0f, 0.0f));
			m = m * matPreRotX;
			
			glm::mat4 trans = glm::mat4x4(1.0f);
			trans = glm::translate(trans, ::gVecGameObjects[index]->position);
			m = m* trans;


			glm::mat4 matPostRotZ = glm::mat4x4(1.0f);
			matPostRotZ = glm::rotate(matPostRotZ, ::gVecGameObjects[index]->orientation2.z, glm::vec3(0.0f, 0.0f, 1.0f));
			m = m * matPostRotZ;

			glm::mat4 matPostRotY = glm::mat4x4(1.0f);
			matPostRotY = glm::rotate(matPostRotY, ::gVecGameObjects[index]->orientation2.y, glm::vec3(0.0f, 1.0f, 0.0f));
			m = m * matPostRotY;

			glm::mat4 matPostRotX = glm::mat4x4(1.0f);
			matPostRotX = glm::rotate(matPostRotX, ::gVecGameObjects[index]->orientation2.x, glm::vec3(1.0f, 0.0f, 0.0f));
			m = m * matPostRotX;

			//float finalScale = VAODrawInfo.scaleForUnitBBox * ::gVecGameObjects[index]->scale;
			float finalScale = ::gVecGameObjects[index]->scale;
			
			glm::mat4 matScale = glm::mat4x4(1.0f);
			matScale = glm::scale(matScale, glm::vec3(finalScale, finalScale, finalScale));
			m = m * matScale;

			p = glm::perspective(0.6f, ratio, 0.1f, 1000.0f);

			glm::mat4 v = glm::mat4(1.0f);

			v = glm::lookAt(gCameraXYZ, gCameraTarget, glm::vec3(0.0f, 1.0f, 0.0f));
			mvp = p * v * m;

			GLint shaderID = ::gpShaderManager->getIDFromFriendlyName("myCustomShader");
			GLint diffuseColour_loc = glGetUniformLocation(shaderID, "diffuseColour");

			glUniform4f(diffuseColour_loc,
				::gVecGameObjects[index]->diffuseColour.r,
				::gVecGameObjects[index]->diffuseColour.g,
				::gVecGameObjects[index]->diffuseColour.b,
				::gVecGameObjects[index]->diffuseColour.a);

			::gpShaderManager->useShaderProgram("myCustomShader");
			glUniformMatrix4fv(mvp_location, 1, GL_FALSE, (const GLfloat*)glm::value_ptr(mvp));

			
			glPolygonMode( GL_FRONT_AND_BACK, ::gVecGameObjects[index]->objectLayoutType);

			glBindVertexArray(VAODrawInfo.vaoId);
			glDrawElements(GL_TRIANGLES,VAODrawInfo.numberOfIndices,GL_UNSIGNED_INT,0);
			glBindVertexArray(0);
		}

		

        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwDestroyWindow(window);
    glfwTerminate();
	delete ::gpShaderManager;
	delete ::gpVAOMeshManager;
    exit(EXIT_SUCCESS);
}