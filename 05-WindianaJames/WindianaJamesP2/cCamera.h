#ifndef CAMERA_HG
#define CAMERA_HG

#include <glad\glad.h>
#include <GLFW\glfw3.h>

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

#include <vector>

enum cameraMovement
{
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT
};

const GLfloat YAW = -90.0f;
const GLfloat PITCH = 0.0f;
const GLfloat SPEED = 10.0f;
const GLfloat SENSITIVITY = 0.05f;
const GLfloat ZOOM = 45.0f;

class cCamera
{
public:
	cCamera( glm::vec3 position = glm::vec3( 0.0f, 0.0f, 5.0f ),
		glm::vec3 up = glm::vec3( 0.0f, 1.0f, 0.0f ),
		GLfloat yaw = YAW,
		GLfloat pitch = PITCH );
	cCamera( GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch );

	glm::mat4 GetViewMatrix( );

	void ProcessKeyboard( cameraMovement direction, GLfloat deltaTime );
	void ProcessMouseMovement( GLfloat xOffset, GLfloat yOffset, GLboolean constrainPitch = true );
	void ProcessMouseScroll( GLfloat yOffset );

	GLfloat GetZoom( )
	{
		return this->zoom;
	}

private:
	// Camera Attributes
	glm::vec3 position;
	glm::vec3 front;
	glm::vec3 up;
	glm::vec3 right;
	glm::vec3 worldUp;

	// Eular Angles
	GLfloat yaw;
	GLfloat pitch;

	// Camera options
	GLfloat movementSpeed;
	GLfloat mouseSensitivity;
	GLfloat zoom;

	void updateCameraVectors( );


};





#endif