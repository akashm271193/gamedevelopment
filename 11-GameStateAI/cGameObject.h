#ifndef _cGameObject_HP_
#define _cGameObject_HP_

#include <glm/vec3.hpp> 
#include <glm/vec4.hpp> 
#include <string>
#include <glad/glad.h>
#include "cModel.h"
#include <map>

enum eTypeOfObject
{	// Ok people, here's the deal:
	SPHERE = 0,		// - it's a sphere
	PLANE = 1,		// - it's a plane
	CAPSULE = 2,    // - it's a capsule
	AABB = 3,	
	PLAYER = 4,
	CURIOUS_AI = 5,
	ANGRY_AI = 6,
	FOLLOWER_AI = 7,
	BOUNDING_SPHERE = 8,
	UNKNOWN = 99	// I don't know
};

class cGameObject
{
public:
	cGameObject();
	virtual ~cGameObject() {};
	cGameObject(cModel* newModel, glm::vec3 maxExtentXYZ, glm::vec3 position, int parentObjId);
	cModel* model;
	glm::vec3 position;
	glm::vec3 originOrientation;
	glm::vec3 ownAxisOrientation;
	glm::vec3 diffuseColour;
	GLenum objectLayoutType;
	bool isWireFrame;
	bool isUpdatedInPhysics;
	bool hasBoundingBoxOrSphere;
	glm::vec3 colour;
	float scale;
	int objectId;
	eTypeOfObject typeOfObject;		// (really an int)
	glm::vec3 velocity;
	glm::vec3 acceleration;
	bool toAnimate;
	float radius;
	cGameObject* boundingSphere;
	void Draw(int shaderId, bool drawBoundingBox = false, glm::vec3 parentObjectPos = glm::vec3(0.0f));
	int parentGameObjectIndexId;
	glm::vec3 objectFront;
};

#endif