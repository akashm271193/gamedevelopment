#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "cMap.h"
#include "cMapCell.h"
#include <rapidjson\document.h>
#include <rapidjson\writer.h>
#include <rapidjson\stringbuffer.h>
#include <rapidjson\filereadstream.h>
#include <cstdio>

class cMapLoader
{
public:
	cMapLoader();
	cMapLoader(std::string filePath);
	~cMapLoader();
	cMap* getMap();
	
	
	
private:
	void loadMapFromFile(std::string filePath);
	bool checkIfSpecialCell(std::string cell, const rapidjson::Value &arrayChars);
	void loadSpecialCharIntoCell(cMapCell* tmpCell, std::string cellValue);
	cMap* map;
	
};