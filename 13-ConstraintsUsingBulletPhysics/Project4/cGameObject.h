#ifndef _GAME_OBJECT_
#define _GAME_OBJECT_

#define GLM_ENABLE_EXPERIMENTAL
#include <glm\game_math.h>
#include <string>

#include "cModel.h"

#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"

enum eObjectType
{
	OBJECT_SPHERE,
	OBJECT_PLANE, 
	OBJECT_CUBE,
	OBJECT_CYLINDER,
	OBJECT_CONE
};

class cGameObject
{
public:
	glm::vec3 Position, Scale, OrientationEuler;
	glm::quat OrientationQuat;
	glm::vec3 Velocity;
	float Mass;

	cModel* Model;

	btCollisionShape* shape;
	btDefaultMotionState* state;
	btRigidBody::btRigidBodyConstructionInfo* info;
	btRigidBody* body;
	btDiscreteDynamicsWorld* world;

	eObjectType ObjectType;


	cGameObject::cGameObject(std::string modelName, std::string modelDir, eObjectType objectType);
	cGameObject(cModel* model, glm::vec3 position, glm::vec3 scale, glm::vec3 orientationEuler, float mass, eObjectType objectType, btDiscreteDynamicsWorld* world);
	cGameObject(std::string modelName, std::string modelDir, glm::vec3 position, glm::vec3 scale, glm::vec3 orientationEuler, float mass, eObjectType objectType, btDiscreteDynamicsWorld* world);

	void setUpGameObject();
	void Draw(int shaderId);
};
#endif // !_GAME_OBJECT_