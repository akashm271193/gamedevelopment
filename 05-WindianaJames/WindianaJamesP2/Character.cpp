#include "Character.h"

Character::Character() 
{
	return;
}

Character::~Character()
{
	return;
}

void Character::RegisterObserver(Observer* observer)
{
	observerCollection.push_back(observer);
}

void Character::UnregisterObserver(Observer* observer)
{
	vector<Observer*>::iterator itObserver = find(observerCollection.begin(),
											      observerCollection.end(), 
												  observer);

	observerCollection.erase(itObserver);
}


void Character::NotifyObserver(string message)
{
	for (int index = 0; index < observerCollection.size(); index ++ )
	{
		observerCollection[index]->Notify(message);
	}
}
