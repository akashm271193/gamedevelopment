#pragma once
#include <iostream>
#include <vector>
#include "cMapCell.h"
#include <string>
#include "cPath.h"
#include <glm\glm.hpp>
#include <glm/vec3.hpp>
class cMap
{
public:
	cMap();
	~cMap();
	void getMapCells(std::vector<cMapCell*> &map);
	void setMapCells(std::vector<cMapCell*> map);
	void setMapRows(int rows);
	void setMapColumns(int columns);
	int  getMapRows();
	int  getMapColumns();
	void setMapStartPoint(std::string startPoint);
	void setMapEndPoint(std::string endPoint);
	void setWindowMapStartPoint(glm::vec3 startPoint);
	void setWindowMapEndPoint(glm::vec3 endPoint);
	void getWindowMapStartPoint(glm::vec3 &startPoint);
	void getWindowMapEndPoint(glm::vec3 &endPoint);

	void setupStartAndEndPoints();
	void setupSkeletonStartAndEndPoints();
	void showMap();
	int startCellIndex, endCellIndex;
	int skeletonStartCellIndex, skeletonEndCellIndex;
	void findShortestPathTillEnd();
	void parseOpenCells(std::vector<cMapCell*> &openCells, std::vector<cMapCell*> &closedCells);
	void getShortestPath();
	cPath* lastShortestPath;

	glm::vec3 mapExtentXYZ = glm::vec3(0.0f);
	void calculateMapExtents(glm::vec3 cellExtentXYZ);
	int mapScale;

private:
	std::vector<cMapCell*> vecCells;
	int mapRows;
	int mapColumns;
	void getCellIndexForPoints(int &pointIndex,std::string point);
	std::string mapStartPoint, mapEndPoint;
	glm::vec3 windowMapStartPoint, windowMapEndPoint;
};

