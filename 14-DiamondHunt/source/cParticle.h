#pragma once

#include "cGameObject.h"
class cParticle : public cGameObject
{
public:
	cParticle();

	void Update(float deltaTime);

	float lifetime;
	float scaleStep;
};
