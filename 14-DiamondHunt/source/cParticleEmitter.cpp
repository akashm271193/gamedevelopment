#include "cParticleEmitter.h"
#include "globalGameStuff.h"

cParticleEmitter::cParticleEmitter()
{
	this->numParticles = 0;
	this->maxNumParticlePerStep = 0;
	this->minLife = 0.0f;
	this->maxLife = 0.0f;
	this->minInitVelocity = glm::vec3(0.0f);
	this->maxInitVelocity = glm::vec3(0.0f);
	this->minRangeFromEmitter = glm::vec3(0.0f);
	this->maxRangeFromEmitter = glm::vec3(0.0f);
}

//random generator
double getRandInRange(double min, double max)
{
	double value = min + static_cast <double> (rand()) / (static_cast <double> (RAND_MAX / (max - min)));
	return value;
}


void cParticleEmitter::Init(int numParticles, int maxParticleCreatePerFrame,
	glm::vec3 minInitVel, glm::vec3 maxInitVel,float minLife, float maxLife,
	glm::vec3 minRange, glm::vec3 maxRange)
{
	
	//check the remaining number of particles to create
	int numParticlesToCreate = numParticles - this->vecParticles.size();
	
	for (int count = 0; count <= numParticlesToCreate; count++)
	{
		//create new particle
		cParticle* tmpGameObject = new cParticle();
		tmpGameObject->model = gMapLoadedModels["fire"];
		tmpGameObject->scale = glm::vec3(0.55f);
		tmpGameObject->scaleStep = tmpGameObject->scale.x / 175.0f;
		tmpGameObject->colour = glm::vec3(1.0f, 0.0f, 0.0f);
		tmpGameObject->toAnimate = true;
		tmpGameObject->objectId = vecParticles.size();
		tmpGameObject->lifetime = getRandInRange(this->minLife, this->maxLife);
		tmpGameObject->position.x = getRandInRange(minRange.x, maxRange.x);
		tmpGameObject->position.y = getRandInRange(minRange.y, maxRange.y);
		tmpGameObject->position.z = getRandInRange(minRange.z, maxRange.z);
		tmpGameObject->velocity.x = getRandInRange(minInitVel.x, maxInitVel.x);
		tmpGameObject->velocity.y = getRandInRange(minInitVel.y, maxInitVel.y);
		tmpGameObject->velocity.z = getRandInRange(minInitVel.z, maxInitVel.z);
		tmpGameObject->acceleration.y = getRandInRange(minInitVel.y, maxInitVel.y);

		this->vecParticles.push_back(tmpGameObject);
	}

	this->numParticles = numParticles;
	for (int index = 0; index != this->numParticles; index++)
	{
		this->vecParticles[index]->lifetime = 0.0f;
	}

	this->maxNumParticlePerStep = maxParticleCreatePerFrame;
	this->minInitVelocity = minInitVel;
	this->maxInitVelocity = maxInitVel;
	this->minLife = minLife;
	this->maxLife = maxLife;
	this->minRangeFromEmitter = minRange;
	this->maxRangeFromEmitter = maxRange;
}


int cParticleEmitter::getLivingParticles(std::vector<cParticle* > &vecLiveParticles)
{
	if (vecParticles.size() < this->numParticles)
	{
		//resize if too small
		vecParticles.resize(this->numParticles, NULL);
	}

	int indexAliveParticles = 0;
	for (int index = 0; index != this->numParticles; index++)
	{
		//check if particle's alive
		if (this->vecParticles[index]->lifetime > 0.0f)
		{
			vecParticles[indexAliveParticles] = this->vecParticles[index];
			indexAliveParticles++;
		}
	}

	return indexAliveParticles;
}


void cParticleEmitter::Update(float deltaTime)
{
	int particlesToMake = this->maxNumParticlePerStep;

	//scan through looking for dead particles
	for (int index = 0; (index != this->numParticles) && (particlesToMake > 0); index++)
	{
		//ceck if particle is dead
		if (this->vecParticles[index]->position.y - 2.0f >= 0.1f)
		{
			//make new particles/reset its variables
			this->vecParticles[index]->lifetime = getRandInRange(this->minLife, this->maxLife);
			this->vecParticles[index]->position.x = getRandInRange(this->minRangeFromEmitter.x, this->maxRangeFromEmitter.x);
			this->vecParticles[index]->position.y = getRandInRange(this->minRangeFromEmitter.y, this->maxRangeFromEmitter.y);
			this->vecParticles[index]->position.z = getRandInRange(this->minRangeFromEmitter.z, this->maxRangeFromEmitter.z);
			this->vecParticles[index]->velocity.x = getRandInRange(this->minInitVelocity.x, this->maxInitVelocity.x);
			this->vecParticles[index]->velocity.y = getRandInRange(this->minInitVelocity.y, this->maxInitVelocity.y);
			this->vecParticles[index]->velocity.z = getRandInRange(this->minInitVelocity.z, this->maxInitVelocity.z);
			this->vecParticles[index]->acceleration.y = getRandInRange(this->minInitVelocity.y, this->maxInitVelocity.y);
			this->vecParticles[index]->scale = glm::vec3(0.45f);

			particlesToMake--;
		}
		else
		{
			this->vecParticles[index]->Update(deltaTime);
		}

	}

	return;
}

void cParticleEmitter::DrawParticles(int shaderId) {
	for (int index = 0; index != this->numParticles; index++)
	{
		this->vecParticles[index]->Draw(shaderId);
	}
	return;
};
