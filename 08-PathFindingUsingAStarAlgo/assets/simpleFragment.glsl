#version 330 core
out vec4 FragColor;
in vec3 objColor;
in vec2 objTexture;

uniform sampler2D texSampler1;
uniform sampler2D texSampler2;

void main(){
	FragColor = mix(texture(texSampler1, objTexture), texture(texSampler2, objTexture), 0.2);
} 