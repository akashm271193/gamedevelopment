#pragma once

#include <string>
#include <iostream>

class Lobbydetails
{
public:
	Lobbydetails();
	~Lobbydetails();
	void getAllDetails();
	void getMaps();
	void getGameModes();
	bool checkLobbyIsValid(std::string map , std::string gameMode);
	
private:
	std::string *maps;
	std::string *gameModes;
	bool isPresentInArray(std::string *strArray,std::string item);
	bool isStringEquals(std::string a, std::string b);

};

