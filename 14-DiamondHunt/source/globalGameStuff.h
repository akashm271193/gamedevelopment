#ifndef _globalGameStuff_HG_
#define _globalGameStuff_HG_


#define _SILENCE_TR1_NAMESPACE_DEPRECATION_WARNING
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:4996)


//	 _____ _                  ___   ___  _  _ _ _____                   _ _ 
//	|_   _| |_  ___ ___ ___  |   \ / _ \| \| ( )_   _|  _ _  ___ ___ __| (_)
//	  | | | ' \/ -_|_-</ -_) | |) | (_) | .` |/  | |   | ' \/ -_) -_) _` |_ 
//	  |_| |_||_\___/__/\___| |___/ \___/|_|\_|___|_|   |_||_\___\___\__,_(_)
//	 __ _| |__ _ __| |  ___ _ _   / __| |  | __\ \    / /                   
//	/ _` | / _` / _` | / _ \ '_| | (_ | |__| _| \ \/\/ /                    
//	\__, |_\__,_\__,_| \___/_|    \___|____|_|   \_/\_/                     
//	|___/                                                                   
//
// These are files that do NOT require glad or GLFW to be included
// (So be careful what you add here)
//

#include <vector>
#include <map>
#include "cGameObject.h"
#include <glm/vec3.hpp>
#include "cMap.h"
#include "cPlayerGameObject.h"
#include "cAIGameObject.h"
#include "Utilities.h"
#include "FileUtilities.h"
#include "cCamera.h"
#include "cMapCell.h"
#include "cMapLoader.h"
#include <Windows.h>
#include "cMenuLoader.h"
#include "cParticleEmitter.h"
// ******************************************************************
// These require the inclusion of the OpenGL and-or GLFW headers
//#include "cVAOMeshManager.h"
#include "cShaderManager.h"
#include "cFBO.h"
//#include "Texture/CTextureManager.h"
//#include "cDebugRenderer.h"
// ******************************************************************

// Remember to #include <vector>...
extern std::vector<cGameObject*> gGameObjects;
extern std::map<std::string, cModel*> gMapLoadedModels;
extern cMapLoader* gMapLoader;
extern cMap* ourMap;
extern cMenuLoader* gMenuLoader;
extern GLFWwindow* mainWindow;
extern cShaderManager* gPShaderManager;
extern std::vector<cGameObject*> gBoundingBox;
extern cFBO* gFBO;
/*********************************/
/*                               */
/*  Global Varibles For Camera   */
/*                               */
extern cCamera* camera;
extern float lastX;
extern float lastY;
extern bool firstMouse;
extern int playerGameObjId;
extern float cameraBindDistance;
extern float deltaTime;
//extern int skeletonGameObjId;

extern const unsigned int SCR_WIDTH;
extern const unsigned int SCR_HEIGHT;

/*********************************/

/*********************************/
/*                               */
/* Global Scene Config Variables */
/*                               */

extern int NumAngry;
//extern float AngrySpeed;
extern int NumCurious;
//extern float CuriousSpeed;
extern int NumFollower;
//extern float FollowerSpeed;
extern float PlayerSpeed;
extern float aiAverageSpeed;
extern float aiMaxBlocks;
extern glm::vec3 CameraPosition;
//extern float CameraUpX;
//extern float CameraUpY;
//extern float CameraUpZ;
//extern float CameraYaw;
//extern float CameraPitch;
//extern float PlayableAreaX;
//extern float PlayableAreaZ;
extern float CullingRadius;
extern float fireOnTime;
extern float fireOffTime;
/*********************************/


extern HANDLE hStdout;
extern CONSOLE_CURSOR_INFO lpCursor;
extern COORD coord;
extern int max_number_of_rows;
void findValueOnLine(glm::vec3 pointA, glm::vec3 pointB, glm::vec3 &pointC);
extern bool isMenuActive;
extern bool toDrawObjects;
extern bool toDrawDebugObjects;
extern int drawToggle;
extern bool isPEDrawn;
// Returns 0 or NULL if not found
//cGameObject* findObjectByFriendlyName( std::string friendlyName, std::vector<cGameObject*> &vec_pGameObjects );
//cGameObject* findObjectByUniqueID( unsigned int ID, std::vector<cGameObject*> &vec_pGameObjects );


//extern cLightManager*	g_pLightManager;	// (theMain.cpp)

// Super basic physics update function
//void PhysicsStep( double deltaTime );
//extern cPhysicsWorld*	g_pPhysicsWorld;	// (theMain.cpp)

//extern cModelAssetLoader* g_pModelAssetLoader;	// (ModelUtilies.cpp)

// The teapots
//extern cGameObject* pTP0;
//const std::string LEFTTEAPOTNAME = "Left Teapot";		// Now that we have a lookup

//extern cCamera* g_pTheCamera;		// (theMain.cpp)
//extern cVAOMeshManager*			g_pVAOManager;		// (theMain.cpp)
//extern cShaderManager*			g_pShaderManager;	// (theMain.cpp)
//extern cBasicTextureManager*	g_pTextureManager;	// (theMain.cpp)
//extern CTextureManager*			g_pTextureManager;	// (theMain.cpp)
//extern cDebugRenderer*			g_pDebugRenderer;	// (theMain.cpp)

//void RenderScene( std::vector< cGameObject* > &vec_pGOs, GLFWwindow* pGLFWWindow, double deltaTime );

//extern cGameObject* g_pSkyBoxObject;	// (theMain.cpp)

//extern	GLFWwindow* g_pGLFWWindow;	// In TheMain.cpp
//extern bool g_IsWindowFullScreen;	// false at start
//void setWindowFullScreenOrWindowed( GLFWwindow* pTheWindow, bool IsFullScreen );	// In TheMain.cpp


// Example of skinned mesh.
// NOTE: This is only ONE instance of an loaded FBX file model
//extern cSimpleAssimpSkinnedMesh* g_pRPGSkinnedMesh;			// In ModelUtilites.cpp




#endif
