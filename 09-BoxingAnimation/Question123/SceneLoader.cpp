// This file is used to laod the models
#include "cGameObject.h"
#include <vector>
#include "Utilities.h"		// getRandInRange()
#include <glm/glm.hpp>
#include "globalGameStuff.h"

// For the cSimpleAssimpSkinnedMeshLoader class
#include "assimp/cSimpleAssimpSkinnedMeshLoader_OneMesh.h"

#include "cAnimationState.h"

extern std::vector< cGameObject* >  g_vecGameObjects;
extern cGameObject* g_pTheDebugSphere;

void LoadModelsIntoScene(void)
{

	

	{	// Skinned mesh  model
		cGameObject* pTempGO = new cGameObject();
		pTempGO->friendlyName = "playerRed";
		// This assigns the game object to the particular skinned mesh type 
		pTempGO->pSimpleSkinnedMesh = ::g_pRPGSkinnedMesh;
		// Add a default animation 
		pTempGO->pAniState = new cAnimationState();
		pTempGO->pAniState->defaultAnimation.name = "assets/rpgCharacterModels/Idle.FBX";
		pTempGO->pAniState->defaultAnimation.frameStepTime = 0.005f;
		// Get the total time of the entire animation
		pTempGO->pAniState->defaultAnimation.totalTime = pTempGO->pSimpleSkinnedMesh->GetDuration();

		cPhysicalProperties physState;
		physState.integrationUpdateType = cPhysicalProperties::DYNAMIC;
		physState.position = glm::vec3(-8.5f,  0.0, 0.0f);
		pTempGO->SetPhysState(physState);
		sMeshDrawInfo meshInfo;
		meshInfo.scale = 0.12f;
		meshInfo.setMeshOrientationEulerAngles(glm::vec3(0.0f, glm::radians(90.0f), 0.0f)); 
		//meshInfo.use
		meshInfo.debugDiffuseColour = glm::vec4( 1.0f, 0.0f, 0.0f, 1.0f );
		meshInfo.bDrawAsWireFrame = true;
		meshInfo.name = ::g_pRPGSkinnedMesh->friendlyName;
		pTempGO->vecMeshes.push_back(meshInfo);
		::g_vecGameObjects.push_back( pTempGO );		// Fastest way to add
	}	
	{	// Skinned mesh  model
		cGameObject* pTempGO = new cGameObject();
		pTempGO->friendlyName = "playerBlue";
		// This assigns the game object to the particular skinned mesh type 
		pTempGO->pSimpleSkinnedMesh = ::g_pRPGSkinnedMesh;
		// Add a default animation 
		pTempGO->pAniState = new cAnimationState();
		pTempGO->pAniState->defaultAnimation.name = "assets/rpgCharacterModels/Idle.FBX";
		//pTempGO->pAniState->defaultAnimation.frameStepTime = 0.01f;
		// Get the total time of the entire animation
		pTempGO->pAniState->defaultAnimation.currentTime = pTempGO->pSimpleSkinnedMesh->GetDuration();

		cPhysicalProperties physState;
		physState.integrationUpdateType = cPhysicalProperties::DYNAMIC;
		physState.position = glm::vec3(+8.5f, 0.0, 0.0f);
		pTempGO->SetPhysState(physState);
		sMeshDrawInfo meshInfo;
		meshInfo.scale = 0.12f;
		meshInfo.setMeshOrientationEulerAngles(glm::vec3(0.0f, glm::radians(270.0f), 0.0f)); 
		meshInfo.bUseDebugColour = true;
		meshInfo.debugDiffuseColour = glm::vec4( 0.0f, 0.0f, 1.0f, 1.0f );
		meshInfo.bDrawAsWireFrame = true;
		meshInfo.name = ::g_pRPGSkinnedMesh->friendlyName;
		pTempGO->vecMeshes.push_back(meshInfo);
		::g_vecGameObjects.push_back( pTempGO );		// Fastest way to add
	}	
	{
		cGameObject* pTempGO = new cGameObject();
		cPhysicalProperties physState;
		physState.integrationUpdateType = cPhysicalProperties::EXCLUDED_FROM_INTEGRATION;
		physState.mass = physState.inverseMass = 0.0f;	// Infinite
		physState.position.x = 10.0f;
		physState.position.y = 0.0f;
		pTempGO->friendlyName = "plane";
		pTempGO->SetPhysState(physState);

		sMeshDrawInfo meshInfo;
		meshInfo.scale = 11.0f;
		meshInfo.debugDiffuseColour = glm::vec4( 1.0f, 0.5f, 1.0f, 1.0f );
		meshInfo.name = "PlaneXZ";
		meshInfo.vecMehs2DTextures.push_back(sTextureBindBlendInfo("barberton_etm_2001121_lrg.bmp", 1.0f));
		meshInfo.bUseDebugColour = false;
		//meshInfo.vecMehs2DTextures.push_back(sTextureBindBlendInfo("GuysOnSharkUnicorn.bmp", 1.0f));
		//meshInfo.vecMehs2DTextures.push_back(sTextureBindBlendInfo("grass_texture.bmp", 1.0f));
		pTempGO->vecMeshes.push_back(meshInfo);

		::g_vecGameObjects.push_back( pTempGO );		// Fastest way to add
	}

	{
		cGameObject* pTempGO = new cGameObject();
		cPhysicalProperties physState;
		physState.position = glm::vec3(-4.0f, 4.0f, 0.0f);
		physState.velocity = glm::vec3(2.0f, 1.0f, 0.0f);
		physState.setOrientationEulerAngles(glm::vec3(0.0f, 0.0f, 0.0f));
		physState.integrationUpdateType = cPhysicalProperties::DYNAMIC;
		physState.rigidBodyShape = cPhysicalProperties::SPHERE;
		physState.rigidBodyMeasurements.sphere_capsule_radius = 1.0f;
		pTempGO->SetPhysState(physState);
		pTempGO->friendlyName = "SphereRadius1";

		sMeshDrawInfo meshInfo;
		meshInfo.scale = 3.0f;
		meshInfo.debugDiffuseColour = glm::vec4( 1.0f, 1.0f, 1.0f, 1.0f );
		meshInfo.name = "SphereRadius1";
		pTempGO->vecMeshes.push_back(meshInfo);
		//
		::g_vecGameObjects.push_back( pTempGO );		// Fastest way to add
	}	

	// Our skybox object
	{
		//cGameObject* pTempGO = new cGameObject();
		::g_pSkyBoxObject = new cGameObject();
		cPhysicalProperties physState;
		::g_pSkyBoxObject->SetPhysState(physState);
		::g_pSkyBoxObject->friendlyName = "skybox";
		sMeshDrawInfo meshInfo;
		meshInfo.scale = 10000.0f;
		meshInfo.debugDiffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		meshInfo.name = "SmoothSphereRadius1InvertedNormals";			
		meshInfo.vecMeshCubeMaps.push_back(sTextureBindBlendInfo("space", 1.0f));
		meshInfo.bIsSkyBoxObject = true;
		::g_pSkyBoxObject->vecMeshes.push_back(meshInfo);
		// IS SKYBOX
		::g_vecGameObjects.push_back(::g_pSkyBoxObject);		// Fastest way to add
	}

	return;
}
