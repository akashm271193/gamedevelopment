#pragma once

#include "iCollisionBody.h"
#include "sCollisionBodyDesc.h"

namespace libPhysics {
	class cCollisionBody : public iCollisionBody
	{
		public:
			cCollisionBody(const sCollisionBodyDesc &bodyDesc);
			virtual ~cCollisionBody();
			virtual void updateCollisionBody(sCollisionBodyDesc &bodyDesc);
			virtual void getCollisionBodyStateFromPhysics(sCollisionBodyDesc &bodyDesc);
			glm::vec3 getPosition();
			glm::vec3 getOrientation();
			glm::vec3 getOldPosition();
			glm::vec3 getOldOrientation();
			glm::vec3 getScale();
			glm::vec3 getMaxExtents();
			ePhysicsGameObjectType getObjType();
			void setPosition(glm::vec3 pos);
			glm::vec3 getObjectFront();
			int getObjectId();
			bool isColliding();
			void setIsColliding(bool status);
			ePhysicsGameObjectType getPhyObjType();
			std::vector<int> getVecCollidedBodies();
			void setVecCollidedBodies(std::vector<int> vecIds);

		private:
			glm::vec3 mPosition;
			glm::vec3 mOwnAxisOrientation;
			glm::vec3 mOldPosition;
			glm::vec3 mOldOrientation;
			glm::vec3 mScale;
			glm::vec3 mMaxExtents;
			ePhysicsGameObjectType objType;
			ePhysicsGameObjectType mPhyObjType;
			glm::vec3 mObjectFront;
			int mObjectId;
			bool mIsColliding;
			std::vector<int> mCollidedBodyIds;
	};
}

