//Shalin
//Dhaval
//Prakash
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>	
#include <ctime>
#include<math.h>
// C++ cin, cout, etc.
//#include "linmath.h"
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr
#include<time.h>

#include <stdlib.h>
#include <stdio.h>
// Add the file stuff library (file stream>
#include <fstream>
#include <sstream>		// "String stream"
#include <string>
#include <thread>
#include <vector>		//  smart array, "array" in most languages

#include "Utilities.h"

#include "ModelUtilities.h"

#include "cMesh.h"

#include "cShaderManager.h" 

#include "cGameObject.h"

#include "cVAOMeshManager.h"


extern "C" {
#include<Lua5.3.3\lua.h>
#include<Lua5.3.3\lualib.h>
#include<Lua5.3.3\lauxlib.h>
}
#define lua_open()  luaL_newstate()
lua_State* L;

// This is the function "signature" for the 
//	function WAY at the bottom of the file.
void PhysicsStep( double deltaTime );

std::vector< cGameObject* >  g_vecGameObjects;

glm::vec3 g_cameraXYZ = glm::vec3(70.0f, 20.0f, 100.0f );	// 5 units "down" z
glm::vec3 g_cameraTarget_XYZ = glm::vec3(70.0f, 0.0f, 0.0f);

cVAOMeshManager* g_pVAOManager = 0;		// or NULL, or nullptr

cShaderManager*		g_pShaderManager;		// Heap, new (and delete)

double gTimeElapsed;
double gTimeStarted;

static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);

	if ( key == GLFW_KEY_SPACE  )
	{
		::g_vecGameObjects[1]->position.y += 0.01f;
	}

	const float CAMERASPEED = 0.1f;
	switch ( key )
	{
	case GLFW_KEY_A:		// Left
		g_cameraXYZ.x -= CAMERASPEED;
		break;
	case GLFW_KEY_D:		// Right
		g_cameraXYZ.x += CAMERASPEED;
		break;
	case GLFW_KEY_W:		// Forward (along z)
		g_cameraXYZ.z += CAMERASPEED;
		break;
	case GLFW_KEY_S:		// Backwards (along z)
		g_cameraXYZ.z -= CAMERASPEED;
		break;
	case GLFW_KEY_Q:		// "Down" (along y axis)
		g_cameraXYZ.y -= CAMERASPEED;
		break;
	case GLFW_KEY_E:		// "Up" (along y axis)
		g_cameraXYZ.y += CAMERASPEED;
		break;

	}
	return;
}





float ip[10][3];
//First time function variable

int isFirsttime[500];
int isEnd[500];
int methodinstance, followObject,followInstance;
float position[500][3];
float orientation[500][3];
int isFirsttime2[50];
float position2[50][3];
int afterinstance;


int inif2 = 0;
int inif = 0;
int i[500];
int j[500];
float ix=0, iy=0, iz=0;
int printflag = 0, positionflag = 0;
int q;
int k;


int easein;
int easeout;

void updateVelocityOfObject(int objId, glm::vec3 destination,float seconds) {
	float distance = glm::distance(g_vecGameObjects[objId]->position, destination);
	float averageVelocity = distance / seconds;
	g_vecGameObjects[objId]->vel = glm::vec3(averageVelocity);
}

int MoveTo(lua_State *L)
{
	inif = 0;

	methodinstance = lua_tonumber(L, 1);
	if (isEnd[methodinstance] != 1)
	{
		isEnd[methodinstance] = 0;
		float cord[3];
		int obj;
		obj = lua_tonumber(L, 2);
		cord[0] = lua_tonumber(L, 3);
		cord[1] = lua_tonumber(L, 4);
		cord[2] = lua_tonumber(L, 5);
		
		float seconds = 100*lua_tonumber(L, 6);

		if (g_vecGameObjects[obj]->vel == glm::vec3(0.0f)) {
			updateVelocityOfObject(obj, glm::vec3(cord[0], cord[1], cord[2]), seconds);
		}

		easein = lua_tonumber(L, 7);
		easeout = lua_tonumber(L, 8);

		float dev = seconds;
		if (isFirsttime[methodinstance] == 1)
		{
			//First Time
			position[methodinstance][0] = g_vecGameObjects[obj]->position.x;
			position[methodinstance][1] = g_vecGameObjects[obj]->position.y;
			position[methodinstance][2] = g_vecGameObjects[obj]->position.z;
			printf("\nMoving Object:%d (%f,%f,%f) -> (%f,%f,%f) | %f seconds\n", obj, position[methodinstance][0], position[methodinstance][1], position[methodinstance][2], cord[0], cord[1], cord[2], seconds);
			isFirsttime[methodinstance] = 0;
			i[methodinstance] = 2000;
		}
	

		
		if (abs(position[methodinstance][0] - g_vecGameObjects[obj]->position.x) <= abs(cord[0] - position[methodinstance][0])*0.15&&abs(abs(cord[0] - position[methodinstance][0])*0.15 - g_vecGameObjects[obj]->position.x) >= 0.1&&i[methodinstance]>=0)
		{
			dev = dev+i[methodinstance];
			i[methodinstance] = i[methodinstance] - ((2000-seconds)/ abs(cord[0] - position[methodinstance][0])*0.15)/easein*12;
			printf("this is I yo:%d",i[methodinstance]);
				
		}
		else
		{
			if (abs(cord[0] - g_vecGameObjects[obj]->position.x) <= (cord[0] - position[methodinstance][0])*0.15&&abs(cord[0] - g_vecGameObjects[obj]->position.x) >= 0.1)
			{


				dev = dev + j[methodinstance];
				j[methodinstance] = j[methodinstance] + 6 * easeout;
				k = k + 2;
			}
			else
			{
				dev = seconds;
			}
		
		}
		
		/*else
		{
			dev=seconds;
		}*/
		
		if (abs(ceil(g_vecGameObjects[obj]->position.x*100)/100 - cord[0])>=0.10 || abs(ceil(g_vecGameObjects[obj]->position.y * 100) / 100 - cord[1]) >= 0.10 || abs(ceil(g_vecGameObjects[obj]->position.z * 100) / 100 - cord[2]) >= 0.10)
		{
			g_vecGameObjects[obj]->position.x += (cord[0] - position[methodinstance][0] )/ dev ;
			g_vecGameObjects[obj]->position.y +=  (cord[1] - position[methodinstance][1]) / dev ;
			g_vecGameObjects[obj]->position.z += (cord[2] - position[methodinstance][2]) / dev ;
			//printf("i[methodinstance] am here in instance %d for object %d to %f units\n", methodinstance, obj, (cord[0] - position[methodinstance][0]) / dev);
			inif = 1;
		}

		if(inif==0 || abs(ceil(g_vecGameObjects[obj]->position.x * 100) / 100 - cord[0]) <= 0.10)
		{
			isEnd[methodinstance] = 1;
			g_vecGameObjects[obj]->vel = glm::vec3(0.0f);
			positionflag = 0;
			//For the last time
			time_t timer;
			struct tm * timeinfo;
			time(&timer);
			timeinfo = localtime(&timer);
			std::cout << asctime(timeinfo) << "\n";
			printf("\nThe method %d has ended", methodinstance);
		
		}


		return 3;
	}
	else
	{
	
		return 3;
	}
	

}

///////////////////////////////

int MoveToAfter(lua_State *L)
{
	afterinstance = lua_tonumber(L, 1);
	methodinstance = lua_tonumber(L, 2);

	inif = 0;
	if (isEnd[afterinstance] == 1 && isEnd[methodinstance] != 1)
	{
		isEnd[methodinstance] = 0;
			//printf("\nMethod 2 start");
			float cord[3];
			int obj;
			obj = lua_tonumber(L, 3);
			cord[0] = lua_tonumber(L, 4);
			cord[1] = lua_tonumber(L, 5);
			cord[2] = lua_tonumber(L, 6);
			float seconds = 100 * lua_tonumber(L, 7);

			if (g_vecGameObjects[obj]->vel == glm::vec3(0.0f)) {
				updateVelocityOfObject(obj, glm::vec3(cord[0], cord[1], cord[2]), seconds);
			}

			easein = lua_tonumber(L, 8);
			easeout = lua_tonumber(L, 9);
			float dev = seconds;
			if (isFirsttime[methodinstance] == 1)
			{
				i[methodinstance] = 2000;
				j[methodinstance] = 0;
				k = 1;
				position[methodinstance][0] = g_vecGameObjects[obj]->position.x;
				position[methodinstance][1] = g_vecGameObjects[obj]->position.y;
				position[methodinstance][2] = g_vecGameObjects[obj]->position.z;
				printf("\nMoving Object:%d (%f,%f,%f) -> (%f,%f,%f) | %f seconds", obj, position[methodinstance][0], position[methodinstance][1], position[methodinstance][2], cord[0], cord[1], cord[2], seconds);
				isFirsttime[methodinstance] = 0;
			}
			if (abs(position[methodinstance][0] - g_vecGameObjects[obj]->position.x) <= abs(cord[0] - position[methodinstance][0])*0.15&&abs(abs(cord[0] - position[methodinstance][0])*0.15 - g_vecGameObjects[obj]->position.x) >= 0.01&&i[methodinstance] >= 0)
			{
				dev = dev + i[methodinstance];
				i[methodinstance] = i[methodinstance] - ((2000 - seconds) / abs(cord[0] - position[methodinstance][0])*0.15) / easein * 12;
			}
			else
			{
				if (abs(cord[0] - g_vecGameObjects[obj]->position.x) <= (cord[0] - position[methodinstance][0])*0.15&&abs(cord[0] - g_vecGameObjects[obj]->position.x) >= 0.01)
				{

					dev = dev + j[methodinstance];
					j[methodinstance] = j[methodinstance] + 6 * easeout;
					k = k + 2;
				}
				else
				{
					dev = seconds;
				}
			}


			if (abs(ceil(g_vecGameObjects[obj]->position.x * 100) / 100 - cord[0]) >= 0.1 || abs(ceil(g_vecGameObjects[obj]->position.y * 100) / 100 - cord[1]) >= 0.10 || abs(ceil(g_vecGameObjects[obj]->position.z * 100) / 100 - cord[2]) >= 0.10)
			{
				g_vecGameObjects[obj]->position.x += ceil(10000 * (cord[0] - position[methodinstance][0]) / dev) / 10000;
				g_vecGameObjects[obj]->position.y += ceil(10000 * (cord[1] - position[methodinstance][1]) / dev) / 10000;
				g_vecGameObjects[obj]->position.z += ceil(10000 * (cord[2] - position[methodinstance][2]) / dev) / 10000;
				//printf("i[methodinstance] am here in instance %d for object %d to %f units\n", methodinstance, obj, (cord[0] - position[methodinstance][0]) / dev);
				inif = 1;
			}
			if (inif == 0)
			{
				/*if (flag2 != 1)
				{
				std::cout << asctime(timeinfo);
				flag2 = 1;
				}*/
				//isFirsttime[methodinstance] = 1;
				isEnd[methodinstance] = 1;
				printf("\nThe method %d has ended", methodinstance);
				positionflag = 0;
			}


			return 3;
	}
	else
	{
		return 3;
	}

}

void rotateOverTime(glm::vec3 &vector, glm::vec3 angle, float timespan, double elapsedTimeSinceStart) {
	float increment = elapsedTimeSinceStart / timespan;
	glm::vec3 partialAngle;
	partialAngle.x = increment * angle.x;
	partialAngle.y = increment * angle.y;
	partialAngle.z = increment * angle.z;
	vector += partialAngle;
}
///////////////////

int OrientTo(lua_State *L)
{
	inif = 0;

	methodinstance = lua_tonumber(L, 1);
	if (isEnd[methodinstance] != 1)
	{
		isEnd[afterinstance] = 0;
		float cord[3];
		int obj;
		methodinstance = lua_tonumber(L, 1);
		obj = lua_tonumber(L, 2);
		cord[0] = lua_tonumber(L, 3);
		cord[1] = lua_tonumber(L, 4);
		cord[2] = lua_tonumber(L, 5);
		float seconds = 100*lua_tonumber(L, 6);
		easein = lua_tonumber(L, 7);
		easeout = lua_tonumber(L, 8);
		float dev = seconds;



		if (isFirsttime[methodinstance] == 1)
		{
			//First Time
			orientation[methodinstance][0] = g_vecGameObjects[obj]->orientation.x;
			orientation[methodinstance][1] = g_vecGameObjects[obj]->orientation.y;
			orientation[methodinstance][2] = g_vecGameObjects[obj]->orientation.z;
			printf("\nMoving Object:%d (%f,%f,%f) -> (%f,%f,%f) | %f seconds\n", obj, orientation[methodinstance][0], orientation[methodinstance][1], orientation[methodinstance][2], cord[0], cord[1], cord[2], seconds);
			isFirsttime[methodinstance] = 0;
			i[methodinstance] = 2000;
			j[methodinstance] = 0;
			k = 1;
			time_t timer;
			struct tm * timeinfo;
			time(&timer);
			timeinfo = localtime(&timer);

			std::cout << asctime(timeinfo) << "\n";
		}


		

		if (abs(orientation[methodinstance][0] - g_vecGameObjects[obj]->orientation.x) <= abs(cord[0] - orientation[methodinstance][0])*0.15&&abs(abs(cord[0] - orientation[methodinstance][0])*0.15 - g_vecGameObjects[obj]->orientation.x) >= 0.1&&i[methodinstance] >= 0)
		{
			dev = dev + i[methodinstance];
			i[methodinstance] = i[methodinstance] - ((2000 - seconds) / abs(cord[0] - orientation[methodinstance][0])*0.15) / easein * 12;
			//k = k +k*1.1;
			//printf("this is i[methodinstance] yo:%d", i[methodinstance]);

		}
		else
		{
			if (abs(cord[0] - g_vecGameObjects[obj]->orientation.x) <= (cord[0] - orientation[methodinstance][0])*0.15&&abs(cord[0] - g_vecGameObjects[obj]->orientation.x) >= 0.1)
			{


				dev = dev + j[methodinstance];
				j[methodinstance] = j[methodinstance] + 6 * easeout;
				k = k + 2;
			}
			else
			{
				dev = seconds;
			}

		}


		
		if (abs(ceil(g_vecGameObjects[obj]->orientation.x*100)/100 - cord[0])>=0.10 || abs(ceil(g_vecGameObjects[obj]->orientation.y * 100) / 100 - cord[1]) >= 0.10 || abs(ceil(g_vecGameObjects[obj]->orientation.z * 100) / 100 - cord[2]) >= 0.10)
		{
			g_vecGameObjects[obj]->orientation.x += (cord[0] - orientation[methodinstance][0] )/ dev ;
			g_vecGameObjects[obj]->orientation.y +=  (cord[1] - orientation[methodinstance][1]) / dev ;
			g_vecGameObjects[obj]->orientation.z += (cord[2] - orientation[methodinstance][2]) / dev ;
			//printf("i[methodinstance] am here in instance %d for object %d to %f units\n", methodinstance, obj, (cord[0] - orientation[methodinstance][0]) / dev);
			inif = 1;
		}

		if(inif==0)
		{

			isEnd[methodinstance] = 1;
			printf("\nThe method %d has ended", methodinstance);
			positionflag = 0;
			//For the last time
			time_t timer;
			struct tm * timeinfo;
			time(&timer);
			timeinfo = localtime(&timer);
			std::cout << asctime(timeinfo) << "\n";
		
		}


		return 3;
	}
	else
	{
	
		return 3;
	}
	

}

int OrientToAfter(lua_State *L)
{
	afterinstance = lua_tonumber(L, 1);
	methodinstance = lua_tonumber(L, 2);

	inif = 0;
	if (isEnd[afterinstance] == 1 && isEnd[methodinstance] != 1)
	{
		isEnd[methodinstance] = 0;
		float cord[3];
		int obj;

		obj = lua_tonumber(L, 3);
		cord[0] = glm::radians(lua_tonumber(L, 4));
		cord[1] = glm::radians(lua_tonumber(L, 5));
		cord[2] = glm::radians(lua_tonumber(L, 6));
		float seconds = 100*lua_tonumber(L, 7);
		easein = lua_tonumber(L, 8);
		easeout = lua_tonumber(L, 9);
		float dev = seconds;
		

		if (isFirsttime[methodinstance] == 1)
		{
			orientation[methodinstance][0] = g_vecGameObjects[obj]->orientation2.x;
			orientation[methodinstance][1] = g_vecGameObjects[obj]->orientation2.y;
			orientation[methodinstance][2] = g_vecGameObjects[obj]->orientation2.z;
			printf("\nMoving Object:%d (%f,%f,%f) -> (%f,%f,%f) | %f seconds", obj, orientation[methodinstance][0], orientation[methodinstance][1], orientation[methodinstance][2], cord[0], cord[1], cord[2], seconds);
			isFirsttime[methodinstance] = 0;
			i[methodinstance] = 2000;
		}
		glfwSetTime(0);
	
		if ((abs(orientation[methodinstance][0] - g_vecGameObjects[obj]->orientation2.x) <= abs(cord[0])) && (abs(orientation[methodinstance][1] - g_vecGameObjects[obj]->orientation2.y) <= abs(cord[1])) && (abs(orientation[methodinstance][2] - g_vecGameObjects[obj]->orientation2.z) <= abs(cord[2])))
		{
			glm::vec3 currObjAngle = glm::vec3(g_vecGameObjects[obj]->orientation2.x, g_vecGameObjects[obj]->orientation2.y, g_vecGameObjects[obj]->orientation2.z);
			glm::vec3 angleToRotate = glm::vec3(cord[0], cord[1], cord[2]);
			rotateOverTime(currObjAngle, angleToRotate, seconds, seconds/20);
			g_vecGameObjects[obj]->orientation2 = currObjAngle;
		}
		else
			isEnd[methodinstance] = 1;
		return 3;
		}
	else
	{
		return 3;
	}
}

int withinstance;

int MoveToWith(lua_State *L)
{
	withinstance = lua_tonumber(L, 1);
	methodinstance = lua_tonumber(L, 2);

	inif = 0;
	if (isEnd[methodinstance] != 1 )
	{
		isEnd[methodinstance] = 0;
		if (isEnd[withinstance] == 0 || isFirsttime[methodinstance]==0)
		{
			
			printf("\nMethod Move to starts here ");
			float cord[3];
			int obj;

			obj = lua_tonumber(L, 3);
			cord[0] = lua_tonumber(L, 4);
			cord[1] = lua_tonumber(L, 5);
			cord[2] = lua_tonumber(L, 6);

			float seconds = 100 * lua_tonumber(L, 7);

			if (g_vecGameObjects[obj]->vel == glm::vec3(0.0f)) {
				updateVelocityOfObject(obj, glm::vec3(cord[0], cord[1], cord[2]), seconds);
			}
			easein = lua_tonumber(L, 8);
			easeout = lua_tonumber(L, 9);
			float dev = seconds;
			if (isFirsttime[methodinstance] == 1)
			{
				i[methodinstance] = 2000;
				j[methodinstance] = 0;
				k = 1;
				position[methodinstance][0] = g_vecGameObjects[obj]->position.x;
				position[methodinstance][1] = g_vecGameObjects[obj]->position.y;
				position[methodinstance][2] = g_vecGameObjects[obj]->position.z;
				printf("\nMoving Object:%d (%f,%f,%f) -> (%f,%f,%f) | %f seconds", obj, position[methodinstance][0], position[methodinstance][1], position[methodinstance][2], cord[0], cord[1], cord[2], seconds);
				isFirsttime[methodinstance] = 0;
			}
			if (abs(position[methodinstance][0] - g_vecGameObjects[obj]->position.x) <= abs(cord[0] - position[methodinstance][0])*0.15&&abs(abs(cord[0] - position[methodinstance][0])*0.15 - g_vecGameObjects[obj]->position.x) >= 0.1&&i[methodinstance] >= 0)
			{
				dev = dev + i[methodinstance];
				i[methodinstance] = i[methodinstance] - ((2000 - seconds) / abs(cord[0] - position[methodinstance][0])*0.15) / easein * 12;
				//k = k +k*1.1;
				//printf("this is i[methodinstance] yo:%d", i[methodinstance]);

			}
			else
			{
				if (abs(cord[0] - g_vecGameObjects[obj]->position.x) <= (cord[0] - position[methodinstance][0])*0.15&&abs(cord[0] - g_vecGameObjects[obj]->position.x) >= 0.1)
				{


					dev = dev + j[methodinstance];
					j[methodinstance] = j[methodinstance] + 6 * easeout;
					k = k + 2;
				}
				else
				{
					dev = seconds;
				}

			}



			/*time_t timer;
			struct tm * timeinfo;
			time(&timer);
			timeinfo = localtime(&timer);
			*/
			/*if (flag1 != 1)
			{
			std::cout << asctime(timeinfo);
			flag1 = 1;
			}
			*/



			if (abs(ceil(g_vecGameObjects[obj]->position.x * 100) / 100 - cord[0]) >= 0.10 || abs(ceil(g_vecGameObjects[obj]->position.y * 100) / 100 - cord[1]) >= 0.10 || abs(ceil(g_vecGameObjects[obj]->position.z * 100) / 100 - cord[2]) >= 0.10)
			{
				g_vecGameObjects[obj]->position.x += ceil(10000 * (cord[0] - position[methodinstance][0]) / dev) / 10000;
				g_vecGameObjects[obj]->position.y += ceil(10000 * (cord[1] - position[methodinstance][1]) / dev) / 10000;
				g_vecGameObjects[obj]->position.z += ceil(10000 * (cord[2] - position[methodinstance][2]) / dev) / 10000;
				//printf("i[methodinstance] am here in instance %d for object %d to %f units\n", methodinstance, obj, (cord[0] - position[methodinstance][0]) / dev);
				inif = 1;
			}

			if (inif == 0)
			{
				/*if (flag2 != 1)
				{
				std::cout << asctime(timeinfo);
				flag2 = 1;
				}*/
				//isFirsttime[methodinstance] = 1;
				isEnd[methodinstance] = 1;
				printf("\nThe method %d has ended", methodinstance);
				positionflag = 0;

			}




			return 3;
		}
		else
		{
			return 3;
		}
	}
	
	else
	{
		return 3;
	}


}

int OrientToWith(lua_State *L)
{
	withinstance = lua_tonumber(L, 1);
	methodinstance = lua_tonumber(L, 2);

	inif = 0;
	if (isEnd[methodinstance] != 1)
	{
		isEnd[methodinstance] = 0;
		if (isEnd[withinstance] == 0 || isFirsttime[methodinstance] == 0)
		{

			printf("\nMethod 2 start");
			float cord[3];
			int obj;

			obj = lua_tonumber(L, 3);
			cord[0] = glm::radians(lua_tonumber(L, 4));
			cord[1] = glm::radians(lua_tonumber(L, 5));
			cord[2] = glm::radians(lua_tonumber(L, 6));

			float seconds = 100 * lua_tonumber(L, 7);
			easein = lua_tonumber(L, 8);
			easeout = lua_tonumber(L, 9);
			float dev = seconds;
			if (isFirsttime[methodinstance] == 1)
			{
				i[methodinstance] = 2000;
				j[methodinstance] = 0;
				k = 1;
				orientation[methodinstance][0] = g_vecGameObjects[obj]->orientation2.x;
				orientation[methodinstance][1] = g_vecGameObjects[obj]->orientation2.y;
				orientation[methodinstance][2] = g_vecGameObjects[obj]->orientation2.z;
				printf("\nMoving Object:%d (%f,%f,%f) -> (%f,%f,%f) | %f seconds", obj, orientation[methodinstance][0], orientation[methodinstance][1], orientation[methodinstance][2], cord[0], cord[1], cord[2], seconds);
				isFirsttime[methodinstance] = 0;
			}
			glfwSetTime(0);

			if ((abs(orientation[methodinstance][0] - g_vecGameObjects[obj]->orientation2.x) <= abs(cord[0])) && (abs(orientation[methodinstance][1] - g_vecGameObjects[obj]->orientation2.y) <= abs(cord[1])) && (abs(orientation[methodinstance][2] - g_vecGameObjects[obj]->orientation2.z) <= abs(cord[2])))
			{
				glm::vec3 currObjAngle = glm::vec3(g_vecGameObjects[obj]->orientation2.x, g_vecGameObjects[obj]->orientation2.y, g_vecGameObjects[obj]->orientation2.z);
				glm::vec3 angleToRotate = glm::vec3(cord[0], cord[1], cord[2]);
				rotateOverTime(currObjAngle, angleToRotate, seconds, seconds / 20);
				g_vecGameObjects[obj]->orientation2 = currObjAngle;
			}
			else
				isEnd[methodinstance] = 1;

			return 3;
		}
		else
		{
			return 3;
		}
	}

	else
	{
		return 3;
	}
}

int LoadAndExecute(lua_State *L,std::string vecArg)
{

	int error = luaL_loadstring(L,vecArg.c_str());

	if (error != 0 /*no error*/)
	{
		std::cout << "-------------------------------------------------------" << std::endl;
		std::cout << "Error running Lua script: ";
		//std::cout << this->m_decodeLuaErrorToString(error) << std::endl;
		std::cout << "-------------------------------------------------------" << std::endl;
		//continue;
	}

	// execute funtion in "protected mode", where problems are 
	//  caught and placed on the stack for investigation
	error = lua_pcall(L,0,0,0);	

	if (error != 0 /*no error*/)
	{
		std::cout << "Lua: There was an error..." << std::endl;
		//std::cout << this->m_decodeLuaErrorToString(error) << std::endl;

		std::string luaError;
		// Get error information from top of stack (-1 is top)
		//luaError.append(lua_tostring(this->m_pLuaState, -1));

		// Make error message a little more clear
		std::cout << "-------------------------------------------------------" << std::endl;
		std::cout << "Error running Lua script: ";
		//std::cout << itScript->first << std::endl;
		//std::cout << luaError << std::endl;
		std::cout << "-------------------------------------------------------" << std::endl;
		// We passed zero (0) as errfunc, so error is on stack)
		//lua_pop(this->m_pLuaState, 1);  /* pop error message from the stack */

		//continue;
	}
	return 1;
}

int Follow(lua_State *L) {
	followInstance = lua_tonumber(L, 1);
	methodinstance = lua_tonumber(L, 2);
	followObject = lua_tonumber(L, 3);
	int followDistance;
	glm::vec3 followOffset = glm::vec3(0.0f);
	inif = 0;

	if (isEnd[methodinstance] != 1) {
		printf("\nMethod 2 start");
		float cord[3];
		int obj;

		obj = lua_tonumber(L, 4);
		followDistance = lua_tonumber(L, 5);
		followOffset.x = lua_tonumber(L, 6);
		followOffset.y = lua_tonumber(L, 7);
		followOffset.z = lua_tonumber(L, 8);


		float seconds = 100 * lua_tonumber(L, 9);
		float dev = seconds;
		if (isFirsttime[methodinstance] == 1)
		{
			i[methodinstance] = 2000;
			j[methodinstance] = 0;
			k = 1;
			position[methodinstance][0] = g_vecGameObjects[obj]->position.x;
			position[methodinstance][1] = g_vecGameObjects[obj]->position.y;
			position[methodinstance][2] = g_vecGameObjects[obj]->position.z;
			printf("\nMoving Object:%d (%f,%f,%f) -> (%f,%f,%f) | %f seconds", obj, orientation[methodinstance][0], orientation[methodinstance][1], orientation[methodinstance][2], cord[0], cord[1], cord[2], seconds);
			isFirsttime[methodinstance] = 0;
		}

		float actualDistance = glm::distance(g_vecGameObjects[followObject]->position, g_vecGameObjects[obj]->position);
		if (actualDistance > followDistance)
		{
			glm::vec3 directionVec = glm::vec3(0.0f);
			if (followDistance != 0.0f ) {
				 directionVec = g_vecGameObjects[followObject]->position - g_vecGameObjects[obj]->position;
			}
			else if (followDistance == 0.0f && (followOffset.x != 0 || followOffset.y != 0 || followOffset.z != 0)) {
				glm::vec3 tmpPosition = g_vecGameObjects[followObject]->position;
				directionVec = (tmpPosition + followOffset) - g_vecGameObjects[obj]->position;
			}
			glm::vec3 normalizedDirectionVec = directionVec / sqrt((directionVec.x*directionVec.x) + (directionVec.y*directionVec.y) + (directionVec.z*directionVec.z));
			float rate = g_vecGameObjects[followObject]->vel.x;
			g_vecGameObjects[obj]->position += (rate)*normalizedDirectionVec;
		}
		else if (actualDistance < followDistance && isEnd[followInstance] == 1)
			isEnd[methodinstance] = 1;
	}
	return 3;
}


/*int FollowCurve(lua_State *L) {
	methodinstance = lua_tonumber(L, 1);
	inif = 0;
	float radius = 0.0f;
	if (isEnd[methodinstance] != 1) {
		printf("\nMethod 2 start");
		float cord[3];
		int obj;

		obj = lua_tonumber(L, 2);
		radius = lua_tonumber(L, 3);
		float seconds = 100 * lua_tonumber(L, 4);
		//easein = lua_tonumber(L, 8);
		//easeout = lua_tonumber(L, 9);
		float dev = seconds;
		if (isFirsttime[methodinstance] == 1)
		{
			i[methodinstance] = 2000;
			j[methodinstance] = 0;
			k = 1;
			position[methodinstance][0] = g_vecGameObjects[obj]->position.x;
			position[methodinstance][1] = g_vecGameObjects[obj]->position.y;
			position[methodinstance][2] = g_vecGameObjects[obj]->position.z;
			orientation[methodinstance][0] = g_vecGameObjects[obj]->orientation2.x;
			orientation[methodinstance][1] = g_vecGameObjects[obj]->orientation2.y;
			orientation[methodinstance][2] = g_vecGameObjects[obj]->orientation2.z;
			printf("\nMoving Object:%d (%f,%f,%f) -> (%f,%f,%f) | %f seconds", obj, orientation[methodinstance][0], orientation[methodinstance][1], orientation[methodinstance][2], cord[0], cord[1], cord[2], seconds);
			isFirsttime[methodinstance] = 0;
		}

	}
	return 3;

}*/

int Parallel(lua_State *L)
{
	
	std::vector<std::string> vecArgumentString;
	std::vector<std::thread> vecThread;

	for (int i = 1; i < 6; i++) {
		std::string tmparg = lua_tostring(L, i);
		if (tmparg != "") {
			vecArgumentString.push_back(tmparg);
		}
	}

	for (int i = 0; i < vecArgumentString.size(); i++) {
			vecThread.push_back(std::thread(LoadAndExecute, L, vecArgumentString[i]));
			vecThread[i].join();
	}

	return 1;
}

int Serial(lua_State *L)
{

	std::vector<std::string> vecArgumentString;

	for (int i = 1; i < 6; i++) {
		std::string tmparg = lua_tostring(L, i);
		if (tmparg != "") {
			vecArgumentString.push_back(tmparg);
			LoadAndExecute(L, vecArgumentString[i-1]);
		}
	}

	return 1;
}

int main(void)
{
	
	for (int i = 0; i <= 49; i++)
	{
		isFirsttime[i] = 1;
		isFirsttime2[i] = 1;
		isEnd[i] = 2;
	}

	
    GLFWwindow* window;
    GLuint vertex_buffer, vertex_shader, fragment_shader, program;
    GLint mvp_location, vpos_location, vcol_location;
    glfwSetErrorCallback(error_callback);

    if (!glfwInit())
        exit(EXIT_FAILURE);




	int height = 480;	/* default */
	int width = 640;	// default
	std::string title = "OpenGL Rocks";

	std::ifstream infoFile("config.txt");
	if ( ! infoFile.is_open() )
	{	// File didn't open...
		std::cout << "Can't find config file" << std::endl;
		std::cout << "Using defaults" << std::endl;
	}   
	else
	{	// File DID open, so read it... 
		std::string a;	
		
		infoFile >> a;	// "Game"	//std::cin >> a;
		infoFile >> a;	// "Config"
		infoFile >> a;	// "width"

		infoFile >> width;	// 1080

		infoFile >> a;	// "height"

		infoFile >> height;	// 768

		infoFile >> a;		// Title_Start

		std::stringstream ssTitle;		// Inside "sstream"
		bool bKeepReading = true;
		do
		{
			infoFile >> a;		// Title_End??
			if ( a != "Title_End" )
			{
				ssTitle << a << " ";
			}
			else
			{	// it IS the end! 
				bKeepReading  = false;
				title = ssTitle.str();
			}
		}while( bKeepReading );


	}//if ( ! infoFile.is_open() )

//	std::vector< cGameObject* >  g_vecGameObjects;
	//Vec Obj 0
	{
		cGameObject* pTempGO = new cGameObject();
		pTempGO->position.x = 70.0f;
		pTempGO->position.y = -1.6f;
		pTempGO->position.z = 0.0f;
		pTempGO->orientation.z = glm::degrees(0.0f);	// Degrees
		pTempGO->orientation2.y = glm::radians(90.0f);	// Degrees
		pTempGO->scale = 30.0f;
		pTempGO->diffuseColour = glm::vec4( 1.0f, 0.0f, 0.0f, 1.0f );
		pTempGO->meshName = "runway";
		::g_vecGameObjects.push_back( pTempGO );		// Fastest way to add
	}
	//Vec Obj 1
	{
		cGameObject* pTempGO = new cGameObject();
		pTempGO->position.x = 70.0f;
		pTempGO->position.y = -1.6f;
		pTempGO->position.z = 5.0f;
		pTempGO->orientation.z = glm::degrees(0.0f);	// Degrees
		pTempGO->orientation2.y = glm::radians(90.0f);	// Degrees
		pTempGO->scale = 30.0f;
		pTempGO->diffuseColour = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
		pTempGO->meshName = "runway";
		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
	}

	//X = 26; y= 1.6, z = 10
	//Vec Object 2
	{
		cGameObject* pTempGO = new cGameObject();
		pTempGO->position.x = 79.0f;
		pTempGO->position.y = -0.6f;
		pTempGO->position.z = -5.0f;
		pTempGO->orientation.x = glm::degrees(0.0f);	// Degrees
		pTempGO->orientation2.x = glm::radians(0.0f);
		pTempGO->orientation2.y = glm::radians(270.0f);// Degrees
		pTempGO->scale = 12.0f;
		pTempGO->diffuseColour = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
		pTempGO->meshName = "tower";
		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
	}
	
	//Vec Object 3
	{
		cGameObject* pTempGO = new cGameObject();
		pTempGO->position.x = 83.0f;
		pTempGO->position.y = 0.0f;
		pTempGO->position.z = 3.0f;
		pTempGO->orientation.x = glm::degrees(0.0f);	// Degrees
		pTempGO->orientation2.x = glm::radians(270.0f);
		pTempGO->orientation2.y = glm::radians(90.0f);// Degrees
		pTempGO->scale = 7.0f;
		pTempGO->diffuseColour = glm::vec4( 1.0f, 1.0f, 1.0f, 1.0f );
		pTempGO->meshName = "airplane";
		::g_vecGameObjects.push_back( pTempGO );		// Fastest way to add
	}

	//Vec Object 4
	{
		cGameObject* pTempGO = new cGameObject();
		pTempGO->position.x = 20.0f;
		pTempGO->position.y = 10.0f;
		pTempGO->position.z = 3.0f;
		pTempGO->orientation.x = glm::degrees(0.0f);	// Degrees
		pTempGO->orientation2.x = glm::radians(270.0f);	// Degrees
		pTempGO->orientation2.y = glm::radians(270.0f);	// Degrees
		pTempGO->scale = 7.0f;
		pTempGO->diffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		pTempGO->meshName = "airplane";
		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
	}	

	//Vec Object 5
	{
		cGameObject* pTempGO = new cGameObject();
		pTempGO->position.x = 0.0f;
		pTempGO->position.y = 10.0f;
		pTempGO->position.z = 3.0f;
		pTempGO->orientation.x = glm::degrees(0.0f);	// Degrees
		pTempGO->orientation2.x = glm::radians(270.0f);	// Degrees
		pTempGO->orientation2.y = glm::radians(270.0f);	// Degrees
		pTempGO->scale = 7.0f;
		pTempGO->diffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		pTempGO->meshName = "airplane";
		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
	}

	//Vec Object 6
	{
		cGameObject* pTempGO = new cGameObject();
		pTempGO->position.x = 0.0f;
		pTempGO->position.y = 2.0f;
		pTempGO->position.z = 3.0f;
		pTempGO->orientation.x = glm::degrees(0.0f);	// Degrees
		pTempGO->orientation2.x = glm::radians(270.0f);	// Degrees
		pTempGO->orientation2.y = glm::radians(270.0f);	// Degrees
		pTempGO->scale = 7.0f;
		pTempGO->diffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		pTempGO->meshName = "airplane";
		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
	}

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	// C++ string
	// C no strings. Sorry. char    char name[7] = "Michael\0";
    window = glfwCreateWindow( width, height, 
							   title.c_str(), 
							   NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwSetKeyCallback(window, key_callback);
    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);

	std::cout << glGetString(GL_VENDOR) << " " 
		<< glGetString(GL_RENDERER) << ", " 
		<< glGetString(GL_VERSION) << std::endl;
	std::cout << "Shader language version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

	::g_pShaderManager = new cShaderManager();

	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	vertShader.fileName = "simpleVert.glsl";	
	fragShader.fileName = "simpleFrag.glsl"; 

	::g_pShaderManager->setBasePath( "assets//shaders//" );

	// Shader objects are passed by reference so that
	//	we can look at the results if we wanted to. 
	if ( ! ::g_pShaderManager->createProgramFromFile(
		        "mySexyShader", vertShader, fragShader ) )
	{
		std::cout << "Oh no! All is lost!!! Blame Loki!!!" << std::endl;
		std::cout << ::g_pShaderManager->getLastError() << std::endl;
		// Should we exit?? 
		return -1;	
//		exit(
	}
	std::cout << "The shaders comipled and linked OK" << std::endl;


	// Load models
	::g_pVAOManager = new cVAOMeshManager();

	GLint sexyShaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	{
		cMesh testMesh;
		testMesh.name = "airplane";
		if ( ! LoadPlyFileIntoMesh( "A380.ply", testMesh ) )
		{ 
			std::cout << "Didn't load model" << std::endl;
			// do something??
		}
		if ( ! ::g_pVAOManager->loadMeshIntoVAO( testMesh, sexyShaderID ) )
		{
			std::cout << "Could not load mesh into VAO" << std::endl;
		}
	}
	{
		cMesh testMesh;
		testMesh.name = "runway";
		if ( ! LoadPlyFileIntoMesh( "road.ply", testMesh ) )
		{ 
			std::cout << "Didn't load model" << std::endl;
			// do something??
		}
		if ( ! ::g_pVAOManager->loadMeshIntoVAO( testMesh, sexyShaderID ) )
		{
			std::cout << "Could not load mesh into VAO" << std::endl;
		}
	}	
	{
		cMesh testMesh;
		testMesh.name = "tower";
		if ( ! LoadPlyFileIntoMesh( "tower.ply", testMesh ) )
		{ 
			std::cout << "Didn't load model" << std::endl;
			// do something??
		}
		if ( ! ::g_pVAOManager->loadMeshIntoVAO( testMesh, sexyShaderID ) )
		{
			std::cout << "Could not load mesh into VAO" << std::endl;
		}
	}	// ENDOF: load models


	GLint currentProgID = ::g_pShaderManager->getIDFromFriendlyName( "mySexyShader" );

	mvp_location = glGetUniformLocation(currentProgID, "MVP");		// program, "MVP");

	glEnable( GL_DEPTH );

	// Gets the "current" time "tick" or "step"
	double lastTimeStep = glfwGetTime();

	// Main game or application loop
	while ( ! glfwWindowShouldClose(window) )
    {
        float ratio;
        int width, height;
        glm::mat4x4 m, p, mvp;			//  mat4x4 m, p, mvp;

        glfwGetFramebufferSize(window, &width, &height);
        ratio = width / (float) height;
        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT );



		// "Draw scene" loop
//		for ( int index = 0; index != MAXNUMBEROFGAMEOBJECTS; index++ )
		// std::vector< cGameObject* > g_vecGameObjects;

		unsigned int sizeOfVector = ::g_vecGameObjects.size();	//*****//
		for ( int index = 0; index != sizeOfVector; index++ )
		{
			// Is there a game object? 
			if ( ::g_vecGameObjects[index] == 0 )	//if ( ::g_GameObjects[index] == 0 )
			{	// Nothing to draw
				continue;		// Skip all for loop code and go to next
			}

			// Was near the draw call, but we need the mesh name
			std::string meshToDraw = ::g_vecGameObjects[index]->meshName;		//::g_GameObjects[index]->meshName;

			sVAOInfo VAODrawInfo;
			if ( ::g_pVAOManager->lookupVAOFromName( meshToDraw, VAODrawInfo ) == false )
			{	// Didn't find mesh
				continue;
			}



			// There IS something to draw

			m = glm::mat4x4(1.0f);	//		mat4x4_identity(m);

			//mat4x4_rotate_Z(m, m, (float) glfwGetTime());
	//		float curTime = (float) glfwGetTime();
	//		glm::rotate( m, curTime, glm::vec3(0.0f, 0.0f, 1.0f) );
	//		glm::rotate( m, 0.0f, glm::vec3(0.0f, 0.0f, 1.0f) );	// Stop bunny

			glm::mat4 matRreRotZ = glm::mat4x4(1.0f);
			matRreRotZ = glm::rotate( matRreRotZ, ::g_vecGameObjects[index]->orientation.z, 
								     glm::vec3(0.0f, 0.0f, 1.0f) );
			m = m * matRreRotZ;

			glm::mat4 matRreRotX = glm::mat4x4(1.0f);
			matRreRotX = glm::rotate(matRreRotX, ::g_vecGameObjects[index]->orientation.x,
				glm::vec3(1.0f, 0.0f, 0.0f));
			m = m * matRreRotX;

			glm::mat4 trans = glm::mat4x4(1.0f);
			trans = glm::translate( trans, 
								    ::g_vecGameObjects[index]->position );
			m = m * trans; 

			glm::mat4 matPostRotZ = glm::mat4x4(1.0f);
			matPostRotZ = glm::rotate( matPostRotZ, ::g_vecGameObjects[index]->orientation2.z, glm::vec3(0.0f, 0.0f, 1.0f) );
			m = m * matPostRotZ;

			//::g_vecGameObjects[index]->orientation2.y += 0.01f;

			glm::mat4 matPostRotY = glm::mat4x4(1.0f);
			matPostRotY = glm::rotate( matPostRotY, ::g_vecGameObjects[index]->orientation2.y, 
								     glm::vec3(0.0f, 1.0f, 0.0f) );
			m = m * matPostRotY;


			glm::mat4 matPostRotX = glm::mat4x4(1.0f);
			matPostRotX = glm::rotate( matPostRotX, ::g_vecGameObjects[index]->orientation2.x,glm::vec3(1.0f, 0.0f, 0.0f) );

			m = m * matPostRotX;
			// TODO: add the other rotation matrix (i.e. duplicate code above)

			// assume that scale to unit bounding box
			// ************* BEWARE *****************
			float finalScale = VAODrawInfo.scaleForUnitBBox * ::g_vecGameObjects[index]->scale;

			glm::mat4 matScale = glm::mat4x4(1.0f);
			matScale = glm::scale( matScale, 
								   glm::vec3( finalScale,
								              finalScale,
								              finalScale ) );
			m = m * matScale;


			//mat4x4_ortho(p, -ratio, ratio, -1.f, 1.f, 1.f, -1.f);
//			p = glm::ortho( -1.0f, 1.0f, -1.0f, 1.0f );
			p = glm::perspective( 0.6f,			// FOV
								  ratio,		// Aspect ratio
								  0.1f,			// Near (as big as possible)
								  1000.0f );	// Far (as small as possible)

			// View or "camera" matrix
			glm::mat4 v = glm::mat4(1.0f);	// identity

			//glm::vec3 cameraXYZ = glm::vec3( 0.0f, 0.0f, 5.0f );	// 5 units "down" z
			v = glm::lookAt( g_cameraXYZ,						// "eye" or "camera" position
							 g_cameraTarget_XYZ,		// "At" or "target" 
							 glm::vec3( 0.0f, 1.0f, 0.0f ) );	// "up" vector

			//mat4x4_mul(mvp, p, m);
//			mvp = p * m;
//			mvp = m * v * p;			// <---- this way
			mvp = p * v * m;			// This way (sort of backwards)

			GLint shaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");
			GLint diffuseColour_loc = glGetUniformLocation( shaderID, "diffuseColour" );

			glUniform4f( diffuseColour_loc, 
						::g_vecGameObjects[index]->diffuseColour.r, 
						::g_vecGameObjects[index]->diffuseColour.g, 
						::g_vecGameObjects[index]->diffuseColour.b, 
						::g_vecGameObjects[index]->diffuseColour.a );


	//        glUseProgram(program);
			::g_pShaderManager->useShaderProgram( "mySexyShader" );

			//glUniformMatrix4fv(mvp_location, 1, GL_FALSE, 
			//                 (const GLfloat*) mvp);
			glUniformMatrix4fv(mvp_location, 1, GL_FALSE, 
							   (const GLfloat*) glm::value_ptr(mvp) );

	//		glPolygonMode( GL_FRONT_AND_BACK, GL_POINT );
			glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	//		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );	// Default

			//glDrawArrays(GL_TRIANGLES, 
			//			 0,			// Start at vertex #0
			//			 testMesh.numberOfVertices );	// g_numberOfVertices);		//453	// 3);		// Draw 3 vertices

//			// Lookup the mesh we are supposed to draw...
//			std::string meshToDraw = ::g_GameObjects[index]->meshName;
//
//			sVAOInfo VAODrawInfo;
//			if ( ::g_pVAOManager->lookupVAOFromName( meshToDraw, VAODrawInfo ) )
//			{	// We found one!!!

				glBindVertexArray( VAODrawInfo.VAO_ID );

				glDrawElements( GL_TRIANGLES, 
								VAODrawInfo.numberOfIndices,		// testMesh.numberOfTriangles * 3,	// How many vertex indices
								GL_UNSIGNED_INT,					// 32 bit int 
								0 );

				glBindVertexArray( 0 );

//			}// if ( ::g_pVAOManager->lookupVAOFromName(

		}//for ( int index = 0...


		std::stringstream ssTitle;
		ssTitle << "Camera (xyz): " 
			<< g_cameraXYZ.x << ", " 
			<< g_cameraXYZ.y << ", " 
			<< g_cameraXYZ.z;
		glfwSetWindowTitle( window, ssTitle.str().c_str() );

        glfwSwapBuffers(window);
        glfwPollEvents();
		
		double curTime = glfwGetTime();
		double deltaTime = curTime - lastTimeStep;
		gTimeElapsed = deltaTime;
		// Essentially the "frame time"
		// Now many seconds that have elapsed since we last checked
		
		
		//PhysicsStep( deltaTime );

		//g_vecGameObjects[0]->position.x = 0.0f;
		
		float x1=13, y1=13, z1=13;
		
		
		L = lua_open();

		/* load Lua base libraries */
		luaL_openlibs(L);

		/* register our function */
		lua_register(L, "MoveTo", MoveTo);
		lua_register(L, "MoveToAfter", MoveToAfter);
		lua_register(L, "OrientTo", OrientTo);
		lua_register(L, "OrientToWith", OrientToWith);
		lua_register(L, "OrientToAfter", OrientToAfter);
		lua_register(L, "MoveToWith", MoveToWith);
		lua_register(L, "Parallel", Parallel);
		lua_register(L, "Serial", Serial);
		lua_register(L, "Follow", Follow);
		luaL_dofile(L, "commands.lua");


		lastTimeStep = curTime;

    }// while ( ! glfwWindowShouldClose(window) )


    glfwDestroyWindow(window);
    glfwTerminate();

	// 
	delete ::g_pShaderManager;
	delete ::g_pVAOManager;

//    exit(EXIT_SUCCESS);
	return 0;
}

// Update the world 1 "step" in time
void PhysicsStep( double deltaTime )
{
	// Distance                          m
	// Velocity = distance / time		 m/s
	// Accleration = velocity / time     m/s/s

	// Distance = time * velocity
	// velocity = time * acceleration

	const glm::vec3 GRAVITY = glm::vec3(0.0f, -1.0f, 0.0f);

	// Identical to the 'render' (drawing) loop
	for ( int index = 0; index != ::g_vecGameObjects.size(); index++ )
	{
		cGameObject* pCurGO = ::g_vecGameObjects[index];

		// New position is based on velocity over time
		glm::vec3 deltaPosition = (float)deltaTime * pCurGO->vel;
		pCurGO->position += deltaPosition;

		// New velocity is based on acceleration over time
		glm::vec3 deltaVelocity =  ( (float)deltaTime * pCurGO->accel )
			                     + ( (float)deltaTime * GRAVITY );

		pCurGO->vel += deltaVelocity;


		// HACK: Collision step
		// Assume the "ground" is -2.0f from the origin in the y
		if ( pCurGO->position.y <= -2.0f )
		{	// Object has "hit" the ground
			pCurGO->vel.y = -pCurGO->vel.y;
		}




	}//for ( int index...



	return;
}
