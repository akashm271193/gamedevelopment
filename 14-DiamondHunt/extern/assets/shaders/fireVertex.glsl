#version 420
layout (location = 0) in vec3 objPos;
layout (location = 1) in vec3 objNorm;
layout (location = 2) in vec2 objTex;
layout (location = 3) in vec3 objTang;
layout (location = 4) in vec3 objBitan;
layout (location = 5) in vec4 objBoneID;
layout (location = 6) in vec4 objBoneWeight;	

//out vec3 objWorldNormal;
out vec3 objWorldPos;
out vec2 objTexture;

//Skinned mesh stuff
const int MAXNUMBEROFBONES = 100;
uniform mat4 bones[MAXNUMBEROFBONES];
uniform int numBonesUsed;
uniform bool isSkinnedMesh;


uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;


void main(){
	gl_Position = projection * view * model * vec4(objPos, 1.0f);
	objTexture = objTex;
	objWorldPos = vec3(model * vec4(objPos,1.0));
}