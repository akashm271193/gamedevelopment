#include "extern.h"
//#include <PhysicsEngine\cPhysicsFactory.h>
#include <MyBulletPhysicsEngine\cBulletPhysicsFactory.h>

nPhysics::iPhysicsFactory* gPhysicsFactory = 0;
nPhysics::iPhysicsWorld* gPhysicsWorld = 0;

int currentBall = 0;
glm::vec3 outsideForce = glm::vec3(0.0f);

bool InitPhysics()
{
	gPhysicsFactory = new nPhysics::cBulletPhysicsFactory();
	gPhysicsWorld = new nPhysics::cBulletPhysicsWorld();
	return true;
}