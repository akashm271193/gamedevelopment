#pragma once
#include "cNode.h"
#include "cPath.h"
#include <map>
#include <algorithm>

class cGraph
{
public:
	cGraph();
	~cGraph();
	std::vector<cNode*> allNodes;
	void printAllNodes();
	void printGraph();
	void addNode();
	void joinNodes(int node1Id, int node2Id);
	void findShortestPath(int node1Id, int node2Id);

private:
	bool addIdToConnectedNodes(int mainNodeId, int nodeIdToConnect);
	bool findIfNodePresentInGraph(int nodeId);
	void parseGraph(int startNodeId, int endNodeId, std::vector<cPath*> &allPaths, std::map<int, std::vector<int>> &mapNodeToConnectedNodes);
	cNode* getCNode(int nodeId);
	std::map<int, std::vector<int>> mapNodeToConnectedNodes;
	void populateNodeMap(std::map<int, std::vector<int>> &mapNodeToConnectedNodes);
};

