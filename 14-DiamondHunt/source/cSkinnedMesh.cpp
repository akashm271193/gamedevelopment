#include "cSkinnedMesh.h"

/*
Parameterless constructor
*/
cSkinnedMesh::cSkinnedMesh() {

}

/*
Parameterless destructor
*/
cSkinnedMesh::~cSkinnedMesh()
{
}


/*
Constructor taking in vertices , textures and if colour is supposed to be loaded or not.
*/
cSkinnedMesh::cSkinnedMesh(std::vector<sVertexAllWithBones> vertices, std::vector<unsigned int> indices,
	std::vector<Texture> textures, bool loadColour)
{
	this->vertices = vertices;
	this->indices = indices;
	this->textures = textures;
	this->toLoadColour = loadColour;
	this->setupMesh();

}

/*
Function to setup the entire mesh in the VAO.
*/
void cSkinnedMesh::setupMesh()
{
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(sVertexAllWithBones), &vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_DYNAMIC_DRAW);

	// vertex positions
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(sVertexAllWithBones), (void*)0);
	glEnableVertexAttribArray(0);
	// vertex normals
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(sVertexAllWithBones), (void*)offsetof(sVertexAllWithBones, Normal));
	glEnableVertexAttribArray(1);
	// vertex texture coords
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(sVertexAllWithBones), (void*)offsetof(sVertexAllWithBones, TexCoords));
	glEnableVertexAttribArray(2);
	// vertex tangents
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(sVertexAllWithBones), (void*)offsetof(sVertexAllWithBones, Tangent));
	glEnableVertexAttribArray(3);
	// vertex binormals
	glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(sVertexAllWithBones), (void*)offsetof(sVertexAllWithBones, BiNormal));
	glEnableVertexAttribArray(4);
	// vertex boneID coords
	unsigned int OFFSET_TO_vBoneIDs_x4_IN_CVERTEX = (unsigned int)offsetof(sVertexAllWithBones, boneID[0]);
	glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(sVertexAllWithBones), reinterpret_cast<void*>(static_cast<uintptr_t>(OFFSET_TO_vBoneIDs_x4_IN_CVERTEX)));
	glEnableVertexAttribArray(5);
	// vertex boneWeight coords
	unsigned int OFFSET_TO_vBoneWeights_x4_IN_CVERTEX = (unsigned int)offsetof(sVertexAllWithBones, boneWeights[0]);
	glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(sVertexAllWithBones), reinterpret_cast<void*>(static_cast<uintptr_t>(OFFSET_TO_vBoneWeights_x4_IN_CVERTEX)));
	glEnableVertexAttribArray(6);
	glBindVertexArray(0);
}



/*
Function to draw each mesh and to bind the textures and the materials to the shader.
*/
void cSkinnedMesh::Draw(GLint shaderId, glm::vec3 colour)
{
	unsigned int colourBoolLoc = glGetUniformLocation(shaderId, "isObjColour");
	glUniform1i(colourBoolLoc, this->toLoadColour); 
	
	unsigned int skinnedMeshBoolLoc = glGetUniformLocation(shaderId, "isSkinnedMesh");
	glUniform1i(skinnedMeshBoolLoc, true);

	if (this->toLoadColour) {
		unsigned int colourLoc = glGetUniformLocation(shaderId, "objColour");
		glUniform3fv(colourLoc, 1, glm::value_ptr(glm::vec3(colour)));
	}

	unsigned int diffuseNr = 0;
	unsigned int specularNr = 0;

	for (unsigned int i = 0; i < textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i); // activate proper texture unit before binding
										  // retrieve texture number (the N in diffuse_textureN)
		std::string number;
		std::string name = textures[i].type;
		if (name == "texture_diffuse")
			number = std::to_string(diffuseNr++);
		else if (name == "texture_specular")
			number = std::to_string(specularNr++);

		unsigned int materialTextureLoc = glGetUniformLocation(shaderId, ("material." + name +  number).c_str());
		glUniform1i(materialTextureLoc, i);

		glBindTexture(GL_TEXTURE_2D, textures[i].id);
	}
	glActiveTexture(GL_TEXTURE0);

	unsigned int diffuseIntLoc = glGetUniformLocation(shaderId, "diffuseTextures");
	glUniform1i(diffuseIntLoc, diffuseNr);

	unsigned int specularIntLoc = glGetUniformLocation(shaderId, "specularTextures");
	glUniform1i(specularIntLoc, specularNr);

	if (diffuseNr != 0) {
		float* diffBlend = getBlendTexRatios(diffuseNr);
		for (int i = 0; i < diffuseNr; i++) {
			unsigned int blendTextureLoc = glGetUniformLocation(shaderId, ("material.texture_diffuseBlendRatio[" + std::to_string(i) + "]").c_str());
			glUniform1i(blendTextureLoc, i);
		}
	}


	// draw mesh
	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}


void cSkinnedMesh::setTexturesForMesh(std::vector<Texture> textures) {
	this->textures = textures;

};
std::vector<Texture> cSkinnedMesh::getTexturesForMesh() {
	return this->textures;
};

float* cSkinnedMesh::getBlendTexRatios(int items) {
	float* ratios = new float[items];
	switch (items) {
	case 1:
		ratios[0] = 1.0f;
		break;
	case 2:
		ratios[0] = 0.8f;
		ratios[1] = 0.2f;
		break;
	case 3:
		ratios[0] = 0.6f;
		ratios[1] = 0.3f;
		ratios[2] = 0.1f;
		break;

	case 4:
		ratios[0] = 0.4f;
		ratios[1] = 0.3f;
		ratios[2] = 0.2f;
		ratios[3] = 0.1f;
		break;

	case 5:
		ratios[0] = 0.4f;
		ratios[1] = 0.2f;
		ratios[2] = 0.2f;
		ratios[3] = 0.1f;
		ratios[4] = 0.1f;
		break;

	}
	return ratios;
}
