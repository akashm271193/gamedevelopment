#ifndef _VERTEX_WITH_TEXTURES_
#define _VERTEX_WITH_TEXTURES_

class cVertexWithTextures
{
public:
	float x, y, z;
	float nx, ny, nz;
	float u1, v1;
	float u2, v2;
	float tx, ty, tz;
	float bx, by, bz;

	cVertexWithTextures();
};

#endif
