#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <conio.h>
#include <algorithm>

#include "buffer.h"
#include "Lobbydetails.h"

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 512

bool shutIt = false;

//Buffers we'll be storing data in
buffer sendBuffer(512);
buffer recvBuffer(512);

//A list of rooms the user is in, so we don't waste server requests doing things we shouldn't
std::vector<std::string> rooms;

bool processCommand(std::string userMessage);
bool connectServer(std::string userMessage);
bool refreshLobbyBrowser(std::string);
bool createLobby(std::string);
bool joinLobby(std::string);
bool leaveLobby(std::string);
bool checkRoom(std::string);
bool sendMessage(std::string, std::string);
bool registerUser(std::string);
bool authenticateUser(std::string);
bool logoutUser(std::string);
void recvMessage();

//Socket is now global
SOCKET ConnectSocket = INVALID_SOCKET;
char recvbuf[DEFAULT_BUFLEN];
int recvbuflen = DEFAULT_BUFLEN;
int iResult;
//For user input using _kbhit
std::string userIn = "";
char keyIn;
std::string gSessionToken = "";
bool gIsLoggedIn = false;
std::string gLobbyJoined = "";
Lobbydetails *lobbyDetails;

int __cdecl main(int argc, char **argv)
{
	rooms.resize(20);
	std::cout << "All Available Commands : \n";
	std::cout << "/connect   [Ip Address] [Port] -- To connect to a game server \n";
	bool isMessagePending = false;
	int recvSize = 0;
	int pendingMessageSize = 0;
	std::string partialMessage = "";
	// Receive until the peer closes the connection
		do 
		{
			if (_kbhit()) 
			{
				char keyIn;
				keyIn = _getch();
				if (keyIn == '\r')
				{
					std::cout << '\r';
					for (int i = 0; i < userIn.length(); i++)
						std::cout << ' ';
					std::cout << '\r';
					shutIt = !processCommand(userIn);
					userIn = "";
				}
				else if (keyIn == 127 || keyIn == 8) //backspace OR delete
				{
					userIn = userIn.substr(0, userIn.length() - 1);
					std::cout << keyIn << ' ' << keyIn;
				}
				else
				{
					userIn += keyIn;
					std::cout << keyIn;
				}
			}

			iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
			if (iResult > 0)
			{
				std::string getData = "";
				if (!isMessagePending) {
					for (int i = 0; i < 4; i++)
						getData += recvbuf[i];
					recvBuffer.writeString(getData);
					recvSize = recvBuffer.readInt32BE();
					pendingMessageSize = recvSize;
				}
				int i = 0;
				if (pendingMessageSize > iResult) {
					recvBuffer.resizeBuffer(recvSize);
					isMessagePending = true;
					pendingMessageSize -= iResult;
					getData = "";
				}
				else {
					isMessagePending = false;
					if (getData != "")
						i = 4;
				}
				for (i; i < iResult; i++)
					getData += recvbuf[i];
				
				partialMessage += getData;
				if (isMessagePending == false) {
					recvBuffer.writeString(partialMessage);
					isMessagePending = false;
					recvSize = 0;
					pendingMessageSize = 0;
					partialMessage = "";
					recvMessage();
				}
			}
			else if (iResult == 0)
				//printf("Connection closed\n");
				continue;
		} while (!shutIt);

		// cleanup
		closesocket(ConnectSocket);
		WSACleanup();

		return 0;
}

void showAvailableCommands() {
	std::cout << "\n All Available Commands : \n";
	std::cout << "/register  [emailID] [Password] -- To register a new user \n";
	std::cout << "/login     [emailID] [Password] -- To login into your account \n";
	std::cout << "/logout    -- To logout from your account \n";
	std::cout << "/viewlobby -- To view  the game lobby list \n";
	std::cout << "/refresh   -- To refresh the game lobby list \n";
	std::cout << "/create    [mapName] [lobbyName] [gameMode] [maxPlayers]  -- To create a new lobby \n";
	std::cout << "/join      [lobbyName] -- To join a new lobby \n";
	std::cout << "/leave     [lobbyName] -- To join a new lobby \n";
	std::cout << "/exit      -- To exit the client \n";
}

std::vector<std::string> explodeString(std::string inputString, char delim)
{
	std::vector<std::string> result;
	std::istringstream iss(inputString);

	if (inputString != "") {
		for (std::string token; std::getline(iss, token, delim); )
		{
			if (!token.empty() && token != "")
				result.push_back(std::move(token));
		}
	}

	return result;
}


void resizeBufferIfRequired(buffer &bufferType, int packetLength)
{
	if (packetLength > DEFAULT_BUFLEN) {
		bufferType.resizeBuffer((unsigned int)packetLength);
	}
}

bool processCommand(std::string userMessage)
{

	std::vector<std::string> result;
	std::istringstream iss(userMessage);
	std::string s;
	iss >> s;
	if (s == "/connect") {
		if (::gSessionToken == "")
			connectServer(userMessage);
		else
			std::cout << "Already connected to a game server !\n";
	}
	else if (s == "/refresh" || s == "/viewlobby") {
		if(::gIsLoggedIn && ::gLobbyJoined == "")
			refreshLobbyBrowser(userMessage);
		else if (::gSessionToken == "")
			std::cout << "Please connect to a game server first \n";
		else if(::gIsLoggedIn == false)
			std::cout << "Please login first to view the lobby browser !\n";
		else if(::gIsLoggedIn && ::gLobbyJoined != "")
			std::cout << "Please leave the current lobby to view the game browser !\n";
	}
	else if (s == "/create") {
		if (::gIsLoggedIn && ::gLobbyJoined == "")
			createLobby(userMessage);
		else if (::gSessionToken == "")
			std::cout << "Please connect to a game server first \n";
		else if (::gIsLoggedIn == false)
			std::cout << "Please login first to create the lobby !\n";
		else if (::gIsLoggedIn && ::gLobbyJoined != "")
			std::cout << "Please leave the current lobby to create the lobby !\n";
	}
	else if (s == "/join") {
		if (::gIsLoggedIn && ::gLobbyJoined == "")
			joinLobby(userMessage);
		else if (::gSessionToken == "")
			std::cout << "Please connect to a game server first \n";
		else if (::gIsLoggedIn == false)
			std::cout << "Please login first to join the lobby !\n";
		else if (::gIsLoggedIn && ::gLobbyJoined != "")
			std::cout << "Already joined the lobby " + gLobbyJoined + "! Please leave first to join another \n";
	}
	else if (s == "/leave") {
		if (::gIsLoggedIn && ::gLobbyJoined != "")
			leaveLobby(userMessage);
		else if (::gSessionToken == "")
			std::cout << "Please connect to a game server first \n";
		else if (::gIsLoggedIn == false)
			std::cout << "Please login first to leave a lobby !\n";
		else if (::gIsLoggedIn && ::gLobbyJoined == "")
			std::cout << "You are not present in any lobby to leave ! Please join a lobby first. \n";
	}
	else if (s == "/send")
	{
		iss >> s;
		if (checkRoom(s))
			sendMessage(s, userMessage);
		else
			std::cout << "Not a part of the room " << s << '\n';
	}
	else if (s == "/register")
	{
		if (::gIsLoggedIn == false && ::gSessionToken != "")
			registerUser(userMessage);
		else if (::gSessionToken == "")
			std::cout << "Please connect to a game server first \n";
		else if (::gIsLoggedIn)
			std::cout << "Already logged in ! Logout first to register.\n";
	}
	else if (s == "/login")
	{
		if (::gIsLoggedIn == false && ::gSessionToken != "")
			authenticateUser(userMessage);
		else if (::gSessionToken == "")
			std::cout << "Please connect to a game server first \n";
		else if (::gIsLoggedIn)
			std::cout << "Already logged in ! Logout first to login again.\n";
	}
	else if (s == "/logout") {
		if (::gIsLoggedIn && ::gSessionToken != "")
			logoutUser(userMessage);
		else if (::gSessionToken == "")
			std::cout << "Please connect to a game server first \n";
		else if (::gIsLoggedIn == false)
			std::cout << "Please login first to logout .\n";
	}
	else if (s == "/exit") {
		::gSessionToken = "";
		::gLobbyJoined = "";
		::gIsLoggedIn = false;
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	else
	{
		std::cout << "Invalid command. Please choose one of the commands to execute : \n ";
		showAvailableCommands();
	}
	return 1;
}

bool validateIpAddress(std::string &ipAddress)
{
	struct sockaddr_in sa;
	int result = inet_pton(AF_INET, ipAddress.c_str(), &(sa.sin_addr));
	if (result == 1)
		return true;
	else
		return false;
}

bool connectServer(std::string input)
{
	using namespace std::literals;
	std::istringstream iss(input);
	std::string ip, port;
	iss >> ip; // Should be /connect
	iss >> ip;
	iss >> port;


	for (int i = 0; i < ip.length(); i++)
		if (ip[i] == ' ')
			ip[i] = '_';

	
	bool isValidIp = validateIpAddress(ip);
	if (!isValidIp)
		return -1;
	

	WSADATA wsaData;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 0;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(ip.c_str(), port.c_str(), &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 0;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 0;
		}

		// Connect to server.
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 0;
	}

	//This makes the socket non-blocking
	u_long iMode = 1;
	iResult = ioctlsocket(ConnectSocket, FIONBIO, &iMode);
	if (iResult != 0) {
		printf("Making socket non-blocking failed : %d\n", iResult);
		return 0;
	}

	return 1;
}

bool authenticateUser(std::string input)
{
	std::istringstream iss(input);
	std::string email, password;
	iss >> email; // Should be /authenticate
	iss >> email;
	iss >> password;

	int messageID = 2;
	int emailLength = email.length();
	int passwordLength = password.length();
	int sessionTokenLength = gSessionToken.length();
	int packetLength = emailLength + passwordLength + sessionTokenLength + 20;

	resizeBufferIfRequired(sendBuffer, packetLength);

	std::cout << gSessionToken << std::endl;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(gSessionToken);
	sendBuffer.writeInt32BE(emailLength);
	sendBuffer.writeString(email);
	sendBuffer.writeInt32BE(passwordLength);
	sendBuffer.writeString(password);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	sendBuffer.resizeBuffer(DEFAULT_BUFLEN);

	return 1;
}

bool registerUser(std::string input)
{
	std::istringstream iss(input);
	std::string email, password;
	iss >> email; // Should be /register
	iss >> email;
	iss >> password;

	int messageID = 4;
	int emailLength = email.length();
	int passwordLength = password.length();
	int sessionTokenLength = ::gSessionToken.length();
	int packetLength = emailLength + passwordLength + sessionTokenLength + 20;

	resizeBufferIfRequired(sendBuffer, packetLength);

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(::gSessionToken);
	sendBuffer.writeInt32BE(emailLength);
	sendBuffer.writeString(email);
	sendBuffer.writeInt32BE(passwordLength);
	sendBuffer.writeString(password);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	sendBuffer.resizeBuffer(DEFAULT_BUFLEN);

	return 1;
}

bool logoutUser(std::string input) {
	int messageID = 6;
	int sessionTokenLength = ::gSessionToken.length();
	int packetLength = sessionTokenLength + 12;

	resizeBufferIfRequired(sendBuffer, packetLength);

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(::gSessionToken);
	
	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	sendBuffer.resizeBuffer(DEFAULT_BUFLEN);

	return 1;
}

bool refreshLobbyBrowser(std::string input) {
	int messageID = 8;
	int sessionTokenLength = ::gSessionToken.length();
	int packetLength = sessionTokenLength + 12;

	resizeBufferIfRequired(sendBuffer, packetLength);

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(::gSessionToken);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	sendBuffer.resizeBuffer(DEFAULT_BUFLEN);


	return 1;
}

bool createLobby(std::string input) {
	std::istringstream iss(input);
	std::string map, lobbyName, gameMode , maxPlayers;
	iss >> map; // Should be /register
	iss >> map;
	iss >> lobbyName;
	iss >> gameMode;
	iss >> maxPlayers;

	lobbyDetails = new Lobbydetails();
	bool isValid  = lobbyDetails->checkLobbyIsValid(map,gameMode);
	if(!isValid)
		return 1;

	int messageID = 10;
	int sessionTokenLength = ::gSessionToken.length();
	int mapLength = map.length();
	int lobbyNameLength = lobbyName.length();
	int gameModeLength = gameMode.length();
	int maxPlayersLength = maxPlayers.length();
	int packetLength = sessionTokenLength +  mapLength +lobbyNameLength + gameModeLength + maxPlayersLength + 28;

	resizeBufferIfRequired(sendBuffer, packetLength);

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(::gSessionToken);
	sendBuffer.writeInt32BE(mapLength);
	sendBuffer.writeString(map);
	sendBuffer.writeInt32BE(lobbyNameLength);
	sendBuffer.writeString(lobbyName);
	sendBuffer.writeInt32BE(gameModeLength);
	sendBuffer.writeString(gameMode);
	sendBuffer.writeInt32BE(maxPlayersLength);
	sendBuffer.writeString(maxPlayers);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}

	sendBuffer.resizeBuffer(DEFAULT_BUFLEN);


	return 1;
}

bool joinLobby(std::string input)
{
	input.erase(0, 6); //Removes "/join " from the beginning of the string

	for (int i = 0; i < input.length(); i++)
		if (input[i] == ' ')
			input[i] = '_';

	int lobbyLength = input.length();
	int messageId = 12;
	int sessionTokenLength = ::gSessionToken.length();
	int packetLength = lobbyLength + sessionTokenLength + 16;

	resizeBufferIfRequired(sendBuffer, packetLength);

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageId);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(::gSessionToken);
	sendBuffer.writeInt32BE(lobbyLength);
	sendBuffer.writeString(input);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}

	sendBuffer.resizeBuffer(DEFAULT_BUFLEN);

	return 1;
}

bool leaveLobby(std::string input)
{
	input.erase(0, 7);

	for (int i = 0; i < input.length(); i++)
		if (input[i] == ' ')
			input[i] = '_';

	int lobbyLength = input.length();
	int messageId = 14;
	int sessionTokenLength = ::gSessionToken.length();
	int packetLength = lobbyLength + sessionTokenLength + 16;

	resizeBufferIfRequired(sendBuffer, packetLength);

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageId);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(::gSessionToken);
	sendBuffer.writeInt32BE(lobbyLength);
	sendBuffer.writeString(input);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}

	sendBuffer.resizeBuffer(DEFAULT_BUFLEN);

	return 1;
}

bool checkRoom(std::string input)
{
	for (std::vector<std::string>::iterator it = rooms.begin(); it != rooms.end(); ++it)
		if (*it == input)
			return 1;

	return 0;
}

bool sendMessage(std::string room, std::string input)
{
	int eraseHead = room.length() + 7; // "/send RoomName " to be removed
	input.erase(0, eraseHead);

	int roomLength = room.length();
	int messageLength = input.length();
	int messageID = 3;
	int packetLength = roomLength + messageLength + 16;
	
	resizeBufferIfRequired(sendBuffer, packetLength);

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(roomLength);
	sendBuffer.writeString(room);
	sendBuffer.writeInt32BE(messageLength);
	sendBuffer.writeString(input);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}

	sendBuffer.resizeBuffer(DEFAULT_BUFLEN);

	return 1;
}

void recvMessage()
	{
	int packetLength = recvBuffer.readInt32BE();
	int messageID = recvBuffer.readInt32BE();
	if (messageID == 0)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		::gSessionToken = recvBuffer.readString(sessionTokenLength);
		std::cout << "You have been successfully connected to the game server. Please login or register to access the lobbies !\n";
		showAvailableCommands();
	}
	else if (messageID == 1)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		std::cout << message << "\n";
	}
	else if (messageID == 2)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		::gIsLoggedIn = true;
		std::cout << message << "\n";
		lobbyDetails = new Lobbydetails();
		lobbyDetails->getAllDetails();
		std::cout << "\n \n Game Lobby Browser \n";
		refreshLobbyBrowser("");
	}
	else if (messageID == 3)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		std::cout << message << "\n";
	}
	else if (messageID == 4)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		::gIsLoggedIn = false;
		::gLobbyJoined = "";
		std::cout << message << "\n";
	}
	else if (messageID == 5)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		::gLobbyJoined = message;
		std::cout << "Created and joined the lobby "+ message + " successfully." << "\n";
	}
	else if (messageID == 6)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		std::vector<std::string> msgData = explodeString(message,'||');
		::gLobbyJoined = msgData[0];
		std::cout << msgData[1] << "\n";
	}
	else if (messageID == 7)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		std::vector<std::string> msgData = explodeString(message, '||');
		::gLobbyJoined = "";
		std::cout << msgData[1] << "\n";
	}
	else if (messageID == 8)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		::gLobbyJoined = "";
		std::cout << message << "\n";
		refreshLobbyBrowser("");
	}
	else if (messageID == 9)
	{
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int rowCount = recvBuffer.readInt32BE();
		for (int i = 0; i < rowCount; i++) {
			for (int j = 1; j < 10; j++) {
				int tmpLength = 0; std::string tmpString = "";
				tmpLength = recvBuffer.readInt32BE();
				tmpString = recvBuffer.readString(tmpLength);
				std::cout << tmpString << " || ";
			}
			std::cout << "\n";
		}
		
	}
	return;
}