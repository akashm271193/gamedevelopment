#version 430
layout (location = 0) in vec3 objPos;
layout (location = 1) in vec3 objNorm;
layout (location = 2) in vec2 objTex;

out vec2 objTexture;
//out vec3 Normal;
//out vec3 ObjectPosition;
//out vec3 LightPosition;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

void main()
{
	gl_Position = projection * view * model * vec4(objPos.x,objPos.y,objPos.z,1.0);
	//objWorldPos = vec3(model * vec4(objPos,1.0));
	//objWorldNormal = normal * objNorm;
	objTexture = objTex;
}