#ifndef _cGameObject_HP_
#define _cGameObject_HP_

#include <glm/vec3.hpp> 
#include <glm/vec4.hpp> 
#include <string>
#include <glad/glad.h>
#include "cModel.h"

enum eTypeOfObject
{	// Ok people, here's the deal:
	SPHERE = 0,		// - it's a sphere
	PLANE = 1,		// - it's a plane
	CAPSULE = 2,    // - it's a capsule
	AABB = 3,	
	PLAYER = 4,	// 3- it's an axis-aligned box
	UNKNOWN = 99	// I don't know
};

class cGameObject
{
public:
	cGameObject();
	~cGameObject();
	cModel* model;
	glm::vec3 position;
	glm::vec3 originOrientation;
	glm::vec3 ownAxisOrientation;
	glm::vec4 diffuseColour;
	GLenum objectLayoutType;
	bool isWireFrame;
	bool isUpdatedInPhysics;
	glm::vec3 colour;
	float scale;
	int objectId;
	eTypeOfObject typeOfObject;		// (really an int)
	glm::vec3 velocity;
	glm::vec3 acceleration;
	bool toAnimate;
	float radius;
	
};

#endif