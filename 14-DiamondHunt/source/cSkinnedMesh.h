#pragma once

#include "cMeshStructure.h"


class cSkinnedMesh
{
public:
	//mesh data
	std::vector<sVertexAllWithBones> vertices;
	std::vector<unsigned int> indices;
	std::vector<Texture> textures;
	
	//Functions
	cSkinnedMesh(std::vector<sVertexAllWithBones> vertices, std::vector<unsigned int> indices,
		std::vector<Texture> textures, bool loadColour);
	void setTexturesForMesh(std::vector<Texture> textures);
	std::vector<Texture> getTexturesForMesh();
	
	cSkinnedMesh();
	~cSkinnedMesh();

	void Draw(GLint shaderId, glm::vec3 colour);

private:
	/*  Render data  */
	unsigned int VAO, VBO, EBO;
	/*  Functions    */
	void setupMesh();
	bool toLoadColour;
	float* cSkinnedMesh::getBlendTexRatios(int items);



};