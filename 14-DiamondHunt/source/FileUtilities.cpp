#include "FileUtilities.h"

rapidjson::Document readConfigFromJsonFile(std::string fileNameWithPath) {
	rapidjson::Document doc;
	FILE *fp;
	if (fopen_s(&fp, fileNameWithPath.c_str(), "rb") != 0)
		return doc;
	char readBuffer[65536];
	rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));
	doc.ParseStream(is);
	fclose(fp);
	return doc;
}

void readGameConfigFile(std::string fileName) {
	rapidjson::Document doc = readConfigFromJsonFile(fileName);
	const rapidjson::Value& camera = doc["Camera"];
	const rapidjson::Value& player = doc["Player"];
	const rapidjson::Value& map = doc["Map"];
	PlayerSpeed = player["speed"].GetFloat();
	CameraPosition = glm::vec3(camera["posX"].GetFloat(), camera["posY"].GetFloat(), camera["posZ"].GetFloat());
	cameraBindDistance = camera["bindingDistance"].GetFloat();
	CullingRadius = map["cullingRadius"].GetFloat();
	aiAverageSpeed = doc["AI"]["averageSpeed"].GetFloat();
	aiMaxBlocks = doc["AI"]["maxBlocks"].GetFloat();
	std::string mapConfigFile = map["configFile"].GetString();
	gMapLoader = new cMapLoader(mapConfigFile);
}
	
void writeDataToJsonFile(rapidjson::Document &doc, std::string fileNameWithPath) {
	FILE *fp = fopen(fileNameWithPath.c_str(), "wb");
	char writeBuffer[65536];
	rapidjson::FileWriteStream os(fp, writeBuffer, sizeof(writeBuffer));	
	rapidjson::Writer<rapidjson::FileWriteStream> writer(os);
	doc.Accept(writer);
	fclose(fp);
}