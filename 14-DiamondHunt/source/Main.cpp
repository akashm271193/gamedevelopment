#include "cGLFWCalls.h"
#include "stb_image.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include "cMapLoader.h"
#include "Physics.h"
#include "SceneLoader.h"
#include "ExternObjCalls.h"
#include "cDebugRenderer.h"-
#include "cFBO.h"

cShaderManager* gPShaderManager;
cGLFWCalls* glfwCaller;
GLFWwindow* mainWindow;
cMapLoader* gMapLoader; 
cMap* ourMap;
cDebugRenderer* gDebugRenderer;



cFBO* gFBO;

cMenuLoader* gMenuLoader;
//vertex array object

std::vector<cGameObject*> gGameObjects;
std::vector<cGameObject*> gBoundingBox;
std::map<std::string, cModel*> gMapLoadedModels;
float PlayerSpeed = 0.0f;
float aiAverageSpeed = 0.0f;
float CullingRadius = 10000.0f;
glm::vec3 CameraPosition = glm::vec3(0.0f);
bool isPlayerHealthZero = false;
bool isMenuActive = false;
std::string windowTitle = "";

int playerGameObjId = -1;
bool toReload = false;
bool toDrawObjects = true;
bool toDrawDebugObjects = false;
int drawToggle = 0;
bool isPEDrawn = false;
//init config value
// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// camera
cCamera* camera;
//cCamera camera(glm::vec3(20.0f, 10.0f, 5.0f),glm::vec3(0.0f,1.0f,0.0f), -180.0f, -30.0f);
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;
float cameraBindDistance = 0.0f;


float deltaTime = 0.0f;	// Time between current frame and last frame
float lastFrame = 0.0f; // Time of last frame
	
//change the one below too if you change this
float fireOnTime = 50.0f;
float fireOffTime = 100.0f;

// lighting
glm::vec3 lightPos(0.0f, 10.0f, 100.0f);


float aiMaxBlocks;
void AIWorld(float deltaTime);
float CuriousSpeed = 2.0f;

/*
	main function
*/
int main()
{
	readGameConfigFile("../extern/config/gameConfig.json");
	ourMap = gMapLoader->getMap();
	glfwCaller = new cGLFWCalls();
	glfwCaller->InitiateGLFW();
	glfwCaller->createNewWindow(SCR_WIDTH, SCR_HEIGHT, windowTitle);
	mainWindow = glfwCaller->getCurrentWindow();

	glfwSetFramebufferSizeCallback(mainWindow, glfwCaller->framebufferSizeCallBack);
	glfwSetKeyCallback(mainWindow, glfwCaller->keyCallBack);
	glfwSetInputMode(mainWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPosCallback(mainWindow, glfwCaller->mouseCallBack);
	glfwSetScrollCallback(mainWindow, glfwCaller->scrollCallBack);

	glEnable(GL_DEPTH_TEST);
	////FBO STUFF
	//glEnable(GL_CULL_FACE);
	//glDepthFunc(GL_LESS);

	camera = new cCamera(CameraPosition); //glm::vec3(8.0f, 10.0f, 30.0f)
	//Code to load shaders
	::gPShaderManager = new cShaderManager();
	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	cShaderManager::cShader textVertShader;
	cShaderManager::cShader textFragShader;

	cShaderManager::cShader menuVertShader;
	cShaderManager::cShader menuFragShader;

	cShaderManager::cShader fireVertShader;	//2nd pass shader
	cShaderManager::cShader fireFragShader;

	vertShader.fileName = "simpleVertex.glsl";
	fragShader.fileName = "simpleFragment.glsl";

	textVertShader.fileName = "textVertex.glsl";
	textFragShader.fileName = "textFragment.glsl";

	menuVertShader.fileName = "menuVertex.glsl";
	menuFragShader.fileName = "menuFragment.glsl";

	fireVertShader.fileName = "fireVertex.glsl";
	fireFragShader.fileName = "fireFragment.glsl";

	::gPShaderManager->setBasePath("../extern/assets/shaders/");

	if (!::gPShaderManager->createProgramFromFile("myCustomShader", vertShader, fragShader)) {
		std::cout << ::gPShaderManager->getLastError() << std::endl; 
		std::cout << " Error in loading shader " << std::endl;
		return -1;
	}

	if (!::gPShaderManager->createProgramFromFile("myTextShader", textVertShader, textFragShader)) {
		std::cout << ::gPShaderManager->getLastError() << std::endl;
		std::cout << " Error in loading shader " << std::endl;
		return -1;
	}
	
	if (!::gPShaderManager->createProgramFromFile("myMenuShader", menuVertShader, menuFragShader)) {
		std::cout << ::gPShaderManager->getLastError() << std::endl;
		std::cout << " Error in loading shader " << std::endl;
		return -1;
	}

	if (!::gPShaderManager->createProgramFromFile("myFireShader", fireVertShader, fireFragShader)) {
		std::cout << ::gPShaderManager->getLastError() << std::endl;
		std::cout << " Error in loading shader " << std::endl;
		return -1;
	}

	glEnable(GL_DEPTH_TEST);

	
	GLint customShaderId = ::gPShaderManager->getIDFromFriendlyName("myCustomShader");
	::gPShaderManager->useShaderProgram(customShaderId);
	
	GLint textShaderId = ::gPShaderManager->getIDFromFriendlyName("myTextShader");

	GLint menuShaderId = ::gPShaderManager->getIDFromFriendlyName("myMenuShader");

	GLint fireShaderId = ::gPShaderManager->getIDFromFriendlyName("myFireShader");

	InitPhysics();
	gPhysicsWorld = gPhysicsFactory->CreateWorld();

	// load models
	// -----------
	loadModelsIntoScene();
	
	// load GameObjects
	// ----------------
	//generateGameObjects();
	std::string gameObjConfig = "../extern/config/gameObjects.json";
	loadGameState(gameObjConfig);
	
	float timeToAAnimateOneStep = 1.0f;
	float stepTime = timeToAAnimateOneStep/10 ;
	
	gDebugRenderer = new cDebugRenderer(customShaderId);
	gPhysicsWorld->addDebugRender(gDebugRenderer);

	unsigned int borderWidthLoc = glGetUniformLocation(customShaderId, "border_width");
	glUniform1f(borderWidthLoc,0.1f);


	unsigned int aspectRatioLoc = glGetUniformLocation(customShaderId, "aspect");
	glUniform1f(aspectRatioLoc, SCR_WIDTH/SCR_HEIGHT);

	gMenuLoader = new cMenuLoader();
	isMenuActive = true;
	glfwSetInputMode(mainWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	glfwSetCursorPosCallback(mainWindow, NULL);
	glfwSetMouseButtonCallback(mainWindow, cGLFWCalls::mouseButtonCallBack);
	gMenuLoader->setActiveMenu(3);
	

	//FBO STUFF
	gFBO = new cFBO(3, SCR_WIDTH, SCR_HEIGHT);
	//fireFBO = new cFBO(3, SCR_WIDTH, SCR_HEIGHT);
	

	camera->createPhysicsWorldObject();
	//Main Window Loop 
	while (!glfwWindowShouldClose(mainWindow))
	{
		//per-frame logic
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		//Call to check for input
		if (!isMenuActive)
			glfwCaller->processInput(mainWindow);
	
		/*glEnable(GL_CULL_FACE);
		glDepthFunc(GL_LESS);*/
		
		//gl command to clear the color buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// get matrix's uniform location and set matrix
		::gPShaderManager->useShaderProgram(customShaderId);

		glm::mat4 view = glm::mat4(1.0f);
		view = camera->GetViewMatrix();

		//FBO CODES
		/*gFBO->Bind();*/
		//glClearColor(0.3f, 0.5f, 0.4f, 1.0f);
		//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



		glm::mat4 projection;
		projection = glm::perspective(glm::radians(camera->Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);

		unsigned int viewLoc = glGetUniformLocation(customShaderId, "view");
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

		unsigned int projectionLoc = glGetUniformLocation(customShaderId, "projection");
		glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));


		//AIWorld(deltaTime);

		gPhysicsWorld->setViewAndProjectionMatrix(view, projection, customShaderId);
		gPhysicsWorld->stepSimulation(deltaTime);
		if(toDrawDebugObjects)
			gPhysicsWorld->drawDebugWorld();


		camera->updateFromPhysicsWorld(gGameObjects);
		isPEDrawn = false;
		for (int i = 0; i < gGameObjects.size(); i++) {
			gGameObjects[i]->updateFromPhysicsWorld();
			if (gGameObjects[i]->typeOfObject == libPhysics::OBJ_PLAYER) {
				cPlayerGameObject* tmpPlayerObj = dynamic_cast<cPlayerGameObject*>(gGameObjects[i]);
				std::cout << "\n Player Health : " << tmpPlayerObj->playerHealth << std::endl;
				if (tmpPlayerObj->playerHealth <= 0.0f) {
					isPlayerHealthZero = true;
					glfwSetWindowShouldClose(mainWindow, GLFW_TRUE);
				}
			}

			if (toDrawObjects) {
				if (isObjectInCullingRadius(gGameObjects[i])){
					if (gGameObjects[i]->hasBoundingBoxOrSphere) {
						if (!gPhysicsWorld->checkIfObjectAlreadyPresent(gGameObjects[i]->collisionBody)) {
							gGameObjects[i]->createPhysicsWorldObject();
						}
					}

					if (gGameObjects[i]->typeOfObject == libPhysics::OBJ_FIRE) 
					{
						isPEDrawn = true;
						//std::cout <<  fireOnTime << " & " << fireOffTime << std::endl;

						if (fireOnTime > 0.0f)
						{
							//if emitter is on
							::gPShaderManager->useShaderProgram(fireShaderId);
							gFBO->Bind();
							glEnable(GL_DEPTH_TEST);

							glUniform1f(glGetUniformLocation(fireShaderId, "screenWidth"), SCR_WIDTH);
							glUniform1f(glGetUniformLocation(fireShaderId, "screenHeight"), SCR_HEIGHT);


							glEnable(GL_BLEND);
							//unsigned int blurLoc = glGetUniformLocation(customShaderId, "isBlur");

							unsigned int fireviewLoc = glGetUniformLocation(fireShaderId, "view");
							glUniformMatrix4fv(fireviewLoc, 1, GL_FALSE, glm::value_ptr(view));

							unsigned int fireprojectionLoc = glGetUniformLocation(fireShaderId, "projection");
							glUniformMatrix4fv(fireprojectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
							gGameObjects[i]->Draw(fireShaderId, true);
							glDisable(GL_BLEND);

							gFBO->Unbind();
							gFBO->Draw();
							gGameObjects[i]->Draw(fireShaderId, true);

							fireOnTime -= 0.1f;

							//check if on time is 0
							if (fireOnTime < 0.0f)
							{
								fireOffTime = 100.0f;
								if (gPhysicsWorld->checkIfObjectAlreadyPresent(gGameObjects[i]->collisionBody)) {
									gPhysicsWorld->RemoveBoundingBox(gGameObjects[i]->collisionBody);
									gGameObjects[i]->hasBoundingBoxOrSphere = false;
								}

							}
						}
						else if (fireOffTime > 0.0f)
						{

							//if emitter is off
							fireOffTime -= 0.1f;
							//check if on time is 0
							if (fireOffTime < 0.0f)
							{
								fireOnTime = 50.0f;
								gGameObjects[i]->hasBoundingBoxOrSphere = true;
								if (!gPhysicsWorld->checkIfObjectAlreadyPresent(gGameObjects[i]->collisionBody)) {
									gGameObjects[i]->createPhysicsWorldObject();
								}
							}
						}
					}	//END Fire
					else {
						if (gGameObjects[i]->toDraw) {
							::gPShaderManager->useShaderProgram(customShaderId);
							gGameObjects[i]->Draw(customShaderId, true);
						}
						else
							gGameObjects[i]->toDraw = true;
					}
				}
				else {
					if (gGameObjects[i]->hasBoundingBoxOrSphere) {
						if (gPhysicsWorld->checkIfObjectAlreadyPresent(gGameObjects[i]->collisionBody)) {
							gPhysicsWorld->RemoveBoundingBox(gGameObjects[i]->collisionBody);
						}
					}
				}
			}	
		}		
		
		if(isMenuActive)
			gMenuLoader->renderMenu(menuShaderId,textShaderId);
		
		glfwSwapBuffers(mainWindow);
		glfwPollEvents();


	}
	// glfw: terminate, clearing all previously allocated GLFW resources.
	// ------------------------------------------------------------------
	glfwTerminate();

	if (isPlayerHealthZero) {
		std::cout << "\n Player health is zero. Game over ! \n";
		system("pause");
	}

	return 0;
}
void findValueOnLine(glm::vec3 pointA, glm::vec3 pointB, glm::vec3 &pointC)
{
	glm::vec3 tmpVec = pointB - pointA;
	glm::vec3 normtmpVec = glm::normalize(tmpVec);
	float distance = 2.0f;
	pointC = pointA + (distance*normtmpVec);
}


void AIWorld(float deltaTime)
{
	cPlayerGameObject* player = dynamic_cast<cPlayerGameObject*>(gGameObjects[playerGameObjId]);

	for (int i = 0; i < gBoundingBox.size(); i++) {
		if (isObjectInCullingRadius(gBoundingBox[i])) {
			cAIGameObject* tmpAIGameObject = dynamic_cast<cAIGameObject*>(gBoundingBox[i]);
			tmpAIGameObject->animateObject(deltaTime, player);
		}	
	}
}