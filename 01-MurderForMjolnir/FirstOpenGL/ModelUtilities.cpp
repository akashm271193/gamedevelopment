#include "ModelUtilities.h"

void ReadFileToToken(std::ifstream &file, std::string token)
{
	bool bKeepReading = true;
	std::string garbage;
	do
	{
		file >> garbage;		// Title_End??
		if (garbage == token)
		{
			return;
		}
	} while (bKeepReading);
	return;
}


bool LoadPlyFileIntoMesh(std::string filename, cMesh &testMesh)
{
	// c_str() changes a string to a "c style char* string"
	std::ifstream plyFile(filename.c_str());
	
	if (!plyFile.is_open())
	{	// Didn't open file, so return
		return false;
	}
	// File is open, let's read it

	ReadFileToToken(plyFile, "vertex");
	plyFile >> testMesh.numberOfVertices;		// numVertices

	ReadFileToToken(plyFile, "face");
	plyFile >> testMesh.numberOfTriangles;		// numIndices

	testMesh.pVertices = new cVertex_xyz_rgb[testMesh.numberOfVertices];
	testMesh.pTriangles = new cTriangle[testMesh.numberOfTriangles];

	ReadFileToToken(plyFile, "end_header");

	// Read vertices
	for (int index = 0; index < testMesh.numberOfVertices; index++)
	{
		//end_header
		//-0.0312216 0.126304 0.00514924 0.850855 0.5 		
		float x, y, z, confidence, intensity;

		plyFile >> x;
		plyFile >> y;
		plyFile >> z;
		//plyFile >> confidence;
		//plyFile >> intensity;

		testMesh.pVertices[index].x = x;
		testMesh.pVertices[index].y = y;
		testMesh.pVertices[index].z = z;
		testMesh.pVertices[index].r = 1.0f;
		testMesh.pVertices[index].g = 1.0f;
		testMesh.pVertices[index].b = 1.0f;
	}

	for (int count = 0; count < testMesh.numberOfTriangles; count++)
	{
		//end_header
		//		
		int discard;

		plyFile >> discard;
		plyFile >> testMesh.pTriangles[count].vertex_ID_0;
		plyFile >> testMesh.pTriangles[count].vertex_ID_1;
		plyFile >> testMesh.pTriangles[count].vertex_ID_2;

	
	}
	// Also add the triangles (aka "faces")

	//testMesh.FlattenIndexedModel();
	testMesh.CalculateExtents();
	return true;
}