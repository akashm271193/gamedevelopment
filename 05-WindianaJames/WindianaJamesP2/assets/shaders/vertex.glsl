#version 430 core

attribute vec3 position;
attribute vec3 normal;
attribute vec4 texturePosition;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

out vec4 texPosition;

void main( )
{
	gl_Position = projection * view * model * vec4( position, 1.0 );
	
	texPosition = texturePosition;
}