#version 420
varying vec2 texpos;
uniform bool igcolour;
uniform sampler2D tex;
uniform vec4 color;
void main () {
	if(igcolour){
		gl_FragColor = vec4(texture2D(tex, texpos).xyz, 1);
	}else{
		gl_FragColor = vec4(1, 1, 1, texture2D(tex, texpos).r) * color;
	}
}
