#include "cNode.h"


cNode::cNode()
{

}


cNode::~cNode()
{
}

bool cNode::addConnection(int newNodeId) {
	
	if (std::find(this->connectedNodes.begin(), this->connectedNodes.end(), newNodeId) != this->connectedNodes.end())
		return false;
	else{
		this->connectedNodes.push_back(newNodeId);
		return true;
	}
}

void cNode::printConnections() {
	for (int j = 0; j < this->connectedNodes.size(); j++) {
		std::cout << this->id << "-->" << this->connectedNodes[j];
		if (j < connectedNodes.size() - 1)
			std::cout << ",";
	}
}