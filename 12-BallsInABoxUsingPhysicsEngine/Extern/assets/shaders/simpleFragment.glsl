#version 430
struct Material {
    sampler2D texture_diffuse1;
	sampler2D texture_diffuse2;
	sampler2D texture_diffuse3;
	sampler2D texture_specular1;
	sampler2D texture_specular2;
    float shininess;
};

uniform bool isCurrent;
uniform Material material;
uniform bool isObjColour;
uniform vec3 objColour;
out vec4 FragColor;
in vec2 objTexture;

void main()
{    
	if(isObjColour)
		FragColor = vec4(objColour,1.0f);
	else{
		 vec4 texResult = texture(material.texture_diffuse1, objTexture);
		 texResult *= texture(material.texture_diffuse2, objTexture);
		 FragColor = texResult;
	}
}