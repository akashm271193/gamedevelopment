#include "cGameObject.h"



cGameObject::cGameObject()
{
	this->scale = 1.0f;
	this->position = glm::vec3(0.0f);
	this->orientation = glm::vec3(0.0f);
	this->orientation2 = glm::vec3(0.0f);
	this->objectLayoutType = GL_LINE;
	// If you aren't sure what the 4th value should be, 
	//	make it a 1.0f ("alpha" or transparency)
	this->diffuseColour = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	return;
}


cGameObject::~cGameObject()
{
}
