#ifndef _EXTERN_HG_
#define _EXTERN_HG_

#include <Interfaces\iPhysicsFactory.h>

extern nPhysics::iPhysicsFactory* gPhysicsFactory;
extern nPhysics::iPhysicsWorld* gPhysicsWorld;
bool InitPhysics();

extern int currentBall;
extern glm::vec3 outsideForce;

#endif