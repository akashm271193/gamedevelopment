#include "cGLFWCalls.h"


cGLFWCalls::cGLFWCalls(){
}


cGLFWCalls::~cGLFWCalls(){
}

void cGLFWCalls::InitiateGLFW(){
	//Initializing GLFW
	glfwInit();

	//Setting Major and minor Context versions for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	//Setting the glfw for cor profile to use extra features
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	
}

bool cGLFWCalls::createNewWindow(int width, int height, std::string title){
	this->mainWindow = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
	if (this->mainWindow == NULL) {
		std::cout << "Error while creating window : \n";
		glfwTerminate();
		return false;
	}
	this->setWindowAsCurrent(this->mainWindow);
	return true;
}


void cGLFWCalls::setWindowAsCurrent(GLFWwindow* window) {
	glfwMakeContextCurrent(window);
	//Load glad for gl commands
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
	}
}

GLFWwindow* cGLFWCalls::getCurrentWindow() {
	return this->mainWindow;
}