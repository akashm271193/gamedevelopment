#ifndef _GAME_LEVEL_
#define _GAME_LEVEL_

#include <vector>

#include <glad\glad.h>
#include <GLFW\glfw3.h>

#include <glm/glm.hpp>

#include "cGameObject.h"
#include "cVAOManager.h"
#include "cResourceManager.h"

class cGameLevel
{
public:
	std::vector<cGameObject> Walls;

	cGameLevel();

	void Load(const GLchar* file, GLuint levelWidth, GLuint levelHeight);
	void Draw(cVAOManager &renderer);

	glm::vec3 startPosition;
	int playerPosCol, playerPosRow;


	std::vector<std::vector<GLuint>> mapData;
	GLboolean IsCompleted();

private:
	void init(std::vector<std::vector<GLuint>> tileData, GLuint levelWidth, GLuint levelHeight);
};

#endif // !_GAME_LEVEL_
