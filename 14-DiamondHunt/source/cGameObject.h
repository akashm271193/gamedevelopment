#ifndef _cGameObject_HP_
#define _cGameObject_HP_

#include <glm/vec3.hpp> 
#include <glm/vec4.hpp> 
#include <string>
#include <glad/glad.h>
#include "cModel.h"
#include <map>
#include "ExternObjCalls.h"

class cMapCell;
//forward declare
class cAnimationState;

class cGameObject
{
public:
	cGameObject();
	virtual ~cGameObject() {};
	cGameObject(cModel* newModel, glm::vec3 maxExtentXYZ, glm::vec3 position, glm::vec3 orientation, int parentObjId);
	cModel* model;
	cAnimationState* animation;
	glm::vec3 position;
	glm::vec3 oldPosition;
	glm::vec3 originOrientation;
	glm::vec3 ownAxisOrientation;
	GLenum objectLayoutType;
	//bool isWireFrame;
	bool isUpdatedInPhysics;
	bool hasBoundingBoxOrSphere;
	glm::vec3 colour;
	glm::vec3 scale;
	int objectId;
	libPhysics::ePhysicsGameObjectType typeOfObject;		// (really an int)
	glm::vec3 velocity;
	glm::vec3 acceleration;
	bool toAnimate;
	float radius;
	cGameObject* boundingSphereOrBox;
	void Draw(int shaderId, bool drawBoundingBox = false, glm::vec3 parentObjectPos = glm::vec3(0.0f));
	int parentGameObjectIndexId;
	glm::vec3 objectFront;
	glm::vec3 objectRight;
	glm::vec3 worldUp;
	void updateObjectVectors();
	bool toGenerateBouundingBoxForCell(cMapCell* curCell);
	bool toDraw;
	int mapCellId;
	void updatePhysicsWorldObject();
	void updateFromPhysicsWorld();
	void createPhysicsWorldObject();
	libPhysics::iCollisionBody* collisionBody;
	libPhysics::sCollisionBodyDesc collisionBodyDesc;
	void CalculateSkinnedMeshBonesAndLoad(cModel* model, unsigned int numBonesUsedLoc, unsigned int bonesArrayLoc);
};																				  

#endif