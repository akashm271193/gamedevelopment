#ifndef _TRIANGLE_WITH_VERTICES_
#define _TRIANGLE_WITH_VERTICES_

#include "cVertex.h"

class cTriangleWithVertices
{
public:
	cTriangleWithVertices();
	~cTriangleWithVertices();

	cVertex Vertex[3];
};
#endif