#include "cBulletDebugDraw.h"
#include "nConvert.h"

namespace libPhysics
{
	cBulletDebugDraw::cBulletDebugDraw(iDebugRenderer* renderer)
	{
		mRender = renderer;
	}

	cBulletDebugDraw::~cBulletDebugDraw()
	{
		mRender = 0;
	}

	void	cBulletDebugDraw::drawBox(const btVector3& extents, const btTransform& trans, const btVector3& color)
	{
		glm::mat4 out;
		ToGlm(trans, out);
		mRender->RenderBox(ToGlm(extents), out, ToGlm(color));
	}

	void	cBulletDebugDraw::drawBox(const btVector3& bbMin, const btVector3& bbMax, const btVector3& color)
	{
		btVector3 extents = bbMax - bbMin;
		btTransform trans = btTransform::getIdentity();
		trans.setOrigin((bbMax + bbMin) * 0.5f);
		drawBox(extents, trans, color);
	}

	void	cBulletDebugDraw::drawBox(const btVector3& bbMin, const btVector3& bbMax, const btTransform& trans, const btVector3& color)
	{
		drawBox(bbMax - bbMin, trans, color);
	}

	void	cBulletDebugDraw::drawLine(const btVector3& from, const btVector3& to, const btVector3& color)
	{
		mRender->RenderLine(ToGlm(from), ToGlm(to), ToGlm(color));
	}

	void	cBulletDebugDraw::drawSphere(btScalar radius, const btTransform& transform, const btVector3& color)
	{
		glm::mat4 out;
		ToGlm(transform, out);
		mRender->RenderSphere(out, radius, ToGlm(color));
	}

	void	cBulletDebugDraw::drawCone(btScalar radius, btScalar height, int upAxis, const btTransform& transform, const btVector3& color)
	{
		glm::mat4 out;
		ToGlm(transform, out);
		mRender->RenderCone(radius, height, upAxis, out, ToGlm(color));
	}

	void	cBulletDebugDraw::drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color)
	{

	}

	void	cBulletDebugDraw::reportErrorWarning(const char* warningString)
	{

	}

	void	cBulletDebugDraw::draw3dText(const btVector3& location, const char* textString)
	{

	}

	void	cBulletDebugDraw::setDebugMode(int debugMode)
	{

	}

	int		cBulletDebugDraw::getDebugMode() const
	{
		return DBG_MAX_DEBUG_DRAW_MODE | btIDebugDraw::DBG_DrawConstraints | btIDebugDraw::DBG_DrawConstraintLimits;
	}

	void	cBulletDebugDraw::drawPlane(const btVector3& planeNormal, btScalar planeConst, const btTransform& transform, const btVector3& color)
	{
		glm::mat4 out;
		ToGlm(transform, out);
		mRender->RenderPlane(ToGlm(planeNormal), planeConst, out, ToGlm(color));
	}

}