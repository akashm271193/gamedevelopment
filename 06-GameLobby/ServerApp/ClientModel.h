#include <string>
#include <winsock2.h>

class ClientModel
{
public:
	SOCKET socketDescriptor;
	std::string ClientName;
	bool isMessagePending;
	int recvSize;
	int pendingMessageSize;
	std::string partialMessage;
	ClientModel();
	~ClientModel();
};

