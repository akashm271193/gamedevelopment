#ifndef _RESOURCE_MANAGER_
#define _RESOURCE_MANAGER_

#include <map>
#include <string>

#include <glad/glad.h>

#include "cShader.h"
#include "cModel.h"
#include "cTexture.h"

class cResourceManager
{
public:
	static std::map<std::string, cShader> Shaders;
	static std::map<std::string, cModel> Models;
	static std::map<std::string, cTexture> Textures;
	static std::map <std::string, float[4] > MinMax;

	static cShader LoadShader(const GLchar* vShaderFile, const GLchar* fShaderFile, std::string name);
	static cShader GetShader(std::string name);

	static cModel LoadModel(std::string modelFile, std::string name);
	static cModel GetModel(std::string name);

	static void cResourceManager::getMinMax(std::string name, float* minx, float* minz, float* maxx, float* maxz);

	static cTexture LoadTexture(const GLchar* textureFile, GLboolean alpha, std::string name);
	static cTexture GetTexture(std::string name);

	static void Clear();
private:
	cResourceManager() { };
	static cShader loadShaderFromFile(const GLchar* vShaderFile, const GLchar* fShaderFile);
	static cModel loadModelFromFile(std::string modelFile, std::string name);
	static cTexture loadTextureFromFile(const GLchar* textureFile, GLboolean alpha);
};
#endif
