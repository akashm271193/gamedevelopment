This is the ReadMe file for settingg up my Project.

1.) CHANGE THE DB CREDENTIALS IN THE AuthMain.cpp file

2.) IMPORT the db schema provided in DatabaseScripts folder ,into the database.

3.) In order to execute the project, please open the .sln file. Configure it to run on Debug x86. In that, since the solution  needs to be built first 
and it will automatically execute the game server and authentication server(requires IP(127.0.0.1) as argument) and followed by client.

4.) Please ensure the order of exectuion - ServerApp   ------ then -----> AuthServer ------- then --------> ClientApp

5.) Once the ClientApp.exe executes , i.e. , the game client starts, you can use only one command, .i.e '/connect'. Connect to 
127.0.0.1 at port 27015.

6.) All other commands are available on connecting to the server. You need to login/register first to use other commands. All commands
have been given explanation once you connect to game server.

7.) All commands start with a '/';

