#include "cGLFWCalls.h"

#include "cAnimationState.h"

#include "cFBO.h"


cGLFWCalls::cGLFWCalls(){
	this->isFacingBackwards = false;
	this->toProcessBackMovement = true;
	this->isAnimationPlaying = false;
}


cGLFWCalls::~cGLFWCalls(){
}

void cGLFWCalls::InitiateGLFW(){
	//Initializing GLFW
	glfwInit();

	//Setting Major and minor Context versions for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	//Setting the glfw for cor profile to use extra features
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	
}

bool cGLFWCalls::createNewWindow(int width, int height, std::string title){
	this->mainWindow = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
	if (this->mainWindow == NULL) {
		std::cout << "Error while creating window : \n";
		glfwTerminate();
		return false;
	}
	this->setWindowAsCurrent(this->mainWindow);
	return true;
}


void cGLFWCalls::setWindowAsCurrent(GLFWwindow* window) {
	glfwMakeContextCurrent(window);
	//Load glad for gl commands
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
	}
}

GLFWwindow* cGLFWCalls::getCurrentWindow() {
	return this->mainWindow;
}

void cGLFWCalls::windowSizeCallback(GLFWwindow* window, int width, int height)
{
	if (gFBO->screenWidth != width || gFBO->screenHeight != height)
	{
		gFBO->Reset(width, height);
	}
}

/*
glfw: whenever the mouse scroll wheel scrolls, this callback is called
*/
// ----------------------------------------------------------------------
void cGLFWCalls::scrollCallBack(GLFWwindow* window, double xoffset, double yoffset)
{
	camera->ProcessMouseScroll(yoffset);
}

/*
glfw: whenever the mouse moves, this callback is called
*/
// -------------------------------------------------------
void cGLFWCalls::mouseCallBack(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	camera->ProcessMouseMovement(xoffset, yoffset);
}

/*
glfw: whenever the mouse moves, this callback is called
*/
// -------------------------------------------------------
void cGLFWCalls::mouseButtonCallBack(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		double xpos, ypos;
		//getting cursor position
		glfwGetCursorPos(window, &xpos, &ypos);
		//std::cout << "Cursor Position at (" << xpos << " : " << ypos << ")" << std::endl;
		gMenuLoader->selectMenuOption(xpos, ypos);
	}
}

void cGLFWCalls::framebufferSizeCallBack(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	//mwidth = width;
	//mheight = height;

	glViewport(0, 0, width, height);
}

//just call the functions once every time you press the button
void cGLFWCalls::keyCallBack(GLFWwindow *window, int key, int scancode, int action, int mode)
{
	//call the player object
	cGameObject* playerChara = ::gGameObjects[playerGameObjId];

	int togSwitch;
	switch (key) {
	case GLFW_KEY_ESCAPE:
		if (action == GLFW_PRESS) {
			if (isMenuActive) {
				if (gMenuLoader->getActiveMenu() == 2 || gMenuLoader->getActiveMenu() == 4) {
					glfwSetWindowShouldClose(window, GLFW_TRUE);
				}
				else {
					if (gMenuLoader->getActiveMenu() == 3)
						gMenuLoader->setActiveMenu(0);
					glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
					glfwSetCursorPosCallback(window, mouseCallBack);
					glfwSetMouseButtonCallback(window, NULL);
				}
				
			}
			else {
				glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
				glfwSetCursorPosCallback(window, NULL);
				glfwSetMouseButtonCallback(window, mouseButtonCallBack);
			}
			isMenuActive = !isMenuActive;
		}
		break;

	case GLFW_KEY_SPACE:
		if (action == GLFW_PRESS) {
			drawToggle += 1;
			togSwitch = (drawToggle) % 3;

			if (togSwitch == 0) {
				toDrawObjects = true;
				toDrawDebugObjects = false;
			}
			else if (togSwitch == 1) {
				toDrawObjects = false;
				toDrawDebugObjects = true;
			}
			else if (togSwitch == 2) {
				toDrawObjects = true;
				toDrawDebugObjects = true;
			}
			
		}
		break;

	case GLFW_KEY_TAB:
		if (action == GLFW_PRESS) {
			if (!camera->isBound) {
				camera->bindCameraToAnObject(gGameObjects[playerGameObjId]);
			}
			else {
				camera->isBound = false;
			}			
		}
		break;
	
	case GLFW_KEY_W:
		 if (action == GLFW_RELEASE) {
			playerChara->animation->vecAnimationQueue.clear();
		}
		break;
	case GLFW_KEY_S:
		if (action == GLFW_RELEASE) {
			playerChara->animation->vecAnimationQueue.clear();
		}
		break;
	
	case GLFW_KEY_R:
		if (action == GLFW_PRESS) {
			if (isMenuActive && gMenuLoader->getActiveMenu() == 4) {
				gMenuLoader->setActiveMenu(0);
				glfwSetCursorPosCallback(window, mouseCallBack);
				glfwSetMouseButtonCallback(window, NULL);
				cPlayerGameObject* tmp = (cPlayerGameObject*) gGameObjects[playerGameObjId];
				tmp->respawnPlayer();
				tmp->collisionBodyDesc.collidedBodiesId.clear();
				tmp->collisionBodyDesc.position = tmp->position;
				tmp->updatePhysicsWorldObject();
				camera->bindCameraToAnObject(gGameObjects[playerGameObjId]);
				glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
				isMenuActive = false;
			}
		}
		break;

	case GLFW_KEY_LEFT_SHIFT:
		if (action == GLFW_PRESS) {
			if (CullingRadius == 10.0f)
				CullingRadius = 10000.0f;
			else
				CullingRadius = 10.0f;
		}
		break;
	case GLFW_KEY_A:
		break;

	case GLFW_KEY_D:
		break;

	}
	//delete playerChara;
}

void cGLFWCalls::processInput(GLFWwindow *window)
{
	//call the player object
	cGameObject* playerChara = ::gGameObjects[playerGameObjId];
	
	//animations
	cAnimationState::sAnimationState walkAnimation;
	walkAnimation.name = "../extern/assets/modelsFBX/fbx/Eve_Walking.fbx";
	//walkAnimation.totalTime = playerChara->model->FindAnimationTotalTime(walkAnimation.name);
	if (isPEDrawn) {
		walkAnimation.totalTime = 1.0f;
		walkAnimation.frameStepTime = 0.01;
	}
	else {
		walkAnimation.totalTime = 1.0f;
		walkAnimation.frameStepTime = 0.01;
	}

	cAnimationState::sAnimationState turnAroundAnimation;
	turnAroundAnimation.name = "../extern/assets/modelsFBX/fbx/EveTurn.fbx";
	//walkBackAnimation.totalTime = playerChara->model->FindAnimationTotalTime(walkAnimation.name);
	if (isPEDrawn) {
		turnAroundAnimation.totalTime = 1.0f;
		turnAroundAnimation.frameStepTime = 0.025;
	}
	else {
		turnAroundAnimation.totalTime = 1.0f;
		turnAroundAnimation.frameStepTime = 0.025;
	}


	//camera Movement code
	camera->MovementSpeed = PlayerSpeed;
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
		if (camera->isBound) {
			if (this->isFacingBackwards) {
				gGameObjects[playerGameObjId]->ownAxisOrientation.y += glm::radians(180.0f);
				gGameObjects[playerGameObjId]->ownAxisOrientation.y = glm::radians(fmod(glm::degrees(gGameObjects[playerGameObjId]->ownAxisOrientation.y), 360.f));
				gGameObjects[playerGameObjId]->updateObjectVectors();
				this->isFacingBackwards = false;
			}
			this->moveForward();
			camera->bindCameraToAnObject(gGameObjects[playerGameObjId]);
			camera->updatePhysicsWorldObject();
			//add animation
			playerChara->animation->vecAnimationQueue.push_back(walkAnimation);
		}
		else
			camera->ProcessKeyboard(FORWARD, deltaTime);
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
		if (camera->isBound) {
			if (!this->isFacingBackwards && this->toProcessBackMovement) {
				if (!this->isAnimationPlaying) {
					this->isAnimationPlaying = true;
					playerChara->animation->vecAnimationQueue.push_back(turnAroundAnimation);
				}
				if (playerChara->animation->vecAnimationQueue.empty()) {
					gGameObjects[playerGameObjId]->ownAxisOrientation.y += glm::radians(180.0f);
					gGameObjects[playerGameObjId]->ownAxisOrientation.y = glm::radians(fmod(glm::degrees(gGameObjects[playerGameObjId]->ownAxisOrientation.y), 360.f));
					gGameObjects[playerGameObjId]->updateObjectVectors();
					this->isFacingBackwards = true;
					this->isAnimationPlaying = false;
				}
			}
			else{
				if (this->toProcessBackMovement) {
					if (camera->rotateCameraAroundTarget(gGameObjects[playerGameObjId], PlayerSpeed, deltaTime)) {

						this->moveForward();
						playerChara->animation->vecAnimationQueue.push_back(walkAnimation);
					}
					else {
						this->isFacingBackwards = false;
						this->toProcessBackMovement = false;
						camera->bindCameraToAnObject(gGameObjects[playerGameObjId]);
					}
					camera->updatePhysicsWorldObject();
				}
				
				//camera->bindCameraToAnObject(gGameObjects[playerGameObjId]);			
			}
			//add animation
			//playerChara->animation->vecAnimationQueue.push_back(walkBackAnimation);
		}
		else
			camera->ProcessKeyboard(BACKWARD, deltaTime);
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_RELEASE) {
		if (camera->isBound) {
			if (this->isFacingBackwards == false) {
				this->toProcessBackMovement = true;
			}
		}
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
		if (camera->isBound) {
			gGameObjects[playerGameObjId]->collisionBodyDesc.oldOrientation = gGameObjects[playerGameObjId]->ownAxisOrientation;
			gGameObjects[playerGameObjId]->ownAxisOrientation.y += PlayerSpeed /*1.0f*/ * deltaTime;
			if (glm::degrees(gGameObjects[playerGameObjId]->ownAxisOrientation.y) < 0.0f) {
				float tmpAngle = glm::degrees(gGameObjects[playerGameObjId]->ownAxisOrientation.y);
				tmpAngle += 360.0f;
				gGameObjects[playerGameObjId]->ownAxisOrientation.y = glm::radians(tmpAngle);
			}
			gGameObjects[playerGameObjId]->ownAxisOrientation.y = glm::radians(fmod(glm::degrees(gGameObjects[playerGameObjId]->ownAxisOrientation.y), 360.f));
			gGameObjects[playerGameObjId]->collisionBodyDesc.ownAxisOrientation = gGameObjects[playerGameObjId]->ownAxisOrientation;
			gGameObjects[playerGameObjId]->updateObjectVectors();
			gGameObjects[playerGameObjId]->collisionBodyDesc.objectFront = gGameObjects[playerGameObjId]->objectFront;
			gGameObjects[playerGameObjId]->updatePhysicsWorldObject();
			camera->bindCameraToAnObject(gGameObjects[playerGameObjId]);
			camera->updatePhysicsWorldObject();
		}
		else
			camera->ProcessKeyboard(LEFT, deltaTime);
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
		if (camera->isBound) {
			gGameObjects[playerGameObjId]->collisionBodyDesc.oldOrientation = gGameObjects[playerGameObjId]->ownAxisOrientation;
			gGameObjects[playerGameObjId]->ownAxisOrientation.y -= PlayerSpeed /*1.0f*/ * deltaTime;
			if (glm::degrees(gGameObjects[playerGameObjId]->ownAxisOrientation.y) < 0.0f) {
				float tmpAngle = glm::degrees(gGameObjects[playerGameObjId]->ownAxisOrientation.y);
				tmpAngle += 360.0f;
				gGameObjects[playerGameObjId]->ownAxisOrientation.y = glm::radians(tmpAngle);
			}
			gGameObjects[playerGameObjId]->ownAxisOrientation.y = glm::radians(fmod(glm::degrees(gGameObjects[playerGameObjId]->ownAxisOrientation.y), 360.f));
			gGameObjects[playerGameObjId]->collisionBodyDesc.ownAxisOrientation = gGameObjects[playerGameObjId]->ownAxisOrientation;
			//gGameObjects[playerGameObjId]->boundingSphereOrBox->ownAxisOrientation.y -= PlayerSpeed /*1.0f*/ * deltaTime;
			gGameObjects[playerGameObjId]->updateObjectVectors();
			gGameObjects[playerGameObjId]->collisionBodyDesc.objectFront = gGameObjects[playerGameObjId]->objectFront;
			gGameObjects[playerGameObjId]->updatePhysicsWorldObject();
			camera->bindCameraToAnObject(gGameObjects[playerGameObjId]);
			camera->updatePhysicsWorldObject();
		}
		else
			camera->ProcessKeyboard(RIGHT, deltaTime);

	}
}


void cGLFWCalls::moveForward() {
	glm::vec3 displacement = gGameObjects[playerGameObjId]->objectFront * PlayerSpeed /*1.0f*/ * deltaTime;
	gGameObjects[playerGameObjId]->position = displacement + gGameObjects[playerGameObjId]->position;
	gGameObjects[playerGameObjId]->collisionBodyDesc.position = gGameObjects[playerGameObjId]->position;
	gGameObjects[playerGameObjId]->updatePhysicsWorldObject();
};
void cGLFWCalls::moveBackward() {};
void cGLFWCalls::moveLeft() {};
void cGLFWCalls::moveRight() {};
