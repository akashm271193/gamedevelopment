What Is OpenGL?
OpenGL is a Graphics rendering API which is operating system independent, window system independent and has high-quality color images composed of geometric and image primitives.
OpenGL APIs can use following …

- Gl [OpenGL API implementation (http://www.opengl.org)]
- Glu [OpenGL Utility]
- Glut – GLUT (OpenGL Utility Toolkit) – Glut is portable windowing API and it is not officially part of OpenGL [OpenGL Utility Toolkit (http://www.opengl.org/resources/libraries/glut/)]
- FLTK [FlashLight ToolKit (http://www.fltk.org/)]
- GLEW

Now lets see How to install OpenGL on out Ubuntu OS.

Now because GLUT (OpenGL Utility Toolkit) depends upon OpenGL and a number of other related libraries, if we install GLUT then OpenGL will be automatically be installed.

Run the following commands to install OpenGL.

```
sudo apt-get update
sudo apt-get install libglu1-mesa-dev freeglut3-dev mesa-common-dev
```

Run the main.cpp with the following command:
```
g++ main.cpp -o firstOpenGlApp -lglut -lGLU -lGL
```

Now run your OpenGl program with following command
```
 ./firstOpenGlApp
```
