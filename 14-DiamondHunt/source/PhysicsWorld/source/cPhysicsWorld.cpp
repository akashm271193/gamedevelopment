#include "cPhysicsWorld.h"
#include "nConvert.h"
#include <iostream>
namespace libPhysics {
cPhysicsWorld::cPhysicsWorld()
{
	this->config = new btDefaultCollisionConfiguration();
	this->dispatcher = new btCollisionDispatcher(config);
	this->broadphase = new btDbvtBroadphase();
	this->constraintSolver = new btSequentialImpulseConstraintSolver();
	this->world = new btDiscreteDynamicsWorld(dispatcher, broadphase, constraintSolver, config);
	world->setGravity(btVector3(0.0f, -9.8f, 0.0f));
	this->world->setInternalTickCallback(myTickCallback, (void*)&collisions);
}


void cPhysicsWorld::addDebugRender(iDebugRenderer* renderer) {
	mDebugDraw = new cBulletDebugDraw(renderer);
	this->world->setDebugDrawer(mDebugDraw);
};

void cPhysicsWorld::AddBoundingBox(iCollisionBody* inObject) {
	cCollisionBody* bBox = dynamic_cast<cCollisionBody*>(inObject);
	if (!bBox)
		return;

	for (int i = 0; i < arrBoundingBoxes.size(); i++) {
		cCollisionBody* tmpObj = (cCollisionBody*)arrBoundingBoxes[i]->getUserPointer();
		if (tmpObj->getObjectId() == bBox->getObjectId()) {
			return;
		}
	}


	btCollisionShape* colShape = new btBoxShape(ToBullet(bBox->getMaxExtents()/2.0f));
	colShape->setMargin(0.01f);

	btTransform startTransform;
	startTransform.setIdentity();
	
	btScalar    tMass(0.0f);

	if (bBox->getPhyObjType() != ePhysicsGameObjectType::PHY_OBJ_STATIC)
		tMass = 1.0f;

	//rigidbody is dynamic if and only if mass is non zero, otherwise static
	bool isDynamic = (tMass != 0.f);

	btVector3 localInertia(0, 0, 0);
	if (isDynamic)
		colShape->calculateLocalInertia(tMass, localInertia);

	startTransform.setOrigin(ToBullet(bBox->getPosition()));
	startTransform.setRotation(btQuaternion(bBox->getOrientation().y, 0.0f, 0.0f));
	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(tMass, myMotionState, colShape, localInertia);

	btRigidBody* body = new btRigidBody(rbInfo);
	body->setUserPointer(bBox);
	body->setCollisionFlags(body->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
	body->setActivationState(DISABLE_DEACTIVATION);
	arrBoundingBoxes.push_back(body);
	this->world->addRigidBody(body);
	btBroadphaseProxy *bproxy = body->getBroadphaseHandle();
	if (bproxy) {
		bproxy->m_collisionFilterGroup = short(btBroadphaseProxy::DefaultFilter);
		bproxy->m_collisionFilterMask = short(btBroadphaseProxy::AllFilter);
	}
	//return body;
}

void cPhysicsWorld::RemoveBoundingBox(iCollisionBody* inObject) {
	cCollisionBody* bBox = dynamic_cast<cCollisionBody*>(inObject);
	if (!bBox)
		return;

	btRigidBody* objectToRemove = NULL;
	int arrIndex = -1;
	for (int i = 0; i < arrBoundingBoxes.size(); i++) {
		cCollisionBody* tmpObj = (cCollisionBody*)arrBoundingBoxes[i]->getUserPointer();
		if (tmpObj->getObjectId() == bBox->getObjectId()) {
			arrIndex = i;
			objectToRemove = arrBoundingBoxes[i];
			break;
		}
	}
	if (objectToRemove != NULL) {
		world->removeRigidBody(objectToRemove);
	}
	for (int i = 0; i < world->getCollisionObjectArray().size(); i++) {
		cCollisionBody* tmpObj = (cCollisionBody*)world->getCollisionObjectArray()[i]->getUserPointer();
		if (tmpObj->getObjectId() == bBox->getObjectId()) {
			world->removeCollisionObject(world->getCollisionObjectArray()[i]);
		}
	}
	if(arrIndex >= 0)
		arrBoundingBoxes.removeAtIndex(arrIndex);
}

void cPhysicsWorld::RemoveAllBoundingBoxes() {
	/*for (int i = 0; i < arrBoundingBoxes.size(); i++) {
		world->removeCollisionObject(arrBoundingBoxes[i]);
	}*/
}

void cPhysicsWorld::drawDebugWorld() {
	this->world->debugDrawWorld();
}

void cPhysicsWorld::UpdateBoundingBox(iCollisionBody* inObject) {
	cCollisionBody* bBox = dynamic_cast<cCollisionBody*>(inObject);
	if (!bBox)
		return;
	
	btRigidBody* objectToUpdate = NULL;	
	for (int i = 0; i < arrBoundingBoxes.size(); i++){
		cCollisionBody* tmpObj = (cCollisionBody*)arrBoundingBoxes[i]->getUserPointer();
		if (tmpObj->getObjectId() == bBox->getObjectId()) {
			objectToUpdate = arrBoundingBoxes[i];
			break;
		}
	}

	if (objectToUpdate != NULL) {
		btTransform updatedWorld;
		objectToUpdate->getMotionState()->getWorldTransform(updatedWorld);
		updatedWorld.setOrigin(ToBullet(bBox->getPosition()));
		updatedWorld.setRotation(btQuaternion(bBox->getOrientation().y, 0.0f, 0.0f));
		objectToUpdate->getMotionState()->setWorldTransform(updatedWorld);
	}	
};
//void cPhysicsWorld::sliderConstraint(btRigidBody *body, btTransform trans)
//{
//	btSliderConstraint* slider = new btSliderConstraint(*body, trans, false);
//	world->addConstraint(slider);
//}
//
//void cPhysicsWorld::sliderConstraint(btRigidBody *body1, btRigidBody *body2, btTransform trans1, btTransform trans2)
//{
//	btSliderConstraint* slider = new btSliderConstraint(*body1,*body2, trans1,trans2, false);
//	world->addConstraint(slider);
//}
//
//void cPhysicsWorld::p2pConstraint(btRigidBody *body, btVector3 trans)
//{
//	btPoint2PointConstraint* p2p = new btPoint2PointConstraint(*body, trans);
//	world->addConstraint(p2p);
//}
//
//void cPhysicsWorld::p2pConstraint(btRigidBody *body1 , btRigidBody *body2, btVector3 trans1, btVector3 trans2)
//{
//	btPoint2PointConstraint* p2p = new btPoint2PointConstraint(*body1, *body2,trans1, trans2);
//	world->addConstraint(p2p);
//}
//
//void cPhysicsWorld::hingeConstraint(btRigidBody* body, btVector3 pivot, btVector3 axis)
//{
//	btHingeConstraint* hinge = new btHingeConstraint(*body, pivot, axis, true);
//	world->addConstraint(hinge);
//}
//
//void cPhysicsWorld::hingeConstraint(btRigidBody* body1, btVector3 pivot1, btVector3 axis1, btRigidBody* body2, btVector3 pivot2, btVector3 axis2)
//{
//	btHingeConstraint* hinge = new btHingeConstraint(*body1, *body2, pivot1, pivot2,axis1, axis2, true);
//	world->addConstraint(hinge);
//}
//void cPhysicsWorld::dofConstraint(btRigidBody* body, btTransform trans)
//{
//	btGeneric6DofConstraint* dof = new btGeneric6DofConstraint(*body, trans, false);
//
//	world->addConstraint(dof);
//}
//void cPhysicsWorld::dofConstraint(btRigidBody* body1, btRigidBody* body2, btTransform trans1, btTransform trans2)
//{
//	btGeneric6DofConstraint* dof = new btGeneric6DofConstraint(*body1, *body2, trans1, trans2, false);
//	world->addConstraint(dof);
//}
	void cPhysicsWorld::stepSimulation(float deltaTime)
	{
		this->world->stepSimulation(deltaTime);
		
		int totalObjs = world->getCollisionObjectArray().size();
		for (int i = 0; i < world->getCollisionObjectArray().size(); i++) {
			cCollisionBody* worldObj = (cCollisionBody*)world->getCollisionObjectArray()[i]->getUserPointer();
			if (worldObj->getPhyObjType() == ePhysicsGameObjectType::PHY_OBJ_KINEMATIC){
				btTransform testTrans = world->getCollisionObjectArray()[i]->getWorldTransform();
				glm::vec3 pos = ToGlm(testTrans.getOrigin());
				glm::vec3 rot = ToGlm(testTrans.getRotation());
				break;

			}
		}

		if (this->collisions.size() > 0) {
			for (int counter = 0; counter < collisions.size(); counter++) {
				cCollisionBody* kineMatObj = collisions[counter]->getKinematicBody();
				cCollisionBody* staticObj = collisions[counter]->getStaticBody();
				if (staticObj == NULL) {
					staticObj = collisions[counter]->getOtherKinematicBody(kineMatObj);
				}
				if (kineMatObj->getObjType() == OBJ_PLAYER) {
					kineMatObj->setIsColliding(true);
					if (staticObj->getObjType() == OBJ_TREASURE || staticObj->getObjType() == OBJ_FIRE) {
						std::vector<int> vecIds = kineMatObj->getVecCollidedBodies();
						vecIds.push_back(staticObj->getObjectId());
						kineMatObj->setVecCollidedBodies(vecIds);
					}
					if (staticObj->getObjType() == OBJ_ANGRY_AI || staticObj->getObjType() == OBJ_CURIOUS_AI || OBJ_FOLLOWER_AI) {
						std::vector<int> vecIds = kineMatObj->getVecCollidedBodies();
						vecIds.push_back(staticObj->getObjectId());
						kineMatObj->setVecCollidedBodies(vecIds);
					}
					//this->UpdateBoundingBox(kineMatObj);
				}
				else if (kineMatObj->getObjType() == OBJ_CAMERA) {
					kineMatObj->setIsColliding(true);
					std::vector<int> vecIds = kineMatObj->getVecCollidedBodies();
					vecIds.push_back(staticObj->getObjectId());
					kineMatObj->setVecCollidedBodies(vecIds);
					//this->UpdateBoundingBox(kineMatObj);
				}
			}
		}
	}

	void cPhysicsWorld::setViewAndProjectionMatrix(glm::mat4 viewMat, glm::mat4 projectionMat, int shadId) {
		this->viewMatrix = viewMat;
		this->projectionMatrix = projectionMat;
		this->shaderId = shadId;
	};

	bool cPhysicsWorld::checkIfObjectAlreadyPresent(iCollisionBody* object) {
		cCollisionBody* bBox = dynamic_cast<cCollisionBody*>(object);
		if (!bBox)
			return false;

		for (int i = 0; i < arrBoundingBoxes.size(); i++) {
			cCollisionBody* tmpObj = (cCollisionBody*)arrBoundingBoxes[i]->getUserPointer();
			if (tmpObj->getObjectId() == bBox->getObjectId()) {
				return true;
			}
		}
		return false;
	};


	void myTickCallback(btDynamicsWorld* dynamicsWorld, btScalar timeStep) {
		btAlignedObjectArray<cCollision*> &vecCollisions = *reinterpret_cast<btAlignedObjectArray<cCollision*>*>(dynamicsWorld->getWorldUserInfo());
		vecCollisions.clear();
		int numManifolds = dynamicsWorld->getDispatcher()->getNumManifolds();
		for (int i = 0; i < numManifolds; i++) {
			btPersistentManifold *contactManifold = dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);
			cCollision* collision = new cCollision();
			const btCollisionObject *objA = contactManifold->getBody0();
			collision->A = (cCollisionBody*)(objA->getUserPointer());
			const btCollisionObject *objB = contactManifold->getBody1();
			collision->B = (cCollisionBody*)(objB->getUserPointer());
			
			if (collision->A->getPhyObjType() == PHY_OBJ_STATIC && collision->B->getPhyObjType() == PHY_OBJ_STATIC)
				continue;


			int numContacts = contactManifold->getNumContacts();
			for (int j = 0; j < numContacts; j++)
			{
				btManifoldPoint& pt = contactManifold->getContactPoint(j);
				if (pt.getDistance() < 0.f)
				{
					collision->contactPtA = pt.getPositionWorldOnA();
					collision->contactPtB = pt.getPositionWorldOnB();
					collision->normalOnB  = pt.m_normalWorldOnB;
				}
			}

			if(numContacts > 0)
				vecCollisions.push_back(collision);
		}
	}
}