Config:
mapStartPoint (5,1) ;
mapEndPoint  (7,7) ;
mapRows 10 ;
mapColumns 10 ;

Map:
0 0 0 0 0 0 0 0 0 0
0 1 1 1 1 1 1 1 1 0
0 1 1 0 0 0 1 1 1 0
0 1 1 0 0 0 1 1 1 0
0 1 1 0 0 0 1 0 1 0
0 1 1 0 0 0 1 0 1 0
0 1 1 0 0 0 1 0 1 0
0 1 1 0 0 0 1 1 1 0
0 1 1 1 1 1 1 1 1 0
0 0 0 0 0 0 0 0 0 0

#ENDCONFIG#