#include <vector>
#include <sapi.h>
#include <sphelper.h>
#include <ft2build.h>
#include FT_FREETYPE_H
class cMenu;
class cMenuObj;
enum eMenuObjType;

class cMenuLoader
{
	FT_Library mft;

	FT_Face mface;

	unsigned int mwidth = 0;
	unsigned int mheight = 0;

	unsigned int attribute_coord;
	unsigned int uniform_igcolour;
	unsigned int uniform_tex;
	unsigned int uniform_color;

	struct point {
		float x;
		float y;
		float s;
		float t;
	};

	float sx ;
	float sy ;

	float yoffset;
	float xoffset;


public:
	cMenuLoader();
	~cMenuLoader();
	void addMenu(cMenu* newMenu);
	void renderMenu(int menuShaderId, int textShaderId);
	void selectMenuOption(float posX, float posY);
	void setActiveMenu(int index);
	int getActiveMenu();

private:
	std::vector<cMenu*> vecMenus;
	void loadMenuObjectShape();
	void loadTextFontShape();
	unsigned int EBO,VBO, VAO,textVAO,textVBO;
	void loadMenus();
	bool initfreetype();
	void render_text(const char *text, float x, float y, float sx, float sy);
	float convertToScreenPos(float pos, char axis);
	glm::vec3 maxExtentXYZ;
	int activeMenuIndex;
	bool isButtonClicked(float posX,float y, cMenuObj* obj);
	eMenuObjType action;
};