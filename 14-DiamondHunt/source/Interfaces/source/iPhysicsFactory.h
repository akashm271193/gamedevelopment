#ifndef _IPHYSICS_FACTORY_HG_
#define _IPHYSICS_FACTORY_HG_

#include "iCollisionBody.h"
#include "sCollisionBodyDesc.h"
#include "iPhysicsWorld.h"

namespace libPhysics
{
	class iPhysicsFactory
	{
	public:
		virtual ~iPhysicsFactory() {}

		virtual iPhysicsWorld* CreateWorld() = 0;
		
		virtual iCollisionBody* CreateCollisionBody(const sCollisionBodyDesc &bodyDesc) = 0;
	};
}

#endif