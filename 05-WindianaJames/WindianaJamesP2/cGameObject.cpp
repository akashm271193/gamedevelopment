#include "cGameObject.h"

cGameObject::cGameObject()
{
	return;
}

cGameObject::cGameObject(std::string name, cTexture texture)
{
	this->Position = glm::vec3(0.0f);
	this->Orientation = glm::vec3(0.0f);
	this->Scale = glm::vec3(1.0f);
	this->Color = glm::vec3(0.0f);
	this->Velocity = glm::vec3(0.0f);

	this->ObjectType = UNKNOWN;

	this->Texture = texture;

	this->Name = name;
}

cGameObject::cGameObject(std::string name, cTexture texture, glm::vec3 position, glm::vec3 orientation, glm::vec3 scale, glm::vec3 color, glm::vec3 velocity)
{
	this->Position = position;
	this->Orientation = orientation;
	this->Scale = scale;
	this->Color = color;
	this->Velocity = velocity;

	this->ObjectType = UNKNOWN;

	this->Texture = texture;

	this->Name = name;
}

cGameObject::~cGameObject()
{
	return;
}

void cGameObject::Draw(cVAOManager & renderer)
{
	sRenderInfo renderInfo;
	renderer.FindModelToDraw(this->Name, renderInfo);
	renderer.DrawTexturedModel3D(renderInfo, this->Texture, this->Position, this->Scale);
}
