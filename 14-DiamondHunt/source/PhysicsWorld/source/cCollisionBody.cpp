#include "cCollisionBody.h"


namespace libPhysics {
	cCollisionBody::cCollisionBody(const sCollisionBodyDesc &bodyDesc)
	{
		this->mPosition = bodyDesc.position;
		this->mOldPosition = bodyDesc.oldPosition;
		this->mOwnAxisOrientation = bodyDesc.ownAxisOrientation;
		this->mMaxExtents = bodyDesc.maxExtents;
		this->mScale = bodyDesc.scale;
		this->mObjectId = bodyDesc.objectId;
		this->mOldOrientation = bodyDesc.oldOrientation;
		this->objType = bodyDesc.objType;
		this->mObjectFront = bodyDesc.objectFront;
		this->mPhyObjType = bodyDesc.phyObjType;
		this->mIsColliding = bodyDesc.isColliding;
		this->mCollidedBodyIds = bodyDesc.collidedBodiesId;
	}

	cCollisionBody::~cCollisionBody()
	{
	}

	glm::vec3 cCollisionBody::getPosition()
	{
		return this->mPosition;
	}
	glm::vec3 cCollisionBody::getOrientation()
	{
		return this->mOwnAxisOrientation;
	}
	glm::vec3 cCollisionBody::getOldPosition()
	{
		return this->mOldPosition;
	}
	glm::vec3 cCollisionBody::getOldOrientation()
	{
		return this->mOldOrientation;
	}
	glm::vec3 cCollisionBody::getScale()
	{
		return this->mScale;
	}
	glm::vec3 cCollisionBody::getMaxExtents()
	{
		return this->mMaxExtents;
	}
	int cCollisionBody::getObjectId()
	{
		return this->mObjectId;
	}
	void cCollisionBody::updateCollisionBody(sCollisionBodyDesc &bodyDesc) {
		this->mPosition = bodyDesc.position;
		this->mOldPosition = bodyDesc.oldPosition;
		this->mOwnAxisOrientation = bodyDesc.ownAxisOrientation;
		this->mMaxExtents = bodyDesc.maxExtents;
		this->mScale = bodyDesc.scale;
		this->mOldOrientation = bodyDesc.oldOrientation;
		this->objType = bodyDesc.objType;
		this->mObjectFront = bodyDesc.objectFront;
		this->mPhyObjType = bodyDesc.phyObjType;
		this->mIsColliding = bodyDesc.isColliding;
		this->mCollidedBodyIds = bodyDesc.collidedBodiesId;
	}

	glm::vec3 cCollisionBody::getObjectFront() {
		return this->mObjectFront;
	}

	void cCollisionBody::getCollisionBodyStateFromPhysics(sCollisionBodyDesc &bodyDesc) {
		bodyDesc.position = this->mPosition;
		bodyDesc.ownAxisOrientation = this->mOwnAxisOrientation;
		bodyDesc.collidedBodiesId = this->mCollidedBodyIds;
		bodyDesc.isColliding = this->mIsColliding;
	};

	ePhysicsGameObjectType cCollisionBody::getObjType() {
		return this->objType;
	};

	void cCollisionBody::setPosition(glm::vec3 pos) {
		this->mPosition = pos;
	};

	bool cCollisionBody::isColliding() {
		return this->mIsColliding;
	}

	void cCollisionBody::setIsColliding(bool status) {
		this->mIsColliding = status;
	};

	ePhysicsGameObjectType cCollisionBody::getPhyObjType() {
		return this->mPhyObjType;
	}

	std::vector<int> cCollisionBody::getVecCollidedBodies() {
		return	this->mCollidedBodyIds;
	};
	void cCollisionBody::setVecCollidedBodies(std::vector<int> vecIds) {
		this->mCollidedBodyIds = vecIds;
	};
}
