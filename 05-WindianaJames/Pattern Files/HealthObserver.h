#ifndef _HEALTHOBSERVER_HG_
#define _HEALTHOBSERVER_HG_

#include "Observer.h"
#include <string>
using namespace std;

class HealthObserver : public Observer
{
public:
	HealthObserver() {}
	virtual void Notify(string message);

};



#endif