#pragma once
#include "cMeshStructure.h"




class cMesh {
public:
	/*  Mesh Data  */
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
	std::vector<Texture> textures;
	/*  Functions  */
	cMesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures,bool loadColour);
	void setTexturesForMesh(std::vector<Texture> textures);
	std::vector<Texture> getTexturesForMesh();
	std::vector<unsigned int> getIndicesForMesh();
	void setIndicesForMesh(std::vector<unsigned int> indexes);
	void updateElementBufferData();
	cMesh();
	~cMesh();
	void Draw(GLint shaderId,glm::vec3 colour);
private:
	/*  Render data  */
	unsigned int VAO, VBO, EBO;
	/*  Functions    */
	void setupMesh();
	bool toLoadColour;
	void getBlendTexRatios(int items);
	float diffBlend[5];
};
