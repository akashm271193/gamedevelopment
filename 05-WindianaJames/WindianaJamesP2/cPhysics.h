#ifndef _PHYSICS
#define _PHYSICS

class cGameObject;

bool PenetrationTestSphereSphere(cGameObject* pA, cGameObject* pB);

#endif // !_PHYSICS
