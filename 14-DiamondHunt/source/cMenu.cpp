#include "cMenu.h"


cMenu::cMenu()
{
}


cMenu::~cMenu()
{
}

cMenu::cMenu(eMenuType type, int numObjects) {
	this->menuType = type;
	this->menuObjects = numObjects;
	glm::vec3 tmpPos = glm::vec3(0.0f, 0.7f, 0.0f);
	cMenuObj* tmpObj;
	rapidjson::Document doc;
	rapidjson::Value &savedGames = rapidjson::Value().SetNull();
	switch (type) {
		case MAIN:
			tmpObj = new cMenuObj(eMenuObjType::CONTINUE);
			tmpObj->position = tmpPos;
			tmpObj->objText = "Continue";
			this->vecMenuObjects.push_back(tmpObj);
			
			tmpObj = new cMenuObj(eMenuObjType::LOAD);
			tmpPos.y = tmpPos.y - 0.4f;
			tmpObj->position = tmpPos;
			tmpObj->objText = "Load";
			this->vecMenuObjects.push_back(tmpObj);
			
			tmpObj = new cMenuObj(eMenuObjType::SAVE);
			tmpPos.y = tmpPos.y - 0.4f;
			tmpObj->position = tmpPos;
			tmpObj->objText = "Save";
			this->vecMenuObjects.push_back(tmpObj);

			tmpObj = new cMenuObj(eMenuObjType::EXIT);
			tmpPos.y = tmpPos.y - 0.4f;
			tmpObj->position = tmpPos;
			tmpObj->objText = "Exit";
			this->vecMenuObjects.push_back(tmpObj);

			break;

		case GAME_STATE_MENU:
			for (int i = 0; i < numObjects-1; i++) {
				tmpObj = new cMenuObj(eMenuObjType::GAME_STATE);
				tmpObj->menuObjId = i;
				tmpObj->position = tmpPos;
				this->vecMenuObjects.push_back(tmpObj);
				tmpPos.y = tmpPos.y - 0.4f;
			}

			tmpObj = new cMenuObj(eMenuObjType::BACK);
			tmpObj->position = tmpPos;
			tmpObj->objText = "Back";
			tmpObj->prevMenuIndex = 0;
			this->vecMenuObjects.push_back(tmpObj);
			tmpPos.y = tmpPos.y - 0.4f;

			if (checkForSavedFiles(doc)) {
				savedGames = doc["SavedGames"];
				for (int i = 0; i < savedGames.Size(); i++) {
					this->vecMenuObjects[i]->objText = "Saved Game " + std::to_string(i+1);
					std::string tmpGame = savedGames[i].GetString();
					this->vecMenuObjects[i]->gameStateFile = "../extern/config/"+ tmpGame+".json";
				}
			}
			break;

		case GAME_END_MENU:
			tmpObj = new cMenuObj(eMenuObjType::DATA);
			tmpObj->menuObjId = 0;
			tmpObj->position = glm::vec3(0.0f);
			tmpObj->scale = glm::vec3(3.0f, 4.0f,0.0f);
			tmpObj->objText = "You have found the diamond ! | Game Over | Press ESC to EXIT !";
			this->vecMenuObjects.push_back(tmpObj);
			break;

		case GAME_START_MENU:
			tmpObj = new cMenuObj(eMenuObjType::DATA);
			tmpObj->menuObjId = 0;
			tmpObj->position = glm::vec3(0.0f);
			tmpObj->scale = glm::vec3(3.0f, 4.0f, 0.0f);
			tmpObj->objText = "Hi Player, it's time to find a diamond | Use W,A,S,D to move around the maze | Stay away from obstacles | Press ESC to BEGIN !";
			this->vecMenuObjects.push_back(tmpObj);
			break;

		case FIRE_END_MENU:
			tmpObj = new cMenuObj(eMenuObjType::DATA);
			tmpObj->menuObjId = 0;
			tmpObj->position = glm::vec3(0.0f);
			tmpObj->scale = glm::vec3(2.0f, 4.0f, 0.0f);
			tmpObj->objText = "May you burn in hell ! | Game Over | Press ESC to EXIT | Press R to Respawn";
			this->vecMenuObjects.push_back(tmpObj);
			break;
	}
}


bool cMenu::checkForSavedFiles(rapidjson::Document &doc) {
	doc = readConfigFromJsonFile("../extern/config/savedGames.json");
	if (doc.IsObject() == 0) {
		addSavedGame("../extern/config/savedGames.json","");
		return false;
	}
	return true;
}