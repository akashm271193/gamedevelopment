#version 420
struct Material {
    sampler2D texture_diffuse0;
	sampler2D texture_diffuse1;
	sampler2D texture_diffuse2;
	sampler2D texture_diffuse3;
	sampler2D texture_diffuse4;
	sampler2D texture_specular0;
	sampler2D texture_specular1;
	float texture_diffuseBlendRatio[5];
	float texture_specularBlendRatio[2];
    float shininess;
}; 

uniform Material material;
uniform bool isObjColour;
uniform vec3 objColour;

out vec4 FragColor;
out vec4 FragNormal;
//in vec2 objTexture;

in vec3 objNormal;
in vec2 objTexture;
in vec3 objTangent;
in vec3 objBitangent;


//in vec3 objWorldNormal;
//in vec3 objWorldPos;
uniform float border_width;
uniform float aspect;  // ratio of width to height
uniform int diffuseTextures;
uniform int specularTextures;

void main()
{ 
   float maxX = 1.0 - border_width;
   float minX = border_width;
   float maxY = maxX / aspect;
   float minY = minX / aspect;

   /*if (objTexture.x < maxX && objTexture.x > minX &&objTexture.y < maxY && objTexture.y > minY) {
		 vec4 texResult = texture(material.texture_diffuse1, objTexture);
		 //texResult *= texture(material.texture_diffuse2, objTexture);
		 FragColor = texResult;
	//} else {
		//FragColor = vec4(1.0,1.0,1.0,1.0);
	//  }*/

	
	if(isObjColour)
		FragColor = vec4(objColour,1.0f);
	else{
		vec4 texResult = vec4(0.0,0.0,0.0,1.0);
		//for(int i = 0 ; i < diffuseTextures ; i++){
		//}
		
		texResult += (texture(material.texture_diffuse0, objTexture) * material.texture_diffuseBlendRatio[0])+
					 (texture(material.texture_diffuse1, objTexture) * material.texture_diffuseBlendRatio[1])+
					 (texture(material.texture_diffuse2, objTexture) * material.texture_diffuseBlendRatio[2])+
					 (texture(material.texture_diffuse3, objTexture) * material.texture_diffuseBlendRatio[3])+
					 (texture(material.texture_diffuse4, objTexture) * material.texture_diffuseBlendRatio[4]);

		//for(int i = 0; i < specularTextures ; i++)
			//texResult += texture(material.texture_specular[i], objTexture);
			
		FragColor = texResult;
	}


		
}