#ifndef _MODEL
#define _MODEL

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include "cVertex.h"
#include "cTriangle.h"
#include "cTriangleWithVertices.h"

class cModel
{
public:
	int NumVertices;
	int NumTriangles;
	int NumIndices;

	std::string Name;
	std::string Filename;

	cVertex* Vertices;
	cTriangle* Triangles;
	cTriangleWithVertices* TrianglesWithVertices;

	cModel();
	cModel(std::string modelDir, std::string name);
	~cModel();
private:

};


#endif