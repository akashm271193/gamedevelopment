#include "globalGameStuff.h"

cCamera::cCamera(glm::vec3 position, glm::vec3 up , float yaw, float pitch) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVTY), Zoom(ZOOM)
{
	Position = position;
	WorldUp = up;
	Yaw = yaw;
	Pitch = pitch;
	updateCameraVectors();
	isBound = false;
	isRotating = false;
	rotationCentre = glm::vec3(0.0f);
	this->rotAngle = 0.0f;

}

// Constructor with scalar values
cCamera::cCamera(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVTY), Zoom(ZOOM)
{
	Position = glm::vec3(posX, posY, posZ);
	WorldUp = glm::vec3(upX, upY, upZ);
	Yaw = yaw;
	Pitch = pitch;
	updateCameraVectors();
	isBound = false;
	isRotating = false;
	rotationCentre = glm::vec3(0.0f);
	this->rotAngle = 0.0f;
}

cCamera::~cCamera() {

}

// Returns the view matrix calculated using Eular Angles and the LookAt Matrix
glm::mat4 cCamera::GetViewMatrix()
{
	return glm::lookAt(Position, Position + Front, Up);
}

// Returns the view matrix calculated using Eular Angles and the LookAt Matrix
glm::mat4 cCamera::GetTPSViewMatrix()
{
	return glm::lookAt(Position, Front, Up);
}

// Processes input received from any keyboard-like input system. Accepts input parameter in the form of camera defined ENUM (to abstract it from windowing systems)
void cCamera::ProcessKeyboard(Camera_Movement direction, float deltaTime)
{
	float velocity = MovementSpeed * deltaTime;
	if (direction == FORWARD)
		Position += Front * velocity;
	if (direction == BACKWARD)
		Position -= Front * velocity;
	if (direction == LEFT)
		Position -= Right * velocity;
	if (direction == RIGHT)
		Position += Right * velocity;
}

// Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
void cCamera::ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch)
{
	xoffset *= MouseSensitivity;
	yoffset *= MouseSensitivity;

	Yaw += xoffset;
	Pitch += yoffset;

	// Make sure that when pitch is out of bounds, screen doesn't get flipped
	if (constrainPitch)
	{
		if (Pitch > 89.0f)
			Pitch = 89.0f;
		if (Pitch < -89.0f)
			Pitch = -89.0f;
	}

	// Update Front, Right and Up Vectors using the updated Eular angles
	updateCameraVectors();
}

// Processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
void cCamera::ProcessMouseScroll(float yoffset)
{
	/*if (Zoom >= 1.0f && Zoom <= 45.0f)
		Zoom -= yoffset;
	if (Zoom <= 1.0f)
		Zoom = 1.0f;
	if (Zoom >= 45.0f)
		Zoom = 45.0f;*/
	if (Zoom >= 1.0f && Zoom <= 89.0f)
		Zoom -= yoffset;
	if (Zoom <= 1.0f)
		Zoom = 1.0f;
	if (Zoom >= 89.0f)
		Zoom = 89.0f;
}

void cCamera::bindCameraToAnObject(cGameObject* target){
	glm::vec3 oppDir = -(target->objectFront);
	glm::vec3 relativeCamPos = cameraBindDistance * oppDir;
	//glm::vec3 tempTargetPos = glm::vec3(target->position.x, 0.0f, target->position.z);
	//float currCamDist = glm::distance(tempTargetPos,glm::vec3(this->Position.x,0.0f,this->Position.z));
	//if(currCamDist<cameraBindDistance){
	//	//if (this->isBound)
	//		return;
	//}
	Position = target->position + relativeCamPos;
	Position.y = 1.5f;
	//Front = target->objectFront;
	Pitch = -15.0f;
	//std::cout << "Acos value : " << acos(target->objectFront.x) << std::endl;
	//std::cout << "Asin value : " << glm::degrees(asin(target->objectFront.z)) << std::endl;
	//float tmpAngle = glm::degrees(acos(target->objectFront.x));
	if (target->objectFront.z < 0.0f && target->objectFront.x < 0.0f)
		Yaw = 180.0f - glm::degrees(asin(target->objectFront.z));
	else if (target->objectFront.z < 0.0f)
		Yaw = glm::degrees(asin(target->objectFront.z));
	else
		Yaw = glm::degrees(acos(target->objectFront.x));
	//std::cout << "Player Orientation : " << glm::degrees(target->ownAxisOrientation.y)<< std::endl;
	//std::cout << "Yaw : " << Yaw << std::endl;
	isBound = true;
	updateCameraVectors();
};


bool cCamera::rotateCameraAroundTarget(cGameObject* target,float playerSpeed, float deltaTime) {
	glm::vec3 camXZPos = glm::vec3(Position.x, 0.0f, Position.z);
	glm::vec3 camXZFront = glm::vec3(this->Front.x, 0.0f, this->Front.z);
	if (!this->isRotating) {
		float cosTheta = glm::dot(glm::vec3(1.0f, 0.0f, 0.0f), target->objectFront);
		float ang = glm::degrees(acos(cosTheta));
		if(target->objectFront.z > 0.0f)
			this->rotAngle = fabs(ang);
		else
			this->rotAngle = 360 - fabs(ang);
		//this->rotAngle = ang + 180.0f;
		this->isRotating = true;
	}
	float dist = glm::distance(glm::vec3(target->position.x, 0.0f, target->position.z), camXZPos);
	rotationCentre = target->position;
	this->rotAngle = std::fmod(this->rotAngle, 360.0f);
	//}
	//glm::vec3 oppDir = -(target->objectFront);
	float cosTheta = glm::dot(target->objectFront, camXZFront);
	if (cosTheta >= 0.95) {
		this->isRotating = false;
		this->rotAngle = 0.0f;
		return false;
	}
	rotAngle = glm::degrees(glm::radians(rotAngle) + (playerSpeed * deltaTime));
	rotAngle = std::fmod(rotAngle, 360.0f);
	glm::vec3 oldPos = Position;

	glm::vec3 distVect = target->position - camXZPos;
	distVect = glm::normalize(distVect);
	if (distVect.z < 0.0f && distVect.x < 0.0f)
		Yaw = 180.0f - glm::degrees(asin(distVect.z));
	else if (distVect.z < 0.0f)
		Yaw = glm::degrees(asin(distVect.z));
	else
		Yaw = glm::degrees(acos(distVect.x));
	
	float dispa = (cameraBindDistance * sin(glm::radians(rotAngle)));
	float dispb = (cameraBindDistance * cos(glm::radians(rotAngle)));

	Position.x = this->rotationCentre.x + dispb;
	Position.z = this->rotationCentre.z + dispa;
	//if (rotAngle < 90.0f) {
	//Position.x = Position.x + dispa;
	//Position.z = Position.z - dispb;
	//}
	//else if (rotAngle >= 90.0f && rotAngle < 180.0f) {
	//Position.x = Position.x + dispa;
	//Position.z = Position.z + dispb;
	//}
	//else if (rotAngle >= 180.0f && rotAngle < 270.0f) {
	//Position.x = Position.x - dispa;
	//Position.z = Position.z + dispb;
	//}
	//else if (rotAngle >= 270.0f && rotAngle <= 359.0f) {
	//Position.x = Position.x - dispa;
	//Position.z = Position.z - dispb;
	//}
	updateCameraVectors();

	return true;
};


// Calculates the front vector from the Camera's (updated) Eular Angles
void cCamera::updateCameraVectors()
{
		// Calculate the new Front vector
		glm::vec3 front;
		front.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
		front.y = sin(glm::radians(Pitch));
		front.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
		Front = glm::normalize(front);
		// Also re-calculate the Right and Up vector
		Right = glm::normalize(glm::cross(Front, WorldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
		Up = glm::normalize(glm::cross(Right, Front));
}


void cCamera::createPhysicsWorldObject() {
	this->collisionBodyDesc.position = this->Position;
	this->collisionBodyDesc.oldPosition = this->Position;
	this->collisionBodyDesc.maxExtents = glm::vec3(0.25f,0.25f,0.25f);
	//this->collisionBodyDesc.ownAxisOrientation = this->ownAxisOrientation;
	//this->collisionBodyDesc.oldOrientation = this->ownAxisOrientation;
	this->collisionBodyDesc.objectId = -1;
	this->collisionBodyDesc.scale = glm::vec3(1.0f);
	this->collisionBodyDesc.objectFront = this->Front;
	this->collisionBodyDesc.isColliding = false;
	this->collisionBodyDesc.objType = libPhysics::OBJ_CAMERA;
	this->collisionBodyDesc.phyObjType = libPhysics::PHY_OBJ_KINEMATIC;

	this->collisionBody = gPhysicsFactory->CreateCollisionBody(this->collisionBodyDesc);
	gPhysicsWorld->AddBoundingBox(this->collisionBody);
}


void cCamera::updatePhysicsWorldObject() {
	this->collisionBodyDesc.position = this->Position;
	this->collisionBodyDesc.isColliding = false;
	this->collisionBody->updateCollisionBody(this->collisionBodyDesc);
	gPhysicsWorld->UpdateBoundingBox(this->collisionBody);
}

void cCamera::updateFromPhysicsWorld(std::vector<cGameObject*> gameObjects) {
	this->collisionBody->getCollisionBodyStateFromPhysics(this->collisionBodyDesc);
	if (this->collisionBodyDesc.isColliding == true) {
		std::vector<int> vecIds = this->collisionBodyDesc.collidedBodiesId;
		for (int i = 0; i < vecIds.size(); i++) {
			gameObjects[vecIds[i]]->toDraw = false;
		}
		this->collisionBodyDesc.isColliding = false;
		this->collisionBodyDesc.collidedBodiesId.clear();
		this->updatePhysicsWorldObject();
	}
}