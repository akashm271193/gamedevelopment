#ifndef _OBSERVER_HG_
#define _OBSERVER_HG_

#include <string>
using namespace std;

class Observer
{
public:
	virtual void Notify(string message) = 0;

};


#endif
