#ifndef _sVertexAllWithBones_HG_
#define _sVertexAllWithBones_HG_

#include <string>
#include <glm\glm.hpp>

static const unsigned int NUMBEROFBONES = 4;

struct sVertexAllWithBones
{
	sVertexAllWithBones() :
		Position(0.0f, 0.0f, 0.0f),
		//Colour(0.0f, 0.0f, 0.0f, 0.0f),
		Normal(0.0f, 0.0f, 0.0f),
		TexCoords(0.0f, 0.0f),
		Tangent(0.0f, 0.0f, 0.0f),
		BiNormal(0.0f, 0.0f, 0.0f)
	{
		memset(this->boneID, 0, sizeof(unsigned int) * NUMBEROFBONES);
		memset(this->boneWeights, 0, sizeof(float) * NUMBEROFBONES);
	};

	glm::vec3 Position;
	//glm::vec4 Colour;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
	glm::vec3 Tangent;
	glm::vec3 BiNormal;

	float boneID[NUMBEROFBONES];
	float boneWeights[NUMBEROFBONES];

};

#endif // !_sVertexAllWithBones_HG_
