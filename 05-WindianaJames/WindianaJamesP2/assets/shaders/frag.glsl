#version 430 core

in vec4 texPosition;
uniform sampler2D texture0;

void main( )
{
	gl_FragColor.rgb = texture(texture0, texPosition.xy).rgb;
}