// HelloTriangle.cpp : Defines the entry point for the console application.

#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "cShaderManager.h"
#include "stb_image.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "cCamera.h"
#include <string>
#include "cGameObject.h"
#include "Utilities.h"
#include "cMapLoader.h"
#include "cPlayerGameObject.h"
#include "cAIGameObject.h"
#include "Physics.h"

cShaderManager* gPShaderManager;
std::vector<cGameObject*> gGameObjects;
std::vector<cGameObject*> gBoundingBox;
std::map<std::string,cModel*> gMapLoadedModels;	
glm::vec3 gGravity = glm::vec3(0.0f, -1.0f, 0.0f);
cMapLoader gMapLoader("map.txt");
cMap* ourMap = gMapLoader.getMap();
bool isPlayerHealthZero = false;

int playerGameObjId = -1;


//init config value
float PlayerStartX = 0.0f;
float PlayerStartZ = 0.0f;
float PlayerSpeed = 0.0f;
int NumAngry = 0;
float AngrySpeed = 0.0f;
int NumCurious = 0;
float CuriousSpeed = 0.0f;
int NumFollower = 0;
float FollowerSpeed = 0.0f;
float CameraPosX = 0.0f;
float CameraPosY = 0.0f;
float CameraPosZ = 0.0f;
float CameraUpX = 0.0f;
float CameraUpY = 0.0f;
float CameraUpZ = 0.0f;
float CameraYaw = 0.0f;
float CameraPitch = 0.0f;
float PlayableAreaX = 0.0f;
float PlayableAreaZ = 0.0f;

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// camera
cCamera camera(glm::vec3(20.0f, 10.0f, 5.0f),glm::vec3(0.0f,1.0f,0.0f), -180.0f, -30.0f);
//cCamera camera(glm::vec3(CameraPosX, CameraPosY, CameraPosZ),glm::vec3(CameraUpX, CameraUpY, CameraUpZ), -CameraYaw, -CameraPitch);
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

float deltaTime = 0.0f;	// Time between current frame and last frame
float lastFrame = 0.0f; // Time of last frame

// lighting
glm::vec3 lightPos(0.0f, 10.0f, 100.0f);

void PortalStep(float deltaTime);

void AIWorld(float deltaTime);

bool checkIfCollision(cGameObject* gameObj);

void findValueOnLine(glm::vec3 pointA, glm::vec3 pointB, glm::vec3 &pointC)
{
	glm::vec3 tmpVec = pointB - pointA;
	glm::vec3 normtmpVec = glm::normalize(tmpVec);
	float distance = 2.0f;
	pointC = pointA + (distance*normtmpVec);
}
void loadConfigFromFile(std::string filename);

void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	//camera Movement code
	float cameraSpeed = 2.5f * deltaTime; // adjust accordingly as per the frame render rate
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboard(RIGHT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
	{
		for (int i = 0; i < gGameObjects.size(); i++) {
			if (gGameObjects[i]->typeOfObject == eTypeOfObject::PLAYER)
			{
				gGameObjects[i]->position.x -= PlayerSpeed /*1.0f*/ * deltaTime;
				gGameObjects[i]->boundingSphere->position.x = gGameObjects[i]->position.x;
			}
		}
	}
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		for (int i = 0; i < gGameObjects.size(); i++) {
			if (gGameObjects[i]->typeOfObject == eTypeOfObject::PLAYER)
			{
				gGameObjects[i]->position.x += PlayerSpeed /*1.0f*/ * deltaTime;
				gGameObjects[i]->boundingSphere->position.x = gGameObjects[i]->position.x;

			}
		}
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		for (int i = 0; i < gGameObjects.size(); i++) {
			if (gGameObjects[i]->typeOfObject == eTypeOfObject::PLAYER)
			{
				gGameObjects[i]->position.z += PlayerSpeed /*1.0f*/ * deltaTime;
				gGameObjects[i]->boundingSphere->position.z = gGameObjects[i]->position.z;

			}
		}
	}
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
	{
		for (int i = 0; i < gGameObjects.size(); i++) {
			if (gGameObjects[i]->typeOfObject == eTypeOfObject::PLAYER)
			{
				gGameObjects[i]->position.z -= PlayerSpeed /*1.0f*/ * deltaTime;
				gGameObjects[i]->boundingSphere->position.z = gGameObjects[i]->position.z;

			}
		}
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
	{
		for (int i = 0; i < gGameObjects.size(); i++) {
			if (gGameObjects[i]->typeOfObject == eTypeOfObject::PLAYER)
			{
				gGameObjects[i]->ownAxisOrientation.y += PlayerSpeed /*1.0f*/ * deltaTime;
				gGameObjects[i]->objectFront = glm::vec3(cos(gGameObjects[i]->ownAxisOrientation.y),0.0f, sin(gGameObjects[i]->ownAxisOrientation.y));
				gGameObjects[i]->objectFront = glm::normalize(gGameObjects[i]->objectFront);

			}
		}
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
	{
		for (int i = 0; i < gGameObjects.size(); i++) {
			if (gGameObjects[i]->typeOfObject == eTypeOfObject::PLAYER)
			{
				gGameObjects[i]->ownAxisOrientation.y -= PlayerSpeed /*1.0f*/ * deltaTime;
				gGameObjects[i]->objectFront = glm::vec3(cos(gGameObjects[i]->ownAxisOrientation.y), 0.0f, sin(gGameObjects[i]->ownAxisOrientation.y));
				gGameObjects[i]->objectFront = glm::normalize(gGameObjects[i]->objectFront);
			}
		}
	}

}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}

/*
	glfw: whenever the mouse moves, this callback is called
*/
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}

/* 
	glfw: whenever the mouse scroll wheel scrolls, this callback is called
*/
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}


void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
		
	
	}
}


void window_close_callback(GLFWwindow* window) {

}
/*
	Function to split the string format of co-ordinates in the lastShortestPath
	variable.Every value is in the format (x,z). Based on the points , we calulate 
	their index and find the relevant position for our player to be.
*/
void getPlayerPositionAsPerPath(glm::vec3 &pos, std::string point, int maxColumns) {
	int *vecValues = new int[2];
	point = point.substr(1, point.length() - 2);
	std::string delimiter = ",";

	size_t last = 0;
	size_t next = 0;
	int index = 0;
	std::string token;
	while (index<2)
	{
		next = point.find(delimiter, last);
		token = point.substr(last, next - last);
		vecValues[index] = (std::stoi(token));
		last = next + 1;
		index++;
	}

	int pointIndex = (vecValues[0] * maxColumns) + vecValues[1];
	pos = gGameObjects[pointIndex]->position;
}


void animatePlayer(cGameObject* gameObject, int &pathIndex, float &time, float stepTime) {
	std::string getPoint = ourMap->lastShortestPath->pathNodes[pathIndex];
	glm::vec3 finalPos = glm::vec3(0.0f);
	getPlayerPositionAsPerPath(finalPos,getPoint, ourMap->getMapColumns());
	glm::vec3 playerPos = glm::vec3(0.0f);
	playerPos.x = ((finalPos.x - gameObject->position.x) * stepTime) + gameObject->position.x;
	playerPos.z = ((finalPos.z - gameObject->position.z) * stepTime) + gameObject->position.z;
	gameObject->position = playerPos;
	time = time - stepTime;
	if (time <= 0.0f) {
		time = 1.0f;
		if (pathIndex == ourMap->lastShortestPath->pathLength)
			gameObject->toAnimate = false;
		else
			pathIndex += 1;
	}
};

/*
	main function
*/
int main()
{
	loadConfigFromFile("Scenario.txt");

	//Initializing GLFW
	glfwInit();
	//Setting Major and minor Context versions for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	//Setting the glfw for cor profile to use extra features
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


	GLFWwindow* mainWindow = glfwCreateWindow(800, 600, "Camera", NULL, NULL);
	if (mainWindow == NULL) {
		std::cout << "Error while creating window : \n";
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(mainWindow);
	glfwSetFramebufferSizeCallback(mainWindow, framebuffer_size_callback);
	glfwSetInputMode(mainWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPosCallback(mainWindow, mouse_callback);
	glfwSetScrollCallback(mainWindow, scroll_callback);
	glfwSetMouseButtonCallback(mainWindow, mouse_button_callback);
	//glfwSetWindowCloseCallback(mainWindow, window_close_callback);

	//Load glad for gl commands
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	//Code to load shaders

	::gPShaderManager = new cShaderManager();
	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	vertShader.fileName = "simpleVertex.glsl";
	fragShader.fileName = "simpleFragment.glsl";

	::gPShaderManager->setBasePath("assets/shaders/");

	if (!::gPShaderManager->createProgramFromFile("myCustomShader", vertShader, fragShader)) {
		std::cout << " Error in loading shader " << std::endl;
		return -1;
	}

	glEnable(GL_DEPTH_TEST);
	
	GLint customShaderId = ::gPShaderManager->getIDFromFriendlyName("myCustomShader");
	::gPShaderManager->useShaderProgram(customShaderId);

	// load models
	// -----------
	cModel* floorModel = new cModel("assets/models/floor.obj",false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("floor", floorModel));
	std::cout << "\n Extents for floor models are : \n";
	floorModel->getModelExtents();
	std::cout << "\n";


	/*cModel* wallModel = new cModel("assets/models/human/human.obj", false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("wall", wallModel));
	std::cout << "\n Extents for wall models are : \n";
	wallModel->getModelExtents();
	std::cout << "\n";*/

	cModel* humanModel = new cModel("assets/models/stormTrooper/stormTrooper.obj", false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("curious", humanModel));
	std::cout << "\n Extents for wall models are : \n";
	humanModel->getModelExtents();
	std::cout << "\n";

	cModel* skeletonModel = new cModel("assets/models/dv/vader.obj", false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("angry", skeletonModel));
	std::cout << "\n Extents for wall models are : \n";
	skeletonModel->getModelExtents();
	std::cout << "\n";

	cModel* playerModel = new cModel("assets/models/player/player.obj", false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("player", playerModel));
	std::cout << "\n Extents for wall models are : \n";
	playerModel->getModelExtents();
	std::cout << "\n";
	
	cModel* sphereModel = new cModel("assets/models/sphere.ply",true);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("sphere", sphereModel));
	std::cout << "\n Extents for wall models are : \n";
	sphereModel->getModelExtents();
	std::cout << "\n";

	cModel* dogModel = new cModel("assets/models/dog/courage.obj", true);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("dog", dogModel));
	std::cout << "\n Extents for wall models are : \n";
	sphereModel->getModelExtents();
	std::cout << "\n";


	std::vector<cMesh*> modelMeshes = floorModel->getAllMeshes();
	for (int i = 0; i < modelMeshes.size(); i++) {
		floorModel->addSeparateTexturesToModel("floor.jpg", "assets/textures", "texture_diffuse", modelMeshes[i]);
	}

	modelMeshes.clear();
	/*modelMeshes = wallModel->getAllMeshes();
	for (int i = 0; i < modelMeshes.size(); i++) {
		wallModel->addSeparateTexturesToModel("wall.bmp", "assets/textures", "texture_diffuse", modelMeshes[i]);
	}*/
	
	ourMap->showMap();
	ourMap->findShortestPathTillEnd();

	//system("pause");

	std::vector<cMapCell*> mapCells;
	ourMap->getMapCells(mapCells);


	ourMap->calculateMapExtents(floorModel->maxExtentXYZ);

	int pathIndex = 0;
	glm::vec3 prevCellPos = glm::vec3(-(floorModel->maxExtentXYZ.x / 2), 0.0f, floorModel->maxExtentXYZ.z / 2);
	int rowId = 0, colId = 0;
	ourMap->setWindowMapStartPoint(glm::vec3(0.0f, 0.0f, 0.0f));
	//----------------
	for (int i = 0; i < mapCells.size(); i++) {
		cGameObject* tmpGameObject = new cGameObject();
		glm::vec3 newPos = glm::vec3(1.0f, 1.0f, prevCellPos.z);
		if (mapCells[i]->isWalkable) {
			tmpGameObject->model = gMapLoadedModels["floor"];
			tmpGameObject->colour = glm::vec3(1.0f, 0.0f, 0.0f);
			newPos.y = 0.0f;
		}
			
		else{
			tmpGameObject->model = gMapLoadedModels["wall"];
			tmpGameObject->colour = glm::vec3(1.0f, 1.0f, 1.0f);
			newPos.y = floorModel->maxExtentXYZ.y / 2;
		}
		tmpGameObject->objectId = gGameObjects.size();
		if (mapCells[i]->rowId > rowId) {
			newPos.z = prevCellPos.z + 1;
			prevCellPos.x = -(tmpGameObject->model->maxExtentXYZ.x / 2);
			rowId = mapCells[i]->rowId;
		}
		newPos.x = prevCellPos.x + 1;
		prevCellPos = newPos;
		tmpGameObject->position = newPos;
		tmpGameObject->typeOfObject = eTypeOfObject::PLANE;
		gGameObjects.push_back(tmpGameObject);
	}
	
	{
		cPlayerGameObject* tmpGameObject = new cPlayerGameObject();
		tmpGameObject->model = gMapLoadedModels["player"];
		cGameObject* pOtherGameObject = gGameObjects[ourMap->startCellIndex];
		glm::vec3 newPos = pOtherGameObject->position;
		tmpGameObject->objectId = gGameObjects.size();
		playerGameObjId = tmpGameObject->objectId;
		tmpGameObject->colour = glm::vec3(0.0f, 0.0f, 0.0f);
		tmpGameObject->scale = 1.0;
		tmpGameObject->ownAxisOrientation = glm::vec3(0.0f, glm::radians(90.0f),0.0f);
		tmpGameObject->objectFront = glm::vec3(1.0f,0.0f,0.0f);
		tmpGameObject->typeOfObject = eTypeOfObject::PLAYER;
		tmpGameObject->position = newPos;
		tmpGameObject->toAnimate = true;
		
		cGameObject* tmpGameObject2 = new cGameObject(gMapLoadedModels["sphere"], gMapLoadedModels["player"]->maxExtentXYZ * tmpGameObject->scale, tmpGameObject->position,tmpGameObject->objectId);
		tmpGameObject2->colour = glm::vec3(0.0f, 1.0f, 0.0f);
		tmpGameObject2->diffuseColour = glm::vec3(0.0f, 1.0f, 0.0f);
		tmpGameObject2->typeOfObject = eTypeOfObject::BOUNDING_SPHERE;
		tmpGameObject2->toAnimate = true;
		tmpGameObject2->objectId = gBoundingBox.size();
		tmpGameObject2->parentGameObjectIndexId = tmpGameObject->objectId;
		gBoundingBox.push_back(tmpGameObject2);

		tmpGameObject->boundingSphere = tmpGameObject2;
		tmpGameObject->hasBoundingBoxOrSphere = true;
		gGameObjects.push_back(tmpGameObject);

	}
	
	
	glm::vec3 mapScreenStartPoint,mapScreenEndPoint;
	ourMap->getWindowMapStartPoint(mapScreenStartPoint);
	ourMap->getWindowMapEndPoint(mapScreenEndPoint);
	
	for(int i = 0 ; i < NumCurious ; i++)
	{
		cAIGameObject* tmpGameObject = new cAIGameObject();
		tmpGameObject->model = gMapLoadedModels["curious"];
		tmpGameObject->objectId = gGameObjects.size();
		tmpGameObject->colour = glm::vec3(0.0f, 1.0f,0.0f);
		tmpGameObject->scale = 0.7;
		//tmpGameObject->ownAxisOrientation = glm::vec3(0.0f, glm::radians(90.0f), 0.0f);
		tmpGameObject->ownAxisOrientation = glm::vec3(0.0f, glm::radians(90.0f), 0.0f);
		tmpGameObject->objectFront = glm::vec3(1.0f, 0.0f, 0.0f);
		tmpGameObject->typeOfObject = eTypeOfObject::CURIOUS_AI;
		tmpGameObject->movementMinPos.x = getRandInRange(mapScreenStartPoint.x + 1.0f, mapScreenEndPoint.x - 1.0f);
		tmpGameObject->movementMinPos.y = 0.0f;
		tmpGameObject->movementMinPos.z = getRandInRange(mapScreenStartPoint.z + 1.0f, mapScreenEndPoint.z - 1.0f);
		tmpGameObject->movementMaxPos.x = getMaxRangeAsPerMin(mapScreenStartPoint.x + 1.0f, mapScreenEndPoint.x - 1.0f, tmpGameObject->movementMinPos.x);
		tmpGameObject->movementMaxPos.y = 0.0f;
		tmpGameObject->movementMaxPos.z = getMaxRangeAsPerMin(mapScreenStartPoint.z + 1.0f, mapScreenEndPoint.z - 1.0f, tmpGameObject->movementMinPos.z);
		findValueOnLine(tmpGameObject->movementMinPos, tmpGameObject->movementMaxPos, tmpGameObject->position);
		tmpGameObject->originalOrientationY = 90.0f;
		tmpGameObject->toAnimate = true;

		cGameObject* tmpGameObject2 = new cGameObject(gMapLoadedModels["sphere"], gMapLoadedModels["curious"]->maxExtentXYZ * tmpGameObject->scale, tmpGameObject->position, tmpGameObject->objectId);
		tmpGameObject2->colour = glm::vec3(0.3f, 0.5f, 1.0f);
		tmpGameObject2->diffuseColour = glm::vec3(0.3f, 0.5f, 1.0f);
		tmpGameObject2->typeOfObject = eTypeOfObject::BOUNDING_SPHERE;
		tmpGameObject2->toAnimate = true;
		tmpGameObject2->objectId = gBoundingBox.size();
		tmpGameObject2->parentGameObjectIndexId = tmpGameObject->objectId;
		gBoundingBox.push_back(tmpGameObject2);

		tmpGameObject->boundingSphere = tmpGameObject2;
		tmpGameObject->hasBoundingBoxOrSphere = true;
		tmpGameObject->action = eAiAction::ANIMATE_AI;
		gGameObjects.push_back(tmpGameObject);
	}

	for (int i = 0; i < NumAngry; i++)
	{
		cAIGameObject* tmpGameObject = new cAIGameObject();
		tmpGameObject->model = gMapLoadedModels["angry"];
		//cGameObject* pOtherGameObject = gGameObjects[ourMap->startCellIndex];
		//glm::vec3 newPos = pOtherGameObject->position;
		tmpGameObject->objectId = gGameObjects.size();
		tmpGameObject->colour = glm::vec3(0.0f, 0.0f, 1.0f);
		tmpGameObject->scale = 0.2;
		tmpGameObject->ownAxisOrientation = glm::vec3(0.0f, glm::radians(90.0f), 0.0f);
		tmpGameObject->objectFront = glm::vec3(1.0f, 0.0f, 0.0f);
		tmpGameObject->typeOfObject = eTypeOfObject::ANGRY_AI;
		tmpGameObject->movementMinPos.x = getRandInRange(mapScreenStartPoint.x + 1.0f, mapScreenEndPoint.x - 1.0f);
		tmpGameObject->movementMinPos.y = 0.0f;
		tmpGameObject->movementMinPos.z = getRandInRange(mapScreenStartPoint.z + 1.0f, mapScreenEndPoint.z - 1.0f);
		tmpGameObject->movementMaxPos.x = getMaxRangeAsPerMin(mapScreenStartPoint.x + 1.0f, mapScreenEndPoint.x - 1.0f, tmpGameObject->movementMinPos.x);
		tmpGameObject->movementMaxPos.y = 0.0f;
		tmpGameObject->movementMaxPos.z = getMaxRangeAsPerMin(mapScreenStartPoint.z + 1.0f, mapScreenEndPoint.z - 1.0f, tmpGameObject->movementMinPos.z);
		tmpGameObject->position.x = getRandInRange(tmpGameObject->movementMinPos.x, tmpGameObject->movementMaxPos.x);
		tmpGameObject->position.y = 0.0f;
		tmpGameObject->position.z = getRandInRange(tmpGameObject->movementMinPos.z, tmpGameObject->movementMinPos.z);
		tmpGameObject->toAnimate = true;


		cGameObject* tmpGameObject2 = new cGameObject(gMapLoadedModels["sphere"], gMapLoadedModels["angry"]->maxExtentXYZ * tmpGameObject->scale, tmpGameObject->position, tmpGameObject->objectId);
		tmpGameObject2->colour = glm::vec3(0.9f, 0.3f, 0.6f);
		tmpGameObject2->diffuseColour = glm::vec3(0.9f, 0.3f, 0.6f);
		tmpGameObject2->typeOfObject = eTypeOfObject::BOUNDING_SPHERE;
		tmpGameObject2->toAnimate = true;
		tmpGameObject2->objectId = gBoundingBox.size();
		tmpGameObject2->parentGameObjectIndexId = tmpGameObject->objectId;
		gBoundingBox.push_back(tmpGameObject2);

		tmpGameObject->action = eAiAction::ANIMATE_AI;
		tmpGameObject->boundingSphere = tmpGameObject2;
		tmpGameObject->hasBoundingBoxOrSphere = true;
		gGameObjects.push_back(tmpGameObject);

	}


	for (int i = 0; i < NumFollower; i++)
	{
		cAIGameObject* tmpGameObject = new cAIGameObject();
		tmpGameObject->model = gMapLoadedModels["dog"];
		//cGameObject* pOtherGameObject = gGameObjects[ourMap->startCellIndex];
		//glm::vec3 newPos = pOtherGameObject->position;
		tmpGameObject->objectId = gGameObjects.size();
		tmpGameObject->colour = glm::vec3(0.580f, 0.000f, 0.827f);
		tmpGameObject->scale = 0.1;
		tmpGameObject->ownAxisOrientation = glm::vec3(0.0f, glm::radians(90.0f), 0.0f);
		tmpGameObject->objectFront = glm::vec3(1.0f, 0.0f, 0.0f);
		tmpGameObject->typeOfObject = eTypeOfObject::FOLLOWER_AI;
		tmpGameObject->movementMinPos.x = getRandInRange(mapScreenStartPoint.x + 1.0f, mapScreenEndPoint.x - 1.0f);
		tmpGameObject->movementMinPos.y = 0.0f;
		tmpGameObject->movementMinPos.z = getRandInRange(mapScreenStartPoint.z + 1.0f, mapScreenEndPoint.z - 1.0f);
		tmpGameObject->movementMaxPos.x = getMaxRangeAsPerMin(mapScreenStartPoint.x + 1.0f, mapScreenEndPoint.x - 1.0f, tmpGameObject->movementMinPos.x);
		tmpGameObject->movementMaxPos.y = 0.0f;
		tmpGameObject->movementMaxPos.z = getMaxRangeAsPerMin(mapScreenStartPoint.z + 1.0f, mapScreenEndPoint.z - 1.0f, tmpGameObject->movementMinPos.z);
		tmpGameObject->position.x = getRandInRange(tmpGameObject->movementMinPos.x, tmpGameObject->movementMaxPos.x);
		tmpGameObject->position.y = 0.0f;
		tmpGameObject->position.z = getRandInRange(tmpGameObject->movementMinPos.z, tmpGameObject->movementMinPos.z);
		tmpGameObject->toAnimate = true;


		cGameObject* tmpGameObject2 = new cGameObject(gMapLoadedModels["sphere"], gMapLoadedModels["dog"]->maxExtentXYZ * tmpGameObject->scale, tmpGameObject->position, tmpGameObject->objectId);
		tmpGameObject2->colour = glm::vec3(0.3f, 0.8f, 0.6f);
		tmpGameObject2->diffuseColour = glm::vec3(0.3f, 0.8f, 0.6f);
		tmpGameObject2->typeOfObject = eTypeOfObject::BOUNDING_SPHERE;
		tmpGameObject2->toAnimate = true;
		tmpGameObject2->objectId = gBoundingBox.size();
		tmpGameObject2->parentGameObjectIndexId = tmpGameObject->objectId;
		gBoundingBox.push_back(tmpGameObject2);

		tmpGameObject->action = eAiAction::ANIMATE_AI;
		tmpGameObject->boundingSphere = tmpGameObject2;
		tmpGameObject->hasBoundingBoxOrSphere = true;
		gGameObjects.push_back(tmpGameObject);
	}

	float timeToAAnimateOneStep = 1.0f;
	float stepTime = timeToAAnimateOneStep/10 ;
	
	unsigned int borderWidthLoc = glGetUniformLocation(customShaderId, "border_width");
	glUniform1f(borderWidthLoc,0.1f);


	unsigned int aspectRatioLoc = glGetUniformLocation(customShaderId, "aspect");
	glUniform1f(aspectRatioLoc, SCR_WIDTH/SCR_HEIGHT);


	//Main Window Loop 
	while (!glfwWindowShouldClose(mainWindow))
	{
		//per-frame logic
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		//Call to check for input
		processInput(mainWindow);
			
		glClearColor(0.3f, 0.5f, 0.4f, 1.0f);
		//gl command to clear the color buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		PortalStep(deltaTime);
		AIWorld(deltaTime);


		// get matrix's uniform location and set matrix
		::gPShaderManager->useShaderProgram(customShaderId);

		glm::mat4 view = glm::mat4(1.0f);
		view = camera.GetViewMatrix();


		glm::mat4 projection;
		projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);

		unsigned int viewLoc = glGetUniformLocation(customShaderId, "view");
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

		unsigned int projectionLoc = glGetUniformLocation(customShaderId, "projection");
		glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

		for (int i = 0; i < gGameObjects.size(); i++) {
			if (gGameObjects[i]->typeOfObject == eTypeOfObject::PLAYER) {
				cPlayerGameObject* tmpPlayerObj = dynamic_cast<cPlayerGameObject*>(gGameObjects[i]);
				std::cout << "\n Player Health : " << tmpPlayerObj->playerHealth << std::endl;
				if (tmpPlayerObj->playerHealth <= 0.0f) {
					isPlayerHealthZero = true;
					glfwSetWindowShouldClose(mainWindow, GLFW_TRUE);
				}

					
			}

			gGameObjects[i]->Draw(customShaderId,true);

			/*if(gGameObjects[i]->typeOfObject == eTypeOfObject::PLAYER && gGameObjects[i]->toAnimate)
				animatePlayer(gGameObjects[i],pathIndex, timeToAAnimateOneStep,stepTime);
			/*if (gGameObjects[i]->typeOfObject == eTypeOfObject::BOUNDING_SPHERE && gGameObjects[i]->toAnimate)
				animatePlayer(gGameObjects[i], pathIndex, timeToAAnimateOneStep, stepTime);*/
			}		

		

		glfwSwapBuffers(mainWindow);
		glfwPollEvents();


	}
	// glfw: terminate, clearing all previously allocated GLFW resources.
	// ------------------------------------------------------------------
	glfwTerminate();
	

	if(isPlayerHealthZero){
		std::cout << "\n Player health is zero. Game over ! \n";
		system("pause");
	}
	return 0;
}


void PortalStep(float deltaTime)
{
	glm::vec3 mapStartPoint, mapEndPoint;


	ourMap->getWindowMapStartPoint(mapStartPoint);
	ourMap->getWindowMapEndPoint(mapEndPoint);
	float reApperaingDistance = 0.01f;

	//check if object goes out of range
	for (int index = 0; index != gGameObjects.size(); index++)
	{

		if (gGameObjects[index]->typeOfObject != eTypeOfObject::PLANE)
		{

			//compare with x values of furthest vertex point of floor
			if (gGameObjects[index]->position.z < mapStartPoint.z)
			{
				gGameObjects[index]->position.z = mapEndPoint.z - reApperaingDistance;
			}
			if (gGameObjects[index]->position.x < mapStartPoint.x)
			{
				gGameObjects[index]->position.x = mapEndPoint.x - reApperaingDistance;
			}
			if (gGameObjects[index]->position.z > mapEndPoint.z)
			{
				gGameObjects[index]->position.z = mapStartPoint.x + reApperaingDistance;
			}
			if (gGameObjects[index]->position.x > mapEndPoint.x)
			{
				gGameObjects[index]->position.x = mapStartPoint.x + reApperaingDistance;
			}
		}
	}

}

void AIWorld(float deltaTime){
	cPlayerGameObject* player = dynamic_cast<cPlayerGameObject*>(gGameObjects[playerGameObjId]);
	
	bool isColliding = false;
	if (checkIfCollision(player->boundingSphere)) {
		isColliding = true;
		player->playerHealth -= 0.5f;
	}

	for (int i = 0; i < gBoundingBox.size(); i++) {
		if (gBoundingBox[i]->objectId == player->boundingSphere->objectId) {			
			if (!isColliding)
				gBoundingBox[i]->colour = gBoundingBox[i]->diffuseColour;
			continue;
		}

		cAIGameObject* tmpAIGameObject = dynamic_cast<cAIGameObject*>(gGameObjects[gBoundingBox[i]->parentGameObjectIndexId]);
		if (!isColliding && tmpAIGameObject->action == eAiAction::ANIMATE_AI) {
			if (tmpAIGameObject->typeOfObject == eTypeOfObject::CURIOUS_AI) {
				gBoundingBox[i]->colour = gBoundingBox[i]->diffuseColour;
			}
			else if (tmpAIGameObject->typeOfObject == eTypeOfObject::ANGRY_AI) {
				gBoundingBox[i]->colour = gBoundingBox[i]->diffuseColour;
			}
		}
		tmpAIGameObject->animateObject(deltaTime,player);
	}
}


bool checkIfCollision(cGameObject* playerBoundingSphere) {
		for (int i = 0 ; i < gBoundingBox.size(); i++) {
			if (playerBoundingSphere->objectId == gBoundingBox[i]->objectId)
				continue;

			if (PenetrationTestSphereSphere(playerBoundingSphere,gBoundingBox[i])) {
				playerBoundingSphere->colour = glm::vec3(1.0f, 1.0f, 1.0f);
				gBoundingBox[i]->colour = glm::vec3(0.0f, 0.0f, 0.0f);
				return true;
			}
		}
		return false;
}


void loadConfigFromFile(std::string filename){
	std::ifstream configFile(filename);
	if (!configFile.is_open()) {
		std::cout << "Error in reading the config file" << std::endl;
		return;
	}
	else {
		std::cout << "File read. Storing config now !" << std::endl;
		std::string strConfigValue;
		/*float PlayerStartX = 0.0f;
		float PlayerStartZ = 0.0f;
		float PlayerSpeed = 0.0f;
		int NumAngry = 0;
		float AngrySpeed = 0.0f;
		int NumCurious = 0;
		float CuriousSpeed = 0.0f;
		float CameraPosX = 0.0f;
		float CameraPosY = 0.0f;
		float CameraPosZ = 0.0f;
		float CameraUpX = 0.0f;
		float CameraUpY = 0.0f;
		float CameraUpZ = 0.0f;
		float CameraYaw = 0.0f;
		float CameraPitch = 0.0f;
		float PlayableAreaX = 0.0f;
		float PlayableAreaZ = 0.0f;*/

		configFile >> strConfigValue;			//"Config:"

		configFile >> strConfigValue;				//"PlayerStartPoint"
		configFile >> strConfigValue;				//"PlayerStartX"
		PlayerStartX = std::stof(strConfigValue);
		configFile >> strConfigValue;				//"PlayerStartZ"
		PlayerStartZ = std::stof(strConfigValue);	

		configFile >> strConfigValue;				//"PlayerStartSpeed"
		configFile >> strConfigValue;				//"PlayerSpeed"
		PlayerSpeed = std::stof(strConfigValue);

		configFile >> strConfigValue;				//"NumAngry"
		configFile >> strConfigValue;				//"NumAngry"
		NumAngry = std::stof(strConfigValue);

		configFile >> strConfigValue;				//"AngrySpeed"
		configFile >> strConfigValue;				//"AngrySpeed"
		AngrySpeed = std::stof(strConfigValue);

		configFile >> strConfigValue;				//"NumCurious"
		configFile >> strConfigValue;				//"NumCurious"
		NumCurious = std::stof(strConfigValue);

		configFile >> strConfigValue;				//"CuriousSpeed"
		configFile >> strConfigValue;				//"CuriousSpeed"
		CuriousSpeed = std::stof(strConfigValue);

		configFile >> strConfigValue;				//"NumFollower"
		configFile >> strConfigValue;				//"NumFollower"
		NumFollower = std::stof(strConfigValue);

		configFile >> strConfigValue;				//"FollowerSpeed"
		configFile >> strConfigValue;				//"FollowerSpeed"
		FollowerSpeed = std::stof(strConfigValue);

		configFile >> strConfigValue;				//"CameraPosition"
		configFile >> strConfigValue;				//"CameraPosX"
		CameraPosX = std::stof(strConfigValue);
		configFile >> strConfigValue;				//"CameraPosY"
		CameraPosY = std::stof(strConfigValue);
		configFile >> strConfigValue;				//"CameraPosZ"
		CameraPosZ = std::stof(strConfigValue);

		configFile >> strConfigValue;				//"CameraUp"
		configFile >> strConfigValue;				//"CameraUpZ"
		CameraUpX = std::stof(strConfigValue);
		configFile >> strConfigValue;				//"CameraUpY"
		CameraUpY = std::stof(strConfigValue);
		configFile >> strConfigValue;				//"CameraUpY"
		CameraUpZ = std::stof(strConfigValue);

		configFile >> strConfigValue;				//"CameraYaw"
		configFile >> strConfigValue;				//"CameraYaw"
		CameraYaw = std::stof(strConfigValue);

		configFile >> strConfigValue;				//"CameraPitch"
		configFile >> strConfigValue;				//"CameraPitch"
		CameraPitch = std::stof(strConfigValue);

		configFile >> strConfigValue;				//"PlayableArea"
		configFile >> strConfigValue;				//"PlayableAreaX"
		PlayableAreaX = std::stof(strConfigValue);
		configFile >> strConfigValue;				//"PlayableAreaZ"
		PlayableAreaZ = std::stof(strConfigValue);
	}
}