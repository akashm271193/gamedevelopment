width 1080
height 720
maxGameObjects 11
meshBasePath assets/meshes/
shaderBasePath assets/shaders/
sizeOfWorld 4.0f
layoutFormat LINE
gameObjectsFile gameObjects.txt
cameraCoordinates 0.0f,20.0f,60.0f
cameraTarget 0.0f,0.0f,0.0f
cameraSpeed 0.1f
title [s] Murder For Mjolnir [e]
#####