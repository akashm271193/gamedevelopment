#version 330 core
layout (location = 0) in vec3 objPos;
uniform mat4 transform;

void main(){
	gl_Position = transform * vec4(objPos.x,objPos.y,objPos.z,1.0);
}