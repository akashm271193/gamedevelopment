#ifndef _cPhysicsWorld_HG_
#define _cPhysicsWorld_HG_

#include "cAABBBroadPhase.h"
#include "../cGameObject.h"
#include "../cVAOMeshManager.h"

class cPhysicsWorld
{
public:
	cPhysicsWorld();
	~cPhysicsWorld();

	//void IntegrationStep(double deltaTime, cAABBBroadPhase* pAABBBroadPhase, std::vector<cGameObject*> vecGameObjects);

	bool isColliding(cAABBBroadPhase* pAABBBroadPhase, std::vector<cGameObject*> vecGameObjects,cVAOMeshManager* meshManager, glm::vec3 position);

};

#endif
