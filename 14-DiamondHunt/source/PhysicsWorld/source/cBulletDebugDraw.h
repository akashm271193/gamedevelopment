#pragma once
#include "iDebugRenderer.h"
#include "LinearMath\btIDebugDraw.h"

namespace libPhysics
{
	class cBulletDebugDraw : public btIDebugDraw
	{
	public:
		cBulletDebugDraw(iDebugRenderer* renderer);
		virtual ~cBulletDebugDraw();
		virtual void	drawBox(const btVector3& extents, const btTransform& trans, const btVector3& color);
		virtual void	drawBox(const btVector3& bbMin, const btVector3& bbMax, const btVector3& color);
		virtual void	drawBox(const btVector3& bbMin, const btVector3& bbMax, const btTransform& trans, const btVector3& color);
		virtual void	drawLine(const btVector3& from, const btVector3& to, const btVector3& color);
		virtual void	drawSphere(btScalar radius, const btTransform& transform, const btVector3& color);
		virtual void	drawCone(btScalar radius, btScalar height, int upAxis, const btTransform& transform, const btVector3& color);
		virtual void	drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color);
		virtual void	reportErrorWarning(const char* warningString);
		virtual void	draw3dText(const btVector3& location, const char* textString);
		virtual void	setDebugMode(int debugMode);
		virtual int		getDebugMode() const;
		virtual void	drawPlane(const btVector3& planeNormal, btScalar planeConst, const btTransform& transform, const btVector3& color);
	protected:
		iDebugRenderer* mRender;
	};
}