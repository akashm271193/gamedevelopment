#ifndef _SPRITE_RENDERER_
#define _SPRITE_RENDERER_

#include <glad\glad.h>
#include <GLFW\glfw3.h>

#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <string>
#include <map>

class cModel;
class cShader;
class cTexture;

struct sRenderInfo
{
	GLuint VAO, VBO, EBO;
	std::string ModelName;
	GLint ShaderID;
	int NumIndices;
	sRenderInfo()
	{
		this->VAO = this->VBO = this->EBO = 0;
		this->ModelName = "";
		this->ShaderID = 0;
		this->NumIndices = 0;
	}
};

class cVAOManager
{
public:
	void LoadTexturedModel(cModel model, cShader shader);
	void DrawTexturedModel3D(sRenderInfo renderInfo, cTexture &texture, glm::vec3 position, glm::vec3 scale = glm::vec3(1.0f), glm::vec3 orientation = glm::vec3(0.0f));
	bool FindModelToDraw(std::string name, sRenderInfo & renderInfo);

private:
	std::map<std::string, sRenderInfo> RenderInformations;
};
#endif // !_MODEL_RENDERER_
