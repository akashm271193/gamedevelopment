#include "include\glad\glad.h"
#include "include\GLFW\glfw3.h"

#include <glm\vec3.hpp>
#include <glm\vec4.hpp>
#include <glm\mat4x4.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\matrix_transform.hpp>



#include "cModel.h"
#include "cLightManager.h"

#include "cCamera.h"
#include "cFactory.h"
#include "cResourceManager.h"
#include "cVAOManager.h"
#include "cGameObject.h"
#include "cGameLevel.h"

#include "Texture\cBasicTextureManager.h"

#include <iostream>
#include <fstream>
#include <vector>


GLfloat deltaTime;
GLfloat lastFrame;
cCamera* Camera;
int screenWidth, screenHeight;
GLfloat lastX = screenWidth / 2.0f;
GLfloat lastY = screenHeight / 2.0f;
bool keys[1024];
bool firstMouse = true;

//Variable to ensure that space bar presses only trigger one movement
bool spaceDown = false;

//A variable that will range from 0 to 3, denoting whether the player is currently facing right, left, up, or down
int facing = 0;


cVAOManager* Renderer;
MonsterFactory* Factory = nullptr;

std::vector<cGameLevel> Levels;
int Level = 0;


static void errorCallback(int error, const char* description);
static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
static void mouseCallback(GLFWwindow* window, double xPos, double yPos);
static void doMovement(Entity* HeroMan, cGameLevel &gameLevel);

int main(void)
{
	GLFWwindow* window;

	glfwSetErrorCallback(errorCallback);

	if (!glfwInit())
	{
		printf("GLFW can't be initialized.\n");
		return 0;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_MAXIMIZED, GL_TRUE);

	window = glfwCreateWindow(1920, 1080, "Windiana James", NULL, NULL);
	if (!window)
	{
		printf("Window can't be created.\n");
		glfwTerminate();
		return 0;
	}

	glfwGetFramebufferSize(window, &screenWidth, &screenHeight);
	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	glViewport(0, 0, screenWidth, screenHeight);

	glfwSetKeyCallback(window, keyCallback);
	glfwSetCursorPosCallback(window, mouseCallback);

	glEnable(GL_DEPTH);
	

	cResourceManager::LoadShader("assets/shaders/vertex.glsl", "assets/shaders/frag.glsl", "Model");

	cResourceManager::LoadModel("assets/models/Teapot.ply", "Windiana");
	cResourceManager::LoadModel("assets/models/Wall.ply", "Wall");
	cResourceManager::LoadModel("assets/models/Floor.ply", "Floor");

	cResourceManager::LoadTexture("assets/textures/processed/windianaJames.png", GL_TRUE, "Windiana");
	cResourceManager::LoadTexture("assets/textures/processed/Wall.png", GL_TRUE, "Wall");
	cResourceManager::LoadTexture("assets/textures/processed/Floor.png", GL_TRUE, "Floor");


	Renderer = new cVAOManager();
	Factory = new MonsterFactory();
	
	


	Renderer->LoadTexturedModel(cResourceManager::GetModel("Wall"), cResourceManager::GetShader("Model"));
	Renderer->LoadTexturedModel(cResourceManager::GetModel("Floor"), cResourceManager::GetShader("Model"));
	cGameLevel one;
	
	one.Load("assets/levels/one.txt", screenWidth , screenHeight);
	Levels.push_back(one);

	Entity* Windiana = Factory->CreateObjectOfType("Windiana",one.startPosition);
	//Camera = new cCamera(glm::vec3(50.0f, 20.0f, 10.0f));
	Camera = new cCamera(glm::vec3(2.7954f, 34.6463f, -48.414f));

	

	glm::mat4 projection = glm::perspective(45.0f, (GLfloat)screenWidth / (GLfloat)screenHeight, 0.1f, 1000.0f);
	cResourceManager::GetShader("Model").SetMatrix4f("projection", projection, true);

	cResourceManager::GetShader("Model").Use().SetInteger("texture0", 0);




	while (!glfwWindowShouldClose(window))
	{
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		glfwPollEvents();
		doMovement(Windiana,Levels[Level]);

		glClearColor(0.0f, 0.5f, 0.5f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glm::mat4 view = Camera->GetViewMatrix();
		cResourceManager::GetShader("Model").SetMatrix4f("view", view, true);

		Levels[Level].Draw(*Renderer);
		Factory->FactoryObjects["Windiana"].Draw(*Renderer);
		

		glfwSwapBuffers(window);
	}
	return 0;
}

static void errorCallback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}
static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (GLFW_KEY_ESCAPE == key && GLFW_PRESS == action)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
		{
			keys[key] = true;
		}
		else if (action == GLFW_RELEASE)
		{
			keys[key] = false;
			if (key == GLFW_KEY_SPACE)
			{
				spaceDown = false;
			}
		}
	}
}
static void mouseCallback(GLFWwindow *window, double xPos, double yPos)
{
	//if (firstMouse)
	//{
	//	lastX = xPos;
	//	lastY = yPos;
	//	firstMouse = false;
	//}

	//GLfloat xOffset = xPos - lastX;
	//GLfloat yOffset = lastY - yPos;

	//lastX = xPos;
	//lastY = yPos;

	//Camera->ProcessMouseMovement(xOffset, yOffset);
}
void doMovement(Entity* HeroMan, cGameLevel &gameLevel)
{
	if (keys[GLFW_KEY_UP])
	{
		Camera->ProcessKeyboard(FORWARD, deltaTime);
	}

	if (keys[GLFW_KEY_DOWN])
	{
		Camera->ProcessKeyboard(BACKWARD, deltaTime);
	}

	if (keys[GLFW_KEY_LEFT])
	{
		Camera->ProcessKeyboard(LEFT, deltaTime);
	}

	if (keys[GLFW_KEY_RIGHT])
	{
		Camera->ProcessKeyboard(RIGHT, deltaTime);
	}


	//Makes changes to the facing variable
	if (keys[GLFW_KEY_A])
	{
		facing = 1;
	}
	if (keys[GLFW_KEY_S])
	{
		facing = 3;
	}
	if (keys[GLFW_KEY_W])
	{
		facing = 2;
	}
	if (keys[GLFW_KEY_D])
	{
		facing = 0;
	}

	//Triggers a movement action, passing the facing variable
	if (keys[GLFW_KEY_SPACE])
	{
		if(!spaceDown)
			Factory->makeWindyMove(facing, HeroMan,gameLevel);
		spaceDown = true;
	}
}