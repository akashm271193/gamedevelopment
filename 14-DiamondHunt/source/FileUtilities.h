#pragma once

#include <rapidjson\document.h>
#include <rapidjson\writer.h>
#include <rapidjson\stringbuffer.h>
#include <rapidjson\filereadstream.h>
#include <rapidjson\filewritestream.h>
#include <cstdio>
#include "globalGameStuff.h"

rapidjson::Document readConfigFromJsonFile(std::string fileNameWithPath);

void readGameConfigFile(std::string file);
void writeDataToJsonFile(rapidjson::Document &doc, std::string fileNameWithPath);