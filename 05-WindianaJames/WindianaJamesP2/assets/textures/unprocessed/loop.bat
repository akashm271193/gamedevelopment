for %%f in (*.png) do (
	UsefulStuff\pngquant\pngquant --quality=50-80 %%f -ext %%f
	UsefulStuff\optipng\optipng %%f
	UsefulStuff\pvrtextool\PVRTexToolCLI.exe -i %%f -o %%~nf.dds -f BC3
	echo "%~dp0..\processed"
	move %%f "%~dp0..\processed"
	move %%~nf.dds "%~dp0..\processed"
)
