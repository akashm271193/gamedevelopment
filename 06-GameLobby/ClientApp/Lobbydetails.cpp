#include "Lobbydetails.h"



Lobbydetails::Lobbydetails()
{
	this->maps = new std::string[5]{ "NewYork","Boston", "London", "Berlin", "Moscow" };
	this->gameModes = new std::string[5]{"FreeForAll","Team","CaptureTheFlag","DeathMatch","LastManStanding"};
}


Lobbydetails::~Lobbydetails()
{
}

void Lobbydetails::getAllDetails() {
	std::cout << "For creating a lobby, you can use one of the maps &  game modes mentioned below : \n";
	this->getMaps();
	this->getGameModes();
}
void Lobbydetails::getMaps() {
	std::cout << "Maps Available : \n";
	for (int i = 0; i < 5; i++) {
		std::cout << this->maps[i] << "\n";
	}
}
void Lobbydetails::getGameModes() {
	std::cout << "Game Modes Available : \n";
	for (int i = 0; i < 5; i++) {
		std::cout << this->gameModes[i] << "\n";
	}
}

bool Lobbydetails::checkLobbyIsValid(std::string map, std::string gameMode) {
	bool isMapValid = false;
	bool isGameModeValid = false;
	if (isPresentInArray(this->maps, map) == true)
		isMapValid = true;
	if (isPresentInArray(this->maps, map) == true)
		isGameModeValid = true;

	if (isMapValid && isGameModeValid)
		return true;
	if (isMapValid == false) {
		std::cout << "Map is invalid . Please use one of the following : \n ";
		this->getMaps();
	}
	if (isGameModeValid == false) {
		std::cout << "GameMode is invalid . Please use one of the following : \n ";
		this->getGameModes();
	}
	return false;
}

bool Lobbydetails::isPresentInArray(std::string *strArray, std::string item) {
	for (int i = 0; i < 5; i++) {
		if (isStringEquals(strArray[i], item))
			return true;	
	}
	return false;
};

bool Lobbydetails::isStringEquals(std::string a, std::string b)
{
	unsigned int sz = a.size();
	if (b.size() != sz)
		return false;
	for (unsigned int i = 0; i < sz; ++i)
		if (tolower(a[i]) != tolower(b[i]))
			return false;
	return true;
}
