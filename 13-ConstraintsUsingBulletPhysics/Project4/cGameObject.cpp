#include "cGameObject.h"
#include "nConvert.h"

cGameObject::cGameObject(std::string modelName, std::string modelDir, eObjectType objectType)
{
	this->Model = new cModel(modelDir.c_str(),false);

	this->Position = glm::vec3(0.0f);
	this->Scale = glm::vec3(1.0f);
	this->OrientationQuat = glm::quat(0.0f, 0.0f, 0.0f, 1.0f);
	this->OrientationEuler = glm::vec3(0.0f);
	this->Velocity = glm::vec3(0.0f);
	this->Mass = 1.0f;

	this->ObjectType = objectType;
}

cGameObject::cGameObject(cModel* model, glm::vec3 position, glm::vec3 scale, glm::vec3 orientationEuler, float mass, eObjectType objectType, btDiscreteDynamicsWorld* world)
{
	this->Model = model;

	this->Position = position;
	this->Scale = scale;
	this->OrientationQuat = glm::quat(orientationEuler);
	this->OrientationEuler = orientationEuler;
	this->Velocity = glm::vec3(0.0f);
	this->Mass = mass;
	this->ObjectType = objectType;
	this->world = world;
	setUpGameObject();
}

cGameObject::cGameObject(std::string modelName, std::string modelDir, glm::vec3 position, glm::vec3 scale, glm::vec3 orientationEuler, float mass, eObjectType objectType, btDiscreteDynamicsWorld* world)
{
	this->Model = new cModel(modelDir.c_str(),false);

	this->Position = position;
	this->Scale = scale;
	this->OrientationQuat = glm::quat(orientationEuler);
	this->OrientationEuler = orientationEuler;
	this->Velocity = glm::vec3(0.0f);
	this->Mass = mass;
	this->ObjectType = objectType;
	this->world = world;
	setUpGameObject();
}

void cGameObject::setUpGameObject() {
	switch (ObjectType)
	{
	case OBJECT_SPHERE:
		shape = new btSphereShape(Scale.x);
		break;
	case OBJECT_PLANE:
		shape = new btStaticPlaneShape(btVector3(0.0f, 1.0f, 0.0f), 1.0f);
		break;
	case OBJECT_CUBE:
		shape = new btBoxShape(btVector3(Scale.x / 2.0f, Scale.y / 2.0f, Scale.z / 2.0f));
		break;
	case OBJECT_CYLINDER:
		shape = new btCylinderShape(btVector3(Scale.x, Scale.y, Scale.z));
		break;
	case OBJECT_CONE:
		shape = new btConeShape(Scale.x, Scale.y * 2.0f);
		break;
	default:
		break;
	}

	btTransform transform;
	transform.setIdentity();
	btVector3 bulletPos = ToBullet(Position);
	transform.setOrigin(bulletPos);

	btVector3 inertia(0.0f, 0.0f, 0.0f);
	if (Mass != 0.0f)
		shape->calculateLocalInertia(Mass, inertia);

	state = new btDefaultMotionState(transform);
	info = new btRigidBody::btRigidBodyConstructionInfo(Mass, state, shape, inertia);
	body = new btRigidBody(*info);
	body->setRestitution(0.95f);

	this->world->addRigidBody(body);
}


void cGameObject::Draw(int shaderId)
{
	this->Model->DrawModel(shaderId);
}