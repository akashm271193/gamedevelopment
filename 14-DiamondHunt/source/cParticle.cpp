#include "cParticle.h"

cParticle::cParticle()
{
	this->position = glm::vec3(0.0f);
	this->velocity = glm::vec3(0.0f);
	this->acceleration = glm::vec3(0.0f);
	this->colour = glm::vec4(0.0f);
	this->lifetime = 0.0f;
	this->scaleStep = 0.0f;
}

void cParticle::Update(float deltaTime)
{
	//euler stuff
	//velocity from acceleration
	this->velocity += this->acceleration * deltaTime;

	//position from velocity
	this->position += this->velocity * deltaTime;

	
	this->lifetime -= deltaTime;
	if ((this->scale.x - this->scaleStep) >= 0.0f)
		this->scale -= this->scaleStep;
	return;
}