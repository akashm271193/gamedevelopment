#ifndef _EXTERN_HG_
#define _EXTERN_HG_

#include "iPhysicsFactory.h"

extern libPhysics::iPhysicsFactory* gPhysicsFactory;
extern libPhysics::iPhysicsWorld* gPhysicsWorld;
bool InitPhysics();

//extern int currentBall;
//extern glm::vec3 outsideForce;

#endif