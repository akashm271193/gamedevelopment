// HelloTriangle.cpp : Defines the entry point for the console application.

#include "cShaderManager.h"
#include "cGLFWCalls.h"
#include "stb_image.h"
#include "cCamera.h"
#include <string>
#include "cGameObject.h"
#include "cPhysicsWorld.h"
#include "nConvert.h"
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>	

cShaderManager* gPShaderManager;
cGLFWCalls* glfwCaller;
GLFWwindow* mainWindow;
cPhysicsWorld* world;
std::map<std::string, cModel*> gMapLoadedModels;


// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;
std::string windowTitle = "Project 2";
// camera
cCamera camera(glm::vec3(0.0f, 30.0f, 70.0f));
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

float deltaTime = 0.0f;	// Time between current frame and last frame
float lastFrame = 0.0f; // Time of last frame

std::vector<cGameObject*> physicsObjects;
int curObj = 0;


// lighting
glm::vec3 lightPos(0.0f, 10.0f, 100.0f);

void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	//camera Movement code
	//float cameraSpeed = 4.0f * deltaTime; // adjust accordingly as per the frame render rate
	camera.MovementSpeed = 4.0f;
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboard(RIGHT, deltaTime);

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
	{

		physicsObjects[curObj]->body->activate(true);
		physicsObjects[curObj]->body->applyCentralForce(btVector3(0.0f, 0.0f, -5.0f));
	}
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{

		physicsObjects[curObj]->body->activate(true);
		physicsObjects[curObj]->body->applyCentralForce(btVector3(0.0f, 0.0f, 5.0f));
	}
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
	{
		physicsObjects[curObj]->body->activate(true);
		physicsObjects[curObj]->body->applyCentralForce(btVector3(5.0f, 0.0f, 0.0f));
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		physicsObjects[curObj]->body->activate(true);
		physicsObjects[curObj]->body->applyCentralForce(btVector3(-5.0f, 0.0f, 0.0f));
	}
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}
void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mode)
{
	if (glfwGetKey(window, GLFW_KEY_TAB) == GLFW_PRESS)
	{
		curObj++;
		if (curObj == physicsObjects.size())
			curObj = 0;
		
		std::cout << "Curr Game Object Id - " <<curObj << std::endl;
	}
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}

int main()
{
	
	glfwCaller = new cGLFWCalls();
	glfwCaller->InitiateGLFW();
	glfwCaller->createNewWindow(SCR_WIDTH,SCR_HEIGHT,windowTitle);
	mainWindow = glfwCaller->getCurrentWindow();
	
	glfwSetFramebufferSizeCallback(mainWindow, framebuffer_size_callback);
	glfwSetInputMode(mainWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPosCallback(mainWindow, mouse_callback);
	glfwSetScrollCallback(mainWindow, scroll_callback);
	glfwSetKeyCallback(mainWindow, keyCallback);

	//Code to load shaders

	::gPShaderManager = new cShaderManager();
	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	vertShader.fileName = "simpleVertex.glsl";
	fragShader.fileName = "simpleFragment.glsl";

	::gPShaderManager->setBasePath("../Extern/assets/shaders/");

	if (!::gPShaderManager->createProgramFromFile("myCustomShader", vertShader, fragShader)) {
		std::cout << " Error in loading shader " << std::endl;
		return -1;
	}

	glEnable(GL_DEPTH_TEST);


	GLint customShaderId = ::gPShaderManager->getIDFromFriendlyName("myCustomShader");
	::gPShaderManager->useShaderProgram(customShaderId);


	// load models
	// -----------
	cModel* sphereModel = new cModel("../Extern/assets/sphereMesh.obj", false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("sphere", sphereModel));
	
	cModel* cubeModel = new cModel("../Extern/assets/cubeMesh.obj", false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("cube", cubeModel));
	
	cModel* cylinderModel = new cModel("../Extern/assets/cylinderMesh.obj", false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("cylinder", cylinderModel));
	
	cModel* coneModel = new cModel("../Extern/assets/coneMesh.obj", false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("cone", coneModel));

	cModel* floorModel = new cModel("../Extern/assets/cubeMesh.obj", false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("floor", floorModel));

	
	std::vector<cMesh*> modelMeshes = sphereModel->getAllMeshes();
	for (int i = 0; i < modelMeshes.size(); i++) {
		sphereModel->addSeparateTexturesToModel("sphere.bmp", "../Extern/assets/textures", "texture_diffuse", modelMeshes[i]);
	}

	modelMeshes.clear();
	modelMeshes = coneModel->getAllMeshes();
	for (int i = 0; i < modelMeshes.size(); i++) {
		coneModel->addSeparateTexturesToModel("cone.png", "../Extern/assets/textures", "texture_diffuse", modelMeshes[i]);
	}

	modelMeshes = cubeModel->getAllMeshes();
	for (int i = 0; i < modelMeshes.size(); i++) {
		cubeModel->addSeparateTexturesToModel("cube.png", "../Extern/assets/textures", "texture_diffuse", modelMeshes[i]);
	}

	modelMeshes = cylinderModel->getAllMeshes();
	for (int i = 0; i < modelMeshes.size(); i++) {
		cylinderModel->addSeparateTexturesToModel("cylinder.png", "../Extern/assets/textures", "texture_diffuse", modelMeshes[i]);
	}

	modelMeshes = floorModel->getAllMeshes();
	for (int i = 0; i < modelMeshes.size(); i++) {
		floorModel->addSeparateTexturesToModel("floor.jpg", "../Extern/assets/textures", "texture_diffuse", modelMeshes[i]);
	}

	

	world = new cPhysicsWorld();
	cGameObject* sphere0 = new cGameObject(sphereModel, glm::vec3(0.0f, 5.0f, 0.0f), glm::vec3(1.0f), glm::vec3(0.0f), 1.0f, eObjectType::OBJECT_SPHERE, world->world);
	cGameObject* sphere1 = new cGameObject(sphereModel, glm::vec3(5.0f, 5.0f, 0.0f), glm::vec3(1.0f), glm::vec3(0.0f), 1.0f, eObjectType::OBJECT_SPHERE, world->world);
	cGameObject* sphere2 = new cGameObject(sphereModel, glm::vec3(0.0f, 10.0f, 0.0f), glm::vec3(1.0f), glm::vec3(0.0f), 1.0f, eObjectType::OBJECT_SPHERE, world->world);
	cGameObject* cube0 = new cGameObject(cubeModel, glm::vec3(-10.0f, 5.0f, 25.0f), glm::vec3(1.0f), glm::vec3(0.0f), 1.0f, eObjectType::OBJECT_CUBE, world->world);
	cGameObject* cube1 = new cGameObject(cubeModel, glm::vec3(-10.0f, 10.0f, 25.0f), glm::vec3(1.0f), glm::vec3(0.0f), 0.0f, eObjectType::OBJECT_CUBE, world->world);
	cGameObject* cylinder0 = new cGameObject(cylinderModel, glm::vec3(5.0f, 3.0f, 10.0f), glm::vec3(1.0f), glm::vec3(0.0f), 1.0f, eObjectType::OBJECT_CYLINDER, world->world);
	cGameObject* cylinder1 = new cGameObject(cylinderModel, glm::vec3(7.0f, 3.0f, 10.0f), glm::vec3(1.0f), glm::vec3(0.0f), 0.0f, eObjectType::OBJECT_CYLINDER, world->world);
	cGameObject* cone0 = new cGameObject(coneModel, glm::vec3(-10.0f, 6.0f, -10.0f), glm::vec3(1.0f), glm::vec3(0.0f), 1.0f, eObjectType::OBJECT_CONE, world->world);
	cGameObject* cone1 = new cGameObject(coneModel, glm::vec3(-20.0f, 6.0f, -20.0f), glm::vec3(1.0f), glm::vec3(0.0f), 0.0f, eObjectType::OBJECT_CONE, world->world);
	cGameObject* plane = new cGameObject(floorModel, glm::vec3(0.0f), glm::vec3(60.0f, 2.0f, 60.0f), glm::vec3(0.0f), 0.0f, eObjectType::OBJECT_PLANE, world->world);

	std::cout << "Curr Game Object Id - 0" << std::endl;
	
	physicsObjects.push_back(plane);

	// Slider
	physicsObjects.push_back(sphere0);
	glm::mat4 sphereTrans0(1.0f);
	sphereTrans0 = glm::translate(sphereTrans0, glm::vec3(0.0f, 5.0f, 0.0f));
	//world->sliderConstraint(sphere0->body, ToBullet(sphereTrans0));

	physicsObjects.push_back(sphere1);
	glm::mat4 sphereTrans1(1.0f);
	sphereTrans1 = glm::translate(sphereTrans1, glm::vec3(5.0f, 5.0f, 0.0f));
	sphereTrans1 = glm::rotate(sphereTrans1, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	world->sliderConstraint(sphere0->body,sphere1->body, ToBullet(sphereTrans0),ToBullet(sphereTrans1));

	physicsObjects.push_back(sphere2);
	glm::mat4 sphereTrans2(1.0f);
	sphereTrans2 = glm::translate(sphereTrans2, glm::vec3(0.0f, 10.0f, 0.0f));
	world->sliderConstraint(sphere2->body, ToBullet(sphereTrans0));


	// Point to Point
	physicsObjects.push_back(cube0);
	physicsObjects.push_back(cube1);
	world->p2pConstraint(cube0->body,cube1->body, ToBullet(glm::vec3(0.0, 10.0f, 0.0f)), ToBullet(glm::vec3(0.0, 5.0f, 0.0f)));
	
	// Hinge
	physicsObjects.push_back(cylinder0);
	physicsObjects.push_back(cylinder1);
	world->hingeConstraint(cylinder0->body, ToBullet(glm::vec3(7.0f, 3.0f, 10.0f)), ToBullet(glm::vec3(0.0f, 1.0f, 0.0f)), cylinder1->body, ToBullet(glm::vec3(7.0f, 3.0f, 10.0f)), ToBullet(glm::vec3(0.0f, 1.0f, 0.0f)));
	// 6 Freedom
	physicsObjects.push_back(cone0);
	physicsObjects.push_back(cone1);
	glm::mat4 cylinderTrans1(1.0f);
	glm::mat4 cylinderTrans2(1.0f);
	cylinderTrans1 = glm::translate(cylinderTrans1, glm::vec3(-20.0f, 6.0f, -20.0f));
	cylinderTrans2 = glm::translate(cylinderTrans2, glm::vec3(-20.0f, 6.0f, -20.0f));
	world->dofConstraint(cone0->body, cone1->body, ToBullet(cylinderTrans1),ToBullet(cylinderTrans2));

	double lastFrame = glfwGetTime();

	
	//Main Window Loop 
	while (!glfwWindowShouldClose(mainWindow))
	{
		//per-frame logic
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		//Call to check for input
		processInput(mainWindow);
			
		glClearColor(0.3f, 0.5f, 0.4f, 1.0f);
		//gl command to clear the color buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// get matrix's uniform location and set matrix
		::gPShaderManager->useShaderProgram(customShaderId);


		glm::mat4 view = glm::mat4(1.0f);
		view = camera.GetViewMatrix();

		glm::mat4 projection;
		projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);

		unsigned int viewLoc = glGetUniformLocation(customShaderId, "view");
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

		unsigned int projectionLoc = glGetUniformLocation(customShaderId, "projection");
		glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

		world->stepSimulation();

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		for (int i = 0; i != physicsObjects.size(); i++)
		{
			if (physicsObjects[i]->Mass != 0.0f)
			{
				btTransform transform;
				physicsObjects[i]->body->getMotionState()->getWorldTransform(transform);
				physicsObjects[i]->Position = ToGlm(transform.getOrigin());
				physicsObjects[i]->OrientationEuler = ToGlm(transform.getRotation());
			}
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, physicsObjects[i]->Position);
			model = glm::rotate(model, physicsObjects[i]->OrientationEuler.x, glm::vec3(1.0f, 0.0f, 0.0f));
			model = glm::rotate(model, physicsObjects[i]->OrientationEuler.y, glm::vec3(0.0f, 1.0f, 0.0f));
			model = glm::rotate(model, physicsObjects[i]->OrientationEuler.z, glm::vec3(0.0f, 0.0f, 1.0f));
			model = glm::scale(model, physicsObjects[i]->Scale);
			unsigned int modelLoc = glGetUniformLocation(customShaderId, "model");
			glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
			physicsObjects[i]->Draw(customShaderId);
		}




		glfwSwapBuffers(mainWindow);
		glfwPollEvents();
	}
	// glfw: terminate, clearing all previously allocated GLFW resources.
	// ------------------------------------------------------------------
	glfwTerminate();
	return 0;
}