// Application1.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <string>
#include <vector>
#include "cGraph.h"

int main()
{
		std::cout << "This is demonstration of Djikstra Algo :: \n\n";
		cGraph* newGraph = new cGraph();
		newGraph->addNode();
		newGraph->addNode();
		newGraph->addNode();
		newGraph->addNode();
		newGraph->addNode();
		newGraph->addNode();
		newGraph->joinNodes(1,2);
		newGraph->joinNodes(2,3);
		newGraph->joinNodes(3,4);
		newGraph->joinNodes(4,5);
		newGraph->joinNodes(5,6);
		newGraph->joinNodes(3,6);
		newGraph->printGraph();
		newGraph->findShortestPath(1,6);
		
		system("pause");

		return 1;
}

