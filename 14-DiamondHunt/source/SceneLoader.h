#include "globalGameStuff.h"
void loadModelsIntoScene(void);
void generateGameObjects(rapidjson::Document &doc);
bool isObjectInCullingRadius(cGameObject* object);
void addObstacleAsGameObject(cMapCell* cell, cGameObject* cellGameObject, std::vector<glm::vec3> &vecPoints, rapidjson::Document &doc);
void loadGameState(std::string filePath);
void saveGameState(std::string filePath);
void addSavedGame(std::string filePath, std::string gameName);