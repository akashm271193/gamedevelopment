#include "cPlayerGameObject.h"


cPlayerGameObject::cPlayerGameObject()
{
	
	this->playerHealth = 100.0f;
}

bool cPlayerGameObject::isPlayerHealthLow()
{

	if (this->playerHealth <= 10.0f)
		return true;

	else
		return false;
}

void cPlayerGameObject::respawnPlayer() {
	float distance = 10000.0f;
	int nearestPtIndex = -1;
	for (int i = 0; i < this->respawnPoints.size(); i++) {
		float tmpDistance = glm::distance(this->position, this->respawnPoints[i]);
		if (tmpDistance < distance) {
			distance = tmpDistance;
			nearestPtIndex = i;
		}
	}

	if (nearestPtIndex != -1)
		this->position = this->respawnPoints[nearestPtIndex];
};
