#include "cGameObject.h"



cGameObject::cGameObject(cModel* newModel,glm::vec3 maxExtentXYZ,glm::vec3 position,int parentObjId) {
	this->model = newModel;
	//this->scale = (maxExtentXYZ.x < maxExtentXYZ.z) ? maxExtentXYZ.x : maxExtentXYZ.z ;
	//this->scale = (this->scale < maxExtentXYZ.y) ? this->scale : maxExtentXYZ.y;
	this->scale = 0.275;
	this->position = glm::vec3(position.x,0.0f,position.z);
	this->originOrientation = glm::vec3(0.0f);
	this->ownAxisOrientation = glm::vec3(0.0f);
	this->objectLayoutType = GL_LINE;
	this->isWireFrame = false;
	this->isUpdatedInPhysics = true;
	this->velocity = glm::vec3(0.0f);
	this->acceleration = glm::vec3(0.0f);
	this->radius = this->scale/2;
	this->typeOfObject = eTypeOfObject::BOUNDING_SPHERE;
	this->objectId = -1;
	this->toAnimate = true;
	this->radius = (this->model->maxExtentXYZ.x* this->scale)/2;
	this->hasBoundingBoxOrSphere = false;
	this->parentGameObjectIndexId = parentObjId;
	this->objectFront = glm::vec3(0.0f);
	return;
}

cGameObject::cGameObject()
{
	this->scale = 1.0f;
	this->position = glm::vec3(0.0f);
	this->originOrientation = glm::vec3(0.0f);
	this->ownAxisOrientation = glm::vec3(0.0f);
	this->objectLayoutType = GL_LINE;
	this->isWireFrame = false;
	this->isUpdatedInPhysics = true;
	this->velocity = glm::vec3(0.0f);
	this->acceleration = glm::vec3(0.0f);
	this->radius = 0.0f;
	this->typeOfObject = eTypeOfObject::UNKNOWN;
	this->objectId = -1;
	this->hasBoundingBoxOrSphere = false;
	this->boundingSphere = NULL;
	this->toAnimate = false;
	this->parentGameObjectIndexId = -1;
	return;
}

void cGameObject::Draw(int shaderId, bool drawBoundingBox, glm::vec3 parentObjectPos) {
	glm::mat4 model = glm::mat4x4(1.0f);

	glm::mat4 matPreRotZ = glm::mat4x4(1.0f);
	matPreRotZ = glm::rotate(matPreRotZ, this->originOrientation.z, glm::vec3(0.0f, 0.0f, 1.0f));
	model = model * matPreRotZ;

	glm::mat4 matPreRotY = glm::mat4x4(1.0f);
	matPreRotY = glm::rotate(matPreRotY, this->originOrientation.y, glm::vec3(0.0f, 1.0f, 0.0f));
	model = model * matPreRotY;

	glm::mat4 matPreRotX = glm::mat4x4(1.0f);
	matPreRotX = glm::rotate(matPreRotX, this->originOrientation.x, glm::vec3(1.0f, 0.0f, 0.0f));
	model = model * matPreRotX;

	glm::mat4 trans = glm::mat4x4(1.0f);
	if(this->typeOfObject == eTypeOfObject::BOUNDING_SPHERE)
		trans = glm::translate(trans, parentObjectPos);
	else
		trans = glm::translate(trans, this->position);
	model = model * trans;


	glm::mat4 matPostRotZ = glm::mat4x4(1.0f);
	matPostRotZ = glm::rotate(matPostRotZ, this->ownAxisOrientation.z, glm::vec3(0.0f, 0.0f, 1.0f));
	model = model * matPostRotZ;

	glm::mat4 matPostRotY = glm::mat4x4(1.0f);
	matPostRotY = glm::rotate(matPostRotY, this->ownAxisOrientation.y, glm::vec3(0.0f, 1.0f, 0.0f));
	model = model * matPostRotY;

	glm::mat4 matPostRotX = glm::mat4x4(1.0f);
	matPostRotX = glm::rotate(matPostRotX, this->ownAxisOrientation.x, glm::vec3(1.0f, 0.0f, 0.0f));
	model = model * matPostRotX;

	float finalScale = this->scale;

	glm::mat4 matScale = glm::mat4x4(1.0f);
	matScale = glm::scale(matScale, glm::vec3(finalScale, finalScale, finalScale));
	model = model * matScale;

	unsigned int modelLoc = glGetUniformLocation(shaderId, "model");
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	if (this->isWireFrame)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	this->model->DrawModel(shaderId, this->colour);

	if (drawBoundingBox && this->hasBoundingBoxOrSphere && this->hasBoundingBoxOrSphere != NULL)
		this->boundingSphere->Draw(shaderId,false,this->position);
}