#pragma once
#include <glm/game_math.h>

namespace libPhysics
{
	class iDebugRenderer
	{
	public:
		virtual ~iDebugRenderer() {}
		virtual void RenderLine(const glm::vec3& from, const glm::vec3& to, const glm::vec3& color) = 0;
		virtual void RenderBox(const glm::vec3& extents, const glm::mat4& trans, const glm::vec3& color) = 0;
		virtual void RenderSphere(const glm::mat4& transform, float radius, const glm::vec3& color) = 0;
		virtual void RenderPlane(const glm::vec3& planeNormal, float planeConst, const glm::mat4& transform, const glm::vec3& color) = 0;
		virtual void RenderCone(float radius, float height, int upAxis, const glm::mat4& transform, const glm::vec3& color) = 0;
	};
}