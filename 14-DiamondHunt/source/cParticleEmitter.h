#pragma once
#include "cGameObject.h"
#include "cParticle.h"
#include <math.h>


class cParticleEmitter : public cGameObject
{
public:
	cParticleEmitter();
	void Init(int numParticles, int maxParticleCreatePerFrame,
			glm::vec3 minInitVel, glm::vec3 maxInitVel,
			float minLife, float maxLife,
			glm::vec3 minRange, glm::vec3 maxRange);

	void Update(float deltaTime);

	//returns number of living particles
	int getLivingParticles(std::vector<cParticle*> &vecLiveParticles);
	void Draw() {
	
	}
	glm::vec3 emitterPosition;
	void DrawParticles(int shaderId);
	
private:
	int numParticles;
	int maxNumParticlePerStep;
	float minLife;
	float maxLife;
	glm::vec3 minInitVelocity;
	glm::vec3 maxInitVelocity;
	glm::vec3 minRangeFromEmitter;
	glm::vec3 maxRangeFromEmitter;
	
	std::vector<cParticle* > vecParticles;

};

