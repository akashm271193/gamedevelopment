#ifndef _cMesh_HG_
#define _cMesh_HG_

#include "cVertex_xyz_rgb.h"
#include "cTriangle.h" 
#include <string>
#include <glm/vec3.hpp>

//#define MAX_VERTEX_ARRAY_SIZE 2048
//#define MAX_INDEX_ARRAY_SIZE 2048
static const int MAX_VERTEX_ARRAY_SIZE = 4096;
static const int MAX_INDEX_ARRAY_SIZE = 4096;

class cMesh
{
public:
	cMesh();
	~cMesh();
	// Array of vertices 
	cVertex_xyz_rgb* pVertices; // Stack
	int numberOfVertices, numberOfTriangles;
	// Array of triangles (indices)
	cTriangle* pTriangles;	// Stack
	std::string name;

	// Takes an indexed model and makes just a vertex array model
	void FlattenIndexedModel(void);

	glm::vec3 minXYZ;
	glm::vec3 maxXYZ;
	glm::vec3 maxExtentXYZ;
	float maxExtent;
	float scaleForUnitBBox;
	void CalculateExtents(void);
};


#endif
