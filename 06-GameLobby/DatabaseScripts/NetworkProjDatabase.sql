CREATE DATABASE  IF NOT EXISTS `gamelobby` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `gamelobby`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: gamelobby
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `salt` char(64) DEFAULT NULL,
  `password` char(64) DEFAULT NULL,
  `sessionToken` varchar(20) DEFAULT NULL,
  `lastLogin` timestamp NULL DEFAULT NULL,
  `createdTimeStamp` timestamp NULL DEFAULT NULL,
  `updatedTimeStamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lobbymaster`
--

DROP TABLE IF EXISTS `lobbymaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lobbymaster` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mapName` varchar(50) DEFAULT NULL,
  `lobbyName` varchar(50) DEFAULT NULL,
  `gameMode` varchar(50) DEFAULT NULL,
  `maxPlayers` int(11) DEFAULT NULL,
  `currentPlayers` int(11) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `isActive` tinyint(4) DEFAULT '1',
  `createdTimeStamp` timestamp NULL DEFAULT NULL,
  `updatedTimeStamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_USERID_ID_idx` (`userId`),
  CONSTRAINT `FK_USERID_ID` FOREIGN KEY (`userId`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lobbyplayers`
--

DROP TABLE IF EXISTS `lobbyplayers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lobbyplayers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lobbyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `isPresent` tinyint(4) NOT NULL DEFAULT '1',
  `createdTimeStamp` timestamp NULL DEFAULT NULL,
  `updatedTimeStamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_LOBBYID_ID_idx` (`lobbyId`),
  KEY `FK_USERID_ID_MAP_idx` (`userId`),
  CONSTRAINT `FK_LOBBYID_ID` FOREIGN KEY (`lobbyId`) REFERENCES `lobbymaster` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_USERID_ID_MAP` FOREIGN KEY (`userId`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `newsessions`
--

DROP TABLE IF EXISTS `newsessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionToken` varchar(10) NOT NULL,
  `createdTimeStamp` timestamp NULL DEFAULT NULL,
  `updatedTimeStamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-15  8:37:05
