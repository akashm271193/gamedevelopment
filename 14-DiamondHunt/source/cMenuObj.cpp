#include "cMenuObj.h"
#include "SceneLoader.h"

cMenuObj::cMenuObj()
{
	
}



cMenuObj::~cMenuObj()
{
}

cMenuObj::cMenuObj(eMenuObjType type) {
	this->menuObjType = type;
	this->objText = "";
	this->prevMenuIndex = 0;
	this->gameStateFile = "";
	this->scale = glm::vec3(1.0f, 1.0f, 0.0f);
}

void cMenuObj::doAction(int &activeMenu,eMenuObjType &lastAction) {
	switch (this->menuObjType) {
		case CONTINUE:
			glfwSetInputMode(mainWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
			glfwSetCursorPosCallback(mainWindow, cGLFWCalls::mouseCallBack);
			glfwSetMouseButtonCallback(mainWindow, NULL);
			isMenuActive = !isMenuActive;
			break;
		
		case LOAD:
			activeMenu = 1; 
			break;
		
		case SAVE:
			activeMenu = 1;
			break;

		case EXIT:
			glfwSetWindowShouldClose(mainWindow, true);
			break;

		case GAME_STATE:
			if (lastAction == LOAD) {
				activeMenu = 0;
				glfwSetInputMode(mainWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
				glfwSetCursorPosCallback(mainWindow, cGLFWCalls::mouseCallBack);
				glfwSetMouseButtonCallback(mainWindow, NULL);
				isMenuActive = !isMenuActive;
				if(this->gameStateFile != "")
					loadGameState(this->gameStateFile);
			}
			if (lastAction == SAVE) {
				this->objText = "Saved Game " + std::to_string(this->menuObjId + 1);
				std::string fileName = "gameState" + std::to_string(this->menuObjId);
				this->gameStateFile = "../extern/config/" + fileName + ".json";
				saveGameState(this->gameStateFile);
				addSavedGame("../extern/config/savedGames.json", fileName);
				return;
			}
			// action save game state
			break;

		case BACK:
			activeMenu = this->prevMenuIndex;
			break;

		case DATA:
			break;
	}
	lastAction = this->menuObjType;
}