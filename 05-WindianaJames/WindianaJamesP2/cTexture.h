#ifndef _TEXTURE_
#define _TEXTURE_

#include <glad/glad.h>

#include <string>

class cTexture
{
public:
	GLuint ID;
	GLuint Width, Height;
	GLuint InternalFormat;
	GLuint ImageFormat;
	GLuint WrapS, WrapT;
	GLuint FilterMin, FilterMax;

	cTexture();

	void Generate(GLuint width, GLuint height, unsigned char* data);
	void Bind() const;
};
#endif
