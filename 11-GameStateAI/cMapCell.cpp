#include "cMapCell.h"

/*
	Parameterless Constructor initiating all members of the class.
*/
cMapCell::cMapCell()
{
	this->rowId = -1;
	this->columnId = -1;
	this->isWalkable = false;
	this->f = 0;
	this->g = 1;
	this->h = 0;
	this->parentId = -1;
}

/*
	Parameterless destructor
*/
cMapCell::~cMapCell()
{
}


/*
	Implementation of A* algorithm (Part 2)
	Takes in 2 lists - open and close, vector of all cells , endPoint Index and max Col and max Rows in the map.
	Then calulates the adjacent F's and is Walkable and still not explored,inserts in the open list.
*/

void cMapCell::calculateFForAdjacentCells(std::vector<cMapCell*> &vecCells, int endIndex, std::vector<cMapCell*> &openCells, std::vector<cMapCell*> &closedCells,int maxColumns, int maxRows) {
	int currentIndexId = (rowId* maxColumns) + columnId;
	int leftIndexId = (this->columnId > 0) ? ((rowId* maxColumns) + (columnId - 1)) : -1;
	int rightIndexId = (columnId < maxColumns) ? ((rowId* maxColumns) + (columnId+1)) : -1;
	int upIndexId = (rowId > 0) ? (((rowId-1)* maxColumns) + columnId) : -1;
	int downIndexId = (rowId < maxRows) ? (((rowId+1)* maxColumns) + columnId) : -1;

	if (leftIndexId != -1) {
		cMapCell* tmpCell = vecCells[leftIndexId];
		std::vector<cMapCell*>::iterator custIterator;
		custIterator = find(closedCells.begin(), closedCells.end(), tmpCell);

		if (tmpCell->isWalkable && custIterator == closedCells.end()) {
			tmpCell->parentId = currentIndexId;
			tmpCell->calculateF(vecCells[endIndex]);
			openCells.push_back(tmpCell);
		}
	}

	if (rightIndexId != -1) {
		cMapCell* tmpCell = vecCells[rightIndexId];
		std::vector<cMapCell*>::iterator custIterator;
		custIterator = find(closedCells.begin(), closedCells.end(), tmpCell);

		if (tmpCell->isWalkable && custIterator == closedCells.end()) {
			tmpCell->parentId = currentIndexId;
			tmpCell->calculateF(vecCells[endIndex]);
			openCells.push_back(tmpCell);
		}
	}

	if (upIndexId != -1) {
		cMapCell* tmpCell = vecCells[upIndexId];
		std::vector<cMapCell*>::iterator custIterator;
		custIterator = find(closedCells.begin(), closedCells.end(), tmpCell);

		if (tmpCell->isWalkable && custIterator == closedCells.end()) {
			tmpCell->parentId = currentIndexId;
			tmpCell->calculateF(vecCells[endIndex]);
			openCells.push_back(tmpCell);
		}
	}

	if (downIndexId != -1 ) {
		cMapCell* tmpCell = vecCells[downIndexId];
		std::vector<cMapCell*>::iterator custIterator;
		custIterator = find(closedCells.begin(), closedCells.end(), tmpCell);

		if (tmpCell->isWalkable && custIterator == closedCells.end()) {
			tmpCell->parentId = currentIndexId;
			tmpCell->calculateF(vecCells[endIndex]);
			openCells.push_back(tmpCell);
		}
	}
}

/*
	Simple algorithm function to calculate F
*/
void cMapCell::calculateF(cMapCell* endPointCell) {
	this->h = abs(endPointCell->rowId - this->rowId) + abs(endPointCell->columnId - this->columnId);
	this->f = this->h + this->g;
}