#version 330 core
struct Material {
    sampler2D texture_diffuse1;
	sampler2D texture_diffuse2;
	sampler2D texture_diffuse3;
	sampler2D texture_specular1;
	sampler2D texture_specular2;
    float shininess;
}; 

uniform Material material;
uniform bool isObjColour;
uniform vec3 objColour;

out vec4 FragColor;
in vec2 objTexture;

//in vec3 objWorldNormal;
//in vec3 objWorldPos;

void main()
{ 
	if(isObjColour)
		FragColor = vec4(objColour,1.0f);
	else
		FragColor = vec4(1.0f,1.0f,1.0f,1.0f);
}