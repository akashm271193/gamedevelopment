#include "cMesh.h"

cMesh::cMesh()
{
	this->numberOfVertices = 0;
	this->numberOfTriangles = 0;
	this->maxExtent = 0.0f;
	this->scaleForUnitBBox = 0.0f;
	return;
}


cMesh::~cMesh()
{

	return;
}

void cMesh::CalculateExtents(void) {
	
	this->minXYZ.x = this->pVertices[0].x;
	this->minXYZ.y = this->pVertices[0].y;
	this->minXYZ.z = this->pVertices[0].z;
	this->maxXYZ.x = this->pVertices[0].x;
	this->maxXYZ.y = this->pVertices[0].y;
	this->maxXYZ.z = this->pVertices[0].z;

	for (int index = 0; index < this->numberOfVertices; index++) {

			if (this->pVertices[index].x < this->minXYZ.x) {
				this->minXYZ.x = this->pVertices[index].x;
			}
			if (this->pVertices[index].x > this->maxXYZ.x) {
				this->maxXYZ.x = this->pVertices[index].x;
			}
			if (this->pVertices[index].y < this->minXYZ.y) {
				this->minXYZ.y = this->pVertices[index].y;
			}
			if (this->pVertices[index].y > this->maxXYZ.y) {
				this->maxXYZ.y = this->pVertices[index].y;
			}
			if (this->pVertices[index].z < this->minXYZ.z) {
				this->minXYZ.z = this->pVertices[index].z;
			}
			if (this->pVertices[index].z > this->maxXYZ.z) {
				this->maxXYZ.z = this->pVertices[index].z;
			}
		}

		this->maxExtentXYZ.x = this->maxXYZ.x - this->minXYZ.x;
		this->maxExtentXYZ.y = this->maxXYZ.x - this->minXYZ.x;
		this->maxExtentXYZ.z = this->maxXYZ.x - this->minXYZ.x;

		this->maxExtent = this->maxExtentXYZ.x;
		if (this->maxExtentXYZ.y > this->maxExtent) {
			this->maxExtent = this->maxExtentXYZ.y;
		}
		if (this->maxExtentXYZ.z > this->maxExtent) {
			this->maxExtent = this->maxExtentXYZ.z;
		}


		this->scaleForUnitBBox = 1.0f / this->maxExtent;
		return;

}


void cMesh::FlattenIndexedModel(void) {
	
	cVertex_xyz_rgb* origVertices = new cVertex_xyz_rgb[this->numberOfVertices];

	for (int index = 0; index < this->numberOfVertices; index++) {
		origVertices[index] = this->pVertices[index];
	}

	delete[] this->pVertices;
	int newArrayLimit = (this->numberOfTriangles * 3) + 30;
	this->pVertices = new cVertex_xyz_rgb[newArrayLimit];
	int countVer = 0;
	for (int countTri = 0; countTri < this->numberOfTriangles; countTri++) {
			this->pVertices[countVer] = origVertices[this->pTriangles[countTri].vertex_ID_0];
			this->pVertices[countVer+1] = origVertices[this->pTriangles[countTri].vertex_ID_1];
			this->pVertices[countVer+2] = origVertices[this->pTriangles[countTri].vertex_ID_2];
			countVer += 3;
	}
	this->numberOfVertices = countVer;


	return;
};