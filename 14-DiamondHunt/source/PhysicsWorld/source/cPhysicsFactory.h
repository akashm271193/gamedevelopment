#ifndef _CPHYSICS_FACTORY_HG_
#define _CPHYSICS_FACTORY_HG_

#include "iPhysicsFactory.h"
#include "cPhysicsWorld.h"

namespace libPhysics
{
	class cPhysicsFactory : public iPhysicsFactory
	{
	public:
		virtual ~cPhysicsFactory();

		virtual iPhysicsWorld* CreateWorld();

		virtual iCollisionBody* CreateCollisionBody(const sCollisionBodyDesc &bodyDesc);

	};
}

#endif