#include "cGameObject.h"



cGameObject::cGameObject()
{
	this->scale = 1.0f;
	this->position = glm::vec3(0.0f);
	this->originOrientation = glm::vec3(0.0f);
	this->ownAxisOrientation = glm::vec3(0.0f);
	this->objectLayoutType = GL_LINE;
	this->isWireFrame = false;
	this->isUpdatedInPhysics = true;
	this->velocity = glm::vec3(0.0f);
	this->acceleration = glm::vec3(0.0f);
	this->radius = 0.0f;
	this->typeOfObject = eTypeOfObject::UNKNOWN;
	this->objectId = -1;
	this->toAnimate = false;
	return;
}

cGameObject::~cGameObject()
{
}
