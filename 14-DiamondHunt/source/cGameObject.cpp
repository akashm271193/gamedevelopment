#include "globalGameStuff.h"
#include "cAnimationState.h"
#include "cFBO.h"
#include "cGLFWCalls.h"
cGameObject::cGameObject(cModel* newModel,glm::vec3 maxExtentXYZ,glm::vec3 position,glm::vec3 orientation, int parentObjId) {
	this->model = newModel;
	this->scale.x = maxExtentXYZ.x;
	this->scale.y = maxExtentXYZ.y;
	this->scale.z = maxExtentXYZ.z;
	this->position = glm::vec3(position.x, maxExtentXYZ.y/2,position.z);
	this->oldPosition = glm::vec3(position.x, maxExtentXYZ.y / 2, position.z);
	this->animation = NULL;
	this->originOrientation = glm::vec3(0.0f);
	this->ownAxisOrientation = orientation;
	this->objectLayoutType = GL_LINE;
	this->isUpdatedInPhysics = true;
	this->velocity = glm::vec3(0.0f);
	this->acceleration = glm::vec3(0.0f);
	this->typeOfObject = libPhysics::OBJ_UNKNOWN;
	this->objectId = -1;
	this->toAnimate = true;
	this->hasBoundingBoxOrSphere = false;
	this->parentGameObjectIndexId = parentObjId;
	this->objectFront = glm::vec3(0.0f,0.0f,1.0f);
	this->worldUp = glm::vec3(0.0f, 0.0f, 0.0f);
	this->updateObjectVectors();
	this->mapCellId = -1;
	return;
}

cGameObject::cGameObject()
{
	this->scale = glm::vec3(1.0f,1.0f,1.0f);
	this->position = glm::vec3(0.0f);
	this->oldPosition = this->position;
	this->originOrientation = glm::vec3(0.0f);
	this->ownAxisOrientation = glm::vec3(0.0f);
	this->objectLayoutType = GL_FILL;
	this->isUpdatedInPhysics = true;
	this->velocity = glm::vec3(0.0f);
	this->acceleration = glm::vec3(0.0f);
	this->radius = 0.0f;
	this->typeOfObject = libPhysics::OBJ_UNKNOWN;
	this->objectId = -1;
	this->hasBoundingBoxOrSphere = false;
	this->boundingSphereOrBox = NULL;
	this->toAnimate = false;
	this->parentGameObjectIndexId = -1;
	this->objectFront = glm::vec3(0.0f, 0.0f, 1.0f);
	this->worldUp = glm::vec3(0.0f, 0.0f, 0.0f);
	this->updateObjectVectors();
	this->toDraw = true;
	this->mapCellId = -1;
	return;
}

void cGameObject::Draw(int shaderId, bool drawBoundingBox, glm::vec3 parentObjectPos) {
	glm::mat4 model = glm::mat4x4(1.0f);

	glm::mat4 matPreRotZ = glm::mat4x4(1.0f);
	matPreRotZ = glm::rotate(matPreRotZ, this->originOrientation.z, glm::vec3(0.0f, 0.0f, 1.0f));
	model = model * matPreRotZ;

	glm::mat4 matPreRotY = glm::mat4x4(1.0f);
	matPreRotY = glm::rotate(matPreRotY, this->originOrientation.y, glm::vec3(0.0f, 1.0f, 0.0f));
	model = model * matPreRotY;

	glm::mat4 matPreRotX = glm::mat4x4(1.0f);
	matPreRotX = glm::rotate(matPreRotX, this->originOrientation.x, glm::vec3(1.0f, 0.0f, 0.0f));
	model = model * matPreRotX;

	glm::mat4 trans = glm::mat4x4(1.0f);
	trans = glm::translate(trans, this->position);
	model = model * trans;


	glm::mat4 matPostRotZ = glm::mat4x4(1.0f);
	matPostRotZ = glm::rotate(matPostRotZ, this->ownAxisOrientation.z, glm::vec3(0.0f, 0.0f, 1.0f));
	model = model * matPostRotZ;

	glm::mat4 matPostRotY = glm::mat4x4(1.0f);
	matPostRotY = glm::rotate(matPostRotY, this->ownAxisOrientation.y, glm::vec3(0.0f, 1.0f, 0.0f));
	model = model * matPostRotY;

	glm::mat4 matPostRotX = glm::mat4x4(1.0f);
	matPostRotX = glm::rotate(matPostRotX, this->ownAxisOrientation.x, glm::vec3(1.0f, 0.0f, 0.0f));
	model = model * matPostRotX;

	glm::mat4 matScale = glm::mat4x4(1.0f);
	matScale = glm::scale(matScale, this->scale);
	model = model * matScale;

	unsigned int modelLoc = glGetUniformLocation(shaderId, "model");
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	glPolygonMode(GL_FRONT_AND_BACK, this->objectLayoutType);


	//SKINNED MESH
	unsigned int skinnedMeshLoc = glGetUniformLocation(shaderId, "isSkinnedMesh");
	if (this->model->isSkinnedMesh)
	{
		unsigned int numBonesUsedLoc = glGetUniformLocation(shaderId, "numBonesUsed");
		unsigned int boneIDArrayLoc = glGetUniformLocation(shaderId, "bones");
		CalculateSkinnedMeshBonesAndLoad(this->model, numBonesUsedLoc, boneIDArrayLoc);

		glUniform1f(skinnedMeshLoc, GL_TRUE);
	}
	else
	{
		glUniform1f(skinnedMeshLoc, GL_FALSE);
	}

	if (this->typeOfObject == libPhysics::OBJ_FIRE) {
		cParticleEmitter* tmpGameObj = (cParticleEmitter*)this;
		tmpGameObj->model->DrawModel(shaderId,this->colour);
		tmpGameObj->Update(deltaTime);
		tmpGameObj->DrawParticles(shaderId);
		tmpGameObj = NULL;
		delete tmpGameObj;
	}
		
	else
		this->model->DrawModel(shaderId, this->colour);
}


void cGameObject::updateObjectVectors()
{
	glm::vec3 front = glm::vec3(0.0f);
	front.x = sin(this->ownAxisOrientation.y);
	front.z = cos(this->ownAxisOrientation.y);
	this->objectFront = glm::normalize(front);
	// Also re-calculate the Right vector
	this->objectRight = glm::normalize(glm::cross(this->objectFront, this->worldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
	return;
}

bool cGameObject::toGenerateBouundingBoxForCell(cMapCell* curCell) {
	std::vector<cMapCell*> cells;
	ourMap->getMapCells(cells);
	if (curCell->isWalkable)
		return false;

	if ((curCell->rowId > 0 && curCell->rowId < (ourMap->getMapRows() - 1)) && (curCell->columnId > 0 && curCell->columnId < (ourMap->getMapColumns() - 1))) {
		int rowId = curCell->rowId;
		int colId = curCell->columnId;
		int upCellId = ((rowId - 1)* ourMap->getMapColumns()) + colId;
		int downCellId = ((rowId + 1)* ourMap->getMapColumns()) + colId;
		int leftCellId = (rowId* ourMap->getMapColumns()) + (colId - 1);
		int rightCellId = (rowId* ourMap->getMapColumns()) + (colId + 1);

		if (!cells[upCellId]->isWalkable && !cells[downCellId]->isWalkable && !cells[leftCellId]->isWalkable && !cells[rightCellId]->isWalkable)
			return false;
		
	}

	return true;
}

void cGameObject::createPhysicsWorldObject() {
	this->collisionBodyDesc.position = this->position;
	this->collisionBodyDesc.oldPosition = this->position;
	this->collisionBodyDesc.ownAxisOrientation = this->ownAxisOrientation;
	this->collisionBodyDesc.oldOrientation = this->ownAxisOrientation;
	this->collisionBodyDesc.objectId = this->objectId;
	this->collisionBodyDesc.scale = this->scale;
	this->collisionBodyDesc.objectFront = this->objectFront;
	this->collisionBodyDesc.isColliding = false;
	this->collisionBodyDesc.objType = this->typeOfObject;


	if (this->typeOfObject == libPhysics::OBJ_FIRE)
		this->collisionBodyDesc.maxExtents = this->scale;
	else
		this->collisionBodyDesc.maxExtents = this->model->maxExtentXYZ * this->scale;

	if (this->typeOfObject == libPhysics::OBJ_PLAYER || this->typeOfObject == libPhysics::OBJ_FOLLOWER_AI || this->typeOfObject == libPhysics::OBJ_ANGRY_AI || this->typeOfObject == libPhysics::OBJ_CURIOUS_AI) {
		this->collisionBodyDesc.phyObjType = libPhysics::PHY_OBJ_KINEMATIC;
		this->collisionBodyDesc.maxExtents.x = (this->collisionBodyDesc.maxExtents.x/2.0f) - 0.2f;
	}
	else 
		this->collisionBodyDesc.phyObjType = libPhysics::PHY_OBJ_STATIC;


	this->collisionBody = gPhysicsFactory->CreateCollisionBody(this->collisionBodyDesc);
	gPhysicsWorld->AddBoundingBox(this->collisionBody);
}


void cGameObject::updatePhysicsWorldObject() {
	this->collisionBody->updateCollisionBody(this->collisionBodyDesc);
	gPhysicsWorld->UpdateBoundingBox(this->collisionBody);
}

void cGameObject::updateFromPhysicsWorld() {
	if (this->hasBoundingBoxOrSphere) {
		this->collisionBody->getCollisionBodyStateFromPhysics(this->collisionBodyDesc);
		if (this->collisionBodyDesc.isColliding == true) {
			if (this->typeOfObject == libPhysics::OBJ_PLAYER) {
				if (this->collisionBodyDesc.collidedBodiesId.size() > 0) {
					for (int i = 0; i < this->collisionBodyDesc.collidedBodiesId.size(); i++) {
						int objId = this->collisionBodyDesc.collidedBodiesId[i];
						if (gGameObjects[objId]->typeOfObject == libPhysics::OBJ_TREASURE) {
							if (!isMenuActive) {
								isMenuActive = true;
								glfwSetInputMode(mainWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
								glfwSetCursorPosCallback(mainWindow, NULL);
								glfwSetMouseButtonCallback(mainWindow, cGLFWCalls::mouseButtonCallBack);
								gMenuLoader->setActiveMenu(2);
							}
						}
						else if (gGameObjects[objId]->typeOfObject == libPhysics::OBJ_FIRE) {
							if (!isMenuActive) {
								isMenuActive = true;
								glfwSetInputMode(mainWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
								glfwSetCursorPosCallback(mainWindow, NULL);
								glfwSetMouseButtonCallback(mainWindow, cGLFWCalls::mouseButtonCallBack);
								gMenuLoader->setActiveMenu(4);
							}
						}
						else if (gGameObjects[objId]->typeOfObject == libPhysics::OBJ_ANGRY_AI) {
							cPlayerGameObject* tmpObj = (cPlayerGameObject*)gGameObjects[playerGameObjId];
							tmpObj->playerHealth -= 1.0f;
						}
					}
					this->collisionBodyDesc.collidedBodiesId.clear();
				}
				this->position = this->oldPosition;
				this->collisionBodyDesc.isColliding = false;
				this->collisionBodyDesc.position = this->position;
				this->updatePhysicsWorldObject();
			}
		}
		else {
			if (this->typeOfObject == libPhysics::OBJ_PLAYER) {
				this->oldPosition = this->position;
			}
		}
	}
}

void cGameObject::CalculateSkinnedMeshBonesAndLoad(cModel* model, unsigned int numBonesUsedLoc, unsigned int bonesArrayLoc)
{
	std::string animationToPlay = "";
	float curFrameTime = 0.0f;

	//check animation queue vector
	if (!this->animation->vecAnimationQueue.empty())
	{
		animationToPlay = this->animation->vecAnimationQueue[0].name;
		curFrameTime = this->animation->vecAnimationQueue[0].currentTime;

		//increment the top animation in the queue
		if (this->animation->vecAnimationQueue[0].IncrementTime())
		{
			this->animation->vecAnimationQueue.erase(this->animation->vecAnimationQueue.begin());
		}
	}
	else
	{
		this->animation->defaultAnimation.IncrementTime();
		animationToPlay = this->animation->defaultAnimation.name;
		curFrameTime = this->animation->defaultAnimation.currentTime;
	}

	// Set up the animation pose:
	std::vector< glm::mat4x4 > vecFinalTransformation;
	std::vector< glm::mat4x4 > vecObjectBoneTransformation;
	std::vector< glm::mat4x4 > vecOffsets;

	this->model->BoneTransform(curFrameTime, animationToPlay, vecFinalTransformation,
								vecObjectBoneTransformation, vecOffsets);

	unsigned int numberOfBonesUsed = static_cast<unsigned int>( vecFinalTransformation.size() );
	glUniform1f(numBonesUsedLoc, numberOfBonesUsed);

	glm::mat4x4* pBoneMatrixArray = &(vecFinalTransformation[0]);

	glUniformMatrix4fv(bonesArrayLoc, numberOfBonesUsed, GL_FALSE, (const GLfloat*)glm::value_ptr(*pBoneMatrixArray));
}