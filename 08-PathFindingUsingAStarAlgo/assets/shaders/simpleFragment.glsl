#version 330 core
struct Material {
    sampler2D texture_diffuse1;
	sampler2D texture_diffuse2;
	sampler2D texture_diffuse3;
	sampler2D texture_specular1;
	sampler2D texture_specular2;
    float shininess;
}; 

uniform Material material;
uniform bool isObjColour;
uniform vec3 objColour;

out vec4 FragColor;
in vec2 objTexture;

//in vec3 objWorldNormal;
//in vec3 objWorldPos;
uniform float border_width;
uniform float aspect;  // ratio of width to height


void main()
{ 
   float maxX = 1.0 - border_width;
   float minX = border_width;
   float maxY = maxX / aspect;
   float minY = minX / aspect;

   //if (objTexture.x < maxX && objTexture.x > minX &&objTexture.y < maxY && objTexture.y > minY) {
		 vec4 texResult = texture(material.texture_diffuse1, objTexture);
		 //texResult *= texture(material.texture_diffuse2, objTexture);
		 FragColor = texResult;
	//} else {
		//FragColor = vec4(1.0,1.0,1.0,1.0);
	//  }

	
	/*if(isObjColour)
		FragColor = vec4(objColour,1.0f);
	else{
		 
		 //texResult *= texture(material.texture_diffuse3, objTexture);
		// texResult *= texture(material.texture_specular1, objTexture);
		// texResult *= texture(material.texture_specular2, objTexture); 
	}*/


		
}