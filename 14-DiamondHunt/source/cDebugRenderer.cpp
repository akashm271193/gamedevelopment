#include "cDebugRenderer.h"
#include <iostream>

cDebugRenderer::cDebugRenderer(int shadId)
{
		this->shaderId = shadId;
}

cDebugRenderer::~cDebugRenderer()
	{

	}

	void cDebugRenderer::SetupBox(const std::string& meshName)
	{
		/*if (!mBox)
		{
			mBox = new cGraphicsComponent();
		}
		if (!mBox->SetMesh(meshName))
		{
			delete mBox;
			mBox = 0;
			Log_Error("yall done messed up with the debug box setup");
		}*/
	}

	void cDebugRenderer::SetupSphere(const std::string& meshName)
	{
		/*if (!mSphere)
		{
			mSphere = new cGraphicsComponent();
		}
		if (!mSphere->SetMesh(meshName))
		{
			delete mSphere;
			mSphere = 0;
			Log_Error("yall done messed up with the debug sphere setup");
		}*/
	}

	void cDebugRenderer::SetupPlane(const std::string& meshName)
	{
		/*if (!mPlane)
		{
			mPlane = new cGraphicsComponent();
		}
		if (!mPlane->SetMesh(meshName))
		{
			delete mPlane;
			mPlane = 0;
			Log_Error("yall done messed up with the debug plane setup");
		}*/
	}

	void cDebugRenderer::SetupCone(const std::string& meshName)
	{
		/*if (!mCone)
		{
			mCone = new cGraphicsComponent();
		}
		if (!mCone->SetMesh(meshName))
		{
			delete mCone;
			mCone = 0;
			Log_Error("yall done messed up with the debug cone setup");
		}*/
	}

	void cDebugRenderer::SetupCylinder(const std::string& meshName)
	{
		/*if (!mCylinder)
		{
			mCylinder = new cGraphicsComponent();
		}
		if (!mCylinder->SetMesh(meshName))
		{
			delete mCylinder;
			mCylinder = 0;
			Log_Error("yall done messed up with the debug cylinder setup");
		}*/
	}

	void cDebugRenderer::RenderLine(const glm::vec3& from, const glm::vec3& to, const glm::vec3& color)
	{
		/*glm::vec3 line = to - from;
		float len = glm::length(line);
		glm::quat q;
		glm::vec3 axis(1.f, 0.f, 0.f);
		glm::vec3 rot = glm::cross(axis, line);
		q.x = rot.x; q.y = rot.y; q.z = rot.z;
		q.w = len + glm::dot(axis, line);
		q = glm::normalize(q);

		//mBox->GetVars()->ModelColor = glm::vec4(color, 0.8f);
		

		glm::mat4 trans;
		trans = glm::translate(trans, (from + to) / 2.f);
		trans *= glm::mat4_cast(q);
		trans = glm::scale(trans, glm::vec3(len / 2.f, 0.05f, 0.05f));
		
		//mBox->GetVars()->ModelMatrix = trans;
		//mBox->Render();*/
	}

	void cDebugRenderer::RenderBox(const glm::vec3& extents, const glm::mat4& trans, const glm::vec3& color)
	{
		cModel* tmpModel = ::gMapLoadedModels["bbox"];
		glm::mat4 model = glm::mat4x4(1.0f);
		model = glm::scale(trans, extents);
		unsigned int modelLoc = glGetUniformLocation(this->shaderId, "model");
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		tmpModel->DrawModel(this->shaderId, color);
	}

	void cDebugRenderer::RenderSphere(const glm::mat4& transform, float radius, const glm::vec3& color)
	{
		/*if (!mSphere) return;
		
		mSphere->GetVars()->ModelColor = glm::vec4(color, 0.5f);
		mSphere->GetVars()->ModelMatrix = glm::scale(transform, glm::vec3(radius));
		mSphere->Render();*/
	}

	void cDebugRenderer::RenderPlane(const glm::vec3& planeNormal, float planeConst, const glm::mat4& transform, const glm::vec3& color)
	{
		/*if (!mPlane) return;
		static float wideScale = 50.f;
		static float narrowScale = 0.2f;

		mPlane->GetVars()->ModelColor = glm::vec4(color, 0.3f);
		mPlane->GetVars()->ModelMatrix = glm::translate(transform, planeNormal*(-1.f + planeConst));
		
		glm::vec3 vCross = glm::cross(planeNormal, glm::vec3(0.f, 1.f, 0.f));
		float s = glm::length(vCross);// sine of angle
		if (s == 0.f) s = 1.f;
		float c = glm::dot(planeNormal, glm::vec3(0.f, 1.f, 0.f)); // cosine of angle
		glm::mat3 skew;
		skew[0][0] = 0.f;  skew[0][1] = -vCross.z; skew[0][2] = vCross.y;
		skew[1][0] = vCross.z;  skew[1][1] = 0.f; skew[1][2] = -vCross.x;
		skew[2][0] = -vCross.y;  skew[2][1] = vCross.x; skew[2][2] = 0.f;
		glm::mat3 skew2 = skew*skew;
		glm::mat3 rot = glm::mat3() + skew + skew2*((1.f - c) / (s*s));
		glm::mat3 scaling3 = glm::mat3();
		scaling3[0][0] = wideScale; scaling3[2][2] = wideScale;  scaling3[1][1] = narrowScale;
		rot *= scaling3;
		glm::mat4 scaling4 = glm::mat4(rot);
		mPlane->GetVars()->ModelMatrix *= scaling4;

		mPlane->Render();*/
	}

	void cDebugRenderer::RenderCone(float radius, float height, int upAxis, const glm::mat4& transform, const glm::vec3& color)
	{
		/*if (!mCone) return;
		mCone->GetVars()->ModelColor = glm::vec4(color, 0.9f);
		glm::mat4 mat = glm::scale(glm::mat4(), glm::vec3(height, radius, radius));
		if (upAxis == 0)
		{
			mat = glm::rotate(mat, glm::pi<float>(), glm::vec3(0.f, 0.f, 1.f));
		}
		else if (upAxis == 1)
		{
			mat = glm::rotate(mat, -glm::pi<float>() / 2.f, glm::vec3(0.f, 0.f, 1.f));
		}
		else if (upAxis == 3)
		{
			mat = glm::rotate(mat, glm::pi<float>() / 2.f, glm::vec3(0.f, 1.f, 0.f));
		}
		else
		{
			bool sexybreakpoint = true; // ok what is upAxis??
		}
		mCone->GetVars()->ModelMatrix = mat * transform;
		mCone->Render();*/
	}