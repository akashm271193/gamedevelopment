#pragma once

#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "globalGameStuff.h"
#include "cAnimationState.h"

class cGLFWCalls
{
public:
	cGLFWCalls();
	~cGLFWCalls();
	void InitiateGLFW();
	GLFWwindow* getCurrentWindow();
	bool createNewWindow(int width, int height, std::string title);
	void windowSizeCallback(GLFWwindow* window, int width, int height);
	static void scrollCallBack(GLFWwindow* window, double xoffset, double yoffset);
	static void mouseCallBack(GLFWwindow* window, double xpos, double ypos);
	static void mouseButtonCallBack(GLFWwindow* window, int button, int action, int mods);
	static void framebufferSizeCallBack(GLFWwindow* window, int width, int height);
	static void keyCallBack(GLFWwindow *window, int key, int scancode, int action, int mode);
	void processInput(GLFWwindow *window);

private:
	GLFWwindow* mainWindow;
	void setWindowAsCurrent(GLFWwindow* window);
	bool isFacingBackwards;
	bool toProcessBackMovement;
	bool isAnimationPlaying;
	void moveForward();
	void moveBackward();
	void moveLeft();
	void moveRight();
};

