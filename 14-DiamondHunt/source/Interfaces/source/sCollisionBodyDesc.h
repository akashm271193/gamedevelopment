#ifndef _SCOLLISION_BODY_DESC_HG_
#define _SCOLLISION_BODY_DESC_HG_

#include <glm\game_math.h>
#include <vector>

namespace libPhysics
{
	enum ePhysicsGameObjectType {
		OBJ_PLAYER = 0,
		OBJ_WALL = 1,
		OBJ_FLOOR = 2,
		OBJ_WRECKING_BALL = 3,
		OBJ_SLIDING_WALL = 4,
		OBJ_SPIKES = 5,
		OBJ_FIRE = 6,
		OBJ_UNKNOWN = 7,
		SPHERE = 8,
		PLANE = 9,
		CAPSULE = 10,
		AABB = 11,
		PLAYER = 12,
		OBJ_CURIOUS_AI = 13,
		OBJ_ANGRY_AI = 14,
		OBJ_FOLLOWER_AI = 15,
		BOUNDING_SPHERE = 16,
		BOUNDING_BOX = 17,
		PHY_OBJ_STATIC = 18,
		PHY_OBJ_KINEMATIC = 19,
		PHY_OBJ_DYNAMIC = 20,
		OBJ_CAMERA = 21,
		OBJ_SKELETON = 22,
		OBJ_TREASURE = 23,
		RESPAWN_PT =24,
		LEFT_AI = 25,
		UP_AI = 26
	};
	struct sCollisionBodyDesc
	{
		sCollisionBodyDesc()
		{
			position = glm::vec3(0.0f);
			ownAxisOrientation = glm::vec3(0.0f);
			scale = glm::vec3(0.0f);
			maxExtents = glm::vec3(0.0f);
			oldPosition = glm::vec3(0.0f);
			oldOrientation = glm::vec3(0.0f);
			objType = ePhysicsGameObjectType::OBJ_UNKNOWN;
			phyObjType = ePhysicsGameObjectType::OBJ_UNKNOWN;
			objectFront = glm::vec3(0.0f);
			objectId = -1;
			isColliding = false;
			collidedBodiesId.clear();
		}
		glm::vec3 position;
		glm::vec3 ownAxisOrientation;
		glm::vec3 scale;
		glm::vec3 maxExtents; /// Mostly redundant 
		glm::vec3 oldOrientation;
		glm::vec3 oldPosition;
		ePhysicsGameObjectType objType;
		ePhysicsGameObjectType phyObjType;
		int objectId;
		glm::vec3 objectFront;
		bool isColliding;
		std::vector<int> collidedBodiesId;
	};
}
#endif