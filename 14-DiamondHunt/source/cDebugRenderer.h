#include <iDebugRenderer.h>
#include "globalGameStuff.h"

class cDebugRenderer : public libPhysics::iDebugRenderer
{
	public:
		cDebugRenderer(int shadId);
		virtual ~cDebugRenderer();
		
		void SetupBox(const std::string& meshName);
		void SetupSphere(const std::string& meshName);
		void SetupPlane(const std::string& meshName);
		void SetupCone(const std::string& meshName);
		void SetupCylinder(const std::string& meshName);

		virtual void RenderLine(const glm::vec3& from, const glm::vec3& to, const glm::vec3& color);
		virtual void RenderBox(const glm::vec3& extents, const glm::mat4& trans, const glm::vec3& color);
		virtual void RenderSphere(const glm::mat4& transform, float radius, const glm::vec3& color);
		virtual void RenderPlane(const glm::vec3& planeNormal, float planeConst, const glm::mat4& transform, const glm::vec3& color);
		virtual void RenderCone(float radius, float height, int upAxis, const glm::mat4& transform, const glm::vec3& color);
	private:
		int shaderId;
		//cGraphicsComponent* mBox;
		//cGraphicsComponent* mSphere;
		//cGraphicsComponent* mPlane;
		//cGraphicsComponent* mCone;
		//cGraphicsComponent* mCylinder;
};