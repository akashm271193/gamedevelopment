#pragma once

#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

class cGLFWCalls
{
public:
	cGLFWCalls();
	~cGLFWCalls();
	void InitiateGLFW();
	GLFWwindow* getCurrentWindow();
	bool createNewWindow(int width, int height, std::string title);

private:
	GLFWwindow* mainWindow;
	void setWindowAsCurrent(GLFWwindow* window);
};

