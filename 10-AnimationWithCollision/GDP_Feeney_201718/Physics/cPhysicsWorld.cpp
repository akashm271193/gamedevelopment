#include "cPhysicsWorld.h"

cPhysicsWorld::cPhysicsWorld()
{

	return;
}

cPhysicsWorld::~cPhysicsWorld()
{

	return;
}

bool cPhysicsWorld::isColliding(cAABBBroadPhase* pAABBBroadPhase, std::vector<cGameObject*> vecGameObjects, cVAOMeshManager* meshManager, glm::vec3 position)
{
	for (int j = 0; j < vecGameObjects.size(); j++) {
		cGameObject* pTheOtherGO = vecGameObjects[j];
		if (pTheOtherGO->friendlyName == "Eve" || pTheOtherGO->friendlyName == "Sophie" || pTheOtherGO->friendlyName == "plane" || pTheOtherGO->friendlyName == "skybox")
			continue;
		cMesh tmpMesh; 
		meshManager->lookupMeshFromName(pTheOtherGO->friendlyName,tmpMesh);
		cAABBv2* tmpAABB;
		pAABBBroadPhase->areThereTrianglesInAABB(tmpMesh.minXYZ,tmpAABB);
		if (tmpAABB->isPointInAABB(position,pTheOtherGO->getPosition(),pTheOtherGO->vecMeshes[0].scale)) {
			return true;
		}
	}
	

	return false;
}
