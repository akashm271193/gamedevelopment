#pragma once
#include "cGameObject.h"
#include <math.h>       /* acos */
#define PI 3.14159265

enum eAiAction
{	
	FOLLOW = 0,
	EVADE = 1,
	ATTACK = 2,
	APPROACH = 3,
	ANIMATE_AI = 4 
};

enum eAIDirection {
	MOVING_TO_MAX = 0,
	MOVING_TO_MIN = 1,
	ATTACK_PLAYER = 2,
	EVADE_PLAYER = 3,
	APPROACH_PLAYER = 4,
	FOLLOW_PLAYER = 5
};

class cPlayerGameObject;


class cAIGameObject : public cGameObject
{
public:
	cAIGameObject();
	eAiAction action;
	glm::vec3 movementMinPos;
	glm::vec3 movementMaxPos;
	void animateObject(float deltaTime, cPlayerGameObject* playerObj);
	float originalOrientationY;
	float attentionRadius;

private:
	void calculateAngleOfOrientation(glm::vec3 destinationPoint, float &angleInDegrees);
	void rotateObject(glm::vec3 destinationPoint,float stepTime, cGameObject* tmpGameObject);
	void translateObject(glm::vec3 destinationAngle,float stepTim, cGameObject* tmpGameObject);
	eAIDirection objectMovmentDir;
	bool isRotating;
	bool isTranslating;
	float targetAngleRotationY;
	bool isFacingPlayer(cPlayerGameObject* playerObj);
	bool isInAttentionRadius(cPlayerGameObject* playerObj);
	void interactWithPlayer(cPlayerGameObject* playerObj,cGameObject* tmpGameObject);
	glm::vec3 targetPosDir;
	glm::vec3 targetPos;
	glm::vec3 prevKnownPlayerPos;
	bool hasPlayerMoved(cPlayerGameObject* playerObj);
};

