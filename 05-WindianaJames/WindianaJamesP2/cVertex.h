#ifndef _cVertex_
#define _cVertex_

class cVertex
{
public:
	cVertex();	// Constructor
	~cVertex();	// Destructor
	float x, y, z;
	float nx, ny, nz;
	float u1, v1;
	float u2, v2;
	float tx, ty, tz;
	float bx, by, bz;
};

#endif
