#pragma once
#include "cGameObject.h"
class cPlayerGameObject : public cGameObject
{
public:
	cPlayerGameObject();
	float playerHealth;
	bool isPlayerHealthLow();
	std::vector<glm::vec3> respawnPoints;
	void respawnPlayer();
};

