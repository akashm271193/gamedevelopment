#include "cResourceManager.h"

#include <iostream>
#include <sstream>
#include <fstream>

#include <SOIL2/SOIL2.h>

std::map< std::string, cShader > cResourceManager::Shaders;
std::map< std::string, cModel > cResourceManager::Models;
std::map< std::string, cTexture> cResourceManager::Textures;
//A map of x,z values that represent a "box" around any model
std::map <std::string, float[4] > cResourceManager::MinMax;

cShader cResourceManager::LoadShader(const GLchar* vShaderFile, const GLchar* fShaderFile, std::string name)
{
	Shaders[name] = loadShaderFromFile(vShaderFile, fShaderFile);
	return Shaders[name];
}

cShader cResourceManager::GetShader(std::string name)
{
	return Shaders[name];
}

cModel cResourceManager::LoadModel(std::string modelFile, std::string name)
{
	Models[name] = loadModelFromFile(modelFile, name);
	return Models[name];
}

cModel cResourceManager::GetModel(std::string name)
{
	return Models[name];
}

cTexture cResourceManager::LoadTexture(const GLchar* textureFile, GLboolean alpha, std::string name)
{
	Textures[name] = loadTextureFromFile(textureFile, alpha);
	return Textures[name];
}

cTexture cResourceManager::GetTexture(std::string name)
{
	return Textures[name];
}

void cResourceManager::getMinMax(std::string name, float* minx, float* minz, float* maxx, float* maxz)
{
	*minx = MinMax[name][0];
	*minz = MinMax[name][1];
	*maxx = MinMax[name][2];
	*maxz = MinMax[name][3];
	return;
}

void cResourceManager::Clear()
{
	for (auto iter : Shaders)
		glDeleteProgram(iter.second.ID);
	for (auto iter : Textures)
		glDeleteTextures(1, &iter.second.ID);
}

cShader cResourceManager::loadShaderFromFile(const GLchar* vShaderFile, const GLchar* fShaderFile)
{
	std::string vertexCode;
	std::string fragmentCode;
	std::string geometryCode;
	try
	{
		std::ifstream vertexShaderFile(vShaderFile);
		std::ifstream fragmentShaderFile(fShaderFile);
		std::stringstream vShaderStream, fShaderStream;

		vShaderStream << vertexShaderFile.rdbuf();
		fShaderStream << fragmentShaderFile.rdbuf();

		vertexShaderFile.close();
		fragmentShaderFile.close();

		vertexCode = vShaderStream.str();
		fragmentCode = fShaderStream.str();
	}
	catch (std::exception e)
	{
		std::cout << "ERROR::SHADER: Failed to read shader files" << std::endl;
	}
	const GLchar *vShaderCode = vertexCode.c_str();
	const GLchar *fShaderCode = fragmentCode.c_str();

	cShader Shader;
	Shader.Compile(vShaderCode, fShaderCode);
	return Shader;
}

cModel cResourceManager::loadModelFromFile(std::string modelDir, std::string name)
{
	cModel model;
	model.Filename = modelDir;
	model.Name = name;
	float minsMaxes[4];

	std::ifstream modelFile(modelDir.c_str());
	std::string temp = "";

	if (!modelFile.is_open())
	{
		std::cout << "File can not be found." << std::endl;
		exit(1);
	}

	while (temp != "end_header")
	{
		modelFile >> temp;
		if (temp == "vertex")
		{
			modelFile >> model.NumVertices;
		}
		if (temp == "face")
		{
			modelFile >> model.NumTriangles;
		}
	}
	model.NumIndices = 3 * model.NumTriangles;

	model.Vertices = new cVertex[model.NumVertices];
	model.Triangles = new cTriangle[model.NumTriangles];
	model.TrianglesWithVertices = new cTriangleWithVertices[model.NumTriangles];
	for (int i = 0; i < model.NumVertices; i++)
	{
		float x, y, z;
		float nx, ny, nz;
		float u1, v1;
		modelFile >> x >> y >> z;
		modelFile >> nx >> ny >> nz;
		modelFile >> u1 >> v1;
		if (i == 0)
		{
			minsMaxes[0] = minsMaxes[2] = x;
			minsMaxes[1] = minsMaxes[3] = z;
		}
		else
		{
			if (x < minsMaxes[0])
				minsMaxes[0] = x;
			if (x > minsMaxes[2])
				minsMaxes[2] = x;

			if (z < minsMaxes[1])
				minsMaxes[1] = z;
			if (z > minsMaxes[3])
				minsMaxes[3] = z;
		}

		model.Vertices[i].x = x;
		model.Vertices[i].y = y;
		model.Vertices[i].z = z;
		model.Vertices[i].nx = nx;
		model.Vertices[i].ny = ny;
		model.Vertices[i].nz = nz;
		model.Vertices[i].u1 = u1;
		model.Vertices[i].v1 = v1;
	}

	for (int i = 0; i < model.NumTriangles; i++)
	{
		int ind0, ind1, ind2;
		int discard;
		modelFile >> discard;
		modelFile >> ind0;
		modelFile >> ind1;
		modelFile >> ind2;

		model.Triangles[i].vertex_ID_0 = ind0;
		model.Triangles[i].vertex_ID_1 = ind1;
		model.Triangles[i].vertex_ID_2 = ind2;

		model.TrianglesWithVertices[i].Vertex[0] = model.Vertices[ind0];
		model.TrianglesWithVertices[i].Vertex[1] = model.Vertices[ind1];
		model.TrianglesWithVertices[i].Vertex[2] = model.Vertices[ind2];
	}
	modelFile.close();
	MinMax[name][0] = minsMaxes[0];
	MinMax[name][1] = minsMaxes[1];
	MinMax[name][2] = minsMaxes[2];
	MinMax[name][3] = minsMaxes[3];
	return model;
}

cTexture cResourceManager::loadTextureFromFile(const GLchar * textureFile, GLboolean alpha)
{
	cTexture texture;
	if (alpha)
	{
		texture.InternalFormat = GL_RGBA;
		texture.ImageFormat = GL_RGBA;
	}

	int width, height;

	unsigned char* image = SOIL_load_image(textureFile, &width, &height, 0, texture.ImageFormat == GL_RGBA ? SOIL_LOAD_RGBA : SOIL_LOAD_RGB);
	const char* error = SOIL_last_result();

	texture.Generate(width, height, image);

	SOIL_free_image_data(image);
	return texture;
}


