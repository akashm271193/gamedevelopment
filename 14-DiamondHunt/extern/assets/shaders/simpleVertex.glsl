#version 420
layout (location = 0) in vec3 objPos;
layout (location = 1) in vec3 objNorm;
layout (location = 2) in vec2 objTex;
layout (location = 3) in vec3 objTang;
layout (location = 4) in vec3 objBitan;
layout (location = 5) in vec4 objBoneID;
layout (location = 6) in vec4 objBoneWeight;	

out vec3 objNormal;
out vec2 objTexture;
out vec3 objTangent;
out vec3 objBitangent;
//out vec4 objBoneIDs;
//out vec4 objBoneWeights;


//out vec3 objWorldNormal;
//out vec3 objWorldPos;

//Skinned mesh stuff
const int MAXNUMBEROFBONES = 100;
uniform mat4 bones[MAXNUMBEROFBONES];
uniform int numBonesUsed;
uniform bool isSkinnedMesh;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
//uniform mat3 normal;


void main(){

	if (!isSkinnedMesh)
	{
	//gl_Position = matMVP * vertPosition;
	gl_Position = projection * view * model * vec4(objPos, 1.0f);
	objTexture = objTex;
	//objWorldPos = vec3(model * vec4(objPos,1.0));
	//objWorldNormal = normal * objNorm;
	}

	//vec4 vertPosition = vec4(objPos, 1.0f);

	if (isSkinnedMesh)
	{
		//calculate the bones transformation
		mat4 BoneTransform = bones[ int(objBoneID[0]) ] * objBoneWeight[0];
		BoneTransform += bones[ int(objBoneID[1]) ] * objBoneWeight[1];
		BoneTransform += bones[ int(objBoneID[2]) ] * objBoneWeight[2];
		BoneTransform += bones[ int(objBoneID[3]) ] * objBoneWeight[3];

		vec4 vertPosition = vec4(objPos, 1.0f);
		
		vertPosition = BoneTransform * vertPosition;

		mat4 matMVP = projection * view * model;
		//final screen space position
		gl_Position = matMVP * vertPosition;

		//inverse transform to keep rotations only
		mat4 matNormal = inverse( transpose( BoneTransform * model));

		objNormal = mat3(matNormal) * normalize(objNorm.xyz);
		objTangent = mat3(matNormal) * normalize(objTang.xyz);
		objBitangent = mat3(matNormal) * normalize(objBitan.xyz);
	}


}