#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include<iostream>
#include<string>
#include<glm/glm.hpp>
#include<vector>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "sVertexAllWithBones.h"
#include <array>
#include <map>


static const int MAXBONESPERVERTEX = 4;

struct Vertex {
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
};

struct Texture {
	unsigned int id;
	std::string type;
	std::string path;
};

struct sVertexBoneData
{
	std::array<float, MAXBONESPERVERTEX> ids;
	std::array<float, MAXBONESPERVERTEX> weights;

	void AddBoneData(unsigned int BoneID, float Weight);
};

struct sBoneInfo
{
	glm::mat4 BoneOffset;
	glm::mat4 FinalTransformation;
	glm::mat4 ObjectBoneTransformation;
};