#pragma once

#include "cMenuObj.h"
#include "SceneLoader.h"
#include <vector>

enum eMenuType {
	MAIN = 0,
	GAME_STATE_MENU = 1,
	GAME_END_MENU = 2,
	GAME_START_MENU = 3,
	FIRE_END_MENU = 4,
	UNKNOWN = 5
};

class cMenu
{
public:
	cMenu();
	~cMenu();
	cMenu(eMenuType type, int numObjects);
	int menuObjects;
	std::vector<cMenuObj*> vecMenuObjects;
	eMenuType menuType;

private:
	bool checkForSavedFiles(rapidjson::Document &doc);
};

