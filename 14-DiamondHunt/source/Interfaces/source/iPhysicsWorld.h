#ifndef _IPHYSICS_WORLD_HG_
#define _IPHYSICS_WORLD_HG_

#include "iCollisionBody.h"
#include "iDebugRenderer.h"
namespace libPhysics
{
	class iPhysicsWorld
	{
	public:
		virtual ~iPhysicsWorld() {} 

		virtual void stepSimulation(float deltaTime) = 0;
		virtual void AddBoundingBox(iCollisionBody* collisionBody) = 0;
		virtual void RemoveBoundingBox(iCollisionBody* collisionBody) = 0;
		virtual void UpdateBoundingBox(iCollisionBody* collisionBody) = 0;
		virtual void setViewAndProjectionMatrix(glm::mat4 viewMat, glm::mat4 projectionMat, int shadId) = 0;
		virtual void addDebugRender(iDebugRenderer* renderer) = 0;
		virtual void drawDebugWorld() = 0;
		virtual bool checkIfObjectAlreadyPresent(iCollisionBody* object)= 0;

	};
}

#endif