#include "cGameLevel.h"

#include <fstream>
#include <sstream>

cGameLevel::cGameLevel()
{
	this->startPosition = glm::vec3(-1.0f);
	this->playerPosCol = -1;
	this->playerPosRow = -1;
	return;
}

void cGameLevel::Load(const GLchar* file, GLuint lWidth, GLuint lHeight)
{
	this->Walls.clear();
	GLuint tileCode;

	cGameLevel level;
	std::string line;
	std::ifstream fstream(file);
	std::vector<std::vector<GLuint>> tileData;

	if (fstream) 
	{
		while (std::getline(fstream, line))
		{
			std::istringstream sstream(line);
			std::vector<GLuint> row;
			while (sstream >> tileCode)
				row.push_back(tileCode);
			tileData.push_back(row);
		}
		if (tileData.size() > 0)
			this->init(tileData, lWidth, lHeight);
	}
}

void cGameLevel::Draw(cVAOManager &Renderer)
{
	for (int i = 0; i < this->Walls.size(); i++)
	{
		cGameObject curObj = this->Walls[i];
		//curObj.Scale = glm::vec3(1.0f);
		curObj.Draw(Renderer);
	}
}



void cGameLevel::init(std::vector<std::vector<GLuint>> tileData, GLuint levelWidth, GLuint levelHeight)
{
	this->mapData = tileData;
	GLuint height = tileData.size();
	GLuint width = tileData[0].size();
	//GLfloat unitWidth = levelWidth / static_cast<GLfloat>(5.25 * width);
	//GLfloat unitHeight = levelHeight / static_cast<GLfloat>(9.25 * height);
	GLfloat unitWidth = 13;
	GLfloat unitHeight = 6.5;


	for (GLuint y = 0; y < height; ++y)
	{
		for (GLuint x = 0; x < width; ++x)
		{
			if (tileData[y][x] == 1)
			{
				glm::vec2 pos(unitWidth * x, unitHeight * y);
				glm::vec2 scale(unitWidth, unitHeight);

				cGameObject curObj("Wall", cResourceManager::GetTexture("Wall"), glm::vec3(pos.x, 0.0f, pos.y), glm::vec3(0.0f), glm::vec3(scale.x, 1.0f,scale.y));
				this->Walls.push_back(curObj);
			}
			else if (tileData[y][x] == 0)
			{
				if (this->startPosition.x == -1.0f && this->startPosition.y == -1.0f && this->startPosition.z == -1.0f) {
					this->startPosition = glm::vec3(x*unitWidth, 3.0f, y*unitHeight);
					this->playerPosCol = x;
					this->playerPosRow = y;
				}
				glm::vec2 pos(unitWidth * x, unitHeight * y);
				glm::vec2 scale(unitWidth, unitHeight);
				cGameObject curObj("Floor", cResourceManager::GetTexture("Floor"), glm::vec3(pos.x, -0.5f, pos.y), glm::vec3(0.0f), glm::vec3(scale.x, 1.0f,scale.y));
				this->Walls.push_back(curObj);
			}
		}
	}
}