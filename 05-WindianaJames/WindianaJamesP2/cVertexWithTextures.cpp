#include "cVertexWithTextures.h"

cVertexWithTextures::cVertexWithTextures()
{
	this->x = 0.0f;
	this->y = 0.0f;
	this->z = 0.0f;
	this->nx = 0.0f;
	this->ny = 0.0f;
	this->nz = 0.0f;
	this->u1 = 0.0f;
	this->v1 = 0.0f;
	this->u2 = 0.0f;
	this->v2 = 0.0f;
	this->tx = 0.0f;
	this->ty = 0.0f;
	this->tz = 0.0f;
	this->bx = 0.0f;
	this->by = 0.0f;
	this->bz = 0.0f;

	return;
}
