#include "cModel.h"

glm::mat4 AIMatrixToGLMMatrix(const aiMatrix4x4 &mat)
{
	return glm::mat4(mat.a1, mat.b1, mat.c1, mat.d1,
		mat.a2, mat.b2, mat.c2, mat.d2,
		mat.a3, mat.b3, mat.c3, mat.d3,
		mat.a4, mat.b4, mat.c4, mat.d4);
}


cModel::cModel(char *path,bool loadColour, bool isSkinnedMesh)
{
	this->toLoadColour = loadColour;
	this->minXYZ = glm::vec3(9999999999.0f, 9999999999.0f, 9999999999.0f);
	this->maxXYZ = glm::vec3(-9999999999.0f, -9999999999.0f, -9999999999.0f);
	this->isSkinnedMesh = isSkinnedMesh;
	if (isSkinnedMesh == false)
	{ 
		this->loadModel(path);
	}
	else if (isSkinnedMesh == true)
	{
		this->loadSkinnedModel(path);
	}
	
}

cModel::cModel()
{
}


cModel::~cModel()
{
}

//***************************************************************
//TODO: Adjust this for skinned mesh too
void cModel::DrawModel(GLint shaderId, glm::vec3 colour) {
	if (this->isSkinnedMesh == false)
	{
		for (unsigned int i = 0; i < meshes.size(); i++)
		{
			meshes[i]->Draw(shaderId, colour);
		}
	}
	else if (this->isSkinnedMesh == true)
	{
		for (unsigned int i = 0; i < skinnedMeshes.size(); i++)
		{
			skinnedMeshes[i]->Draw(shaderId, colour);
		}
	}
}

//***************************************************************
//TODO: Adjust this for skinned mesh too
void cModel::processNode(aiNode *node, const aiScene *scene)
{
	if (this->isSkinnedMesh == false)
	{
		// process all the node's meshes (if any)
		for (unsigned int i = 0; i < node->mNumMeshes; i++)
		{
			aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
			meshes.push_back(processMesh(mesh, scene));
		}
		// then do the same for each of its children
		for (unsigned int i = 0; i < node->mNumChildren; i++)
		{
			processNode(node->mChildren[i], scene);
		}
	}
	else if (this->isSkinnedMesh == true)
	{
		// process all the node's meshes (if any)
		for (unsigned int i = 0; i < node->mNumMeshes; i++)
		{
			aiMesh *skinnedMesh = scene->mMeshes[node->mMeshes[i]];
			skinnedMeshes.push_back(processSkinnedMesh(skinnedMesh, scene));
		}
		// then do the same for each of its children
		for (unsigned int i = 0; i < node->mNumChildren; i++)
		{
			processNode(node->mChildren[i], scene);
		}
	}
	
}

void cModel::translateModel(glm::vec3 newPos) {
	for (int i = 0; i < this->meshes.size(); i++) {
		for (int j = 0; j < this->meshes[i]->vertices.size(); j++) {
			this->meshes[i]->vertices[j].Position.x += newPos.x;
			this->meshes[i]->vertices[j].Position.y += newPos.y;
			this->meshes[i]->vertices[j].Position.z += newPos.z;
		}
	}
}


//***************************************************************
// MESH
//**************************************************************
//load normal mesh models
void cModel::loadModel(std::string path)
{
	scene = import.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		std::cout << "ERROR::ASSIMP::" << import.GetErrorString() << std::endl;
		return;
	}
	this->directory = path.substr(0, path.find_last_of('/'));

	this->processNode(scene->mRootNode, scene);

	this->maxExtent = this->maxExtentXYZ.x;
	if (this->maxExtentXYZ.y > this->maxExtent) {
		this->maxExtent = this->maxExtentXYZ.y;
	}
	if (this->maxExtentXYZ.z > this->maxExtent) {
		this->maxExtent = this->maxExtentXYZ.z;
	}

	this->scaleForUnitBBox = 1.0f / this->maxExtent;
}

//process normal mesh models
cMesh* cModel::processMesh(aiMesh *mesh, const aiScene *scene)
{
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
	std::vector<Texture> textures;

	for (unsigned int i = 0; i < mesh->mNumVertices; i++)
	{
		// process vertex positions, normals and texture coordinates
		Vertex vertex;	
		glm::vec3 vector;
		vector.x = mesh->mVertices[i].x;
		vector.y = mesh->mVertices[i].y;
		vector.z = mesh->mVertices[i].z;

		// Code to find the extents of the model
		// -------------------------------------
		if (vector.x < this->minXYZ.x) {
			this->minXYZ.x = vector.x;
		}
		if (vector.x > this->maxXYZ.x) {
			this->maxXYZ.x = vector.x;
		}
		if (vector.y < this->minXYZ.y) {
			this->minXYZ.y = vector.y;
		}
		if (vector.y > this->maxXYZ.y) {
			this->maxXYZ.y = vector.y;
		}
		if (vector.z < this->minXYZ.z) {
			this->minXYZ.z = vector.z;
		}
		if (vector.z > this->maxXYZ.z) {
			this->maxXYZ.z = vector.z;
		}

		this->maxExtentXYZ.x = this->maxXYZ.x - this->minXYZ.x;
		this->maxExtentXYZ.y = this->maxXYZ.y - this->minXYZ.y;
		this->maxExtentXYZ.z = this->maxXYZ.z - this->minXYZ.z;
		// -------------------------------------------
		// Model extents code ends

		vertex.Position = vector; 
		vector.x = mesh->mNormals[i].x;
		vector.y = mesh->mNormals[i].y;
		vector.z = mesh->mNormals[i].z;
		vertex.Normal = vector;
		if (mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
		{
			glm::vec2 vec;
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.TexCoords = vec;
		}
		else {
			vertex.TexCoords = glm::vec2(0.0f, 0.0f);
		}

		vertices.push_back(vertex);
	}
	// process indices
	for (unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		for (unsigned int j = 0; j < face.mNumIndices; j++)
			indices.push_back(face.mIndices[j]);
	}
		// process material
	if (mesh->mMaterialIndex >= 0)
	{
		aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];
		std::vector<Texture> diffuseMaps = loadMaterialTextures(material,aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		std::vector<Texture> specularMaps = loadMaterialTextures(material,aiTextureType_SPECULAR, "texture_specular");
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
	}

	return new cMesh(vertices, indices, textures,this->toLoadColour);
}
// END OF MESH
//**************************************************************


//**************************************************************
// SKINNED MESH
//**************************************************************
//Initialize
bool cModel::Initialize(unsigned int meshIndex)
{
	this->vecVertexBoneData.resize(this->scene->mMeshes[meshIndex]->mNumVertices);
	this->LoadBones(this->scene->mMeshes[meshIndex], this->vecVertexBoneData);
	return true;
}


//load skinned mesh models
void cModel::loadSkinnedModel(std::string path)
{
	scene = import.ReadFile(path, aiProcess_Triangulate | aiProcess_OptimizeMeshes | aiProcess_OptimizeGraph | aiProcess_JoinIdenticalVertices);

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		std::cout << "ERROR::ASSIMP::" << import.GetErrorString() << std::endl;
		return;
	}
	this->directory = path.substr(0, path.find_last_of('/'));

	int currentInd = this->skinnedMeshes.size();

	this->mGlobalInverseTransformation = AIMatrixToGLMMatrix(scene->mRootNode->mTransformation);
	this->mGlobalInverseTransformation = glm::inverse(this->mGlobalInverseTransformation);

	//calculate bone things
	if (this->Initialize(currentInd))
	{
		this->processNode(scene->mRootNode, scene);

		this->maxExtent = this->maxExtentXYZ.x;
		if (this->maxExtentXYZ.y > this->maxExtent) {
			this->maxExtent = this->maxExtentXYZ.y;
		}
		if (this->maxExtentXYZ.z > this->maxExtent) {
			this->maxExtent = this->maxExtentXYZ.z;
		}

		this->scaleForUnitBBox = 1.0f / this->maxExtent;


	}
		
	
}

//load bones
void cModel::LoadBones(const aiMesh* Mesh, std::vector<sVertexBoneData> &Bones)
{
	for (unsigned int boneIndex = 0; boneIndex != Mesh->mNumBones; boneIndex++)
	{
		unsigned int BoneIndex = 0;
		std::string BoneName(Mesh->mBones[boneIndex]->mName.data);

		std::map<std::string, unsigned int>::iterator it = this->m_mapBoneNameToBoneIndex.find(BoneName);
		if (it == this->m_mapBoneNameToBoneIndex.end())
		{
			BoneIndex = this->mNumBones;
			this->mNumBones++;
			sBoneInfo bi;
			this->mBoneInfo.push_back(bi);

			this->mBoneInfo[BoneIndex].BoneOffset = AIMatrixToGLMMatrix(Mesh->mBones[boneIndex]->mOffsetMatrix);
			this->m_mapBoneNameToBoneIndex[BoneName] = BoneIndex;
		}
		else
		{
			BoneIndex = it->second;
		}

		for (unsigned int WeightIndex = 0; WeightIndex != Mesh->mBones[boneIndex]->mNumWeights; WeightIndex++)
		{
			unsigned int VertexID = Mesh->mBones[boneIndex]->mWeights[WeightIndex].mVertexId;
			float Weight = Mesh->mBones[boneIndex]->mWeights[WeightIndex].mWeight;
			Bones[VertexID].AddBoneData(BoneIndex, Weight);
		}
	}
}

//load animations
void cModel::loadAnimation(std::string path)
{

	unsigned int Flags = aiProcess_Triangulate | aiProcess_OptimizeMeshes | aiProcess_OptimizeGraph | aiProcess_JoinIdenticalVertices;

	Assimp::Importer* pImporter = new Assimp::Importer();

	const aiScene* pAniScene = pImporter->ReadFile(path, aiProcess_Triangulate | aiProcess_OptimizeMeshes | aiProcess_OptimizeGraph | aiProcess_JoinIdenticalVertices);

	if (!pAniScene || pAniScene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !pAniScene->mRootNode)
	{
		std::cout << "ERROR::ASSIMP::" << pImporter->GetErrorString() << std::endl;
		return;
	}
	this->directory = path.substr(0, path.find_last_of('/'));

	//this->processNode(scene->mRootNode, scene);

	this->maxExtent = this->maxExtentXYZ.x;
	if (this->maxExtentXYZ.y > this->maxExtent) {
		this->maxExtent = this->maxExtentXYZ.y;
	}
	if (this->maxExtentXYZ.z > this->maxExtent) {
		this->maxExtent = this->maxExtentXYZ.z;
	}

	this->scaleForUnitBBox = 1.0f / this->maxExtent;

	this->mapAnimationNameToScene[path] = pAniScene;
}


//process skinned mesh models
cSkinnedMesh* cModel::processSkinnedMesh(aiMesh *mesh, const aiScene *scene)
{
	std::vector<sVertexAllWithBones> vertices;
	std::vector<unsigned int> indices;
	std::vector<Texture> textures;

	for (unsigned int i = 0; i < mesh->mNumVertices; i++)
	{
		// process vertex positions, normals and texture coordinates
		sVertexAllWithBones vertex;
		glm::vec3 vector;
		vector.x = mesh->mVertices[i].x;
		vector.y = mesh->mVertices[i].y;
		vector.z = mesh->mVertices[i].z;

		// Code to find the extents of the model
		// -------------------------------------
		if (vector.x < this->minXYZ.x) {
			this->minXYZ.x = vector.x;
		}
		if (vector.x > this->maxXYZ.x) {
			this->maxXYZ.x = vector.x;
		}
		if (vector.y < this->minXYZ.y) {
			this->minXYZ.y = vector.y;
		}
		if (vector.y > this->maxXYZ.y) {
			this->maxXYZ.y = vector.y;
		}
		if (vector.z < this->minXYZ.z) {
			this->minXYZ.z = vector.z;
		}
		if (vector.z > this->maxXYZ.z) {
			this->maxXYZ.z = vector.z;
		}

		this->maxExtentXYZ.x = this->maxXYZ.x - this->minXYZ.x;
		this->maxExtentXYZ.y = this->maxXYZ.y - this->minXYZ.y;
		this->maxExtentXYZ.z = this->maxXYZ.z - this->minXYZ.z;
		// -------------------------------------------
		// Model extents code ends

		vertex.Position = vector;

		//Normals
		vector.x = mesh->mNormals[i].x;
		vector.y = mesh->mNormals[i].y;
		vector.z = mesh->mNormals[i].z;
		vertex.Normal = vector;

		//Texcoords
		if (mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
		{
			glm::vec2 vec;
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.TexCoords = vec;
		}
		else {
			vertex.TexCoords = glm::vec2(0.0f, 0.0f);
		}

		//Tangents nad Bitangents/Binormals
		if (mesh->HasTangentsAndBitangents())
		{
			glm::vec3 vecTangent;
			vecTangent.x = mesh->mTangents[0].x;
			vecTangent.y = mesh->mTangents[0].y;
			vecTangent.z = mesh->mTangents[0].z;
			vertex.Tangent = vecTangent;

			glm::vec3 vecBitangent;
			vecBitangent.x = mesh->mBitangents[0].x;
			vecBitangent.y = mesh->mBitangents[0].y;
			vecBitangent.z = mesh->mBitangents[0].z;
			vertex.BiNormal = vecBitangent;
		}

		float trial = vecVertexBoneData[i].ids[0];
		//bone ID
		vertex.boneID[0] = this->vecVertexBoneData[i].ids[0];
		vertex.boneID[1] = this->vecVertexBoneData[i].ids[1];
		vertex.boneID[2] = this->vecVertexBoneData[i].ids[2];
		vertex.boneID[3] = this->vecVertexBoneData[i].ids[3];

		//bone Weights
		vertex.boneWeights[0] = this->vecVertexBoneData[i].weights[0];
		vertex.boneWeights[1] = this->vecVertexBoneData[i].weights[1];
		vertex.boneWeights[2] = this->vecVertexBoneData[i].weights[2];
		vertex.boneWeights[3] = this->vecVertexBoneData[i].weights[3];

		vertices.push_back(vertex);
	}
	// process indices
	for (unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		for (unsigned int j = 0; j < face.mNumIndices; j++)
			indices.push_back(face.mIndices[j]);
	}
	// process material
	if (mesh->mMaterialIndex >= 0)
	{
		aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];
		std::vector<Texture> diffuseMaps = loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		std::vector<Texture> specularMaps = loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
	}
	
	return new cSkinnedMesh(vertices, indices, textures, this->toLoadColour);
}

//try this if it works
void sVertexBoneData::AddBoneData(unsigned int BoneID, float Weight)
{
	for (unsigned int Index = 0; Index < sizeof(this->ids) / sizeof(this->ids[0]); Index++)
	{
		if (this->weights[Index] == 0.0f)
		{
			this->ids[Index] = (float)BoneID;
			this->weights[Index] = Weight;
			return;
		}
	}
}


void cModel::BoneTransform(float TimeInSeconds, std::string animationName,
					std::vector<glm::mat4> &FinalTransformation,
					std::vector<glm::mat4> &Globals,
					std::vector<glm::mat4> &Offsets)
{
	glm::mat4 Identity(1.0f);

	float TicksPerSecond = static_cast<float>(scene->mAnimations[0]->mTicksPerSecond != 0 ?
		scene->mAnimations[0]->mTicksPerSecond : 25.0);

	float TimeInTicks = TimeInSeconds * TicksPerSecond;
	float AnimationTime = fmod(TimeInTicks, (float)scene->mAnimations[0]->mDuration);

	// use the "animation" file to look up these nodes
	// (need the matOffset information from the animation file)
	this->ReadNodeHeirarchy(AnimationTime, animationName, scene->mRootNode, Identity);

	FinalTransformation.resize(this->mNumBones);
	Globals.resize(this->mNumBones);
	Offsets.resize(this->mNumBones);

	for (unsigned int BoneIndex = 0; BoneIndex < this->mNumBones; BoneIndex++)
	{
		FinalTransformation[BoneIndex] = this->mBoneInfo[BoneIndex].FinalTransformation;
		Globals[BoneIndex] = this->mBoneInfo[BoneIndex].ObjectBoneTransformation;
		Offsets[BoneIndex] = this->mBoneInfo[BoneIndex].BoneOffset;
	}
}

//looks into animation map and returns total time
float cModel::FindAnimationTotalTime(std::string animationName)
{
	std::map< std::string, const aiScene*>::iterator itAnim = this->mapAnimationNameToScene.find(animationName);

	if (itAnim == this->mapAnimationNameToScene.end())
	{
		return 0.0f;
	}

	//scale the animation from 0 to 1
	return itAnim->second->mAnimations[0]->mDuration;
}



const aiNodeAnim* cModel::FindNodeAnimationChannel(const aiAnimation* pAnimation, aiString boneName)
{
	for (unsigned int ChannelIndex = 0; ChannelIndex != pAnimation->mNumChannels; ChannelIndex++)
	{
		if (pAnimation->mChannels[ChannelIndex]->mNodeName == boneName)
		{
			return pAnimation->mChannels[ChannelIndex];
		}
	}
	return 0;
}


void cModel::CalcInterpolatedRotation(float AnimationTime, const aiNodeAnim* pNodeAnim, aiQuaternion &out)
{
	if (pNodeAnim->mNumRotationKeys == 1)
	{
		out = pNodeAnim->mRotationKeys[0].mValue;
		return;
	}

	unsigned int RotationIndex = this->FindRotation(AnimationTime, pNodeAnim);
	unsigned int NextRotationIndex = (RotationIndex + 1);
	assert(NextRotationIndex < pNodeAnim->mNumRotationKeys);
	float DeltaTime = (float)(pNodeAnim->mRotationKeys[NextRotationIndex].mTime - pNodeAnim->mRotationKeys[RotationIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mRotationKeys[RotationIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiQuaternion& StartRotationQ = pNodeAnim->mRotationKeys[RotationIndex].mValue;
	const aiQuaternion& EndRotationQ = pNodeAnim->mRotationKeys[NextRotationIndex].mValue;
	aiQuaternion::Interpolate(out, StartRotationQ, EndRotationQ, Factor);
	out = out.Normalize();

	return;
}

void cModel::CalcInterpolatedPosition(float AnimationTime, const aiNodeAnim* pNodeAnim, aiVector3D &out)
{
	if (pNodeAnim->mNumPositionKeys == 1)
	{
		out = pNodeAnim->mPositionKeys[0].mValue;
		return;
	}

	unsigned int PositionIndex = this->FindPosition(AnimationTime, pNodeAnim);
	unsigned int NextPositionIndex = (PositionIndex + 1);
	assert(NextPositionIndex < pNodeAnim->mNumPositionKeys);
	float DeltaTime = (float)(pNodeAnim->mPositionKeys[NextPositionIndex].mTime - pNodeAnim->mPositionKeys[PositionIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mPositionKeys[PositionIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiVector3D& StartPosition = pNodeAnim->mPositionKeys[PositionIndex].mValue;
	const aiVector3D& EndPosition = pNodeAnim->mPositionKeys[NextPositionIndex].mValue;
	out = (EndPosition - StartPosition) * Factor + StartPosition;

	return;
}

void cModel::CalcInterpolatedScaling(float AnimationTime, const aiNodeAnim* pNodeAnim, aiVector3D &out)
{
	if (pNodeAnim->mNumScalingKeys == 1)
	{
		out = pNodeAnim->mScalingKeys[0].mValue;
		return;
	}

	unsigned int ScalingIndex = this->FindScaling(AnimationTime, pNodeAnim);
	unsigned int NextScalingIndex = (ScalingIndex + 1);
	assert(NextScalingIndex < pNodeAnim->mNumScalingKeys);
	float DeltaTime = (float)(pNodeAnim->mScalingKeys[NextScalingIndex].mTime - pNodeAnim->mScalingKeys[ScalingIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mScalingKeys[ScalingIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiVector3D& StartScale = pNodeAnim->mScalingKeys[ScalingIndex].mValue;
	const aiVector3D& EndScale = pNodeAnim->mScalingKeys[NextScalingIndex].mValue;
	out = (EndScale - StartScale) * Factor + StartScale;

	return;
}


void cModel::CalcGLMInterpolatedRotation(float AnimationTime, const aiNodeAnim* pNodeAnim, glm::quat &out)
{
	if (pNodeAnim->mNumRotationKeys == 1)
	{
		out.w = pNodeAnim->mRotationKeys[0].mValue.w;
		out.x = pNodeAnim->mRotationKeys[0].mValue.x;
		out.y = pNodeAnim->mRotationKeys[0].mValue.y;
		out.z = pNodeAnim->mRotationKeys[0].mValue.z;
		return;
	}

	unsigned int RotationIndex = this->FindRotation(AnimationTime, pNodeAnim);
	unsigned int NextRotationIndex = (RotationIndex + 1);
	assert(NextRotationIndex < pNodeAnim->mNumRotationKeys);
	float DeltaTime = (float)(pNodeAnim->mRotationKeys[NextRotationIndex].mTime - pNodeAnim->mRotationKeys[RotationIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mRotationKeys[RotationIndex].mTime) / DeltaTime;
	if (Factor < 0.0f) Factor = 0.0f;
	if (Factor > 1.0f) Factor = 1.0f;
	const aiQuaternion& StartRotationQ = pNodeAnim->mRotationKeys[RotationIndex].mValue;
	const aiQuaternion& EndRotationQ = pNodeAnim->mRotationKeys[NextRotationIndex].mValue;

	glm::quat StartGLM = glm::quat(StartRotationQ.w, StartRotationQ.x, StartRotationQ.y, StartRotationQ.z);
	glm::quat EndGLM = glm::quat(EndRotationQ.w, EndRotationQ.x, EndRotationQ.y, EndRotationQ.z);

	out = glm::slerp(StartGLM, EndGLM, Factor);

	out = glm::normalize(out);

	return;
}

void cModel::CalcGLMInterpolatedPosition(float AnimationTime, const aiNodeAnim* pNodeAnim, glm::vec3 &out)
{
	if (pNodeAnim->mNumPositionKeys == 1)
	{
		out.x = pNodeAnim->mPositionKeys[0].mValue.x;
		out.y = pNodeAnim->mPositionKeys[0].mValue.y;
		out.z = pNodeAnim->mPositionKeys[0].mValue.z;
		return;
	}

	unsigned int PositionIndex = this->FindPosition(AnimationTime, pNodeAnim);
	unsigned int NextPositionIndex = (PositionIndex + 1);
	assert(NextPositionIndex < pNodeAnim->mNumPositionKeys);
	float DeltaTime = (float)(pNodeAnim->mPositionKeys[NextPositionIndex].mTime - pNodeAnim->mPositionKeys[PositionIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mPositionKeys[PositionIndex].mTime) / DeltaTime;
	if (Factor < 0.0f) Factor = 0.0f;
	if (Factor > 1.0f) Factor = 1.0f;
	const aiVector3D& StartPosition = pNodeAnim->mPositionKeys[PositionIndex].mValue;
	const aiVector3D& EndPosition = pNodeAnim->mPositionKeys[NextPositionIndex].mValue;
	glm::vec3 start = glm::vec3(StartPosition.x, StartPosition.y, StartPosition.z);
	glm::vec3 end = glm::vec3(EndPosition.x, EndPosition.y, EndPosition.z);

	out = (end - start) * Factor + start;

	return;
}

void cModel::CalcGLMInterpolatedScaling(float AnimationTime, const aiNodeAnim* pNodeAnim, glm::vec3 &out)
{
	if (pNodeAnim->mNumScalingKeys == 1)
	{
		out.x = pNodeAnim->mScalingKeys[0].mValue.x;
		out.y = pNodeAnim->mScalingKeys[0].mValue.y;
		out.z = pNodeAnim->mScalingKeys[0].mValue.z;
		return;
	}

	unsigned int ScalingIndex = this->FindScaling(AnimationTime, pNodeAnim);
	unsigned int NextScalingIndex = (ScalingIndex + 1);
	assert(NextScalingIndex < pNodeAnim->mNumScalingKeys);
	float DeltaTime = (float)(pNodeAnim->mScalingKeys[NextScalingIndex].mTime - pNodeAnim->mScalingKeys[ScalingIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mScalingKeys[ScalingIndex].mTime) / DeltaTime;
	if (Factor < 0.0f) Factor = 0.0f;
	if (Factor > 1.0f) Factor = 1.0f;
	const aiVector3D& StartScale = pNodeAnim->mScalingKeys[ScalingIndex].mValue;
	const aiVector3D& EndScale = pNodeAnim->mScalingKeys[NextScalingIndex].mValue;
	glm::vec3 start = glm::vec3(StartScale.x, StartScale.y, StartScale.z);
	glm::vec3 end = glm::vec3(EndScale.x, EndScale.y, EndScale.z);
	out = (end - start) * Factor + start;

	return;
}


unsigned int cModel::FindRotation(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	for (unsigned int RotationKeyIndex = 0; RotationKeyIndex != pNodeAnim->mNumRotationKeys - 1; RotationKeyIndex++)
	{
		if (AnimationTime < (float)pNodeAnim->mRotationKeys[RotationKeyIndex + 1].mTime)
		{
			return RotationKeyIndex;
		}
	}

	return 0;
}

unsigned int cModel::FindPosition(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	for (unsigned int PositionKeyIndex = 0; PositionKeyIndex != pNodeAnim->mNumPositionKeys - 1; PositionKeyIndex++)
	{
		if (AnimationTime < (float)pNodeAnim->mPositionKeys[PositionKeyIndex + 1].mTime)
		{
			return PositionKeyIndex;
		}
	}

	return 0;
}

unsigned int cModel::FindScaling(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	for (unsigned int ScalingKeyIndex = 0; ScalingKeyIndex != pNodeAnim->mNumScalingKeys - 1; ScalingKeyIndex++)
	{
		if (AnimationTime < (float)pNodeAnim->mScalingKeys[ScalingKeyIndex + 1].mTime)
		{
			return ScalingKeyIndex;
		}
	}

	return 0;
}


float cModel::GetDuration(void)
{
	float duration = (float)(scene->mAnimations[0]->mDuration / scene->mAnimations[0]->mTicksPerSecond);

	return duration;
}


void cModel::ReadNodeHeirarchy(float AnimationTime, std::string animationName,
			const aiNode* pNode, const glm::mat4 &ParentTransformMatrix)
{
	aiString NodeName(pNode->mName.data);

	// Original version picked the "main scene" animation...
	const aiAnimation* pAnimation = scene->mAnimations[0];

	// Search for the animation we want... 
	std::map< std::string, const aiScene* >::iterator itAnimation
		= mapAnimationNameToScene.find(animationName);

	// Did we find it? 
	if (itAnimation != mapAnimationNameToScene.end())
	{
		// Yes, there is an animation called that...
		// ...replace the animation with the one we found
		pAnimation = reinterpret_cast<const aiAnimation*>(itAnimation->second->mAnimations[0]);
	}

	// Transformation of the node in bind pose
	glm::mat4 NodeTransformation = AIMatrixToGLMMatrix(pNode->mTransformation);

	const aiNodeAnim* pNodeAnim = this->FindNodeAnimationChannel(pAnimation, NodeName);

	if (pNodeAnim)
	{
		// Get interpolated scaling
		glm::vec3 scale;
		this->CalcGLMInterpolatedScaling(AnimationTime, pNodeAnim, scale);
		glm::mat4 ScalingM = glm::scale(glm::mat4(1.0f), scale);

		// Get interpolated rotation (quaternion)
		glm::quat ori;
		this->CalcGLMInterpolatedRotation(AnimationTime, pNodeAnim, ori);
		glm::mat4 RotationM = glm::mat4_cast(ori);

		// Get interpolated position 
		glm::vec3 pos;
		this->CalcGLMInterpolatedPosition(AnimationTime, pNodeAnim, pos);
		glm::mat4 TranslationM = glm::translate(glm::mat4(1.0f), pos);


		// Combine the above transformations
		NodeTransformation = TranslationM * RotationM * ScalingM;
	}
	glm::mat4 ObjectBoneTransformation = ParentTransformMatrix * NodeTransformation;

	std::map<std::string, unsigned int>::iterator it = this->m_mapBoneNameToBoneIndex.find(std::string(NodeName.data));
	if (it != this->m_mapBoneNameToBoneIndex.end())
	{
		unsigned int BoneIndex = it->second;
		this->mBoneInfo[BoneIndex].ObjectBoneTransformation = ObjectBoneTransformation;
		this->mBoneInfo[BoneIndex].FinalTransformation = this->mGlobalInverseTransformation
			* ObjectBoneTransformation
			* this->mBoneInfo[BoneIndex].BoneOffset;
	}
	else
	{
		int breakpoint = 0;
	}

	for (unsigned int ChildIndex = 0; ChildIndex != pNode->mNumChildren; ChildIndex++)
	{
		this->ReadNodeHeirarchy(AnimationTime, animationName,
			pNode->mChildren[ChildIndex], ObjectBoneTransformation);
	}
}

void cModel::blurIndices() {
	for (int i = 0; i < meshes.size(); i++) {
		std::vector<unsigned int> indexes = meshes[i]->getIndicesForMesh();
		for (int j = 0; j < indexes.size() - 1; j++) {
			int newVal = (indexes[j] + indexes[j + 1]) % indexes.size();
			indexes[j] = newVal;
		}
		meshes[i]->setIndicesForMesh(indexes);
	}
};









// END OF SKINNED MESH
//**************************************************************




std::vector<Texture> cModel::loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName)
{
	std::vector<Texture> textures;
	for (unsigned int i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString str;
		mat->GetTexture(type, i, &str);
		bool skip = false;
		for (unsigned int j = 0; j < textures_loaded.size(); j++)
		{
			if (std::strcmp(textures_loaded[j].path.data(), str.C_Str()) == 0)
			{
				textures.push_back(textures_loaded[j]);
				skip = true;
				break;
			}
		}
		if (!skip)
		{   // if texture hasn't been loaded already, load it
			Texture texture;
			texture.id = TextureFromFile(str.C_Str(), directory);
			texture.type = typeName;
			texture.path = str.C_Str();
			textures.push_back(texture);
			textures_loaded.push_back(texture); // add to loaded textures
		}
	}
	return textures;
}


void cModel::addSeparateTexturesToModel(std::string fileName, std::string directory, std::string typeName, cMesh* mesh){	//Add own texture
	// if texture hasn't been loaded already, load it
	std::vector<Texture> vecTex = mesh->getTexturesForMesh();
	Texture texture;
	texture.id = TextureFromFile(fileName.c_str(), directory);
	texture.type = typeName;
	texture.path = fileName.c_str();
	vecTex.push_back(texture);
	mesh->setTexturesForMesh(vecTex);
	//textures_loaded.push_back(texture);
}

unsigned int TextureFromFile(const char *path, const std::string &directory, bool gamma)
{
	std::string filename = std::string(path);
	filename = directory + '/' + filename;

	unsigned int textureID;
	glGenTextures(1, &textureID);

	int width, height, nrComponents;
	unsigned char *data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
	if (data)
	{
		GLenum format;
		if (nrComponents == 1)
			format = GL_RED;
		else if (nrComponents == 3)
			format = GL_RGB;
		else if (nrComponents == 4)
			format = GL_RGBA;

		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}
	else
	{
		std::cout << "Texture failed to load at path: " << path << std::endl;
		stbi_image_free(data);
	}

	return textureID;
}

void cModel::getModelExtents() {
	std::cout << "Model XYZ extents : " << this->maxExtentXYZ.x << "   ----  "<< this->maxExtentXYZ.y << "  -----  "<< this->maxExtentXYZ.z<< std::endl;
}

std::vector<cMesh*> cModel::getAllMeshes() {
	return this->meshes;
}