#include "cVAOManager.h"
#include "cResourceManager.h"
#include "cModel.h"
#include "cShader.h"
#include "cTexture.h"

#include <glm\gtc\type_ptr.hpp>


void cVAOManager::LoadTexturedModel(cModel model, cShader shader)
{
	sRenderInfo* RenderInfo = new sRenderInfo;

	glGenVertexArrays(1, &RenderInfo->VAO);
	glGenBuffers(1, &RenderInfo->VBO);
	glGenBuffers(1, &RenderInfo->EBO);

	glBindVertexArray(RenderInfo->VAO);
	glBindBuffer(GL_ARRAY_BUFFER, RenderInfo->VBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, RenderInfo->EBO);

	int sizeOfCVertex = sizeof(cVertex);
	int sizeOfModelVertices = sizeOfCVertex * model.NumVertices;

	int sizeOfCTriangle = sizeof(cTriangle);
	int sizeOfModelTriangles = sizeOfCTriangle * model.NumTriangles;

	glBufferData(GL_ARRAY_BUFFER, sizeOfModelVertices, model.Vertices, GL_STATIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeOfModelTriangles, model.Triangles, GL_STATIC_DRAW);

	GLuint positionLocation = glGetAttribLocation(shader.ID, "position");
	GLuint normalLocation = glGetAttribLocation(shader.ID, "normal");
	GLuint textureLocation = glGetAttribLocation(shader.ID, "texturePosition");

	glEnableVertexAttribArray(positionLocation);
	glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, sizeOfCVertex,
		reinterpret_cast<void*>(static_cast<uintptr_t>(offsetof(cVertex, x))));
	glEnableVertexAttribArray(normalLocation);
	glVertexAttribPointer(normalLocation, 3, GL_FLOAT, GL_FALSE, sizeOfCVertex,
		reinterpret_cast<void*>(static_cast<uintptr_t>(offsetof(cVertex, nx))));
	glEnableVertexAttribArray(textureLocation);
	glVertexAttribPointer(textureLocation, 4, GL_FLOAT, GL_FALSE, sizeOfCVertex,
		reinterpret_cast<void*>(static_cast<uintptr_t>(offsetof(cVertex, u1))));

	RenderInfo->ModelName = model.Name;
	RenderInfo->NumIndices = model.NumIndices;
	RenderInfo->ShaderID = shader.ID;
	this->RenderInformations[model.Name] = *RenderInfo;

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(positionLocation);
	glDisableVertexAttribArray(normalLocation);
	glDisableVertexAttribArray(textureLocation);
}

void cVAOManager::DrawTexturedModel3D(sRenderInfo renderInfo, cTexture &texture, glm::vec3 position, glm::vec3 scale, glm::vec3 orientation)
{
	glUseProgram(renderInfo.ShaderID);

	glm::mat4 model = glm::mat4(1.0f);
	
	model = glm::translate(model, glm::vec3(position));
	model = glm::rotate(model, orientation.x, glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, orientation.y, glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::rotate(model, orientation.z, glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(scale));

	//renderInfo.Shader->SetMatrix4f("model", model);
	GLuint matrixLocation;
	matrixLocation = glGetUniformLocation(renderInfo.ShaderID, "model");
	glEnableVertexAttribArray(matrixLocation);
	glUniformMatrix4fv(matrixLocation, 1, GL_FALSE, glm::value_ptr(model));

	glActiveTexture(GL_TEXTURE0);
	texture.Bind();

	glBindVertexArray(renderInfo.VAO);
	glDrawElements(GL_TRIANGLES, renderInfo.NumIndices, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

bool cVAOManager::FindModelToDraw(std::string name, sRenderInfo &renderInfo)
{
	std::map<std::string, sRenderInfo>::iterator iter = this->RenderInformations.find(name);

	if (iter == this->RenderInformations.end())
		return false;
	renderInfo = iter->second;
	return true;
}