#ifndef _cVAOMeshManager_HG_
#define _cVAOMeshManager_HG_

#include <string>
class cMesh;
#include <map>


struct sVAOInfo {
	sVAOInfo() : vaoId(0), shaderId(-1), vertexBufferId(0), indexBufferId(0), numberOfVertices(0),
		numberOfTriangles(0), numberOfIndices(0) , scaleForUnitBBox(1.0f){}
	unsigned int vertexBufferId;
	unsigned int indexBufferId;
	unsigned int numberOfVertices;
	unsigned int numberOfTriangles;
	unsigned int numberOfIndices;
	std::string friendlyName;
	int shaderId;
	unsigned int vaoId;
	float scaleForUnitBBox;
};


class cVAOMeshManager {

public:
	cVAOMeshManager();
	~cVAOMeshManager();

	bool LoadMeshIntoVAO(cMesh &theMesh,int shaderId);

	bool LookUpVAOByName(std::string, sVAOInfo &thVAOInfo);

	std::map<std::string, sVAOInfo> m_mapNameToVAO;
};

#endif
