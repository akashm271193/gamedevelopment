#ifndef _SHADER_
#define _SHADER_

#include <string>

#include <glad\glad.h>
#include <glm\glm.hpp>
#include <glm\common.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

class cShader
{
public:
	GLuint ID;

	cShader();
	cShader& Use();

	void Compile(const GLchar* vertexSouce, const GLchar* fragmentSource);

	void SetFloat(const GLchar* name, GLfloat value, GLboolean useShader = false);
	void SetInteger(const GLchar* name, GLint value, GLboolean useShader = false);
	void SetVector2f(const GLchar* name, GLfloat x, GLfloat y, GLboolean useShader = false);
	void SetVector2f(const GLchar* name, const glm::vec2 &value, GLboolean useShader = false);
	void SetVector3f(const GLchar* name, GLfloat x, GLfloat y, GLfloat z, GLboolean useShader = false);
	void SetVector3f(const GLchar* name, const glm::vec3 &value, GLboolean useShader = false);
	void SetVector4f(const GLchar* name, GLfloat x, GLfloat y, GLfloat z, GLfloat w, GLboolean useShader = false);
	void SetVector4f(const GLchar* name, const glm::vec4 &value, GLboolean useShader = false);
	void SetMatrix4f(const GLchar* name, const glm::mat4 &matrix, GLboolean useShader = false);
private:
	void checkCompileErrors(GLuint object, std::string type);
};
#endif // !CSHADER
