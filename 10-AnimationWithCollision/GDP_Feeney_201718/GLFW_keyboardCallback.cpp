#include "globalOpenGL_GLFW.h"
#include "globalGameStuff.h"

#include <iostream>

#include "cAnimationState.h"

bool isShiftKeyDown( int mods, bool bByItself = true );
bool isCtrlKeyDown( int mods, bool bByItself = true );
bool isAltKeyDown( int mods, bool bByItself = true );
bool areAllModifierKeysUp(int mods);
bool areAnyModifierKeysDown(int mods);

extern std::string g_AnimationToPlay;
extern cAABBBroadPhase* g_modelAABBBroadPhase;
extern cPhysicsWorld* g_pPhysicsWorld;

enum eCharacterAnim
{
	SOPHIE,
	EVE
};

eCharacterAnim currentCharacter;

/*static*/ void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);


	// Fullscreen to windowed mode on the PRIMARY monitor (whatever that is)
	if ( isAltKeyDown(mods, true) && key == GLFW_KEY_ENTER )
	{
		if ( action == GLFW_PRESS )
		{
			::g_IsWindowFullScreen = !::g_IsWindowFullScreen;

			setWindowFullScreenOrWindowed( ::g_pGLFWWindow, ::g_IsWindowFullScreen );

		}//if ( action == GLFW_PRESS )
	}//if ( isAltKeyDown(...


	//cGameObject* pLeftTeapot = findObjectByFriendlyName(LEFTTEAPOTNAME, ::g_vecGameObjects);

	cGameObject* pSophie = findObjectByFriendlyName( "Sophie", ::g_vecGameObjects );
	cGameObject* pEve = findObjectByFriendlyName( "Eve", ::g_vecGameObjects);


	const float CAMERASPEED = 10.0f;

	const float CAM_ACCELL_THRUST = 100.0f;
	cGameObject* pTempGO = new cGameObject();

	//animation sequences
	cAnimationState::sStateDetails walkAnimation;
	walkAnimation.name = "assets/modelsFBX/Eve_Walking.fbx";
//	walkAnimation.totalTime = pTempGO->pSimpleSkinnedMesh->FindAnimationTotalTime(walkAnimation.name);
	walkAnimation.totalTime = 0.7f;
	walkAnimation.frameStepTime = 0.02f;

	cAnimationState::sStateDetails runAnimation;
	runAnimation.name = "assets/modelsFBX/Eve_Run.fbx";
	//rightAnimation.totalTime = pTempGO->pSimpleSkinnedMesh->FindAnimationTotalTime(rightAnimation.name);
	runAnimation.totalTime = 1.0f;
	runAnimation.frameStepTime = 0.03f;

	cAnimationState::sStateDetails jumpAnimation;
	jumpAnimation.name = "assets/modelsFBX/Eve_Jump.fbx";
	//jumpAnimation.totalTime = pTempGO->pSimpleSkinnedMesh->FindAnimationTotalTime(jumpAnimation.name);
	jumpAnimation.totalTime = 1.0f;
	jumpAnimation.frameStepTime = 0.01f;

	cAnimationState::sStateDetails actionAnimation;
	actionAnimation.name = "assets/modelsFBX/Eve_Dig_Plant.fbx";
	//jumpAnimation.totalTime = pTempGO->pSimpleSkinnedMesh->FindAnimationTotalTime(jumpAnimation.name);
	actionAnimation.totalTime = 4.5f;
	actionAnimation.frameStepTime = 0.01f;


	if ( isShiftKeyDown(mods, true)  )
	{
		switch (key)
		{
		case GLFW_KEY_1:
			::g_pLightManager->vecLights[0].attenuation.y *= 0.99f;	// less 1%
			break;
		case GLFW_KEY_2:
			::g_pLightManager->vecLights[0].attenuation.y *= 1.01f; // more 1%
			if (::g_pLightManager->vecLights[0].attenuation.y <= 0.0f)
			{
				::g_pLightManager->vecLights[0].attenuation.y = 0.001f;	// Some really tiny value
			}
			break;
		case GLFW_KEY_3:	// Quad
			::g_pLightManager->vecLights[0].attenuation.z *= 0.99f;	// less 1%
			break;
		case GLFW_KEY_4:	//  Quad
			::g_pLightManager->vecLights[0].attenuation.z *= 1.01f; // more 1%
			if (::g_pLightManager->vecLights[0].attenuation.z <= 0.0f)
			{
				::g_pLightManager->vecLights[0].attenuation.z = 0.001f;	// Some really tiny value
			}
			break;

		// Lights
		// CAMERA and lighting
		case GLFW_KEY_A:	// Left
							/*if ( action == GLFW_PRESS )
							{*/
			if (currentCharacter == SOPHIE)
			{
				glm::vec3 tempPosition = pSophie->m_PhysicalProps.position;
				tempPosition.x -= 3.0f;
				if (g_pPhysicsWorld->isColliding(g_modelAABBBroadPhase, g_vecGameObjects, g_pVAOManager, tempPosition))
					break;
				pSophie->m_PhysicalProps.position.x -= 3.0f;
				pSophie->m_PhysicalProps.directedVelocity.x = -3.0f;
				pSophie->pAniState->vecAnimationQueue.push_back(runAnimation);
			}
			else if (currentCharacter == EVE)
			{
				glm::vec3 tempPosition = pEve->m_PhysicalProps.position;
				tempPosition.x -= 3.0f;
				if (g_pPhysicsWorld->isColliding(g_modelAABBBroadPhase, g_vecGameObjects, g_pVAOManager, tempPosition))
					break;

				pEve->m_PhysicalProps.position.x -= 3.0f;
				pEve->m_PhysicalProps.directedVelocity.x = -3.0f;
				pEve->pAniState->vecAnimationQueue.push_back(runAnimation);
			}
			if (action == GLFW_RELEASE)
			{
				pSophie->m_PhysicalProps.directedVelocity.x = 0.0f;
				pSophie->pAniState->vecAnimationQueue.clear();
				pEve->m_PhysicalProps.directedVelocity.x = 0.0f;
				pEve->pAniState->vecAnimationQueue.clear();
			}

			//}
			break;
		case GLFW_KEY_D:	// Right
							/*if ( action == GLFW_PRESS )
							*/
			if (currentCharacter == SOPHIE)
			{
				glm::vec3 tempPosition = pSophie->m_PhysicalProps.position;
				tempPosition.x += 3.0f;
				if (g_pPhysicsWorld->isColliding(g_modelAABBBroadPhase, g_vecGameObjects, g_pVAOManager, tempPosition))
					break;
				pSophie->m_PhysicalProps.position.x += 3.0f;
				pSophie->m_PhysicalProps.directedVelocity.x = +3.0f;
				pSophie->pAniState->vecAnimationQueue.push_back(runAnimation);
			}
			else if (currentCharacter == EVE)
			{
				glm::vec3 tempPosition = pEve->m_PhysicalProps.position;
				tempPosition.x += 3.0f;
				if (g_pPhysicsWorld->isColliding(g_modelAABBBroadPhase, g_vecGameObjects, g_pVAOManager, tempPosition))
					break;
				pEve->m_PhysicalProps.position.x += 3.0f;
				pEve->m_PhysicalProps.directedVelocity.x = +3.0f;
				pEve->pAniState->vecAnimationQueue.push_back(runAnimation);
			}
			if (action == GLFW_RELEASE)
			{
				pSophie->m_PhysicalProps.directedVelocity.x = 0.0f;
				pSophie->pAniState->vecAnimationQueue.clear();
				pEve->m_PhysicalProps.directedVelocity.x = 0.0f;
				pEve->pAniState->vecAnimationQueue.clear();
			}
			break;
		case GLFW_KEY_W:	// Back;				
			if (currentCharacter == SOPHIE)
			{
				glm::vec3 tempPosition = pSophie->m_PhysicalProps.position;
				tempPosition.z -= 3.0f;
				if (g_pPhysicsWorld->isColliding(g_modelAABBBroadPhase, g_vecGameObjects, g_pVAOManager, tempPosition))
					break;
				pSophie->m_PhysicalProps.position.z -= 3.0f;
				pSophie->m_PhysicalProps.directedVelocity.z = -3.0f;
				pSophie->pAniState->vecAnimationQueue.push_back(runAnimation);
			}
			else if (currentCharacter == EVE)
			{
				glm::vec3 tempPosition = pEve->m_PhysicalProps.position;
				tempPosition.z -= 3.0f;
				if (g_pPhysicsWorld->isColliding(g_modelAABBBroadPhase, g_vecGameObjects, g_pVAOManager, tempPosition))
					break;
				pEve->m_PhysicalProps.position.z -= 3.0f;
				pEve->m_PhysicalProps.directedVelocity.z = -3.0f;
				pEve->pAniState->vecAnimationQueue.push_back(runAnimation);
			}
			if (action == GLFW_RELEASE)
			{
				pSophie->m_PhysicalProps.directedVelocity.x = 0.0f;
				pSophie->pAniState->vecAnimationQueue.clear();
				pEve->m_PhysicalProps.directedVelocity.x = 0.0f;
				pEve->pAniState->vecAnimationQueue.clear();
			}
			break;
		case GLFW_KEY_S:	// Forward:
			if (currentCharacter == SOPHIE)
			{
				glm::vec3 tempPosition = pSophie->m_PhysicalProps.position;
				tempPosition.z += 3.0f;
				if (g_pPhysicsWorld->isColliding(g_modelAABBBroadPhase, g_vecGameObjects, g_pVAOManager, tempPosition))
					break;
				pSophie->m_PhysicalProps.position.z += 3.0f;
				pSophie->m_PhysicalProps.directedVelocity.z = +3.0f;
				pSophie->pAniState->vecAnimationQueue.push_back(runAnimation);
			}
			else if (currentCharacter == EVE)
			{
				glm::vec3 tempPosition = pEve->m_PhysicalProps.position;
				tempPosition.z += 3.0f;
				if (g_pPhysicsWorld->isColliding(g_modelAABBBroadPhase, g_vecGameObjects, g_pVAOManager, tempPosition))
					break;
				pEve->m_PhysicalProps.position.z += 3.0f;
				pEve->m_PhysicalProps.directedVelocity.z = +3.0f;
				pEve->pAniState->vecAnimationQueue.push_back(runAnimation);
			}
			if (action == GLFW_RELEASE)
			{
				pSophie->m_PhysicalProps.directedVelocity.x = 0.0f;
				pSophie->pAniState->vecAnimationQueue.clear();
				pEve->m_PhysicalProps.directedVelocity.x = 0.0f;
				pEve->pAniState->vecAnimationQueue.clear();
			}
		case GLFW_KEY_Q:		// "Down" (along y axis)
			::g_pLightManager->vecLights[0].position.y -= CAMERASPEED;
			break;
		case GLFW_KEY_E:		// "Up" (along y axis)
			::g_pLightManager->vecLights[0].position.y += CAMERASPEED;
			break;
		case GLFW_KEY_G:
			{
				float angle = ::g_pLightManager->vecLights[0].getLightParamSpotPrenumAngleOuter();
				::g_pLightManager->vecLights[0].setLightParamSpotPrenumAngleOuter(angle + 0.01f);
			}
			break;
		case GLFW_KEY_H:
			{
				float angle = ::g_pLightManager->vecLights[0].getLightParamSpotPrenumAngleOuter();
				::g_pLightManager->vecLights[0].setLightParamSpotPrenumAngleOuter(angle - 0.01f);
			}
			break;
		};//switch (key)
	}//if ( isShiftKeyDown(mods, true) )


	if (areAllModifierKeysUp(mods)  )
	{
		//	const float CAMERASPEED = 100.0f;
		switch (key)
		{
		case GLFW_KEY_1:
			::g_pTheCamera->setCameraMode(cCamera::MODE_FLY_USING_LOOK_AT);
			std::cout << "Camera now in " << ::g_pTheCamera->getCameraModeString() << std::endl;;
			break;
		case GLFW_KEY_2:
			::g_pTheCamera->setCameraMode(cCamera::MODE_FOLLOW);
			std::cout << "Camera now in " << ::g_pTheCamera->getCameraModeString() << std::endl;;
			break;
		case GLFW_KEY_3:
			::g_pTheCamera->setCameraMode(cCamera::MODE_MANUAL);
			std::cout << "Camera now in " << ::g_pTheCamera->getCameraModeString() << std::endl;;
			break;

		// CAMERA and lighting
		case GLFW_KEY_J:		// Left
//			::g_pTheCamera->FlyCamLA->moveRight(-CAMERASPEED);	// strafe
			::g_pTheCamera->FlyCamLA->move(glm::vec3(-CAMERASPEED, 0.0f, 0.0f));
			break;
		case GLFW_KEY_L:		// Right
//			::g_pTheCamera->FlyCamLA->moveRight(+CAMERASPEED);	// strafe
			::g_pTheCamera->FlyCamLA->move(glm::vec3(+CAMERASPEED, 0.0f, 0.0f));
			break;
		case GLFW_KEY_I:		// Forward (along z)
//			::g_pTheCamera->FlyCamLA->moveForward(+CAMERASPEED);
			::g_pTheCamera->FlyCamLA->move(glm::vec3(0.0f, 0.0f, -CAMERASPEED));
			break;
		case GLFW_KEY_K:		// Backwards (along z)
//			::g_pTheCamera->FlyCamLA->moveForward(-CAMERASPEED);		// Backwards
			::g_pTheCamera->FlyCamLA->move(glm::vec3(0.0f, 0.0f, +CAMERASPEED));		// Backwards
			break;
		case GLFW_KEY_U:		// "Down" (along y axis)
//			::g_pTheCamera->FlyCamLA->moveUp(-CAMERASPEED);	// "Z minus 10000 meters, Mr. Sulu!"
			::g_pTheCamera->FlyCamLA->move(glm::vec3(0.0f, -CAMERASPEED, 0.0f));		// Backwards
			break;
		case GLFW_KEY_O:		// "Up" (along y axis)
//			::g_pTheCamera->FlyCamLA->moveUp(+CAMERASPEED);	// "Z minus 10000 meters, Mr. Sulu!"
			::g_pTheCamera->FlyCamLA->move(glm::vec3(0.0f, +CAMERASPEED, 0.0f));		// Backwards
			break;

		case GLFW_KEY_5:
			break;
		case GLFW_KEY_6:
			break;
		case GLFW_KEY_7:
			break;
		case GLFW_KEY_8:
			break;
		case GLFW_KEY_9:
			break;
		case GLFW_KEY_0:
			break;
			
		case GLFW_KEY_Q:
			if (action == GLFW_PRESS)
			{
				if (currentCharacter == SOPHIE)
				{
					currentCharacter = EVE;
					std::cout << "EVE" << std::endl;
				}
				else if (currentCharacter == EVE)
				{
					currentCharacter = SOPHIE;
					std::cout << "SOPHIE" << std::endl;
				}
				else
				{
					//set default to SOPHIE
					currentCharacter = SOPHIE;
				}
			}
			break;

		case GLFW_KEY_A:	// Left
			/*if ( action == GLFW_PRESS )
			{*/
			if (currentCharacter == SOPHIE)
			{
				glm::vec3 tempPosition = pSophie->m_PhysicalProps.position;
				tempPosition.x -= 1.5f;
				if (g_pPhysicsWorld->isColliding(g_modelAABBBroadPhase, g_vecGameObjects, g_pVAOManager, tempPosition))
					break;
				pSophie->m_PhysicalProps.position.x -= 1.5f;
				pSophie->m_PhysicalProps.directedVelocity.x = -1.5f;
				pSophie->pAniState->vecAnimationQueue.push_back(walkAnimation);
			}
			else if (currentCharacter == EVE)
			{
				glm::vec3 tempPosition = pEve->m_PhysicalProps.position;
				tempPosition.x -= 1.5f;
				if (g_pPhysicsWorld->isColliding(g_modelAABBBroadPhase, g_vecGameObjects, g_pVAOManager, tempPosition))
					break;
				pEve->m_PhysicalProps.position.x -= 1.5f;
				pEve->m_PhysicalProps.directedVelocity.x = -1.5f;
				pEve->pAniState->vecAnimationQueue.push_back(walkAnimation);
			}
			if (action == GLFW_RELEASE)
			{
				pSophie->m_PhysicalProps.directedVelocity.x = 0.0f;
				pSophie->pAniState->vecAnimationQueue.clear();
				pEve->m_PhysicalProps.directedVelocity.x = 0.0f;
				pEve->pAniState->vecAnimationQueue.clear();
			}
			//}
			break;
		case GLFW_KEY_D:	// Right
			/*if ( action == GLFW_PRESS )
			*/
			if (currentCharacter == SOPHIE)
			{
				glm::vec3 tempPosition = pSophie->m_PhysicalProps.position;
				tempPosition.x += 1.5f;
				if (g_pPhysicsWorld->isColliding(g_modelAABBBroadPhase, g_vecGameObjects, g_pVAOManager, tempPosition))
					break;
				pSophie->m_PhysicalProps.position.x += 1.5f;
				pSophie->m_PhysicalProps.directedVelocity.x = +1.5f;
				pSophie->pAniState->vecAnimationQueue.push_back(walkAnimation);
			}
			else if (currentCharacter == EVE)
			{
				glm::vec3 tempPosition = pEve->m_PhysicalProps.position;
				tempPosition.x += 1.5f;
				if (g_pPhysicsWorld->isColliding(g_modelAABBBroadPhase, g_vecGameObjects, g_pVAOManager, tempPosition))
					break;
				pEve->m_PhysicalProps.position.x += 1.5f;
				pEve->m_PhysicalProps.directedVelocity.x = +1.5f;
				pEve->pAniState->vecAnimationQueue.push_back(walkAnimation);
			}
			if (action == GLFW_RELEASE)
			{
				pSophie->m_PhysicalProps.directedVelocity.x = 0.0f;
				pSophie->pAniState->vecAnimationQueue.clear();
				pEve->m_PhysicalProps.directedVelocity.x = 0.0f;
				pEve->pAniState->vecAnimationQueue.clear();
			}
			break;
		case GLFW_KEY_W:	// Back;
			//pSophie->m_PhysicalProps.position.z -= 5.0f;
//			pSophie->pAniState->defaultAnimation.name = "assets/modelsFBX/RPG-Character_Unarmed-Roll-Backward(FBX2013).fbx";
			if (currentCharacter == SOPHIE)
			{
				glm::vec3 tempPosition = pSophie->m_PhysicalProps.position;
				tempPosition.z -= 1.5f;
				if (g_pPhysicsWorld->isColliding(g_modelAABBBroadPhase, g_vecGameObjects, g_pVAOManager, tempPosition))
					break;

				pSophie->m_PhysicalProps.position.z -= 1.5f;
				pSophie->m_PhysicalProps.directedVelocity.z = -1.5f;
				pSophie->pAniState->vecAnimationQueue.push_back(walkAnimation);
			}
			else if (currentCharacter == EVE)
			{
				glm::vec3 tempPosition = pEve->m_PhysicalProps.position;
				tempPosition.z -= 1.5f;
				if (g_pPhysicsWorld->isColliding(g_modelAABBBroadPhase, g_vecGameObjects, g_pVAOManager, tempPosition))
					break;
				pEve->m_PhysicalProps.position.z -= 1.5f;
				pEve->m_PhysicalProps.directedVelocity.z = -1.5f;
				pEve->pAniState->vecAnimationQueue.push_back(walkAnimation);
			}
			if (action == GLFW_RELEASE)
			{
				pSophie->m_PhysicalProps.directedVelocity.x = 0.0f;
				pSophie->pAniState->vecAnimationQueue.clear();
				pEve->m_PhysicalProps.directedVelocity.x = 0.0f;
				pEve->pAniState->vecAnimationQueue.clear();
			}
			break;
		case GLFW_KEY_S:	// Forward:
			if (currentCharacter == SOPHIE)
			{
				glm::vec3 tempPosition = pSophie->m_PhysicalProps.position;
				tempPosition.z += 1.5f;
				if (g_pPhysicsWorld->isColliding(g_modelAABBBroadPhase, g_vecGameObjects, g_pVAOManager, tempPosition))
					break;
				pSophie->m_PhysicalProps.position.z += 1.5f;
				pSophie->m_PhysicalProps.directedVelocity.z = +1.5f;
				pSophie->pAniState->vecAnimationQueue.push_back(walkAnimation);
			}
			else if (currentCharacter == EVE)
			{
				glm::vec3 tempPosition = pEve->m_PhysicalProps.position;
				tempPosition.z += 1.5f;
				if (g_pPhysicsWorld->isColliding(g_modelAABBBroadPhase, g_vecGameObjects, g_pVAOManager, tempPosition))
					break;
				pEve->m_PhysicalProps.position.z += 1.5f;
				pEve->m_PhysicalProps.directedVelocity.z = +1.5f;
				pEve->pAniState->vecAnimationQueue.push_back(walkAnimation);
			}
			if (action == GLFW_RELEASE)
			{
				pSophie->m_PhysicalProps.directedVelocity.x = 0.0f;
				pSophie->pAniState->vecAnimationQueue.clear();
				pEve->m_PhysicalProps.directedVelocity.x = 0.0f;
				pEve->pAniState->vecAnimationQueue.clear();
			}
			break;
		case GLFW_KEY_V:	// action
			if (action == GLFW_PRESS)
			{
				if (currentCharacter == SOPHIE)
				{
					pSophie->m_PhysicalProps.directedVelocity.z = +1.5f;
					pSophie->pAniState->vecAnimationQueue.push_back(actionAnimation);
				}
				else if (currentCharacter == EVE)
				{
					pEve->m_PhysicalProps.directedVelocity.z = +1.5f;
					pEve->pAniState->vecAnimationQueue.push_back(actionAnimation);
				}
			}
			break;
		case GLFW_KEY_SPACE:	// Jump
			if ( action == GLFW_PRESS)
			{
				/*if (GLFW_KEY_A == GLFW_PRESS)
				{*/
					//pSophie->pAniState->defaultAnimation.name = "assets/modelsFBX/RPG-Character_Unarmed-Jump(FBX2013).FBX";
					pSophie->pAniState->vecAnimationQueue.push_back(jumpAnimation);
				//}
			}

			break;


		}//switch
	}//if (areAllModifierKeysUp(mods))

	// HACK: print output to the console
//	std::cout << "Light[0] linear atten: "
//		<< ::g_pLightManager->vecLights[0].attenuation.y << ", "
//		<< ::g_pLightManager->vecLights[0].attenuation.z << std::endl;
	return;
}



// Helper functions
bool isShiftKeyDown( int mods, bool bByItself /*=true*/ )
{
	if ( bByItself )
	{	// shift by itself?
		if ( mods == GLFW_MOD_SHIFT )	{ return true; }
		else							{ return false; }
	}
	else
	{	// shift with anything else, too
		if ( ( mods & GLFW_MOD_SHIFT ) == GLFW_MOD_SHIFT )	{	return true;	}
		else												{	return false;	}
	}
	// Shouldn't never get here, so return false? I guess?
	return false;
}

bool isCtrlKeyDown( int mods, bool bByItself /*=true*/ )
{
	if ( bByItself )
	{	// shift by itself?
		if ( mods == GLFW_MOD_CONTROL )	{ return true; }
		else							{ return false; }
	}
	else
	{	// shift with anything else, too
		if ( ( mods & GLFW_MOD_CONTROL) == GLFW_MOD_CONTROL)	{	return true;	}
		else												{	return false;	}
	}
	// Shouldn't never get here, so return false? I guess?
	return false;
}

bool isAltKeyDown( int mods, bool bByItself /*=true*/ )
{
	if (bByItself)
	{	// shift by itself?
		if (mods == GLFW_MOD_ALT) { return true; }
		else { return false; }
	}
	else
	{	// shift with anything else, too
		if ((mods & GLFW_MOD_ALT) == GLFW_MOD_ALT) { return true; }
		else { return false; }
	}
	// Shouldn't never get here, so return false? I guess?
	return false;
}

bool areAllModifierKeysUp(int mods)
{
	if ( isShiftKeyDown(mods) )	{	return false;	}
	if ( isCtrlKeyDown(mods) ) 	{	return false;	}
	if ( isAltKeyDown(mods) )	{	return false;	}

	// All of them are up
	return true;
}//areAllModifierKeysUp()

bool areAnyModifierKeysDown(int mods)
{
	if ( isShiftKeyDown(mods) )		{	return true;	}
	if ( isCtrlKeyDown(mods) )		{	return true;	}
	if ( isAltKeyDown(mods) )		{	return true;	}
	// None of them are down
	return false;
}