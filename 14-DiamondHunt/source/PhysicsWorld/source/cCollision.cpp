#include "cCollision.h"
#include "nConvert.h"


namespace libPhysics {
	cCollision::cCollision()
	{
	}


	cCollision::~cCollision()
	{
	}

	cCollisionBody* cCollision::getKinematicBody() {
		if (A->getPhyObjType() == PHY_OBJ_KINEMATIC)
			return A;
		else if (B->getPhyObjType() == PHY_OBJ_KINEMATIC)
			return B;
		else
			return NULL;
	
	};
	cCollisionBody* cCollision::getStaticBody() {
		if (A->getPhyObjType() == PHY_OBJ_STATIC)
			return A;
		else if (B->getPhyObjType() == PHY_OBJ_STATIC)
			return B;
		else
			return NULL;
	};

	cCollisionBody* cCollision::getOtherKinematicBody(cCollisionBody* otherBody) {
		if (A->getObjectId()== otherBody->getObjectId())
			return B;
		else 
			return A;
	};

	btVector3 cCollision::getNewPosition() {
		btVector3 objectFront;
		btVector3 playerPos;
		btVector3 contactPoint;
		glm::vec3 orientation;
		glm::vec3 oldPosition;
		if (A->getPhyObjType() == PHY_OBJ_KINEMATIC) {
			playerPos = ToBullet(A->getPosition());
			objectFront = ToBullet(A->getObjectFront());
			orientation = A->getOrientation();
			oldPosition = A->getOldPosition();
			contactPoint = this->contactPtB;
		}
		else if (B->getPhyObjType() == PHY_OBJ_KINEMATIC) {
			playerPos = ToBullet(B->getPosition());
			objectFront = ToBullet(B->getObjectFront());
			orientation = B->getOrientation();
			oldPosition = B->getOldPosition();
			contactPoint = this->contactPtA;
		}
		contactPoint.setY(0.0f);

		/*float cosTheta = glm::dot(ToGlm(objectFront), glm::normalize(ToGlm(contactPoint)));
		float angle = glm::degrees(acos(cosTheta));
		angle = 90.0f - angle;
		orientation.y += glm::radians(angle);
		glm::vec3 responseFront = glm::vec3(0.0f);
		responseFront.x = sin(orientation.y);
		responseFront.z = cos(orientation.y);*/

		float distance = glm::distance(ToGlm(playerPos), oldPosition);
		glm::vec3 dispDir = ToGlm(playerPos) - oldPosition;
		dispDir = glm::normalize(dispDir);
		btVector3 disp = (distance * 0.95) * ToBullet(dispDir);
		playerPos = ToBullet(oldPosition) + disp;
		
		return playerPos;
	};
};
