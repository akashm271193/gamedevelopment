#pragma once
#include "globalGameStuff.h"
#include "cGLFWCalls.h"
enum eMenuObjType {
	LOAD = 0,
	SAVE = 1,
	CONTINUE = 2,
	EXIT = 3,
	GAME_STATE = 4,
	BACK = 5,
	DATA =6,
	UNKNOWN_OBJ = 7
};

class cMenuObj
{
public:
	cMenuObj();
	~cMenuObj();
	cMenuObj(eMenuObjType objType);
	glm::vec3 position;
	glm::vec3 scale;
	std::string objText;
	void doAction(int &activeMenuIndex, eMenuObjType &action);
	int prevMenuIndex;
	std::string gameStateFile;
	int menuObjId;


private:
	eMenuObjType menuObjType;
};