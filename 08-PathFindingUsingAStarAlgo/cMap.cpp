#include "cMap.h"

/*
	Parameterless Constructor
*/
cMap::cMap()
{
	this->lastShortestPath = new cPath();
}

/*
	Parameterless Destructor
*/
cMap::~cMap()
{
}

/*
	Function to get all the cells of the map read from mapreader
	and stored in vector of cMapCell.
*/
void cMap::getMapCells(std::vector<cMapCell*> &mapCells) {
	mapCells = this->vecCells;
}


/*
	Function to set the values in the vector of cMapCell.
*/
void cMap::setMapCells(std::vector<cMapCell*> mapCells) {
	this->vecCells = mapCells;
}

/*
	Function to set number of rows in the map.
*/
void cMap::setMapRows(int rows) {
	this->mapRows = rows;

};

/*
	Function to set number of columns in the map.
*/
void cMap::setMapColumns(int columns) {
	this->mapColumns = columns;

};


/*
	Function to get number of rows in the map.
*/
int  cMap::getMapRows() {
	return this->mapRows;

};

/*
	Function to get number of columns in the map.
*/
int  cMap::getMapColumns() {
	return this->mapColumns;

};

/*
	Function to set the starting point in the map.
*/
void cMap::setMapStartPoint(std::string startPoint) {
	this->mapStartPoint = startPoint;

};

/*
	Function to set the end point in the map.
*/
void cMap::setMapEndPoint(std::string endPoint) {
	this->mapEndPoint = endPoint;

};

/*
	Function to setup the end point in the map.
*/
void cMap::setupStartAndEndPoints() {
	this->getCellIndexForPoints(this->startCellIndex,this->mapStartPoint);
	this->getCellIndexForPoints(this->endCellIndex, this->mapEndPoint);
};


/*
	Function to split the string format of co-ordinates in the lastShortestPath		
	variable.Every value is in the format (x,z). Based on the points , we calulate
	their index and find the relevant position for our player to be.
*/
void cMap::getCellIndexForPoints(int &pointIndex, std::string point) {
	int *vecValues = new int[2];
	point = point.substr(1, point.length()-2);
	std::string delimiter = ",";

	size_t last = 0;
	size_t next = 0;
	int index = 0;
	std::string token;
	while (index<2)
	{
		next = point.find(delimiter, last);
		token = point.substr(last, next - last);
		vecValues[index] = (std::stoi(token));
		last = next + 1;
		index++;
	}

	pointIndex = (vecValues[0] * this->mapColumns) + vecValues[1];
}


/*
	Function to display the map read with start and end points as 'S' and 'E'
*/
void cMap::showMap() {
	std::cout << "\n Map looks like : \n";
	for (int i = 0; i < this->mapRows; i++) {
		for (int j = 0; j < this->mapColumns; j++) {
			int indexPos = (i * this->mapColumns) + j;
			if(indexPos  == this->startCellIndex)
				std::cout << "S";
			else if(indexPos == this->endCellIndex)
				std::cout << "E";
			else
				std::cout << (this->vecCells[indexPos]->isWalkable) ? 1 : 0 ;
			std::cout << " ";
		}
		std::cout << "\n";
	}
}

/*
	Control function to find the shortest path
*/
void cMap::findShortestPathTillEnd() {
	std::vector<cMapCell*> openCells;
	std::vector<cMapCell*> closedCells;

	openCells.push_back(vecCells[startCellIndex]);
	this->parseOpenCells(openCells, closedCells);
	this->getShortestPath();
};


/*
	Implementation of A* algorithm (Part 1)
	Takes in 2 lists - open and close. 
	Then calulates the adjacent F's (Part 2  in cMesh) and then moves onto the smallest
	F and starts iterating again. 
*/
void cMap::parseOpenCells(std::vector<cMapCell*> &openCells, std::vector<cMapCell*> &closedCells){
	
	std::vector<cMapCell*>::iterator custIterator;
	custIterator = find(openCells.begin(), openCells.end(), this->vecCells[this->endCellIndex]);
	if (custIterator != openCells.end())
		return;

	int minF = INT_MAX;
	cMapCell* curMapCell = new cMapCell();
	int currMapId = -1;
	for (int i = 0; i < openCells.size(); i++) {
		if (openCells[i]->f < minF) {
			curMapCell = openCells[i];
			minF = openCells[i]->f;
			currMapId = i;
		}
	}
	curMapCell->calculateFForAdjacentCells(this->vecCells, endCellIndex, openCells, closedCells,this->mapColumns, this->mapRows);
	closedCells.push_back(curMapCell);
	openCells.erase(openCells.begin() + currMapId);
	this->parseOpenCells(openCells, closedCells);
};


/*
	Function to get the shortest path using the parents of each cell traversed
	from the end point of the maze to the start point.
*/
void cMap::getShortestPath() {
	cMapCell* currCell = vecCells[endCellIndex];
	std::string cellPos = "(" + std::to_string(currCell->rowId) + "," + std::to_string(currCell->columnId)+")";
	cPath* shortestPath = new cPath(cellPos);
	this->lastShortestPath->pathNodes.clear();
	while (currCell != vecCells[startCellIndex]) {
		int parentId = currCell->parentId;
		currCell = vecCells[parentId];
		cellPos = "(" + std::to_string(currCell->rowId) + "," + std::to_string(currCell->columnId) + ")";
		shortestPath->pathNodes.push_back(cellPos);
		shortestPath->pathLength += 1;
	}
	cellPos = "(" + std::to_string(currCell->rowId) + "," + std::to_string(currCell->columnId) + ")";
	
	std::cout << "\nShortest Path : ";
	this->lastShortestPath->pathLength = shortestPath->pathLength;

	int counter = 0;
	std::vector<std::string>::reverse_iterator rit = shortestPath->pathNodes.rbegin();
	for (; rit != shortestPath->pathNodes.rend(); ++rit){
		this->lastShortestPath->pathNodes.push_back(*rit);
		std::cout << *rit;
		if (counter < shortestPath->pathNodes.size() - 1)
			std::cout << " -> ";
		counter++;
	}
	std::cout << '\n';

}