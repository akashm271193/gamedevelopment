#ifndef _cVertex_xyz_rgb_HG_
#define _cVertex_xyz_rgb_HG_

class cVertex_xyz_rgb
{
public:
	cVertex_xyz_rgb();	// Constructor
	~cVertex_xyz_rgb();	// Destructor
	float x;		
	float y;
	float z;
	float r, g, b;
};

#endif
