#include "SceneLoader.h"
#include "cAnimationState.h"

void loadModelsIntoScene(){
	// load models
	// -----------
	//PLAYER CHARACTER
	//SKINNED MESH
	cModel* playerModel = new cModel("../extern/assets/modelsFBX/fbx/Look_Around.fbx", false, true);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("player", playerModel));
	playerModel->loadAnimation("../extern/assets/modelsFBX/fbx/Eve_Walking.fbx");
	playerModel->loadAnimation("../extern/assets/modelsFBX/fbx/EveTurn.fbx");


	//SKELETON
	//SKINNED MESH
	cModel* skeletonModel = new cModel("../extern/assets/modelsFBX/fbx/Skeleton_Walk.fbx", false, true);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("skeleton", skeletonModel));
	skeletonModel->loadAnimation("../extern/assets/modelsFBX/fbx/Skeleton_Walk.fbx");
	skeletonModel->loadAnimation("../extern/assets/modelsFBX/fbx/Skeleton_Turn.fbx");

	/*cModel* mageModel = new cModel("../extern/assets/modelsFBX/fbx/Mage_walk.fbx", false, true);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("mage", mageModel));
	mageModel->loadAnimation("../extern/assets/modelsFBX/fbx/Mage_Turn180.fbx");
	mageModel->loadAnimation("../extern/assets/modelsFBX/fbx/Mage_walk.fbx");

	cModel* nwModel = new cModel("../extern/assets/modelsFBX/fbx/Nightshade_walk.fbx", false, true);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("nw", nwModel));
	nwModel->loadAnimation("../extern/assets/modelsFBX/fbx/Nightshade_Turn180.fbx");
	nwModel->loadAnimation("../extern/assets/modelsFBX/fbx/Nightshade_walk.fbx");*/

	
	cModel* floorModel = new cModel("../extern/assets/models/floor.obj", false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("floor", floorModel));

	cModel* wallModel = new cModel("../extern/assets/models/wall.obj", false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("wall", wallModel));

	cModel* sphereModel = new cModel("../extern/assets/models/sphere.ply", true);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("sphere", sphereModel));

	cModel* fireModel = new cModel("../extern/assets/models/sphere_uv.obj", false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("fire", fireModel));

	cModel* boundingBoxModel = new cModel("../extern/assets/models/wall.obj", true);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("bbox", boundingBoxModel));

	cModel* floorObstacleModel = new cModel("../extern/assets/models/floor.obj", true, false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("floorOb", floorObstacleModel));

	cModel* diamondModel = new cModel("../extern/assets/models/diamond.obj", true, false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("diamondOb", diamondModel));

	std::vector<cMesh*> modelMeshes = floorModel->getAllMeshes();
	for (int i = 0; i < modelMeshes.size(); i++) {
		floorModel->addSeparateTexturesToModel("floor.jpg", "../extern/assets/textures", "texture_diffuse", modelMeshes[i]);
	}

	modelMeshes.clear();

	modelMeshes = wallModel->getAllMeshes();
	for (int i = 0; i < modelMeshes.size(); i++) {
		wallModel->addSeparateTexturesToModel("wall.bmp", "../extern/assets/textures", "texture_diffuse", modelMeshes[i]);
	}

	modelMeshes.clear();

	modelMeshes = fireModel->getAllMeshes();
	for (int i = 0; i < modelMeshes.size(); i++) {
		fireModel->addSeparateTexturesToModel("fire.png", "../extern/assets/textures", "texture_diffuse", modelMeshes[i]);
	}

	fireModel->blurIndices();
	modelMeshes.clear();
}


void generateGameObjects(rapidjson::Document &doc){

	std::vector<glm::vec3> respawnPoints;

	std::vector<cMapCell*> mapCells;
	ourMap->getMapCells(mapCells);

	cModel* floorModel = gMapLoadedModels["floor"];

	ourMap->calculateMapExtents(floorModel->maxExtentXYZ);

	int pathIndex = 0;
	glm::vec3 prevCellPos = glm::vec3(-((floorModel->maxExtentXYZ.x * ourMap->mapScale) / 2), 0.0f, (floorModel->maxExtentXYZ.z * ourMap->mapScale )/ 2);
	int rowId = 0, colId = 0;
	ourMap->setWindowMapStartPoint(glm::vec3(0.0f, 0.0f, 0.0f));
	//----------------
	for (int i = 0; i < mapCells.size(); i++) {
		cGameObject* tmpGameObject = new cGameObject();
		glm::vec3 newPos = glm::vec3(1.0f, 1.0f, prevCellPos.z);
		if (mapCells[i]->isWalkable) {
			tmpGameObject->model = gMapLoadedModels["floor"];
			tmpGameObject->colour = glm::vec3(1.0f, 0.0f, 0.0f);
			newPos.y = 0.0f;
				
		}

		else {
			tmpGameObject->model = gMapLoadedModels["wall"];
			tmpGameObject->colour = glm::vec3(1.0f, 1.0f, 1.0f);
			newPos.y = (tmpGameObject->model->maxExtentXYZ.y / 2) * ourMap->mapScale;
		}
		tmpGameObject->objectId = gGameObjects.size();
		if (mapCells[i]->rowId > rowId) {
			newPos.z = prevCellPos.z + (tmpGameObject->model->maxExtentXYZ.x * ourMap->mapScale);
			prevCellPos.x = -(tmpGameObject->model->maxExtentXYZ.x / 2 * ourMap->mapScale);
			rowId = mapCells[i]->rowId;
		}
		newPos.x = prevCellPos.x + (tmpGameObject->model->maxExtentXYZ.x * ourMap->mapScale);
		prevCellPos = newPos;
		tmpGameObject->position = newPos;
		tmpGameObject->typeOfObject = libPhysics::PLANE;
		tmpGameObject->scale = glm::vec3(ourMap->mapScale); 
		if (tmpGameObject->toGenerateBouundingBoxForCell(mapCells[i])) {
				tmpGameObject->createPhysicsWorldObject();
				tmpGameObject->hasBoundingBoxOrSphere = true;

		}

		tmpGameObject->mapCellId = i;
		if (mapCells[i]->hasObstacle)
			addObstacleAsGameObject(mapCells[i], tmpGameObject,respawnPoints,doc);

		tmpGameObject->objectId = gGameObjects.size();
		gGameObjects.push_back(tmpGameObject);
	}


	{
		cPlayerGameObject* tmpGameObject = new cPlayerGameObject();
		tmpGameObject->model = gMapLoadedModels["player"];
		tmpGameObject->objectId = gGameObjects.size();
		playerGameObjId = tmpGameObject->objectId;
		tmpGameObject->scale = glm::vec3(0.01f);
		tmpGameObject->ownAxisOrientation = glm::vec3(0.0f, glm::radians(90.0f), 0.0f);
		tmpGameObject->typeOfObject = libPhysics::OBJ_PLAYER;
		const rapidjson::Value &pos = doc["Player"]["position"];
		tmpGameObject->position = glm::vec3(pos[0].GetFloat(), pos[1].GetFloat(), pos[2].GetFloat());
		tmpGameObject->oldPosition = tmpGameObject->position;
		tmpGameObject->toAnimate = true;
		tmpGameObject->toDraw = true;
		tmpGameObject->respawnPoints = respawnPoints;
		tmpGameObject->animation = new cAnimationState();
		tmpGameObject->animation->defaultAnimation.name = "../extern/assets/modelsFBX/fbx/Look_Around.fbx";
		tmpGameObject->animation->defaultAnimation.frameStepTime = 0.0025f;
		tmpGameObject->animation->defaultAnimation.totalTime = tmpGameObject->model->GetDuration();

		//tmpGameObject->boundingSphereOrBox = tmpGameObject2;
		tmpGameObject->createPhysicsWorldObject();
		tmpGameObject->hasBoundingBoxOrSphere = true;
		tmpGameObject->updateObjectVectors();
		gGameObjects.push_back(tmpGameObject);
		camera->bindCameraToAnObject(tmpGameObject);
	}

	glm::vec3 mapScreenStartPoint, mapScreenEndPoint;
	ourMap->getWindowMapStartPoint(mapScreenStartPoint);
	ourMap->getWindowMapEndPoint(mapScreenEndPoint);

	/*for (int i = 0; i < NumCurious; i++)
	{
		cAIGameObject* tmpGameObject = new cAIGameObject();
		tmpGameObject->model = gMapLoadedModels["curious"];
		tmpGameObject->objectId = gGameObjects.size();
		tmpGameObject->colour = glm::vec3(0.0f, 1.0f, 0.0f);
		tmpGameObject->scale = 0.7;
		//tmpGameObject->ownAxisOrientation = glm::vec3(0.0f, glm::radians(90.0f), 0.0f);
		tmpGameObject->ownAxisOrientation = glm::vec3(0.0f, glm::radians(90.0f), 0.0f);
		tmpGameObject->objectFront = glm::vec3(1.0f, 0.0f, 0.0f);
		tmpGameObject->typeOfObject = eTypeOfObject::CURIOUS_AI;
		tmpGameObject->movementMinPos.x = getRandInRange(mapScreenStartPoint.x + 1.0f, mapScreenEndPoint.x - 1.0f);
		tmpGameObject->movementMinPos.y = 0.0f;
		tmpGameObject->movementMinPos.z = getRandInRange(mapScreenStartPoint.z + 1.0f, mapScreenEndPoint.z - 1.0f);
		tmpGameObject->movementMaxPos.x = getMaxRangeAsPerMin(mapScreenStartPoint.x + 1.0f, mapScreenEndPoint.x - 1.0f, tmpGameObject->movementMinPos.x);
		tmpGameObject->movementMaxPos.y = 0.0f;
		tmpGameObject->movementMaxPos.z = getMaxRangeAsPerMin(mapScreenStartPoint.z + 1.0f, mapScreenEndPoint.z - 1.0f, tmpGameObject->movementMinPos.z);
		findValueOnLine(tmpGameObject->movementMinPos, tmpGameObject->movementMaxPos, tmpGameObject->position);
		tmpGameObject->originalOrientationY = 90.0f;
		tmpGameObject->toAnimate = true;

		cGameObject* tmpGameObject2 = new cGameObject(gMapLoadedModels["sphere"], gMapLoadedModels["curious"]->maxExtentXYZ * tmpGameObject->scale, tmpGameObject->position, tmpGameObject->objectId);
		tmpGameObject2->colour = glm::vec3(0.3f, 0.5f, 1.0f);
		tmpGameObject2->diffuseColour = glm::vec3(0.3f, 0.5f, 1.0f);
		tmpGameObject2->typeOfObject = eTypeOfObject::BOUNDING_SPHERE;
		tmpGameObject2->toAnimate = true;
		tmpGameObject2->objectId = gBoundingBox.size();
		tmpGameObject2->parentGameObjectIndexId = tmpGameObject->objectId;
		gBoundingBox.push_back(tmpGameObject2);

		tmpGameObject->boundingSphereOrBox = tmpGameObject2;
		tmpGameObject->hasBoundingBoxOrSphere = true;
		tmpGameObject->action = eAiAction::ANIMATE_AI;
		gGameObjects.push_back(tmpGameObject);
	}

	for (int i = 0; i < NumAngry; i++)
	{
		cAIGameObject* tmpGameObject = new cAIGameObject();
		tmpGameObject->model = gMapLoadedModels["angry"];
		//cGameObject* pOtherGameObject = gGameObjects[ourMap->startCellIndex];
		//glm::vec3 newPos = pOtherGameObject->position;
		tmpGameObject->objectId = gGameObjects.size();
		tmpGameObject->colour = glm::vec3(0.0f, 0.0f, 1.0f);
		tmpGameObject->scale = 0.2;
		tmpGameObject->ownAxisOrientation = glm::vec3(0.0f, glm::radians(90.0f), 0.0f);
		tmpGameObject->objectFront = glm::vec3(1.0f, 0.0f, 0.0f);
		tmpGameObject->typeOfObject = eTypeOfObject::ANGRY_AI;
		tmpGameObject->movementMinPos.x = getRandInRange(mapScreenStartPoint.x + 1.0f, mapScreenEndPoint.x - 1.0f);
		tmpGameObject->movementMinPos.y = 0.0f;
		tmpGameObject->movementMinPos.z = getRandInRange(mapScreenStartPoint.z + 1.0f, mapScreenEndPoint.z - 1.0f);
		tmpGameObject->movementMaxPos.x = getMaxRangeAsPerMin(mapScreenStartPoint.x + 1.0f, mapScreenEndPoint.x - 1.0f, tmpGameObject->movementMinPos.x);
		tmpGameObject->movementMaxPos.y = 0.0f;
		tmpGameObject->movementMaxPos.z = getMaxRangeAsPerMin(mapScreenStartPoint.z + 1.0f, mapScreenEndPoint.z - 1.0f, tmpGameObject->movementMinPos.z);
		tmpGameObject->position.x = getRandInRange(tmpGameObject->movementMinPos.x, tmpGameObject->movementMaxPos.x);
		tmpGameObject->position.y = 0.0f;
		tmpGameObject->position.z = getRandInRange(tmpGameObject->movementMinPos.z, tmpGameObject->movementMinPos.z);
		tmpGameObject->toAnimate = true;


		cGameObject* tmpGameObject2 = new cGameObject(gMapLoadedModels["sphere"], gMapLoadedModels["angry"]->maxExtentXYZ * tmpGameObject->scale, tmpGameObject->position, tmpGameObject->objectId);
		tmpGameObject2->colour = glm::vec3(0.9f, 0.3f, 0.6f);
		tmpGameObject2->diffuseColour = glm::vec3(0.9f, 0.3f, 0.6f);
		tmpGameObject2->typeOfObject = eTypeOfObject::BOUNDING_SPHERE;
		tmpGameObject2->toAnimate = true;
		tmpGameObject2->objectId = gBoundingBox.size();
		tmpGameObject2->parentGameObjectIndexId = tmpGameObject->objectId;
		gBoundingBox.push_back(tmpGameObject2);

		tmpGameObject->action = eAiAction::ANIMATE_AI;
		tmpGameObject->boundingSphereOrBox = tmpGameObject2;
		tmpGameObject->hasBoundingBoxOrSphere = true;
		gGameObjects.push_back(tmpGameObject);

	}


	for (int i = 0; i < NumFollower; i++)
	{
		cAIGameObject* tmpGameObject = new cAIGameObject();
		tmpGameObject->model = gMapLoadedModels["dog"];
		//cGameObject* pOtherGameObject = gGameObjects[ourMap->startCellIndex];
		//glm::vec3 newPos = pOtherGameObject->position;
		tmpGameObject->objectId = gGameObjects.size();
		tmpGameObject->colour = glm::vec3(0.580f, 0.000f, 0.827f);
		tmpGameObject->scale = 0.1;
		tmpGameObject->ownAxisOrientation = glm::vec3(0.0f, glm::radians(90.0f), 0.0f);
		tmpGameObject->objectFront = glm::vec3(1.0f, 0.0f, 0.0f);
		tmpGameObject->typeOfObject = eTypeOfObject::FOLLOWER_AI;
		tmpGameObject->movementMinPos.x = getRandInRange(mapScreenStartPoint.x + 1.0f, mapScreenEndPoint.x - 1.0f);
		tmpGameObject->movementMinPos.y = 0.0f;
		tmpGameObject->movementMinPos.z = getRandInRange(mapScreenStartPoint.z + 1.0f, mapScreenEndPoint.z - 1.0f);
		tmpGameObject->movementMaxPos.x = getMaxRangeAsPerMin(mapScreenStartPoint.x + 1.0f, mapScreenEndPoint.x - 1.0f, tmpGameObject->movementMinPos.x);
		tmpGameObject->movementMaxPos.y = 0.0f;
		tmpGameObject->movementMaxPos.z = getMaxRangeAsPerMin(mapScreenStartPoint.z + 1.0f, mapScreenEndPoint.z - 1.0f, tmpGameObject->movementMinPos.z);
		tmpGameObject->position.x = getRandInRange(tmpGameObject->movementMinPos.x, tmpGameObject->movementMaxPos.x);
		tmpGameObject->position.y = 0.0f;
		tmpGameObject->position.z = getRandInRange(tmpGameObject->movementMinPos.z, tmpGameObject->movementMinPos.z);
		tmpGameObject->toAnimate = true;


		cGameObject* tmpGameObject2 = new cGameObject(gMapLoadedModels["sphere"], gMapLoadedModels["dog"]->maxExtentXYZ * tmpGameObject->scale, tmpGameObject->position, tmpGameObject->objectId);
		tmpGameObject2->colour = glm::vec3(0.3f, 0.8f, 0.6f);
		tmpGameObject2->diffuseColour = glm::vec3(0.3f, 0.8f, 0.6f);
		tmpGameObject2->typeOfObject = eTypeOfObject::BOUNDING_SPHERE;
		tmpGameObject2->toAnimate = true;
		tmpGameObject2->objectId = gBoundingBox.size();
		tmpGameObject2->parentGameObjectIndexId = tmpGameObject->objectId;
		gBoundingBox.push_back(tmpGameObject2);

		tmpGameObject->action = eAiAction::ANIMATE_AI;	
		tmpGameObject->boundingSphereOrBox = tmpGameObject2;
		tmpGameObject->hasBoundingBoxOrSphere = true;
		gGameObjects.push_back(tmpGameObject);
	*/
}

bool isObjectInCullingRadius(cGameObject* object) {
	if (glm::distance(object->position, gGameObjects[playerGameObjId]->position) <= CullingRadius)
		return true;
	else
		return false;
}


void addObstacleAsGameObject(cMapCell* cell, cGameObject* cellGameObject,std::vector<glm::vec3> & vecPoints, rapidjson::Document &doc) {
		if (cell->obstacleType == libPhysics::OBJ_SPIKES) {
			cellGameObject->model = gMapLoadedModels["floorOb"];
			cellGameObject->colour = glm::vec3(0.0f, 1.0f, 0.0f);
			cellGameObject->objectLayoutType = GL_FILL;
		}
		else if (cell->obstacleType == libPhysics::OBJ_FIRE) {
			cParticleEmitter* tmpGameObject2 = new cParticleEmitter();
			tmpGameObject2->model = gMapLoadedModels["floorOb"];
			tmpGameObject2->colour = glm::vec3(0.0f, 1.0f, 0.0f);
			tmpGameObject2->objectId = gGameObjects.size();
			tmpGameObject2->emitterPosition = cellGameObject->position;


			glm::vec3 temp = (cellGameObject->model->minXYZ * cellGameObject->scale);
			glm::vec3 temp2 = (cellGameObject->model->maxXYZ * cellGameObject->scale);

			glm::vec3 minRange = tmpGameObject2->emitterPosition - glm::vec3(fabsf(temp.x), fabsf(temp.y), fabsf(temp.z));
			glm::vec3 maxRange = tmpGameObject2->emitterPosition + glm::vec3(fabsf(temp2.x), fabsf(temp2.y), fabsf(temp2.z));

			tmpGameObject2->position = cellGameObject->position;
			tmpGameObject2->position.y -= 0.1f;
			tmpGameObject2->scale = cellGameObject->scale;
			tmpGameObject2->ownAxisOrientation = cellGameObject->ownAxisOrientation;
			tmpGameObject2->Init(200, 200, glm::vec3(0.1f, 0.1f, 0.1f),	// Min init vel
				glm::vec3(0.5f, 1.5f, 0.5f),	// max init vel
				1.0f, 3.0f,					//min life, max life
				minRange,	//min range from emitter
				maxRange);	//max range from emitter
			tmpGameObject2->objectLayoutType = GL_FILL;
			tmpGameObject2->typeOfObject = libPhysics::OBJ_FIRE;
			tmpGameObject2->createPhysicsWorldObject();
			tmpGameObject2->hasBoundingBoxOrSphere = true;
			gGameObjects.push_back(tmpGameObject2);
		}
		else if (cell->obstacleType == libPhysics::OBJ_TREASURE) {
			cGameObject* tmpGameObject2 = new cGameObject();
			tmpGameObject2->model = gMapLoadedModels["diamondOb"];
			tmpGameObject2->position = cellGameObject->position;
			tmpGameObject2->position.y -= 0.5f;
			tmpGameObject2->scale = cellGameObject->scale * 0.5f;
			tmpGameObject2->colour = glm::vec3(0.0f, 0.0f, 1.0f);
			tmpGameObject2->objectId = gGameObjects.size();
			tmpGameObject2->typeOfObject = libPhysics::OBJ_TREASURE;
			tmpGameObject2->ownAxisOrientation = cellGameObject->ownAxisOrientation;
			tmpGameObject2->createPhysicsWorldObject();
			tmpGameObject2->hasBoundingBoxOrSphere = true;
			gGameObjects.push_back(tmpGameObject2);

		}
		else if (cell->obstacleType == libPhysics::OBJ_SLIDING_WALL) {
			cellGameObject->model = gMapLoadedModels["bbox"];
			cellGameObject->colour = glm::vec3(0.0f, 1.0f, 0.0f);
			cellGameObject->objectLayoutType = GL_FILL;
		}
		else if (cell->obstacleType == libPhysics::RESPAWN_PT) {
			vecPoints.push_back(cellGameObject->position);
		}

		else if (cell->obstacleType == libPhysics::OBJ_ANGRY_AI) {
			cAIGameObject* tmpGameObject = new cAIGameObject();
			tmpGameObject->model = gMapLoadedModels["skeleton"];
			tmpGameObject->objectId = gGameObjects.size();
			tmpGameObject->scale = glm::vec3(0.01f);
			tmpGameObject->ownAxisOrientation = glm::vec3(0.0f, glm::radians(270.0f), 0.0f);
			tmpGameObject->typeOfObject = libPhysics::OBJ_ANGRY_AI;
			tmpGameObject->position = cellGameObject->position;
			tmpGameObject->movementMaxPos = cellGameObject->position;
			tmpGameObject->movementMinPos = cellGameObject->position;
			tmpGameObject->movementMinPos.x = cellGameObject->position.x - (aiMaxBlocks* ourMap->mapScale);
			tmpGameObject->toAnimate = true;
			tmpGameObject->toDraw = true;
			tmpGameObject->animation = new cAnimationState();
			tmpGameObject->animation->defaultAnimation.name = "../extern/assets/modelsFBX/fbx/Skeleton_Walk.fbx";
			tmpGameObject->animation->defaultAnimation.frameStepTime = 0.05f;
			tmpGameObject->animation->defaultAnimation.totalTime = tmpGameObject->model->GetDuration();

			tmpGameObject->updateObjectVectors();
			tmpGameObject->createPhysicsWorldObject();
			tmpGameObject->hasBoundingBoxOrSphere = true;

			gBoundingBox.push_back(tmpGameObject);
			gGameObjects.push_back(tmpGameObject);
		}

		else if (cell->obstacleType == libPhysics::OBJ_CURIOUS_AI) {
			cAIGameObject* tmpGameObject = new cAIGameObject();
			tmpGameObject->model = gMapLoadedModels["skeleton"];
			tmpGameObject->objectId = gGameObjects.size();
			tmpGameObject->scale = glm::vec3(0.01f);
			tmpGameObject->ownAxisOrientation = glm::vec3(0.0f, glm::radians(180.0f), 0.0f);
			tmpGameObject->typeOfObject = libPhysics::OBJ_CURIOUS_AI;
			tmpGameObject->position = cellGameObject->position;
			tmpGameObject->movementMaxPos = cellGameObject->position;
			tmpGameObject->movementMinPos = cellGameObject->position;
			tmpGameObject->movementMinPos.z = cellGameObject->position.z - (aiMaxBlocks* ourMap->mapScale);
			tmpGameObject->toAnimate = true;
			tmpGameObject->toDraw = true;
			tmpGameObject->animation = new cAnimationState();
			tmpGameObject->animation->defaultAnimation.name = "../extern/assets/modelsFBX/fbx/Skeleton_Walk.fbx";
			tmpGameObject->animation->defaultAnimation.frameStepTime = 0.05f;
			tmpGameObject->animation->defaultAnimation.totalTime = tmpGameObject->model->GetDuration();

			tmpGameObject->updateObjectVectors();
			tmpGameObject->createPhysicsWorldObject();
			tmpGameObject->hasBoundingBoxOrSphere = true;

			gBoundingBox.push_back(tmpGameObject);
			gGameObjects.push_back(tmpGameObject);
		}

		else if (cell->obstacleType == libPhysics::OBJ_FOLLOWER_AI) {
			cAIGameObject* tmpGameObject = new cAIGameObject();
			tmpGameObject->model = gMapLoadedModels["skeleton"];
			tmpGameObject->objectId = gGameObjects.size();
			tmpGameObject->scale = glm::vec3(0.005f);
			tmpGameObject->ownAxisOrientation = glm::vec3(0.0f, glm::radians(270.0f), 0.0f);
			tmpGameObject->typeOfObject = libPhysics::OBJ_FOLLOWER_AI;
			tmpGameObject->position = cellGameObject->position;
			tmpGameObject->movementMaxPos = cellGameObject->position;
			tmpGameObject->movementMinPos = cellGameObject->position;
			tmpGameObject->movementMinPos.x = cellGameObject->position.x - (aiMaxBlocks* ourMap->mapScale);
			tmpGameObject->toAnimate = true;
			tmpGameObject->toDraw = true;
			tmpGameObject->animation = new cAnimationState();
			tmpGameObject->animation->defaultAnimation.name = "../extern/assets/modelsFBX/fbx/Skeleton_Walk.fbx";
			tmpGameObject->animation->defaultAnimation.frameStepTime = 0.05f;
			tmpGameObject->animation->defaultAnimation.totalTime = tmpGameObject->model->GetDuration();

			tmpGameObject->updateObjectVectors();
			tmpGameObject->createPhysicsWorldObject();
			tmpGameObject->hasBoundingBoxOrSphere = true;

			gBoundingBox.push_back(tmpGameObject);
			gGameObjects.push_back(tmpGameObject);
		}
}

void loadGameState(std::string filePath) {
	//TODO :: Clear physics world
	for (int i = 0; i < gGameObjects.size(); i++) {
		if (gGameObjects[i]->hasBoundingBoxOrSphere) {
			if (gPhysicsWorld->checkIfObjectAlreadyPresent(gGameObjects[i]->collisionBody)) {
				gPhysicsWorld->RemoveBoundingBox(gGameObjects[i]->collisionBody);
			}
		}
	}
	gGameObjects.clear();
	
	rapidjson::Document configDoc = readConfigFromJsonFile(filePath);
	generateGameObjects(configDoc);
}

void saveGameState(std::string filePath) {
	rapidjson::Document doc;
	doc.SetObject();
	rapidjson::Document::AllocatorType& allocator = doc.GetAllocator();
	rapidjson::Value parentObj(rapidjson::kObjectType);
	rapidjson::Value obj(rapidjson::kObjectType);
	rapidjson::Value a(rapidjson::kArrayType);
	a.PushBack(rapidjson::Value().SetFloat(gGameObjects[playerGameObjId]->position.x), doc.GetAllocator());
	a.PushBack(rapidjson::Value().SetFloat(gGameObjects[playerGameObjId]->position.y), doc.GetAllocator());
	a.PushBack(rapidjson::Value().SetFloat(gGameObjects[playerGameObjId]->position.z), doc.GetAllocator());
	obj.AddMember("position", a, doc.GetAllocator());
	doc.AddMember("Player", obj, doc.GetAllocator());
	writeDataToJsonFile(doc, filePath);
}

void addSavedGame(std::string filePath,std::string gameName) {
	rapidjson::Document doc;
	doc.SetObject();
	rapidjson::Document::AllocatorType& allocator = doc.GetAllocator();
	rapidjson::Value a(rapidjson::kArrayType);
	if (gameName == "") {
		doc.AddMember("SavedGames", a, doc.GetAllocator());
		writeDataToJsonFile(doc, filePath);

	}
	else {
		rapidjson::Document tmpDoc = readConfigFromJsonFile(filePath);
		const rapidjson::Value& savedGames = tmpDoc["SavedGames"];
		for (int i = 0; i < savedGames.Size(); i++) {
			std::string tmpGame = savedGames[i].GetString();
			if (gameName == tmpGame)
				return;
			rapidjson::Value tmpStr;
			char buffer[50];
			int len = sprintf(buffer, "%s", tmpGame.c_str()); // dynamically created string.
			tmpStr.SetString(buffer, len, doc.GetAllocator());
			a.PushBack(tmpStr, doc.GetAllocator());
			memset(buffer, 0, sizeof(buffer));
		}
		rapidjson::Value tmpStr;
		char buffer[50];
		int len = sprintf(buffer, "%s", gameName.c_str()); // dynamically created string.
		tmpStr.SetString(buffer, len, doc.GetAllocator());
		a.PushBack(tmpStr, doc.GetAllocator());
		memset(buffer, 0, sizeof(buffer));
		doc.AddMember("SavedGames", a, doc.GetAllocator());
		//std::remove(filePath.c_str());
		writeDataToJsonFile(doc, filePath);

	}
}