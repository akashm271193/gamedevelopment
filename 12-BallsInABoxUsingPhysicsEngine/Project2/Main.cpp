// HelloTriangle.cpp : Defines the entry point for the console application.

#include "cShaderManager.h"
#include "cGLFWCalls.h"
#include "stb_image.h"
#include "cCamera.h"
#include <string>
#include "cPhysicsEngine.h"

cShaderManager* gPShaderManager;
cGLFWCalls* glfwCaller;
GLFWwindow* mainWindow;
cPhysicsEngine *Scene;

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;
std::string windowTitle = "Project 1";
// camera
cCamera camera(glm::vec3(0.0f, 100.0f, 60.0f));
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

float deltaTime = 0.0f;	// Time between current frame and last frame
float lastFrame = 0.0f; // Time of last frame

// lighting
glm::vec3 lightPos(0.0f, 10.0f, 100.0f);

void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	//camera Movement code
	//float cameraSpeed = 4.0f * deltaTime; // adjust accordingly as per the frame render rate
	camera.MovementSpeed = 4.0f;
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboard(RIGHT, deltaTime);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}

int main()
{
	
	glfwCaller = new cGLFWCalls();
	glfwCaller->InitiateGLFW();
	glfwCaller->createNewWindow(SCR_WIDTH,SCR_HEIGHT,windowTitle);
	mainWindow = glfwCaller->getCurrentWindow();
	
	glfwSetFramebufferSizeCallback(mainWindow, framebuffer_size_callback);
	glfwSetInputMode(mainWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPosCallback(mainWindow, mouse_callback);
	glfwSetScrollCallback(mainWindow, scroll_callback);

	//Code to load shaders

	::gPShaderManager = new cShaderManager();
	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	//cShaderManager::cShader lampVertShader;
	//cShaderManager::cShader lampFragShader;

	vertShader.fileName = "simpleVertex.glsl";
	fragShader.fileName = "simpleFragment.glsl";

	//lampVertShader.fileName = "lightVertex.glsl";
	//lampFragShader.fileName = "lightFragment.glsl";

	::gPShaderManager->setBasePath("../Extern/assets/shaders/");

	if (!::gPShaderManager->createProgramFromFile("myCustomShader", vertShader, fragShader)) {
		std::cout << " Error in loading shader " << std::endl;
		return -1;
	}

	glEnable(GL_DEPTH_TEST);
	
	Scene = new cPhysicsEngine();


	GLint customShaderId = ::gPShaderManager->getIDFromFriendlyName("myCustomShader");
	::gPShaderManager->useShaderProgram(customShaderId);

	if (!InitPhysics())
		std::cout << "Error in loading Physics Factory" << std::endl;

	gPhysicsWorld = gPhysicsFactory->CreateWorld();

	Scene->LoadModelsIntoScene("../Extern/assets/scene.json");

	double lastFrame = glfwGetTime();

	
	//Main Window Loop 
	while (!glfwWindowShouldClose(mainWindow))
	{
		//per-frame logic
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		//Call to check for input
		processInput(mainWindow);
			
		glClearColor(0.3f, 0.5f, 0.4f, 1.0f);
		//gl command to clear the color buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// get matrix's uniform location and set matrix
		::gPShaderManager->useShaderProgram(customShaderId);


		glm::mat4 view = glm::mat4(1.0f);
		view = camera.GetViewMatrix();

		glm::mat4 projection;
		projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);

		unsigned int viewLoc = glGetUniformLocation(customShaderId, "view");
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

		unsigned int projectionLoc = glGetUniformLocation(customShaderId, "projection");
		glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

		gPhysicsWorld->TimeStep(deltaTime);
		
		Scene->RenderScene(customShaderId);

		glfwSwapBuffers(mainWindow);
		glfwPollEvents();
	}
	// glfw: terminate, clearing all previously allocated GLFW resources.
	// ------------------------------------------------------------------
	glfwTerminate();
	return 0;
}