#include "cFactory.h"

#include "src\json.hpp"
#include <string>

#include "cResourceManager.h"
#include "cGameObject.h"
#include "cVAOManager.h"
#include "cGameLevel.h"


extern cVAOManager* Renderer;

//call in scene loader as a new object
class Windiana : public Entity
{
public:
	Windiana() { }
	unsigned int health;
	unsigned int experience;
	float speed;
	float minx, minz, maxx, maxz;
};
// type of monster class
// gotta make one for each monster type
class Goblin : public Entity
{
public:
	Goblin() { }
	unsigned int health;
	unsigned int experience;
	float speed;
};
class Skeleton : public Entity
{
public:
	Skeleton(){}
	unsigned int health;
	unsigned int experience;
	float speed;
};


Entity* MonsterFactory::CreateObjectOfType(string type,glm::vec3 position = glm::vec3(0.0f))
{
	// Insert new type of monsters
	if (type == "Goblin") 
	{	
		//add new class to the entityArray
		nlohmann::json monsterJSON;
		std::ifstream monsterStream("assets/models/goblin.json");
		Entity* monster = new Goblin();
		cModel* pCurModel = new cModel("assets/models/Goblin.ply", "Goblin");
		cGameObject* pCurObj = new cGameObject("Goblin", cResourceManager::GetTexture("Goblin"));

		//vecEntities.push_back(monster);
		return monster;
	} 
	else if (type == "Skeleton")
	{
		nlohmann::json monsterJSON;
		std::ifstream monsterStream("assets/models/skeleton.json");
		Entity* monster = new Goblin();
		cModel* pCurModel = new cModel("assets/models/Skeleton.ply", "Skeleton");
		cGameObject* pCurObj = new cGameObject("Skeleton", cResourceManager::GetTexture("Skeleton"));

		//vecEntities.push_back(monster);
		return monster;
	}
	//go on adding more
	else if (type == "Windiana")
	{
		nlohmann::json playerJSON;
		std::ifstream playerStream("assets/models/player.json");
		Entity* player = new Windiana();
		//cModel* pCurModel = new cModel("assets/models/Teapot.ply", "Windiana");
		cGameObject* pCurObj = new cGameObject("Windiana", cResourceManager::GetTexture("Windiana"));
		pCurObj->Color = glm::vec3(0.4f, 0.4f, 0.9f);
		pCurObj->Name = "Windiana";
		pCurObj->ObjectType = SPHERE;
		//pCurObj->bIsUpdatedInPhysics = false;
		pCurObj->Position = position;
		pCurObj->Scale = glm::vec3(0.1f);
		float min1, min2, max1, max2;
		cResourceManager::getMinMax("Windiana", &min1, &min2, &max1, &max2);
		Windiana* windyBoy = (Windiana*)player;
		windyBoy->minx = min1 * pCurObj->Scale.x; windyBoy->minz = min2 * pCurObj->Scale.x;
		windyBoy->maxx = max1 * pCurObj->Scale.x; windyBoy->maxz = max2 * pCurObj->Scale.x;
		/*windyBoy->minx = min1; windyBoy->minz = min2;
		windyBoy->maxx = max1; windyBoy->maxz = max2;*/

		FactoryObjects[pCurObj->Name] = *pCurObj;
		Renderer->LoadTexturedModel(cResourceManager::GetModel("Windiana"), cResourceManager::GetShader("Model"));

		//vecEntities.push_back(player);
		return player;
	}

	return 0;
}


//Based on which way Windiana is facing, move one tile in that direction
//If moving left or right, Windy moves an entire "length" of his model,
//And if moving up or down, he will move one "width"'s worth
void MonsterFactory::makeWindyMove(int facing, Entity* Windy,cGameLevel &gameLevel)
{
	//0 = RIGHT, 1 = LEFT, 2 = UP, 3 = DOWN

	int currCharPosCol = gameLevel.playerPosCol;
	int currCharPosRow = gameLevel.playerPosRow;
	
	Windiana* windyObj = (Windiana*)Windy;
	if (facing < 2)
	{
		float xLength = windyObj->maxx - windyObj->minx;
		if (facing == 0 && currCharPosCol > 0)
		{
			if (gameLevel.mapData[currCharPosRow][currCharPosCol-1] == 0) {
				FactoryObjects["Windiana"].Position.x -= xLength;
				gameLevel.playerPosCol--;
			}
		}
		else if(facing == 1 && currCharPosCol < gameLevel.mapData[0].size()-1){
			if (gameLevel.mapData[currCharPosRow][currCharPosCol+1] == 0) {
				FactoryObjects["Windiana"].Position.x += xLength;
				gameLevel.playerPosCol++;
			}
		}
	}
	else
	{
		float zLength = windyObj->maxz - windyObj->minz;
		if (facing == 2 && currCharPosRow < gameLevel.mapData.size()-1)
		{
			if (gameLevel.mapData[currCharPosRow +1][currCharPosCol] == 0) {
				FactoryObjects["Windiana"].Position.z += zLength;
				gameLevel.playerPosRow++;

			}
		}
		else if (facing == 3 && currCharPosRow > 0){
			if (gameLevel.mapData[currCharPosRow - 1][currCharPosCol] == 0) {
				FactoryObjects["Windiana"].Position.z -= zLength;
				gameLevel.playerPosRow--;
			}
		}
	}
}