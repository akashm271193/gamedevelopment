#ifndef _cAnimationState_HG_
#define _cAnimationState_HG_

#include <string>
#include <vector>

class cAnimationState
{
public:
	cAnimationState() {};

	struct sAnimationState
	{
		sAnimationState() :
			currentTime(0.0f),
			totalTime(0.0f),
			frameStepTime(0.0f) {};

		std::string name;
		float currentTime;
		float totalTime;
		float frameStepTime;
		bool IncrementTime(bool bResetToZero = true);
	};

	std::vector<sAnimationState> vecAnimationQueue;
	sAnimationState defaultAnimation;
};


#endif // !_cAnimationState_HG_
