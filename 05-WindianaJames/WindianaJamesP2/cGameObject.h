#ifndef _GAME_OBJECT_
#define _GAME_OBJECT_

#include <glad\glad.h>
#include <GLFW\glfw3.h>

#include <glm\vec3.hpp>

#include "cVAOManager.h"
#include "cTexture.h"

enum eObjectType
{
	SPHERE,		// Sphere
	PLANE,		// Plane
	CAPSULE,	// Capsule
	AABB,		// Axis-Aligned Box
	UNKNOWN = 99
};


class cGameObject
{
public:
	glm::vec3 Position, Orientation, Scale;
	glm::vec3 Color;
	glm::vec3 Velocity;
	std::string Name;

	eObjectType ObjectType;

	cTexture Texture;

	cGameObject();
	cGameObject(std::string name, cTexture texture);
	cGameObject(std::string name, cTexture texture, glm::vec3 position, glm::vec3 orientation = glm::vec3(0.0f), glm::vec3 scale = glm::vec3(1.0f), glm::vec3 color = glm::vec3(1.0f), glm::vec3 velocity = glm::vec3(0.0f));
	~cGameObject();

	virtual void Draw(cVAOManager &renderer);
};

#endif // !_GAME_OBJECT_
